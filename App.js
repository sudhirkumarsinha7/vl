/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';

import React,{useEffect} from 'react';
import { StatusBar, BackHandler, SafeAreaView, View,Text } from 'react-native';
import Routes from './src/Screens/Routes';
import { Provider } from 'react-redux';
import Store from './src/Redux/Store';
import { RootSiblingParent } from 'react-native-root-siblings';

const App = (props) => {
    useEffect(() => {
    const backAction = () => {
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }, []);
  return (
    <RootSiblingParent> 
    <Provider store={Store}>
       <Routes />
    </Provider>
    </RootSiblingParent>
  );
}


// const styles = StyleSheet.create({
//   sectionContainer: {
//     marginTop: 32,
//     paddingHorizontal: 24,
//   },
//   sectionTitle: {
//     fontSize: 24,
//     fontWeight: '600',
//   },
//   sectionDescription: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//   },
//   highlight: {
//     fontWeight: '700',
//   },
// });
export default App
