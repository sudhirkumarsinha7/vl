
import React, { useState, useEffect } from 'react';
import {
  LayoutAnimation,
  UIManager,
  Platform,
  Button,
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  trackJourney,
} from '../../Redux/Action/order';
import Header from '../../components/Header';
import {
  getAlertNotifications,
  getTransactionNotifications
} from '../../Redux/Action/alert';
import _ from 'lodash';
import { Colors, DeviceWidth, DeviceHeight } from '../../Style';
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../components/Toast';
import { CustomMapView } from '../../components/Common/trackerHelper'
import Loader from '../../components/Loader';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';

const Track = (props) => {
  const { t, i18n } = useTranslation();
  const [text, setText] = useState('');
  const isFocused = useIsFocused();

  useEffect(() => {
    async function fetchData() {
      GetData();
    }
    fetchData();
  }, [isFocused]);
  const GetData=()=>{
    props.getAlertNotifications();
    props.getTransactionNotifications();
  }
  const SearchText = () => {
    return <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        borderRadius: 8,
        backgroundColor: Colors.whiteFF,
        marginTop: verticalScale(-DeviceWidth / 7)
      }}>
      <TextInput
        style={{
          color: '#0093E9', width: '85%', marginLeft: scale(10), height: scale(35), margin: scale(5), alignItems: 'center',
          justifyContent: 'center',
          fontSize: scale(14)
        }}
        onChangeText={text => setText(text)}
        value={text}
        placeholder={t('Search by Track ID')}
        multiline={true}
        placeholderTextColor='#0093E9'
      />
      <TouchableOpacity
        disabled={text ? false : true}
        onPress={() => SearchFilterFunction()}>
        <Image
          style={{
            width: scale(16.85),
            height: scale(16.85),
            margin: scale(5),
          }}

          source={require('../../assets/Search.png')}
        />
      </TouchableOpacity>
    </View>
  }
  const SearchFilterFunction = async () => {
    const result = await props.trackJourney(text)
    //  console.log('SearchFilterFunction RESULT '+ JSON.stringify(result))
    if (result.data && (result.data.status || result.data.success)) {
      props.navigation.navigate('TrackDeatils');;
      setText('')
    } else {
      const emsg = (result && result.data && result.data.message) || t('Invalid ID. Please try again.')
      ToastShow(
        emsg,
        'error',
        'long',
        'top',
      )
    }
  }
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#0093E9" />
      <Header navigation={props.navigation}
        name={t('trackntrace')}
      />
      {SearchText()}
      <Loader />
      <CustomMapView />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
  container1: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    loder: state.loder,
    userpermission: state.userinfo.userpermission,
    TrackingData: state.shipment.TrackingData,

  };
}
export default connect(mapStateToProps, {
  trackJourney,
  getAlertNotifications,
  getTransactionNotifications
},
)(Track);