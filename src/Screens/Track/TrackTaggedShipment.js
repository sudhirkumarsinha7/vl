
import React, { useState, useEffect,useRef } from 'react';
import {
  LayoutAnimation,
  UIManager,
  Platform,
  Button,
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  trackJourney,
  getOrderDetails,
} from '../../Redux/Action/order';
import {
  getViewShipment,
} from '../../Redux/Action/shipment';
import { HeaderWithBackBlue } from '../../components/Header';

import _ from 'lodash';
import { Colors, DeviceWidth, DeviceHeight } from '../../Style';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../components/Toast';
import {
  CustomMapView, TrackPoDetails,
  TrackedShipmentDetails,
  TrackRecord,
  TrackLocation
} from '../../components/Common/trackerHelper'
import Loader from '../../components/Loader';
import ChainofcustodyMap from './ChainofcustodyMap';
import CurrentLocationMap from './CurrentLocationMap';
import MapView, { Marker,Polygon } from 'react-native-maps';
import { LocationIcon } from '../../components/Common/common';

import Section_Select from '../../components/Section_Select';
const radioItems = [
  {
    label: 'Chain of Custody',
    selected: true,
  },
  {
    label: 'Current Location',
    selected: false,
  },
]
const TrackTaggedShipment = (props) => {
  const { t, i18n } = useTranslation();
  const [text, setText] = useState('');
  const [selectedItem, setTab] = useState('Chain of Custody');
  const mapRef = useRef(null);

  const SearchText = () => {
    return <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        borderRadius: 8,
        backgroundColor: Colors.whiteFF,
      }}>
      <TextInput
        style={{
          color: '#0093E9', width: '85%', marginLeft: scale(10), height: scale(35), margin: scale(5), alignItems: 'center',
          justifyContent: 'center',
          fontSize: scale(14)
        }}
        onChangeText={text => setText(text)}
        value={text}
        placeholder={t('Search by Track ID')}
        multiline={true}
        placeholderTextColor='#0093E9'
      />
      <TouchableOpacity
        disabled={text ? false : true}
        onPress={() => SearchFilterFunction()}>
        <Image
          style={{
            width: scale(16.85),
            height: scale(16.85),
            margin: scale(5),
          }}

          source={require('../../assets/Search.png')}
        />
      </TouchableOpacity>
    </View>
  }
  const SearchFilterFunction = async () => {
    const result = await props.trackJourney(text)
    console.log('SearchFilterFunction RESULT ' + JSON.stringify(result))
    if (result.data && (result.data.status || result.data.success)) {
      setText('')
    } else {
      const emsg = (result && result.data && result.data.message) || t('Invalid ID. Please try again.')
      ToastShow(
        emsg,
        'error',
        'long',
        'top',
      )
    }
  }
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });
    radioItems[index].selected = true;
    const label = radioItems[index].label
    console.log('radioItems[index].label ' + label)
    setTab(label)
  }
  const getTaggedShipmentDetails = async id => {

    let result = await props.trackJourney(id, true);
    if (result.data && result.data.success) {
      props.navigation.navigate('TrackTaggedShipment');
    }
  };
  const getOrderDetails = async id => {
    const result = await props.getOrderDetails(id);

    if (result.length) {
      const orderData = result[0];
      props.navigation.navigate('ViewOrder', { item: orderData });
    }
  };
  const getShipmentDetails = async id => {
    props.getViewShipment(id);
    props.navigation.navigate('ViewShipment', { item: {} });
  };
  const { TrackingData = {}, userpermission = {} } = props;
  // console.log('TrackingData RESULT '+ JSON.stringify(TrackingData))

  const { shipment = {}, order = {} } = userpermission;
  const {
    poDetails = {},
    inwardShipmentsArray = [],
    outwardShipmentsArray = [],
    trackedShipment = [],
    poShipmentsArray = [],
    currentLocationData = {}
  } = TrackingData;
  let coordinates = [];
  let newData = [];

  newData = inwardShipmentsArray
    .concat(trackedShipment)
    .concat(outwardShipmentsArray);
  if (poShipmentsArray && poShipmentsArray.length) {
    newData = poShipmentsArray;
  }
  newData.length &&
    newData.map(marker => {
      if (
        marker.supplier &&
        marker.supplier.warehouse &&
        marker.supplier.warehouse.location
      ) {
        coordinates.push(marker.supplier.warehouse.location);
      }
      if (
        marker.receiver &&
        marker.receiver.warehouse &&
        marker.receiver.warehouse.location
      ) {
        coordinates.push(marker.receiver.warehouse.location);
      }
    });
  let newCordinate = [];

  newCordinate =
    coordinates.length &&
    coordinates.map(each => {
      return each.coordinates
    });
  const polygonnCordinate = newCordinate.length && newCordinate.map(coordsArr => {
    let coords = {
      latitude: coordsArr[0],
      longitude: coordsArr[1],
    }
    return coords;
  });
  const allWarehoues = Object.keys(currentLocationData)
  const currentLocationList = allWarehoues.length && allWarehoues.map(each => {
    if (currentLocationData[each].length) {
      return currentLocationData[each]
    }
  })
  const location = currentLocationList.length && currentLocationList[0] && currentLocationList[0].length && currentLocationList[0][0] && currentLocationList[0][0].warehouse && currentLocationList[0][0].warehouse.location

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#0093E9" />
      <HeaderWithBackBlue navigation={props.navigation}
        name={t('trackntrace')}
      />
      <ScrollView
        style={{
          borderRadius: 18,
          marginTop: verticalScale(-DeviceWidth / 5),
        }}>
          {SearchText()}
        <View style={{ borderRadius: 10, backgroundColor: 'white', margin: 10 }}>
            <View style={{ height: DeviceWidth / 1.12, margin: 8 }}>

              {selectedItem === 'Current Location' ?  
              <MapView
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  borderRadius: 10,
                }}
                showsUserLocation={false}
                zoomEnabled={true}
                zoomControlEnabled={true}
                initialRegion={{
                  latitude:
                  location && location.coordinates && location.coordinates.length
                      ? location.coordinates[0]
                      : 17.4295865,
                  longitude:
                  location &&location.coordinates && location.coordinates.length? location.coordinates[1]
                      : 78.368776,

                  latitudeDelta: 100,
                  longitudeDelta: 30,
                }}
                ref={mapRef}
                >


                  
                {currentLocationList && currentLocationList.length > 0
                  ? currentLocationList.map((marker1, index) =>
                  marker1.map((marker, index) =>
                    marker.warehouse && marker.warehouse.location && marker.warehouse.location.coordinates &&
                      marker.warehouse.location.coordinates.length == 2 ? (
                        // <Marker coordinate={tokyoRegion} />

                      <Marker
                        coordinate={{
                          latitude: parseFloat(
                            parseFloat(
                              marker.warehouse.location.coordinates[0],
                            ).toFixed(6),
                          ),
                          longitude: parseFloat(
                            parseFloat(
                              marker.warehouse.location.coordinates[1],
                            ).toFixed(6),
                          ),
                        }}
                        title={
                          marker.productName
                        }
                        zIndex={3}
                        description={
                          marker.warehouse && marker.warehouse.title
                        }>
                          <LocationIcon/>
                      </Marker>
                    ) : null,
                  ))
                  : null}


              </MapView> 
              :
              selectedItem === 'Chain of Custody' ? <MapView
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    borderRadius: 10,
                  }}
                  showsUserLocation={false}
                  zoomEnabled={true}
                  zoomControlEnabled={true}
                  initialRegion={{
                    latitude:
                      newCordinate && newCordinate.length && newCordinate[newCordinate.length - 1]
                        ? newCordinate[newCordinate.length - 1][0]
                        : 17.4295865,
                    longitude:
                      newCordinate && newCordinate.length && newCordinate[newCordinate.length - 1]
                        ? newCordinate[newCordinate.length - 1][1]
                        : 78.368776,
                    latitudeDelta: 100,
                    longitudeDelta: 30,
                  }}
                  ref={mapRef}
                  >
                  {newData && newData.length > 0
                    ? newData.map((marker, index) =>
                      marker.supplier &&
                        marker.supplier.warehouse &&
                        marker.supplier.warehouse.location &&
                        marker.supplier.warehouse.location.coordinates &&
                        marker.supplier.warehouse.location.coordinates.length === 2 ? (
                        <Marker
                          coordinate={{
                            latitude: parseFloat(
                              parseFloat(
                                marker.supplier.warehouse.location.coordinates[0],
                              ).toFixed(6),
                            ),
                            longitude: parseFloat(
                              parseFloat(
                                marker.supplier.warehouse.location.coordinates[1],
                              ).toFixed(6),
                            ),
                          }}
                          title={
                            marker.supplier &&
                            marker.supplier.warehouse &&
                            marker.supplier.warehouse.title
                          }
                          zIndex={3}
                          description={
                            marker.supplier &&
                            marker.supplier.warehouse &&
                            marker.supplier.warehouse.warehouseAddress &&
                            marker.supplier.warehouse.warehouseAddress.firstLine
                          }>
                          <LocationIcon />
                        </Marker>
                      ) : null,
                    )
                    : null}
                  {newData && newData.length > 0
                    ? newData.map((marker, index) =>
                      marker.receiver &&
                        marker.receiver.warehouse &&
                        marker.receiver.warehouse.location &&
                        marker.receiver.warehouse.location.coordinates &&
                        marker.receiver.warehouse.location.coordinates.length === 2 ? (
                        <Marker
                          coordinate={{
                            latitude: parseFloat(
                              parseFloat(
                                marker.receiver.warehouse.location.coordinates[0],
                              ).toFixed(6),
                            ),
                            longitude: parseFloat(
                              parseFloat(
                                marker.receiver.warehouse.location.coordinates[1],
                              ).toFixed(6),
                            ),
                          }}
                          title={
                            marker.receiver &&
                            marker.receiver.warehouse &&
                            marker.receiver.warehouse.title
                          }
                          zIndex={3}
                          description={
                            marker.receiver &&
                            marker.receiver.warehouse &&
                            marker.receiver.warehouse.warehouseAddress &&
                            marker.receiver.warehouse.warehouseAddress.firstLine
                          }>
                          <LocationIcon />
                        </Marker>
                      ) : null,
                    )
                    : null}
                  {polygonnCordinate && polygonnCordinate.length >= 2 ? (
                    <Polygon
                      coordinates={polygonnCordinate}
                      strokeColor="#0093E9"
                      strokeWidth={3}
                    />
                  ) : null}


                </MapView>
              :null}
            </View>
          </View>
        <View style={{ flexDirection: 'row' }}>
          {radioItems.map((item, key) => (
            <Section_Select
              key={key}
              button={item}
              onClick={() => changeActiveRadioButton(key)}
            />
          ))}
          <View style={{ height: scale(10) }} />

        </View>

        {selectedItem === 'Current Location' ? <View>
          <TrackLocation
            currentLocationList={currentLocationList} />
        </View> :selectedItem === 'Chain of Custody' ? <View
          style={{
            borderRadius: 20,
            margin: 5,
          }}>
          {poDetails && poDetails.id ? (
            <TrackPoDetails
              navigation={props.navigation}
              item={poDetails}
              onClickFunction={() => getOrderDetails(poDetails.id)}
              orderPermission={order}
            />
          ) : null}
          {newData.length
            ? newData.map((eachShipment, index) => (
              <TrackedShipmentDetails
                navigation={props.navigation}
                item={eachShipment}
                onClickFunction={() =>
                  getShipmentDetails(eachShipment.id)
                }
                onClickNextScreen={() =>
                  getTaggedShipmentDetails(eachShipment.id)
                }
                shipmentPermission={shipment}
              />
            ))
            : null}

        </View>:null}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
  container1: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    loder: state.loder,
    userpermission: state.userinfo.userpermission,
    TrackingData: state.shipment.TrackingData,

  };
}
export default connect(mapStateToProps, {
  trackJourney,
  getOrderDetails,
  getViewShipment
},
)(TrackTaggedShipment);