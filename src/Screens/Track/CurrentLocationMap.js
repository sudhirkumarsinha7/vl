
import React, { useState, useRef, Component } from 'react';
import {

  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  trackJourney,
} from '../../Redux/Action/order';

import _ from 'lodash';
import { Colors, DeviceWidth, DeviceHeight } from '../../Style';
import MapView from 'react-native-maps';
import { LocationIcon } from '../../components/Common/common';
import { withTranslation } from 'react-i18next';
import { compose } from 'redux';
class CurrentLocationMap extends Component {

  render() {
    // console.log('render shipment ' + JSON.stringify(this.props.shipment));

    const { TrackingData, t } = this.props;
    // console.log('render userpermission ' + JSON.stringify(userpermission));
    let {
      poDetails,
      inwardShipmentsArray = [],
      outwardShipmentsArray = [],
      trackedShipment = [],
      poShipmentsArray = [],
      currentLocationData = {}
    } = TrackingData;
    // const {currentLocationData = {}}=Data
    let coordinates = [];
    let newData = [];
    // newData  =inwardShipmentsArray.concat(inwardShipmentsArray, outwardShipmentsArray)
    // console.log('render TrackingData ' + JSON.stringify(TrackingData));

    newData = inwardShipmentsArray
      .concat(trackedShipment)
      .concat(outwardShipmentsArray);
    if (poShipmentsArray && poShipmentsArray.length) {
      newData = poShipmentsArray;
    }
    newData.length &&
      newData.map(marker => {
        if (
          marker.supplier &&
          marker.supplier.warehouse &&
          marker.supplier.warehouse.location
        ) {
          coordinates.push(marker.supplier.warehouse.location);
        }
        if (
          marker.receiver &&
          marker.receiver.warehouse &&
          marker.receiver.warehouse.location
        ) {
          coordinates.push(marker.receiver.warehouse.location);
        }
      });
    let newCordinate = [];

    newCordinate =
      coordinates.length &&
      coordinates.map(each => {
        return each.coordinates
      });
    const polygonnCordinate = newCordinate.length && newCordinate.map(coordsArr => {
      let coords = {
        latitude: coordsArr[0],
        longitude: coordsArr[1],
      }
      return coords;
    });
    // console.log('render newCordinate ' + JSON.stringify(newCordinate));
    const allWarehoues = Object.keys(currentLocationData)
    const currentLocationList = allWarehoues.length && allWarehoues.map(each => {
      if (currentLocationData[each].length) {
        return currentLocationData[each]
      }
    })
    const location = currentLocationList.length && currentLocationList[0] && currentLocationList[0].length && currentLocationList[0][0] && currentLocationList[0][0].warehouse && currentLocationList[0][0].warehouse.location

    // console.log('render newCordinate location' + JSON.stringify(location));


    return (
      <View style={{ borderRadius: 10, backgroundColor: 'white', margin: 10 }}>
        <View style={{ height: DeviceWidth / 1.12, margin: 8 }}>
          <MapView
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              borderRadius: 10,
            }}
            showsUserLocation={false}
            zoomEnabled={true}
            zoomControlEnabled={true}
            initialRegion={{
              latitude:
                location && location.coordinates && location.coordinates.length
                  ? location.coordinates[0]
                  : 17.4295865,
              longitude:
                location && location.coordinates && location.coordinates.length ? location.coordinates[1]
                  : 78.368776,

              latitudeDelta: 100,
              longitudeDelta: 30,
            }}
            ref={map => { this.map = map }}>
            {/* {currentLocationList && currentLocationList.length > 0
            ? currentLocationList.map((marker1, index) =>
            marker1.map((marker, index) =>
              marker.warehouse && marker.warehouse.location && marker.warehouse.location.coordinates &&
                marker.warehouse.location.coordinates.length == 2 ? (
                  // console.log('marker.warehouse.location.coordinates' +  marker.warehouse.location.coordinates)
                <MapView.Marker
                  coordinate={{
                    latitude: parseFloat(
                      parseFloat(
                        marker.warehouse.location.coordinates[0],
                      ).toFixed(6),
                    ),
                    longitude: parseFloat(
                      parseFloat(
                        marker.warehouse.location.coordinates[1],
                      ).toFixed(6),
                    ),
                  }}
                  title={
                    marker.productName
                  }
                  zIndex={3}
                  description={
                    marker.warehouse && marker.warehouse.title
                  }>
                    <LocationIcon/>
                </MapView.Marker>
              ) : null,
            ))
            : null} */}


          </MapView>
          {/* {selectedItem === 'Current Location' ?  
        <MapView
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            borderRadius: 10,
          }}
          showsUserLocation={false}
          zoomEnabled={true}
          zoomControlEnabled={true}
          initialRegion={{
            latitude:
            location && location.coordinates && location.coordinates.length
                ? location.coordinates[0]
                : 17.4295865,
            longitude:
            location &&location.coordinates && location.coordinates.length? location.coordinates[1]
                : 78.368776,

            latitudeDelta: 100,
            longitudeDelta: 30,
          }}
          ref={map => { this.map = map }}>
          {currentLocationList && currentLocationList.length > 0
            ? currentLocationList.map((marker1, index) =>
            marker1.map((marker, index) =>
              marker.warehouse && marker.warehouse.location && marker.warehouse.location.coordinates &&
                marker.warehouse.location.coordinates.length == 2 ? (
                  // console.log('marker.warehouse.location.coordinates' +  marker.warehouse.location.coordinates)
                <MapView.Marker
                  coordinate={{
                    latitude: parseFloat(
                      parseFloat(
                        marker.warehouse.location.coordinates[0],
                      ).toFixed(6),
                    ),
                    longitude: parseFloat(
                      parseFloat(
                        marker.warehouse.location.coordinates[1],
                      ).toFixed(6),
                    ),
                  }}
                  title={
                    marker.productName
                  }
                  zIndex={3}
                  description={
                    marker.warehouse && marker.warehouse.title
                  }>
                    <LocationIcon/>
                </MapView.Marker>
              ) : null,
            ))
            : null}


        </MapView> 
        :
          <MapView
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              borderRadius: 10,
            }}
            showsUserLocation={false}
            zoomEnabled={true}
            zoomControlEnabled={true}
            initialRegion={{
              latitude:
                newCordinate && newCordinate.length && newCordinate[newCordinate.length - 1]
                  ? newCordinate[newCordinate.length - 1][0]
                  : 17.4295865,
              longitude:
                newCordinate && newCordinate.length && newCordinate[newCordinate.length - 1]
                  ? newCordinate[newCordinate.length - 1][1]
                  : 78.368776,
              latitudeDelta: 100,
              longitudeDelta: 30,
            }}
            ref={map => { this.map = map }}>
            {newData && newData.length > 0
              ? newData.map((marker, index) =>
                marker.supplier &&
                  marker.supplier.warehouse &&
                  marker.supplier.warehouse.location &&
                  marker.supplier.warehouse.location.coordinates &&
                  marker.supplier.warehouse.location.coordinates.length === 2 ? (
                  <MapView.Marker
                    coordinate={{
                      latitude: parseFloat(
                        parseFloat(
                          marker.supplier.warehouse.location.coordinates[0],
                        ).toFixed(6),
                      ),
                      longitude: parseFloat(
                        parseFloat(
                          marker.supplier.warehouse.location.coordinates[1],
                        ).toFixed(6),
                      ),
                    }}
                    title={
                      marker.supplier &&
                      marker.supplier.warehouse &&
                      marker.supplier.warehouse.title
                    }
                    zIndex={3}
                    description={
                      marker.supplier &&
                      marker.supplier.warehouse &&
                      marker.supplier.warehouse.warehouseAddress &&
                      marker.supplier.warehouse.warehouseAddress.firstLine
                    }>
                    <LocationIcon />
                  </MapView.Marker>
                ) : null,
              )
              : null}
            {newData && newData.length > 0
              ? newData.map((marker, index) =>
                marker.receiver &&
                  marker.receiver.warehouse &&
                  marker.receiver.warehouse.location &&
                  marker.receiver.warehouse.location.coordinates &&
                  marker.receiver.warehouse.location.coordinates.length === 2 ? (
                  <MapView.Marker
                    coordinate={{
                      latitude: parseFloat(
                        parseFloat(
                          marker.receiver.warehouse.location.coordinates[0],
                        ).toFixed(6),
                      ),
                      longitude: parseFloat(
                        parseFloat(
                          marker.receiver.warehouse.location.coordinates[1],
                        ).toFixed(6),
                      ),
                    }}
                    title={
                      marker.receiver &&
                      marker.receiver.warehouse &&
                      marker.receiver.warehouse.title
                    }
                    zIndex={3}
                    description={
                      marker.receiver &&
                      marker.receiver.warehouse &&
                      marker.receiver.warehouse.warehouseAddress &&
                      marker.receiver.warehouse.warehouseAddress.firstLine
                    }>
                    <LocationIcon />
                  </MapView.Marker>
                ) : null,
              )
              : null}
            {polygonnCordinate && polygonnCordinate.length >= 2 ? (
              <MapView.Polygon
                coordinates={polygonnCordinate}
                strokeColor="#0093E9"
                strokeWidth={3}
              />
            ) : null}


          </MapView>
        } */}
        </View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    TrackingData: state.shipment.TrackingData,
    loder: state.loder,
    userpermission: state.userinfo.userpermission,
  };
}

export default compose(withTranslation('translation'), connect(
  mapStateToProps,
  {},
))(CurrentLocationMap);
