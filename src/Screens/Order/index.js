import React, { useState, useEffect } from 'react';
import {
  LayoutAnimation,
  UIManager,
  Platform,
  Button,
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  userinfo,
  fetchOutboundPurchaseOrders,
  fetchInboundPurchaseOrders,
  getOrderDetails,
  getTrackingDetails,
  trackJourney,
  searchProductByType,
  searchProductByName,
  Purchase_Order_Create,
} from '../../Redux/Action/order';
import {
  getchainOfCustody,
  getViewShipment,

} from '../../Redux/Action/shipment';
import { getOrderAnalytics } from '../../Redux/Action/analytics'
import Header from '../../components/Header';
import Card from '../../components/Card';
import Empty_Card from '../../components/Empty_Card';
import Section_Select from '../../components/Section_Select';
import { OrderList, getPOStatus } from '../../components/Common/shipmentHelper';
import _ from 'lodash';
import { compareValues, NetworkUtils } from '../../Util/utils';
import { Colors, DeviceWidth } from '../../Style';
import { CustomButton1 } from '../../components/Common/common';
import { useTranslation } from 'react-i18next';
import { queryAllorder, queryDeleteAllOrder } from '../../databases/allSchemas'
import { ToastShow } from '../../components/Toast';
import Search from '../../components/Search'
import { NavigationContainer, useIsFocused } from '@react-navigation/native';

export const STATUS = [
  { id: 'ACCEPTED', label: 'Accepted' },
  { id: 'CREATED', label: 'Sent' },
  { id: 'TRANSIT&PARTIALLYFULFILLED', label: 'Transit & PartiallyFullyFilled' },
  { id: 'FULLYFULFILLED', label: 'FullyFilled' },
  { id: 'REJECTED', label: 'Rejected' },
  { label: 'ALL', id: '1' },
];
const radioItems = [
  {
    label: 'Order Sent',
    selected: true,
  },
  {
    label: 'Order Received',
    selected: false,
  },
];
const OrderScreen = (props) => {
  const isFocused = useIsFocused();

  const [selectedItem, setTab] = useState('')
  const [userOrgID, setUserOrgID] = useState();
  const [isLoadingState, setIsloadingState] = useState();
  const [searchString, setSearchString] = useState('')
  const { t, i18n } = useTranslation();

  useEffect(() => {
    async function fetchData() {
      GetData();
    }
    fetchData();
  }, [isFocused]);


  const GetData = async () => {
    radioItems.map(item => {
      if (item.selected) {
        setTab(item.label)
      }
    });
    props.userinfo();
    props.fetchInboundPurchaseOrders(0, 10, props.orderSentFilter);
    props.fetchOutboundPurchaseOrders(
      0,
      10,
      props.orderReceiveFilter,
    );
    props.getOrderAnalytics();
    const orgSplit = props.user.organisation?.split('/');
    const ownOrg = orgSplit[0];
    const orgId = orgSplit[1];
    setUserOrgID(orgId)

  };

  let { ordersAnalytics, outboundPOs, inboundPOs, userpermission = {}, orderSentFilter, orderReceiveFilter } = props;

  delete orderSentFilter.from;
  delete orderSentFilter.to;

  delete orderReceiveFilter.to;
  delete orderReceiveFilter.from;
  let orderSentFilterList = [];
  let orderReceiveFilterList = [];
  orderSentFilterList = Object.keys(orderSentFilter).map((key) => { if (orderSentFilter[key]) { if (key != 'productName' && key != 'status') return [key, orderSentFilter[key]] } });
  orderReceiveFilterList = Object.keys(orderReceiveFilter).map((key) => { if (orderReceiveFilter[key]) { if (key != 'productName' && key != 'status') return [key, orderReceiveFilter[key]] } });
  let sentCount = orderSentFilterList.filter(each => {
    if (each) {
      return each
    }
  })
  let receiveCount = orderReceiveFilterList.filter(each => {
    if (each) {
      return each
    }
  })
  let count = 0;
  count = selectedItem === 'Order Sent' ? sentCount.length : receiveCount.length
  let { order = {} } = userpermission;
  outboundPOs = _.uniqBy(outboundPOs, function (x) {
    return x.id;
  });
  inboundPOs = _.uniqBy(inboundPOs, function (x) {
    return x.id;
  });
  outboundPOs = outboundPOs.sort(compareValues('createdAt', 'desc'));
  inboundPOs = inboundPOs.sort(compareValues('createdAt', 'desc'));
  const onRefresh = async () => {
    const isConnected = await NetworkUtils.isNetworkAvailable()

    if (isConnected) {
      await SyncOrderData();
      await props.fetchInboundPurchaseOrders(0, 50, props.orderSentFilter);
      await props.fetchOutboundPurchaseOrders(
        0,
        50,
        props.orderReceiveFilter,
      );
      await props.getOrderAnalytics();
    }
  };
  const SyncOrderData = async () => {
    queryAllorder().then((todoLists) => {
      let data = todoLists;
      console.log('queryAllorder ORDER LIST ' + JSON.stringify(data));
      if (data.length) {
        for (var i = 0; i < data.length; i++) {

          props.Purchase_Order_Create(data[i])
        }
        queryDeleteAllOrder()
      }


    }).catch((error) => {
      console.log('sync QueryAllorder error' + JSON.stringify(error));

    });
  }
  const loadMore = async () => {
    //Call the Service to get the latest data
    let { outboundPOs, inboundPOs } = props;
    outboundPOs = _.uniqBy(outboundPOs, function (x) {
      return x.id;
    });
    inboundPOs = _.uniqBy(inboundPOs, function (x) {
      return x.id;
    });
    let minLength = Math.max(inboundPOs.length, outboundPOs.length);
    props.fetchInboundPurchaseOrders(inboundPOs.length, inboundPOs.length + 10, props.orderSentFilter);
    props.fetchOutboundPurchaseOrders(
      outboundPOs.length, outboundPOs.length + 10,
      props.orderReceiveFilter,
    );
    props.getOrderAnalytics();
  };


  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });
    radioItems[index].selected = true;
    console.log('radioItems[index].label ' + radioItems[index].label)
    setTab(radioItems[index].label)
  }

  const getTrackDetails = async id => {
    await props.trackJourney(id);
    props.navigation.navigate('Track', { screen: 'TrackDeatils' });;
  };
  const getDetails = (item, id) => {
    props.navigation.navigate('ViewOrder', {
      item,
      selectedTab: selectedItem,
    });
  };
  const eachBound = item => {
    let { userpermission = {}, } = props;

    let { order = {}, track = {} } = userpermission;
    return (
      <OrderList
        item={item}
        onClickFunction={() => getTrackDetails(item.id)}
        onView={() => getDetails(item, item.id)}
        userOrgID={userOrgID}
        selectedTab={selectedItem}
        oprderPermission={order}
        trackPermission={track}
      />
    );
  };
  const OrderAnalytics = () => {
    return (
      <View>
        <ScrollView
          horizontal={true}
          contentContainerStyle={{
            margin: verticalScale(10),
          }}
          showsHorizontalScrollIndicator={true}>
          <Card
            image={require('../../assets/Total_Orders_Sent.png')}
            bgcolor="#35BFCC96"
            textcolor="#2BA3AE"
            quantity={(ordersAnalytics && ordersAnalytics.outboundPO) || 0}
            card_name={t("total_orders_sent")}
            nextScreen={() => changeActiveRadioButton(0)}
            isDisable={false}
          />
          <View>
            <Text>{'  '}</Text>
          </View>
          <Card
            image={require('../../assets/Total_Orders_Received.png')}
            bgcolor="#FFAB1D"
            textcolor="#FA7923"
            quantity={(ordersAnalytics && ordersAnalytics.inboundPO) || 0}
            card_name={t("total_orders_received")}
            nextScreen={() => changeActiveRadioButton(1)}
            isDisable={false}
          />
          <View>
            <Text>{'  '}</Text>
          </View>

          <Card
            image={require('../../assets/Total_Orders_Pending.png')}
            bgcolor="#89D1F0"
            textcolor="#0093E9"
            quantity={(ordersAnalytics && ordersAnalytics.pendingOrders) || 0}
            card_name={t("total_orders_pending")}

          />
          <View>
            <Text>{'  '}</Text>
          </View>
          <Card
            image={require('../../assets/Total_Orders_Rejected.png')}
            bgcolor="#0A69C5"
            textcolor="#0159EA"
            quantity={(ordersAnalytics && ordersAnalytics.rejectedOrders) || 0}
            card_name={t("total_orders_rejected")}

          />
          <View style={{ marginRight: 20 }}>
            <Text>{'  '}</Text>
          </View>

        </ScrollView>
      </View>)
  }
  const ActionButton = () => {
    return <View
      style={{
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: verticalScale(5),
      }}>
      {order.createOrder ?
        <CustomButton1
          img={require('../../assets/Purchaseorder.png')}
          backgroundColor={'#FA7923'}
          imgWidth={scale(13.14)}
          imgHight={scale(15)}
          label1={t('create_new_order')}
          Press={() => props.navigation.navigate('CreateOrder')}
        /> : null}
    </View>
  }
  const FilterDesign = () => {
    return (<View
      style={{
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: verticalScale(10),
        marginRight: scale(10)
      }}>
      <Text
        style={{
          fontSize: scale(16),
          color: '#0093E9',
          fontWeight: 'bold',
        }}>
        {t('Recent Orders')}
      </Text>
      {order.orderFilters ? <TouchableOpacity
        style={{
          borderRadius: 10,
          padding: scale(7),
          paddingLeft: scale(10),
          paddingRight: scale(10),
          backgroundColor: Colors.blueCF,
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}
        onPress={() => props.navigation.navigate('FilterOrder', { selectedTab: selectedItem })}>
        <Image
          style={{ width: scale(9.35), height: scale(11.68) }}
          source={require('../../assets/Filter.png')}
        />
        <Text
          style={{
            fontSize: scale(12),
            color: '#FFFFFF',
            fontWeight: '900',
            marginLeft: scale(5),
            marginRight: scale(5)
          }}>
          {count ? t('Filter') + '(' + count + ')' : t('Filter')}
        </Text>
        <Image
          style={{ width: scale(10.01), height: scale(5.72), }}
          resizeMode="cover"
          source={require('../../assets/downarrow.png')}
        />
      </TouchableOpacity> : null}
    </View>)
  }

  const OrderScroll = (item) => {
    return <View>
      {order.orderAnalytics ? OrderAnalytics() : null}
      {ActionButton()}
      {FilterDesign()}

      <View style={{ flexDirection: 'row' }}>
        {radioItems.map((item, key) => (
          <Section_Select
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
          />
        ))}
        <View style={{ height: scale(10) }} />

      </View>
      {selectedItem === 'Order Sent' ? (
        outboundPOs.length === 0 ? (
          <View style={{ marginTop: verticalScale(0) }}>
            <Empty_Card Text="No Order Sent Yet. " />
          </View>
        ) : (
          order.viewInboundOrders ? (<View>
            <FlatList
              onEndReachedThreshold={0.7}
              style={{ width: '100%' }}
              keyExtractor={(item, index) => index.toString()}
              data={outboundPOs}
              renderItem={({ item }) => eachBound(item)}
            />
            {props.receiveOrderCount != outboundPOs.length ? (
              <TouchableOpacity
                style={{ justifyContent: 'center', alignItems: 'center' }}
                onPress={() => loadMore()}>
                <Text
                  style={{
                    justifyContent: 'center',
                    backgroundColor: '#0093E9',
                    alignItems: 'center',
                    color: '#ffffff',
                    padding: 10,
                    borderRadius: 10,
                    fontWeight: 'bold',
                  }}>
                  {t('load more')}

                </Text>
              </TouchableOpacity>
            ) : null}
          </View>) : (
            <View style={{ marginTop: verticalScale(0) }}>
              <Empty_Card Text="Order Sent Permission Denied" />
            </View>
          )
        )
      ) : inboundPOs.length === 0 ? (
        <View style={{ marginTop: verticalScale(0) }}>
          <Empty_Card Text="No Order Received  Yet. " />
        </View>
      ) : (
        order.viewOutboundOrders ? (<View>
          <FlatList
            onEndReachedThreshold={0.7}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={inboundPOs}
            renderItem={({ item }) => eachBound(item)}
          />
          {props.sentOrderCount != inboundPOs.length ? (
            <TouchableOpacity
              style={{ justifyContent: 'center', alignItems: 'center' }}
              onPress={() => loadMore()}>
              <Text
                style={{
                  justifyContent: 'center',
                  backgroundColor: '#0093E9',
                  alignItems: 'center',
                  color: '#ffffff',
                  padding: 10,
                  borderRadius: 10,
                  fontWeight: 'bold',
                }}>
                {t('load more')}
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>) : (
          <View style={{ marginTop: verticalScale(0) }}>
            <Empty_Card Text="Order Received Permission Denied" />
          </View>
        )
      )}
    </View>
  }
  
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#0093E9" />
      <Header navigation={props.navigation} name={t('Your Order')} />
      <Search
        navigation={props.navigation}
        fromScreen={'Order'} />
      <FlatList
        onEndReachedThreshold={0.7}
        style={{ width: '100%' }}
        keyExtractor={(item, index) => index.toString()}
        data={['test']}
        renderItem={({ item }) => OrderScroll(item)}
        refreshControl={
          <RefreshControl
          refreshing={false}
            onRefresh={()=>onRefresh()}
            tintColor={Colors.blueE9}
            titleColor={Colors.blueE9}
            colors={['#a076e8', '#5dc4dd']}
            progressBackgroundColor={Colors.blueAE}
          />
        }
      />


    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
  lottie: {
    width: scale(100),
    height: scale(100),
  },
});
function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    loder: state.loder,
    purchaseorder: state.purchaseorder.purchaseorder,
    inboundPOs: state.purchaseorder.inboundPOs,
    outboundPOs: state.purchaseorder.outboundPOs,
    ordersAnalytics: state.purchaseorder.ordersAnalytics,
    orderReceiveFilter: state.purchaseorder.orderReceiveFilter,
    orderSentFilter: state.purchaseorder.orderSentFilter,
    sentOrderCount: state.purchaseorder.sentOrderCount,
    receiveOrderCount: state.purchaseorder.receiveOrderCount,
    userpermission: state.userinfo.userpermission,
  };
}
export default connect(mapStateToProps, {
  userinfo,
  trackJourney,
  getTrackingDetails,
  getchainOfCustody,
  getViewShipment,
  searchProductByType,
  searchProductByName,
  getOrderDetails,
  getOrderAnalytics,
  fetchOutboundPurchaseOrders,
  fetchInboundPurchaseOrders,
},
)(OrderScreen);
