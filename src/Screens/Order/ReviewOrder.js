

import React, { useState, useEffect } from 'react';
import {

  View,
  ScrollView,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  Purchase_Order_Create,
  fetchInboundPurchaseOrders,
  fetchOutboundPurchaseOrders,
} from '../../Redux/Action/order';
import {
  getOrderAnalytics
} from '../../Redux/Action/analytics';

import { HeaderWithBackBlue } from '../../components/Header';

import {ViewProdutList } from '../../components/Common/shipmentHelper';

import _ from 'lodash';
import { compareValues, NetworkUtils } from '../../Util/utils';
import { Colors, DeviceWidth } from '../../Style';

import { useTranslation } from 'react-i18next';
import { queryAllTodoLists, queryAllInventryData, queryDeleteAllInventryData } from '../../databases/allSchemas'
import { ToastShow } from '../../components/Toast';
import {addOrderlocal} from '../../databases/realmConnection'
import {queryAllorder} from '../../databases/allSchemas'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { CustomTextView, CustomInputText } from '../../components/Common/common';
import Loader from '../../components/Loader';
const ReviewOrder = (props) => {
  const { t, i18n } = useTranslation();
  const [processState, setProcessState] = useState(false)

  const OrderForm = props.route.params.values
  console.log('OrderForm review'+ JSON.stringify(OrderForm))
  _SavePO = async () => {
    const isoDate = new Date().toISOString();

    const data = {
      externalId: '',
      creationDate: isoDate,
      lastUpdatedOn: isoDate,
      supplier: {
        supplierOrganisation: OrderForm.suppierOrganisationid,
        supplierIncharge: OrderForm.primaryContactId || null,
      },
      customer: {
        customerOrganisation: OrderForm.customerOrganisationid,
        region:OrderForm.region,
        country:OrderForm.country,
        customerIncharge: OrderForm.customerprimaryContactId || null,
        shippingAddress: {
          shippingAddressId: OrderForm.customerDeliveryLocationid,
          shipmentReceiverId: OrderForm.shipmentReceiverId || null,
        },
      },
      products: OrderForm.products,
      poStatus: 'CREATED',
    };
    const isConnected = await NetworkUtils.isNetworkAvailable()
    let result ={}
    setProcessState(true)
    if(isConnected){
      result = await props.Purchase_Order_Create(data);
    }else{
      result =  await addOrderlocal(data)
    }
    setProcessState(false)

    console.log('Review_Purchase_Order result ' + JSON.stringify(result));
    // queryAllorder().then((todoLists) => {
    //   let data = todoLists;
    //               console.log('queryAllorder 334 '+ JSON.stringify(data));
                   
    //   }).catch((error) => {
    //     console.log('syncInventoryData error'+ JSON.stringify(error));
  
    //   });
    console.log('result.data.data.poId ' + JSON.stringify(result.data.data.poId));

    if (result.status === 200) {
      const  smsg = result.data && result.data.data && result.data.data.poId
      ? t('Your order ') +
      result.data.data.poId +
      t(' has been added successfully!')
      : t('Your order has been added successfully!');
      ToastShow(
        smsg,
         'success',
         'long',
         'top',
       )
      await props.fetchInboundPurchaseOrders(0, 50, props.orderSentFilter);
      await props.fetchOutboundPurchaseOrders(0, 50, props.orderReceiveFilter);
      await props.getOrderAnalytics();
      props.navigation.navigate('Order');
    } else if (result.status === 500) {
      const  emsg =   (result.data && result.data.message && result.data.message.errmsg) ||
      (result.data && result.data.message && result.data.message.name) ||
      'API Filed with 500';
      ToastShow(
        emsg,
         'error',
         'long',
         'top',
       )
    } else {
      const  emsg1 = (result && result.data && result.data.message) ||
      'something is wrong';
      ToastShow(
        emsg1,
         'error',
         'long',
         'top',
       )
    }
  };
  return (
    <View style={styles.container}>
       <HeaderWithBackBlue
        name={t('review_order')}
        navigation={props.navigation}
      />
        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{
           marginTop: verticalScale(-DeviceWidth/7),
          }}
        >
      <ViewProdutList
            Products={OrderForm.products}
            isOrderView={true}
            isReviewOrder={true} 
            />
               <View
            style={{
              marginTop: verticalScale(10),
            }}>
            <View
              style={{
                marginTop: verticalScale(10),
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
                margin: 15,
              }}>
              <Text
                style={{
                  fontSize: scale(13),
                  fontWeight: 'bold',
                  marginLeft: 10,
                  marginTop: verticalScale(10),
                }}>
                {t('Order From:')}
              </Text>
              <CustomTextView
                leftText={t('Organization Type')}
                rightText={OrderForm.FromOrgType || ''}
              />
              <CustomTextView
                leftText={t('Organization Name')}
                rightText={OrderForm.suppierOrganisationname || ''}
              />
              <CustomTextView
                leftText={t('Organization ID')}
                rightText={OrderForm.suppierOrganisationid || ''}
              />
                            <Loader Processing ={processState}/>

            </View>
            <View
              style={{
                marginTop: verticalScale(10),
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
                margin: 15,
              }}>
              <Text
                style={{
                  fontSize: scale(13),
                  fontWeight: 'bold',
                  marginTop: verticalScale(10),
                  marginLeft: 10,
                }}>
                {t('Deliver To:')}
              </Text>
              <CustomTextView
                leftText={t('Organization Type')}
                rightText={OrderForm.ToOrgType || ''}
              />
              {global.defaultCountrycode === 'cr' ? null : (<View>
                <CustomTextView
                  leftText={t('Region')}
                  rightText={OrderForm.region || ''}
                />
                <CustomTextView
                  leftText={t('Country')}
                  rightText={OrderForm.country || ''}
                /></View>)}
              <CustomTextView
                leftText={t('Organization Name')}
                rightText={OrderForm.customerOrganisationName || ''}
              />
              <CustomTextView
                leftText={t('Organization Location')}
                rightText={OrderForm.customerDeliveryLocation || ''}
              />
              <CustomTextView
                leftText={t('Organization ID')}
                rightText={OrderForm.customerOrganisationid || ''}
              />
              

            </View>
          </View>
      </ScrollView>
      <View
          style={{
            flexDirection: 'row',
            margin: verticalScale(15),
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            bottom: 10
          }}>
          {/* position: 'absolute', bottom: 10, */}
          <TouchableOpacity
            style={{
              height: scale(40),
              flex: 0.5,
              borderRadius: 10,
              backgroundColor: '#FFFFFF',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#0093E9',
              borderWidth: 1,
              margin: 10,

            }}
            onPress={() => props.navigation.goBack()}>
            <Image
              style={{ width: scale(13.73), height: scale(13.73) }}
              source={require('../../assets/edit.png')}
            />
            <Text
              style={{
                fontSize: scale(13),
                color: '#0093E9',
                fontWeight: 'bold',
                marginLeft: scale(10),
              }}>
              {t('edit')}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => _SavePO()}
            style={{
              height: scale(40),
              borderRadius: 8,
              margin: 10,
              flex: 0.5,
              flexDirection: 'row',
              backgroundColor: '#FFAB1D',
              alignItems: 'center',
              justifyContent: 'space-evenly',
            }}>
           
            <Text
              style={{
                fontSize: scale(15),
                color: '#FFFFFF',
                fontWeight: 'bold',
              }}>
              {t('save')}
            </Text>
          </TouchableOpacity>
        </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
function mapStateToProps(state) {
  return {
    loder: state.loder,
  };
}
export default connect(mapStateToProps, {
  Purchase_Order_Create,
  fetchInboundPurchaseOrders,
  fetchOutboundPurchaseOrders,
  getOrderAnalytics,
},
)(ReviewOrder);
