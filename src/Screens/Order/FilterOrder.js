import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
  ActivityIndicator,
} from 'react-native';
import Header, {
  HeaderWithBack,
} from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  CustomDropDownFilter,
} from '../../components/Common/common';
import {
  fetchSupplierAndReceiverList,
  filterOrderSent,
  fetchInboundPurchaseOrders,
  fetchOutboundPurchaseOrders,
  filterOrderReceive,
  fetchProductIdsCustomerLocationsOrganisations,
  getOrderIds,
} from '../../Redux/Action/order';


import _ from 'lodash';

import { FilterByDate, CustomDateRange } from '../../components/Common/Filter';
import { Colors, DeviceWidth } from '../../Style';
import { useTranslation } from 'react-i18next';

export const STATUS = [
  { id: 'ACCEPTED', label: 'Accepted' },
  { id: 'CREATED', label: 'Sent' },
  { id: 'TRANSIT%26PARTIALLYFULFILLED', label: 'Transit & PartiallyFullyFilled' },
  { id: 'TRANSIT%26FULLYFULFILLED', label: 'Transit & FullyFilled' },
  { id: 'PARTIALLYFULFILLED', label: 'Partially Fulfilled' },
  { id: 'FULLYFULFILLED', label: 'FullyFilled' },
  { id: 'REJECTED', label: 'Rejected' },
  { id: 'CANCELLED', label: 'CANCELLED' },
];
export const STATUS1 = [
  { id: 'ACCEPTED', label: 'Aceptada' },
  { id: 'CREATED', label: 'Enviada' },
  { id: 'TRANSIT%26PARTIALLYFULFILLED', label: 'Tránsito y parcialmente lleno' },
  { id: 'TRANSIT%26FULLYFULFILLED', label: 'Tránsito y Totalmente Llen' },
  { id: 'PARTIALLYFULFILLED', label: 'Parcialmente cumplida' },
  { id: 'FULLYFULFILLED', label: 'Completamente Lleno' },
  { id: 'REJECTED', label: 'Rechazada' },
  { id: 'CANCELLED', label: 'CANCELADA' },
];
const OrderFilter = (props) => {
  const selectedTab = props.route.params.selectedTab

  const { t, i18n } = useTranslation();
  const [fromId, setFromId] = useState('')
  const [from, setFrom] = useState('')
  const [toId, setToID] = useState('')
  const [to, setTo] = useState('')
  const [orderId, setOrderId] = useState('')
  const [deliveryLocation, setDeliveryLocation] = useState('')
  const [deliveryLocationId, setDeliveryLocationId] = useState('')

  const [dateFilter, setDateFilter] = useState('')
  const [status, setStatus] = useState('')
  const [statusId, setStatusId] = useState('')
  const [productName, setProductName] = useState('')
  const [productID, setProductID] = useState('')
  const [fromDate, setFromDate] = useState('')
  const [toDate, setToDate] = useState('')
  useEffect(() => {
    async function fetchData() {
      await props.fetchProductIdsCustomerLocationsOrganisations();
      await props.getOrderIds();
      let { orderSentFilter, orderReceiveFilter } = props;
      if (selectedTab === 'Order Sent') {
        setFromId(orderSentFilter.fromId || '')
        setToID(orderSentFilter.toId || '')
        setFrom(orderSentFilter.from || orderSentFilter.fromId || '')
        setTo(orderSentFilter.to || orderSentFilter.toId || '')
        setOrderId(orderSentFilter.orderId || '')
        setDeliveryLocation(orderSentFilter.deliveryLocation || '')
        setDeliveryLocationId(orderSentFilter.deliveryLocationId || '')
        setProductName(orderSentFilter.productName || '')
        setProductID(orderSentFilter.productID || '')
        setDateFilter(orderSentFilter.dateFilter || '')
        setStatusId(orderSentFilter.statusId || '')
        setStatus(orderSentFilter.status || '')
        setFromDate(orderSentFilter.fromDate || '')
        setToDate(orderSentFilter.toDate || '')
      } else {
        setFromId(orderReceiveFilter.fromId || '')
        setToID(orderReceiveFilter.toId || '')
        setFrom(orderReceiveFilter.from || orderReceiveFilter.fromId || '')
        setTo(orderReceiveFilter.to || orderReceiveFilter.toId || '')
        setOrderId(orderReceiveFilter.orderId || '')
        setDeliveryLocation(orderReceiveFilter.deliveryLocation || '')
        setDeliveryLocationId(orderReceiveFilter.deliveryLocationId || '')
        setProductName(orderReceiveFilter.productName || '')
        setProductID(orderReceiveFilter.productID || '')
        setDateFilter(orderReceiveFilter.dateFilter || '')
        setStatusId(orderReceiveFilter.statusId || '')
        setStatus(orderReceiveFilter.status || '')
        setFromDate(orderReceiveFilter.fromDate || '')
        setToDate(orderReceiveFilter.toDate || '')
      }

    }
    fetchData();
  }, []);


  const onFromChange = (item) => {
    setFrom(item.value)
    setFromId(item.value)

  }
  const onStatusChange = (item) => {
    if (item.value !== '1') {
      setStatus(item.value)
      setStatusId(item.value)
    }
  }
  const onToChange = (item) => {
    setTo(item.value)
    setToID(item.value)

  }
  const onProdChange = (item) => {
    setProductID(item.value)
    setProductName(item.value)

  }
  const onOrderChange = (item) => {
    setOrderId(item.value)
  }
  const onDelChange = (item) => {
    setDeliveryLocation(item.value)
    setDeliveryLocationId(item.value)
  }
  const onChangeDateRange = (val) => {
    setFromDate(val.startDate)
    setToDate(val.endDate)

  }
  const onChangeDateFilter = (val) => {
    setDateFilter(val)
  }
  const apply = async () => {
    let { orderSentFilter, orderReceiveFilter } = props;
    let Filter = {
      fromId: fromId || '',
      toId: toId || '',
      from: from || '',
      to: to || '',
      orderId: orderId || '',
      productName: productName || '',
      productID: productID || '',
      dateFilter: dateFilter || '',
      deliveryLocation: deliveryLocation || '',
      deliveryLocationId: deliveryLocationId || '',
      statusId: statusId || '',
      status: status || '',
      fromDate: fromDate || '',
      toDate: toDate || '',
    };
    if (selectedTab === 'Order Sent') {
      props.filterOrderSent(Filter);
      await props.fetchInboundPurchaseOrders(0, 10, Filter);
    } else {
      props.filterOrderReceive(Filter);
      await props.fetchOutboundPurchaseOrders(0, 10, Filter);
    }
    props.navigation.goBack();
  }
  const clearALL = () => {
    let { orderSentFilter, orderReceiveFilter } = props;
    let Filter = {
      fromId: '',
      toId: '',
      from: '',
      to: '',
      orderId: '',
      productName: '',
      productID: '',
      dateFilter: '',
      deliveryLocation: '',
      deliveryLocationId: '',
      statusId: '',
      status: '',
      fromDate: '',
      toDate: '',
    };
    setFromId('')
    setToID('')
    setFrom('')
    setTo('')
    setOrderId('')
    setDeliveryLocation('')
    setDeliveryLocationId('')
    setProductName('')
    setProductID('')
    setDateFilter('')
    setStatusId('')
    setStatus('')
    setFromDate('')
    setToDate('')
    if (selectedTab === 'Order Sent') {
      props.filterOrderSent(Filter);
      props.fetchInboundPurchaseOrders(0, 50, Filter);
    } else {
      props.filterOrderReceive(Filter);
      props.fetchOutboundPurchaseOrders(0, 50, Filter);
    }
  }


  // console.log(
  //   'orderSentFilter ' + JSON.stringify(props.orderSentFilter),
  // );

  const { prodCustomerLoc = {}, OrderIDList = [], userLang } = props;
  const {
    organisations = [],
    deliveryLocations = [],
    productIds = [],
  } = prodCustomerLoc;

  return (
    <View
      style={{
        flex: 1,
      }}>
      <StatusBar backgroundColor="#0093E9" />
      <HeaderWithBack
        name={t('Filter')}
        navigation={props.navigation}
        isfilter={true}
        onPress={() => clearALL()}
        clearFilter={t('Clear Filter')}
      />

      <ScrollView
        nestedScrollEnabled={true}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{ marginTop: verticalScale(-DeviceWidth / 5) }}>
        <View
          style={{
            margin: verticalScale(10),
            borderRadius: 10,
            padding: 5,
          }}>
          {selectedTab === 'Order Sent' ? (
            <CustomDropDownFilter
              displayName={'From'}
              stateName={from}

              image={require('../../assets/from_filter.png')}
              dropDowndata={organisations}
              label={'name'}
              mapKey={'id'}
              onChangeValue={onFromChange}

            />
          ) : (
            <CustomDropDownFilter
              displayName={'To'}
              stateName={to}
              image={require('../../assets/from_filter.png')}
              dropDowndata={organisations}
              label={'name'}
              mapKey={'id'}
              onChangeValue={onToChange}

            />
          )}

          <CustomDropDownFilter
            displayName={'Product'}
            stateName={productID}
            image={require('../../assets/Inventory1.png')}
            dropDowndata={productIds}
            label={'name'}
            mapKey={'id'}
            onChangeValue={onProdChange}

          />
          <CustomDropDownFilter

            displayName={'Delivery Location'}
            stateName={deliveryLocation}

            image={require('../../assets/Shipment1.png')}
            dropDowndata={deliveryLocations}
            label={'title'}
            mapKey={'id'}
            onChangeValue={onDelChange}
          />
          <CustomDropDownFilter
            displayName={'Order ID'}
            stateName={orderId}
            image={require('../../assets/Inventory1.png')}
            dropDowndata={OrderIDList}
            label={'id'}
            mapKey={'id'}
            onChangeValue={onOrderChange}

          />
          <CustomDropDownFilter
            displayName={'Status'}
            stateName={status}
            image={require('../../assets/status.png')}
            dropDowndata={userLang === 'es' ? STATUS1 : STATUS}
            label={'label'}
            mapKey={'id'}
            onChangeValue={onStatusChange}
            search={false}

          />
          <CustomDateRange
            displayName={'Order Date'}
            fromDate={fromDate}
            toDate={toDate}
            onChangeValue={onChangeDateRange}
            image={require('../../assets/calendar.png')}

          />
          <FilterByDate dateFilter={dateFilter} onChangeValue={onChangeDateFilter} />
        </View>
      </ScrollView>

      {props.loder ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <ActivityIndicator color="#0000ff" size={'large'} />
        </View>
      ) : null}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignContent: 'center',
          marginBottom: 20,
          width: scale(328),
          bottom: 0,
        }}>
        <TouchableOpacity
          style={{
            padding: 8,
            borderRadius: 10,
            borderWidth: 1,
            borderColor: '#58b1da',
            height: scale(45),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            width: '30%',
            marginRight: 10,
          }}
          onPress={() => props.navigation.goBack()}>
          <Text
            style={{
              color: '#58b1da',
              fontSize: 16,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {'  '}
            {t('Cancel')}{' '}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            height: scale(45),
            borderRadius: 10,
            backgroundColor: Colors.blueE9,
            flexDirection: 'row',
            padding: 8,
            justifyContent: 'space-evenly',
            width: '50%',
          }}
          onPress={() => apply()}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{ width: scale(12.35), height: scale(12.68) }}
              source={require('../../assets/Filter.png')}
            />
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: scale(13),
                color: '#FFFFFF',
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              {t('Apply Filter')}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    userLang: state.userinfo.userLang,
    supplierAndReceivers: state.shipment.supplierAndReceivers,
    orderReceiveFilter: state.purchaseorder.orderReceiveFilter,
    orderSentFilter: state.purchaseorder.orderSentFilter,
    prodCustomerLoc: state.purchaseorder.prodCustomerLoc,
    OrderIDList: state.purchaseorder.OrderIDList,
    loder: state.loder,
  };
}

export default connect(
  mapStateToProps,
  {
    fetchSupplierAndReceiverList,
    filterOrderSent,
    fetchInboundPurchaseOrders,
    fetchOutboundPurchaseOrders,
    filterOrderReceive,
    fetchProductIdsCustomerLocationsOrganisations,
    getOrderIds,
  },
)(OrderFilter);
