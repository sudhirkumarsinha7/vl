
import React, { useState, useEffect } from 'react';
import {

  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
  Image,
  Keyboard
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  getRegions,
  getCountryDetailsByRegion,
  getOrganizations,
} from '../../Redux/Action/order';
import {
  getInventoryAnalytics,
  getOrderAnalytics
} from '../../Redux/Action/analytics';
import {
  getProductNames,
  fetchOrganizations,
  getOrganizationsByType,
  getWarehouseByOrgId

} from '../../Redux/Action/shipment';

import { HeaderWithBackBlue } from '../../components/Header';
import _ from 'lodash';
import { Colors, DeviceWidth } from '../../Style';
import { CustomDropdown, CustomInputText, CustomDropDownLocation } from '../../components/Common/common';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../components/Toast';
import Search from '../../components/Search'
import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';
import { global } from '../../Util/config'
import Loader from '../../components/Loader';

const AddNewProduct = () => {
  const add = {
    productId: '',
    id:'',
    manufacturer: '',
    name: '',
    productQuantity: '',
    type: '',
    unitofMeasure: {
      id: '',
      name: '',
    },
  }
  return add
}
const CreateOrder = (props) => {
  const { t, i18n } = useTranslation();
  const [mandatory, setIsmantory] = useState(false)
  const [processState, setProcessState] = useState(false)

  const [ProductType, setProductType] = useState([])
  const [ProductNameList, setProductNameList] = useState([])
  const [fromOrganisations, setFromOrgList] = useState([])
  const [Regions, setRegions] = useState([])
  const [countries, setCountriesList] = useState([])
  const [toOrganization, setToOrganization] = useState([])
  const [warehouses, setWarehouses] = useState([])
  useEffect(() => {
    async function fetchData() {
      await props.getProductNames();
      let { product_name } = props;
      let getProductNames = _.uniqBy(product_name, function (x) {
        return x.type;
      });
      setProductType(getProductNames)

      const orgSplit = props.user.organisation?.split('/');
      const ownOrg = orgSplit[0];
      const orgId = orgSplit[1];
      const fetchOrganizations = await props.fetchOrganizations();
      const res = await props.getOrganizationsByType(
        props.user && props.user.configuration_id || global.defaultConfigID,
      );
    }
    fetchData();
  }, []);

  const OrgTypeList = props.OrgTypeList?.filter(each => {
    if (each.name) {
      return each
    }
  })


  var validationSchema = Yup.object().shape(
    {
      FromOrgType: Yup.string().required(t('Required')),
      ToOrgType: Yup.string().required(t('Required')),
      region: Yup.string().required(t('Required')),
      country: Yup.string().required(t('Required')),
      suppierOrganisationid: Yup.string().required(t('Required')),
      customerOrganisationid: Yup.string().required(t('Required')),
      customerDeliveryLocationid: Yup.string().required(t('Required')),
      products: Yup.array()
        .of(
          Yup.object().shape({
            manufacturer: Yup.string().required(t('Required')),
            name: Yup.string().required(t('Required')),
            productQuantity: Yup.string().required(t('Required')),
            type: Yup.string().required(t('Required')),
          })
        ).required(t('Required'))
    },
    [],
  ); 
  const OrderForm = useFormik({
    initialValues: {
      products: [
        {
          productId: '',
          id:'',
          manufacturer: '',
          name: '',
          productQuantity: '',
          type: '',
          unitofMeasure: {
            id: '',
            name: '',
          },
        }
      ],
      FromOrgType: '',
      suppierOrganisationid: '',
      suppierOrganisationname: '',
      ToOrgType: '',
      customerOrganisationid: '',
      customerOrganisationName: '',
      customerDeliveryLocation: '',
      region: '',
      country: '',
      shipmentReceiverId: '',
      customerDeliveryLocationid: ''

    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _Review = () => {
    setIsmantory(true);
    Keyboard.dismiss();
    OrderForm.handleSubmit();
  };
  const AddAnotherProduct = () => {
    const data = [...OrderForm.values.products]
    const newData = AddNewProduct()
    const finalData = [...data, newData]
    let allProduct = {
      products: finalData
    }
    OrderForm.setValues({
      ...OrderForm.values,
      products: finalData,
    });
    // OrderForm.setValues(allProduct)
  }
  const cancel = index => {
    const productClone = JSON.parse(JSON.stringify(OrderForm?.values?.products));
    // console.log(Object.keys(productClone));
    productClone.splice(index, 1);
    let allProduct = {
      products: productClone
    }
    OrderForm.setValues(allProduct)
  };
  const DeleteIcon = index => {
    return <View
      style={{
        flexDirection: 'row',
        marginLeft: verticalScale(5),
        alignItems: 'center',

      }}>
      <Text style={{ width: '80%', fontWeight: 'bold' }}>
        {t('Product') + ' ' + (index + 1)}
      </Text>
      <View
        style={{
          width: scale(35),
          height: scale(35),
          borderRadius: 400,
          backgroundColor: Colors.whiteF2,
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 10,
          marginTop: 5,
        }}
        opacity={5}>
        <TouchableOpacity
          style={{
            justifyContent: 'flex-end',
          }}
          onPress={() => cancel(index)}
          disabled={OrderForm?.values?.products.length === 1 ? true : false}>

          {OrderForm?.values?.products.length === 1 ? (
            <Image
              style={{ width: scale(13.5), height: scale(35) }}
              source={require('../../assets/deletegrey.png')}
              resizeMode="contain"
            />
          ) : (
            <Image
              style={{ width: scale(13.5), height: scale(35) }}
              source={require('../../assets/deleteblue.png')}
              resizeMode="contain"
            />
          )}

        </TouchableOpacity>
      </View>
    </View>
  };

  const handleSubmit = async values => {
    console.log('handleSubmit ' + JSON.stringify(values))
    props.navigation.navigate('ReviewOrder', {
      values: values,
    });
  };
  const ProductListComponent = () => {
    return (<View>
      {OrderForm?.values?.products.map((each, index) => {
        return <View style={{ backgroundColor: Colors.whiteFF, margin: 15, borderRadius: 5 }}>
          {DeleteIcon(index)}
          <CustomDropdown
            displayName={'Product Type'}
            stateName={OrderForm?.values?.products[index]?.type}
            dropDowndata={ProductType}
            onChangeValue={(val) => onChangeProductType(val, index)}
            label={'type'}
            mapKey={'type'}
            mandatory={mandatory}
          />
          <CustomDropdown
            displayName={'Product Name'}
            stateName={OrderForm?.values?.products[index]?.productId}
            dropDowndata={ProductNameList}
            onChangeValue={(val) => onChangeProductName(val, index)}
            label={'name'}
            mapKey={'id'}
            mandatory={mandatory}
          />
          {/* <View style={{ marginTop: scale(15) }} /> */}
          <CustomInputText
            label={"Product Id"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={OrderForm.handleChange(`products[${index}].productId`)}
            val={OrderForm?.values?.products[index]?.productId}
            errorMsg={OrderForm.errors && OrderForm.errors.products && OrderForm.errors.products[index] && OrderForm.errors.products[index].productId}
            mandatory={mandatory}
            disabled={true}

          />
          <CustomInputText
            label={"manufacturer"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={OrderForm.handleChange(`products[${index}].manufacturer`)}
            val={OrderForm?.values?.products[index]?.manufacturer}
            errorMsg={OrderForm.errors && OrderForm.errors.products && OrderForm.errors.products[index] && OrderForm.errors.products[index].manufacturer}
            disabled={true}
            mandatory={mandatory}

          />

          <CustomInputText
            // label={"Quantity (" + each.unitofMeasure.name + ')'}
            label={each.unitofMeasure && each.unitofMeasure.name? "Quantity (" +  each.unitofMeasure.name + ')':'Quantity'}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={OrderForm.handleChange(`products[${index}].productQuantity`)}
            val={OrderForm?.values?.products[index]?.productQuantity}
            keyboardType={"numeric"}
            errorMsg={OrderForm.errors && OrderForm.errors.products && OrderForm.errors.products[index] && OrderForm.errors.products[index].productQuantity}
            mandatory={mandatory}
          />

        </View>

      })}
    </View>)
  }
  const onChangeProductType = (item, i) => {
    OrderForm.handleChange({ target: { name: `products[${i}].type`, value: item?.value } })
    OrderForm.handleChange({ target: { name: `products[${i}].productId`, value: '' } })
    OrderForm.handleChange({ target: { name: `products[${i}].name`, value: '' } })
    OrderForm.handleChange({ target: { name: `products[${i}].manufacturer`, value: '' } })
    const getProducts = props.product_name.filter(each => {
      if (each.type == item?.value) {
        return each
      }
    })
    setProductNameList(getProducts)
  };
  const onChangeProductName = (item, i) => {
    OrderForm.handleChange({ target: { name: `products[${i}].productId`, value: item?.value } })
    OrderForm.handleChange({ target: { name: `products[${i}].id`, value: item?.value } })
    OrderForm.handleChange({ target: { name: `products[${i}].name`, value: item.label } })
    OrderForm.handleChange({ target: { name: `products[${i}].manufacturer`, value: item.eachItem.manufacturer } })
    OrderForm.handleChange({ target: { name: `products[${i}].manufacturer`, value: item.eachItem.manufacturer } })
    OrderForm.handleChange({
      target: {
        name: `products[${i}].unitofMeasure`, value: item.eachItem.unitofMeasure
      }
    })

  };
  const OrderFromViewComponent = () => {
    return (<View
      style={{
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
      }}>
      <Text
        style={{
          marginLeft: verticalScale(13),
          fontWeight: 'bold',
          marginTop: verticalScale(10),
        }}>
        {t('Order From:')}
      </Text>

      <CustomDropdown


        displayName={'Organization Type'}
        stateName={OrderForm?.values?.FromOrgType}
        dropDowndata={OrgTypeList}
        onChangeValue={(val) => selectFromOrgType(val)}
        label={'name'}
        mapKey={'name'}
        mandatory={mandatory}
      />
      <CustomDropdown
        displayName={'Organization Name'}
        stateName={OrderForm?.values?.suppierOrganisationid}
        dropDowndata={fromOrganisations}
        onChangeValue={(val) => selectFromOrgName(val)}
        label={'name'}
        mapKey={'id'}
        mandatory={mandatory}
      />
               {/* <View style={{ marginTop: scale(15) }} /> */}

      <CustomInputText
        label={"Organization ID"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        // onChange={OrderForm.handleChange(suppierOrganisationid)}
        val={OrderForm?.values?.suppierOrganisationid}
        errorMsg={OrderForm.errors && OrderForm.errors.suppierOrganisationid}
        disabled={true}
        mandatory={mandatory}

      />

    </View>)
  }
  const selectFromOrgType = (item) => {
    let filterOrg = props.organizations.filter(each => {
      if (each.id) {
        if (each.type === item.label) {
          return each;
        }
      }
    });
    setFromOrgList(filterOrg)
    OrderForm.handleChange({ target: { name: `FromOrgType`, value: item?.value } })
    OrderForm.handleChange({ target: { name: `suppierOrganisationid`, value: '' } })
    OrderForm.handleChange({ target: { name: `suppierOrganisationname`, value: '' } })

  };
  const selectFromOrgName = (item1) => {
    console.log('selectFromOrgName ' + JSON.stringify(item1))
    let item = item1.eachItem
    OrderForm.handleChange({ target: { name: `primaryContactId`, value: item?.primaryContactId } })
    OrderForm.handleChange({ target: { name: `suppierOrganisationid`, value: item1?.value } })
    OrderForm.handleChange({ target: { name: `suppierOrganisationname`, value: item1?.label } })

  };
  const DeliverToComponent = () => {
    return (<View
      style={{
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
      }}>
      <Text
        style={{
          marginLeft: verticalScale(13),
          fontWeight: 'bold',
          marginTop: verticalScale(10),
        }}>
        {t('Deliver To:')}
      </Text>

      <CustomDropdown


        displayName={'Organization Type'}
        stateName={OrderForm?.values?.ToOrgType}
        dropDowndata={OrgTypeList}
        onChangeValue={(val) => selectToOrgType(val)}
        label={'name'}
        mapKey={'name'}
        mandatory={mandatory}
      />

      {global.defaultCountrycode === 'cr' ? null : <View>
        <CustomDropdown
          displayName={'Region'}
          stateName={OrderForm?.values?.region}
          dropDowndata={Regions}
          onChangeValue={(val) => selectToRegion(val)}
          mandatory={mandatory}
        />
        <CustomDropdown
          displayName={'Country'}
          stateName={OrderForm?.values?.country}
          dropDowndata={countries}
          onChangeValue={(val) => selectToCountry(val)}
          mandatory={mandatory}
        />


      </View>}
      <CustomDropdown
        displayName={'Organization Name'}
        stateName={OrderForm?.values?.customerOrganisationid}
        dropDowndata={toOrganization}
        label={'name'}
        mapKey={'id'}
        onChangeValue={(val) => getDeliveryData(val)}
        mandatory={mandatory}
      />
               {/* <View style={{ marginTop: scale(15) }} /> */}

      <CustomInputText
        label={"Organization ID"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        val={OrderForm?.values?.customerOrganisationid}
        errorMsg={OrderForm.errors && OrderForm.errors.customerOrganisationid}
        disabled={true}
        mandatory={mandatory}

      />
      <CustomDropDownLocation
        displayName={'Delivery Location'}
        stateName={OrderForm?.values?.customerDeliveryLocationid}
        dropDowndata={warehouses}
        label={'name'}
        mapKey={'id'}
        onChangeValue={(val) => selectDeliveryLoc(val)}
        mandatory={mandatory}
      />




    </View>)
  }
  const selectToOrgType = async item => {
    OrderForm.handleChange({ target: { name: `ToOrgType`, value: item?.value } })
    OrderForm.handleChange({ target: { name: `customerOrganisationid`, value: '' } })
    OrderForm.handleChange({ target: { name: `customerOrganisationName`, value: '' } })
    OrderForm.handleChange({ target: { name: `customerDeliveryLocation`, value: '' } })
    OrderForm.handleChange({ target: { name: `region`, value: '' } })
    OrderForm.handleChange({ target: { name: `country`, value: '' } })
    setProcessState(true)

    const getRegionsData = await props.getRegions(item?.value);
    console.log('getRegionsData '+JSON.stringify(getRegionsData))
    let Regions = getRegionsData.filter(each => {
      if (each) {
        return each
      }
    })
    setRegions(Regions);
    setProcessState(false)

    setToOrganization([]);
    setWarehouses([]);
    setCountriesList([])
    if (global.defaultCountrycode === 'cr') {
      let data = {
        'value': 'Costa Rica',
        'label': 'Costa Rica'
      }
      selectToCountry(data, item.label)
    }
  };
  const selectToRegion = async item => {
    OrderForm.handleChange({ target: { name: `region`, value: item.value } })

    if (OrderForm?.values?.ToOrgType) {
      setProcessState(true)
      const countryList =await props.getCountryDetailsByRegion(item.value, OrderForm?.values?.ToOrgType);
      console.log('country list'+JSON.stringify(countryList))
      setCountriesList(countryList);
      setProcessState(false)
      setToOrganization([]);
      setWarehouses([]);
      OrderForm.handleChange({ target: { name: `customerOrganisationid`, value: '' } })
      OrderForm.handleChange({ target: { name: `customerOrganisationName`, value: '' } })
      OrderForm.handleChange({ target: { name: `customerDeliveryLocation`, value: '' } })
      OrderForm.handleChange({ target: { name: `country`, value: '' } })
      OrderForm.handleChange({ target: { name: `shipmentReceiverId`, value: '' } })
      OrderForm.handleChange({ target: { name: `customerDeliveryLocationid`, value: '' } })
    } else {
      ToastShow(
        t('Please Select Delivery org Type'),
        'error',
        'long',
        'top',
      )
    }
  };
  const selectToCountry = async (item, orgType = '') => {
    OrderForm.handleChange({ target: { name: `country`, value: item.value } })

    if ((OrderForm?.values?.ToOrgType && OrderForm?.values?.region) || orgType) {
      setProcessState(true)

      const orgList = await props.getOrganizations(item.value, orgType || OrderForm?.values?.ToOrgType);
      setToOrganization(orgList);
      setProcessState(false)

      setWarehouses([]);
      OrderForm.handleChange({ target: { name: `customerOrganisationid`, value: '' } })
      OrderForm.handleChange({ target: { name: `customerOrganisationName`, value: '' } })
      OrderForm.handleChange({ target: { name: `customerDeliveryLocation`, value: '' } })
      OrderForm.handleChange({ target: { name: `shipmentReceiverId`, value: '' } })
      OrderForm.handleChange({ target: { name: `customerDeliveryLocationid`, value: '' } })
    } else {
      if (!OrderForm?.values?.region) {
        ToastShow(
          t('Please Select Delivery Region'),
          'error',
          'long',
          'top',
        )
      } else if (!OrderForm?.values?.ToOrgType) {
        ToastShow(
          t('Please Select Delivery org Type'),
          'error',
          'long',
          'top',
        )
      }
    }
  };
  const  getDeliveryData = async item => {
    let item1 = item.eachItem
    setProcessState(true)

    const list = await props.getWarehouseByOrgId(item?.value);
    setWarehouses(list)

    setProcessState(false)

    OrderForm.handleChange({ target: { name: `customerprimaryContactId`, value: item1?.primaryContactId } })
      OrderForm.handleChange({ target: { name: `customerOrganisationid`, value:  item?.value } })
      OrderForm.handleChange({ target: { name: `customerOrganisationName`, value: item?.label } })
      OrderForm.handleChange({ target: { name: `shipmentReceiverId`, value: '' } })
      OrderForm.handleChange({ target: { name: `customerDeliveryLocationid`, value: '' } })
      OrderForm.handleChange({ target: { name: `customerDeliveryLocation`, value: '' } })

  };
  const selectDeliveryLoc = async (item) => {
    let item1 = item.eachItem
    OrderForm.handleChange({ target: { name: `customerDeliveryLocationid`, value: item?.value } })
    OrderForm.handleChange({ target: { name: `customerDeliveryLocation`, value: item.label } })
    OrderForm.handleChange({ target: { name: `shipmentReceiverId`, value: item1?.supervisors[0] } })

  }
  return (
    <View style={styles.container}>
      <HeaderWithBackBlue
        name={t('create_new_order')}
        navigation={props.navigation}
      />
      <View style={{ marginTop: verticalScale(-DeviceWidth / 7) }}>

        <KeyboardAwareScrollView extraScrollHeight={50} >
          {ProductListComponent()}

          <TouchableOpacity
            style={{
              borderRadius: 10,
              flexDirection: 'row',
              marginLeft: 10,
            }}
            onPress={AddAnotherProduct}>
            <View style={{
              backgroundColor: '#FFFFFF',
              borderRadius: 5,
              marginLeft: scale(5),
              padding: 5,
              elevation: 5,
              paddingLeft: scale(10),
              paddingRight: scale(10)
            }}>
              <Text
                style={{
                  fontSize: scale(12),
                  color: '#0093E9',
                  fontWeight: 'bold',
                }}>
                {t('+ Add Another Product')}
              </Text>
            </View>
          </TouchableOpacity>
          {OrderFromViewComponent()}
          {DeliverToComponent()}
          <Loader Processing ={processState}/>

          <View
            style={{
              flexDirection: 'row',
              margin: verticalScale(15),
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
            }}>
            <TouchableOpacity
              style={{
                height: scale(40),
                backgroundColor: '#FFFFFF',
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#0093E9',
                borderWidth: 1,
                flex: 0.5,
                margin: 10,
              }}
              onPress={() => props.navigation.goBack()}>
              <Text
                style={{
                  marginLeft: scale(13),
                  fontSize: scale(16),
                  fontWeight: 'bold',
                  color: '#0093E9',
                  alignSelf: 'center'
                }}>
                {t('Cancel')}
              </Text>
            </TouchableOpacity>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 1 }}
              colors={['#0093E9', '#36C2CF']}
              style={{
                // marginTop: verticalScale(45),
                // width: scale(90),
                // height: scale(35),
                borderRadius: 8,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                flex: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  margin: 10,


                }}
                onPress={() => _Review()}>
                <Text
                  style={{
                    marginLeft: scale(13),
                    fontSize: scale(16),
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                  }}>
                  {t('Review')}
                </Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <View style={{ marginBottom: verticalScale(DeviceWidth / 2) }} />

        </KeyboardAwareScrollView>
      </View>

    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
});
function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    OrgTypeList: state.userinfo.OrgTypeList,
    loder: state.loder,
    organizations: state.productdetail.Organizations,
    product_name: state.productdetail.product_name,
    wareHouseDetails: state.inventory.wareHouseDetailsbyorgid,
    regionsByOrgType: state.productdetail.regionsByOrgType,
    countriesByOrgType: state.productdetail.countriesByOrgType,
    orgListByOrgType: state.productdetail.orgListByOrgType,
  };
}
export default connect(mapStateToProps, {
  getProductNames,
  fetchOrganizations,
  getOrganizationsByType,
  getRegions,
  getCountryDetailsByRegion,
  getOrganizations,
  getWarehouseByOrgId,
  getOrderAnalytics
},
)(CreateOrder);
