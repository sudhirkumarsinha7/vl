import React,{useState,useEffect} from 'react';
import {
  LayoutAnimation,
  UIManager,
  Platform,
  Button,
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  StatusBar,
  Animated,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  changePOStatus,
  fetchOutboundPurchaseOrders,
  fetchInboundPurchaseOrders,
} from '../../Redux/Action/order';
import {
  getOrderAnalytics,
} from '../../Redux/Action/analytics';
import PopUp from '../../components/PopUp';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Header, { HeaderWithBackBlue } from '../../components/Header';

import {
  OrderViewCard,
  ViewProdutList,
  getPOStatus,
} from '../../components/Common/shipmentHelper';
import { useTranslation } from 'react-i18next';
import { compose } from 'redux';
import { Colors, DeviceHeight,DeviceWidth } from '../../Style';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { ToastShow } from '../../components/Toast';

const ViewOrder =(props)=>{
  const isFocused = useIsFocused();
  const { t, i18n } = useTranslation();

  const item = props.route.params.item
  const selectedTab = props.route.params.selectedTab
const [userOrgID,setUserID] = useState('')
useEffect(() => {
  async function fetchData() {
    const orgSplit = props.user.organisation?.split('/');
    const ownOrg = orgSplit[0];
    const orgId = orgSplit[1];
    setUserID(orgId)
  }
  fetchData();
}, [isFocused]);



  const changeStatus = async status => {

    const data = { status: status, orderID: item.id };
    const result = await props.changePOStatus(data);
    console.log('changeStatus result' + JSON.stringify(result));

    if (result.status === 200) {
    
      const  smsg = t('Order') + ' ' + t(status) + ' ' + t('SuccessFully')
      ToastShow(
        smsg,
         'success',
         'long',
         'top',
       )
      await props.fetchInboundPurchaseOrders(0, 50, props.orderSentFilter);
      await props.fetchOutboundPurchaseOrders(
        0,
        50,
        props.orderReceiveFilter,
      );
      await props.getOrderAnalytics();
      props.navigation.goBack();
    } else if (result.status === 500) {
      const  emsg =   (result.data && result.data.message && result.data.message.errmsg) ||
      (result.data && result.data.message && result.data.message.name) ||
      'API Filed with 500';
      ToastShow(
        emsg,
         'error',
         'long',
         'top',
       )
    
    } else {
      const  emsg1 = (result && result.data && result.data.message) ||
      'something is wrong';
      ToastShow(
        emsg1,
         'error',
         'long',
         'top',
       )
      
    }
  };

    let { products = [], Products = [] } = item;
    let { userpermission,} = props;
    let { order = {} } = userpermission;

    return (
      <KeyboardAwareScrollView extraScrollHeight={50} >

      <View style={styles.container}>
        <StatusBar backgroundColor="#0093E9" />

        <HeaderWithBackBlue
          name={t('View Order')}
          navigation={props.navigation}
        />
        
        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{marginTop: verticalScale(-DeviceWidth/7) }}>
          <OrderViewCard
            item={item}
            userOrgID={userOrgID}
            selectedTab={selectedTab}
          />
          {props.loder ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}>
              <ActivityIndicator color="#0000ff" />
            </View>
          ) : null}
          <Text
            style={{
              fontSize: scale(16),
              fontWeight: 'bold',
              color: '#0093E9',
              marginLeft: 15,
            }}>
            {t('product_details')}
          </Text>
          <ViewProdutList
            Products={products}
            isOrderView={true}
          />
          {getPOStatus(
            item.poStatus,
            userOrgID,
            item.supplier && item.supplier.supplierOrganisation,
           selectedTab,
            t,
          ) === t('Received') && order.acceptRejectOrder ? (
            <View
              style={{
                flexDirection: 'row',
                height: scale(40),
                borderRadius: 8,
                marginBottom: verticalScale(10),
              
                magin: 10,
                justifyContent: 'space-evenly',
              }}>
              <TouchableOpacity
                style={{
                  width: '40%',
                  height: scale(40),
                  borderRadius: 10,
                  backgroundColor: 'green',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderColor: 'green',
                  borderWidth: 1,
                }}
                onPress={() => changeStatus('ACCEPTED')}>
                <Text
                  style={{
                    fontSize: scale(13),
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                    marginLeft: scale(10),
                  }}>
                  {t('Accept Order')}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  changeStatus('REJECTED');
                }}
                style={{
                  width: '40%',
                  height: scale(40),
                  borderRadius: 8,
                  flexDirection: 'row',
                  backgroundColor: 'red',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                <Text
                  style={{
                    fontSize: scale(15),
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                  }}>
                  {t('Reject Order')}
                </Text>
              </TouchableOpacity>
            </View>
          ) : getPOStatus(
            item.poStatus,
            userOrgID,
            item.supplier && item.supplier.supplierOrganisation,
            selectedTab,
            t,
          ) === t('Sent') ? <TouchableOpacity
            onPress={() => {
              changeStatus('CANCELLED');
            }}
            style={{
              height: scale(40),
              borderRadius: 8,
              flexDirection: 'row',
              backgroundColor: Colors.red23,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: scale(25),
              marginRight: scale(25)
            }}>
            <Text
              style={{
                fontSize: scale(15),
                color: '#FFFFFF',
                fontWeight: 'bold',
              }}>
              {t('Cancel Order')}
            </Text>
          </TouchableOpacity> : null}
        </ScrollView>
      </View>
      </KeyboardAwareScrollView>
    );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  lottie: {
    width: scale(100),
    height: scale(100),
  },
});

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    loder: state.loder,
    orderReceiveFilter: state.purchaseorder.orderReceiveFilter,
    orderSentFilter: state.purchaseorder.orderSentFilter,
    userpermission: state.userinfo.userpermission,
  };
}

export default connect(
  mapStateToProps,
  {
    changePOStatus,
    fetchOutboundPurchaseOrders,
    fetchInboundPurchaseOrders,
    getOrderAnalytics,
  },
)(ViewOrder);
