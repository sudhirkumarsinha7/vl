import React, { useEffect,useState } from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  BackHandler,
  StatusBar,
} from 'react-native';
import jwt_decode from 'jwt-decode';
import AsyncStorage from '@react-native-async-storage/async-storage';
import setAuthToken from '../../Util/setAuthToken';
import { useTranslation } from 'react-i18next';
// import { changeLang } from '../../Action';
import { connect } from 'react-redux';
import {global} from '../../Util/config'


const AuthLoadingScreen = (props) => {
  const [currentLanguage, setLanguage] = useState(global.defaultCountrycode === 'cr'?'es':'en');
  const { t, i18n } = useTranslation();
   useEffect(() => {
    _loadData()
  }, []);

  const changeLanguage = value => {
    // console.log(value)
    i18n
      .changeLanguage(value)
      .then(() => setLanguage(value))
      .catch(err => console.log(err));
    // props.changeLang(value);

  };
 const  _loadData = async () => {
    const token = await AsyncStorage.getItem('token');
    console.log('auth_tokenn', token);
    if (token) {
      const decoded = jwt_decode(token);
      console.log('decode', decoded);
      const currenTime = Date.now() / 1000;
      if (decoded.exp < currenTime) {
        await AsyncStorage.clear();
        props.navigation.navigate('Auth');
      } else {
        const lang = await AsyncStorage.getItem('lang');
        if (lang) {
          changeLanguage(lang)

        }
        setAuthToken(token, lang);
        props.navigation.navigate('App');
      }
    } else {
      props.navigation.navigate('Auth');
    }
  };

  return (
    <View style={styles.container}>
      <ActivityIndicator />
      {/* <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center',}} /> */}
      <StatusBar barStyle="default" />
    </View>
  );

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

function mapStateToProps(state) {
  return {
    loder: state.loder,
  };
}

export default connect(mapStateToProps, {  })(AuthLoadingScreen);
