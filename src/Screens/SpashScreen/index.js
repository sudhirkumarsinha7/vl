import React, { useEffect } from 'react';
import { View, Text, StyleSheet, ImageBackground, Image, StatusBar, Animated } from 'react-native';
import { useTranslation } from "react-i18next";
import { scale, verticalScale, moderateScale } from '../../Util'
import VersionNumber from 'react-native-version-number';

// const SplashScreen = () => {
// const { t } = useTranslation();

//   return (
//     <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
//       <Text>SplashScreen Screen </Text>
//     </View>
//   );
// };
const ImageLoader = (props) => {
    const opacity = new Animated.Value(0)

    const onLoad = () => {
        Animated.timing(opacity, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }).start();
    };


    return (
        <Animated.Image
            onLoad={onLoad}
            {...props}
            style={[
                {
                    opacity: opacity,
                    transform: [
                        {
                            scale: opacity.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0.85, 1],
                            }),
                        },
                    ],
                },
                props.style,
            ]}
        />
    );
}

const SplashScreen = (props) => {


    useEffect(() => {
        async function navigaeScreen() {
            setTimeout(() => {
                props.navigation.navigate('AuthLoading');
            }, 5000);
        }
        navigaeScreen();
    }, []);
    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="cover"
            source={require('../../assets/Splash_Background.png')}>
            <StatusBar hidden />

            <View style={styles.container}>
                <ImageLoader
                    style={{ width: scale(298), height: scale(36), marginBottom: '70%' }}
                    source={require('../../assets/VACCINELEDGER.png')}
                    resizeMode="contain"
                />

                <View style={{ alignItems: 'center', marginBottom: '10%' }}>
                    <View style={{ alignItems: 'center' }}>
                        <Text
                            style={{
                                fontSize: scale(12),
                                color: '#707070',
                            }}>
                            Powered By
                        </Text>
                        <Image
                            style={{
                                width: scale(140),
                                height: scale(24),
                                borderWidth: 0,
                                marginTop: verticalScale(8),
                            }}
                            source={require('../../assets/STATWIGLogo.png')}
                            resizeMode="contain"
                        />
                    </View>
                   
                    <Text
                        style={{
                            color: '#707070',
                            fontSize: scale(14),
                            marginTop: verticalScale(8),
                        }}>
                        Version {VersionNumber.appVersion}
                    </Text>
                    <Text
                        style={{
                            color: '#707070',
                            fontSize: scale(12),
                            marginTop: verticalScale(8),
                        }}>
                         © 2022 StaTwig. All rights reserved.
                    </Text>
                </View>
            </View>
        </ImageBackground>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
});
export default SplashScreen;
