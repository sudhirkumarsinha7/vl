import React, { useEffect, useState } from 'react';
import {
  saveUser,
  checkUser,
  checkUseremailId,
  checkUserphoneNumber,
  getAllRegion,
  getAllCountries,
  getAllState,
  getAllCities,
  checkGoogleLogin
} from '../../Redux/Action/auth';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  StatusBar,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  Switch,
  Alert,
  BackHandler,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import { Colors } from '../../Style';
import { registerTwillio } from '../../Redux/Action/alert';

import { Input } from '@rneui/base';
import { connect } from 'react-redux';
import { config, global } from '../../Util/config';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import PhoneInput, { isValidNumber } from 'react-native-phone-number-input';
import { Region, NewLocation, SiginupScreenForm } from '../../components/Common/defaultValue';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../components/Toast';
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
import AntDesign from 'react-native-vector-icons/AntDesign'
import { localImage } from '../../Util/imges';
import { RadioButton } from 'react-native-paper';
import RadioForm from 'react-native-simple-radio-button';
let orgRadio = [{ label: 'Existing Organization  ', value: 'E' }, { label: 'New Organization', value: 'N' }];
let orgRadio1 = [{ label: 'Organización existente  ', value: 'E' }, { label: 'Nueva organización', value: 'N' }];

import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
import {
  DropdownIcon1,
  SearchableDropdown,
  CustomInputText,
  CustomInputTextImage
} from '../../components/Common/common';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
const userinfo = { name: 'admin', pass: 'admin', email: 'sh@gmail.com' };
const shadowStyle = {
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowOpacity: 0.3,
  shadowRadius: 4.65,

  elevation: 8,
};

const Signup = props => {
  const [organisations, setOrganisations] = useState([]);
  const [organisationsList, setOrganisationsList] = useState([]);
  const [orgTypeList, setOrganizationsType] = useState([]);
  const [isVisible, setIsVisible] = useState(false);
  const [ismanDatory, setIsmantory] = useState(false);
  const [isLocationPickerVisible, setIsLocationPicker] = useState(false);
  const [isTypeVisible, setIsTypeVisible] = useState(false);
  const [isOtherOrganisation, setIsOtherOrganisation] = useState(false);
  const [adminAwaiting, setAdminAwaiting] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const { t, i18n } = useTranslation();
  const [existOrg, setExistOrg] = React.useState('');
  useEffect(() => {
    async function fetchData() {
      googleConfig()

      const data = props.route.params.data
      await props.getAllRegion();
      let orgType = await axios.get(
        config().getOrganizationsByType + global.defaultConfigID,
      );
      console.log('orgType data' + JSON.stringify(orgType));
      orgType =
        (orgType.data.data &&
          orgType.data.data[0] &&
          orgType.data.data[0].organisationTypes) ||
        [];

      orgType = orgType.filter(each => {
        if (each.name) {
          return each
        }
      })
      // console.log('orgType ' + JSON.stringify(orgType));

      const orgs = await axios.get(config().getOrganisationsAtSignup);
      let orgData = orgs?.data?.data;
      // console.log("orgData" + JSON.stringify(orgData));

      let Other = { id: '0', name: 'Other' };
      orgData.push(Other);
      let Defaultorg = [];
      // console.log("orgData ee"+ JSON.stringify(orgData));
      Defaultorg.push(Other);
      setOrganisationsList(orgs?.data?.data);
      setOrganisations(Defaultorg);
      setOrganizationsType(orgType);
      if (global.defaultCountrycode === 'cr') {
        let costa = { "value": 53, "label": "Costa Rica" }
        onCountryChange(costa)
      }
      if (data && data.emailId) {
        signupform.handleChange({ target: { name: `emailId`, value: data.emailId } })
        signupform.handleChange({ target: { name: `firstName`, value: data.firstName } })
        signupform.handleChange({ target: { name: `lastName`, value: data.lastName } })
      }
    }
    fetchData();
  }, []);
  const googleConfig = async () => {
    GoogleSignin.configure({
      scopes: ['profile', 'email'], // what API you want to access 
      // scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      webClientId: global.GAPI_WEB_CLIENT_ID, // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      hostedDomain: '', // specifies a hosted domain restriction
      forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      accountName: '', // [Android] specifies an account name on the device that should be used
      iosClientId: global.GOIS_WEB_CLIENT_ID, // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
      googleServicePlistPath: '', // [iOS] if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
      openIdRealm: '', // [iOS] The OpenID2 realm of the home web server. This allows Google to include the user's OpenID Identifier in the OpenID Connect ID token.
      profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
    });
    await GoogleSignin.signOut();
  }
  const gSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log('gSignIn userInfo ' + JSON.stringify(userInfo))
      let userData = userInfo.user;
      const data1 = {
        tokenId: userInfo.idToken,
      };
      const gRes = await props.checkGoogleLogin(data1, props.userLang)
      await GoogleSignin.signOut();

      if (gRes?.status === 200) {
        //do something

        RemotePushService(userData.email);

      } else if (gRes.status === 500) {
        let err = (gRes.data && gRes.data.message) || 'API failed';
        if (typeof err === 'string') {
          err = err;
        } else {
          err = 'API failed 500 Internal Server Error';
        }
        ToastShow(
          t(err),
          'error',
          'long',
          'top',
        )
      } else {

        signupform.handleChange({ target: { name: `emailId`, value: userData.email } })
        signupform.handleChange({ target: { name: `firstName`, value: userData.givenName } })
        signupform.handleChange({ target: { name: `lastName`, value: userData.familyName } })

      }


    } catch (error) {
      ToastShow(
        t(error),
        'error',
        'long',
        'top',
      )
      // console.log('gSignIn error ' + JSON.stringify(error))
      // console.log('gSignIn statusCodes ' + JSON.stringify(statusCodes))

      if (error.code === statusCodes.SIGN_IN_CANCELLED) {

      } else if (error.code === statusCodes.IN_PROGRESS) {

      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      } else {
      }
    }
  }

  const RemotePushService = userIDPushNotification => {
    PushNotification.configure({
      onRegister: function (data) {
        var reqParamsRaw = {
          token_id: data.token,
          device_type: Platform.OS,
          username: userIDPushNotification,
        };
        // console.log('reqParamsRaw' + JSON.stringify(reqParamsRaw));
        props.registerTwillio(reqParamsRaw);
        // console.log(reqParamsRaw);
      },
      onNotification: function (notification) {
        PushNotification.localNotification(notification);
        if (Platform.OS === 'ios') {
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        }
      },
      onAction: function (notification) {
        //  console.log("ACTION:", notification.action);
        console.log('NOTIFICATION:', notification);
      },
      onRegistrationError: function (err) {
        console.log('Not Registered:' + err.message);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      // Android only: GCM or FCM Sender ID
      senderID: '937115966227',
      popInitialNotification: true,
      requestPermissions: true,
    });
    props.navigation.navigate('App');
  };
  var validationSchema = Yup.object().shape(
    {
      // emailId: Yup.string().matches(reg_email, 'Please Enter valid email id'),
      phoneNumber: Yup.string().when(['emailId', 'mob'], {
        is: (emailId, mob) => !emailId || mob,
        then: Yup.string()
          .test('phoneNumber', t('Please Enter valid number'), val =>
            isValidNumber(val),
          )
          .required(t('PhoneNumber Required')),
      }),
      emailId: Yup.string().when(['mob', 'phoneNumber'], {
        is: (mob, phoneNumber) => !mob || !phoneNumber,
        then: Yup.string()
          .matches(reg_email, t('Please Enter valid email id'))
          .required(t('Email Required')),
      }),
      firstName: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('First Name Required')),
      lastName: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('Last Name Required')),
      organisationId: Yup.string().required(t('Organistion ID Required')),
    },
    ['phoneNumber', 'emailId', 'firstName', 'lastName', 'organisationId'],
  ); // <-- HERE!!!!!!!!
  var validationOrgSchema = Yup.object().shape(
    {
      organisationName: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('Required')),
      pincode: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('Required')),
      line1: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('Required')),
      city: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('Required')),
      state: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('Required')),
      country: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('Required')),

    },
    ['organisationName', 'pincode', 'line1', 'city', 'state', 'country'],
  ); // <-- HERE!!!!!!!!
  const signupform = useFormik({
    initialValues: {
      emailId: '',
      firstName: '',
      lastName: '',
      organisationId: '',
      phoneNumber: ' ',
      organisationName: '',
      type: '',
      mob: '',
    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _signup = () => {
    setIsmantory(true);
    Keyboard.dismiss();
    signupform.handleSubmit();
  };


  const orgForm = useFormik({
    initialValues: {
      organisationName: '',
      line1: '',
      city: '',
      pincode: '',
      state: '',
      country: '',
      countryId: '',
      stateId: '',
      cityId: '',
    },
    validationOrgSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _AddOrg = async () => {
    console.log('_AddOrg values');
    Keyboard.dismiss();
    orgForm.handleSubmit();
  };
  const handleSubmit = async values => {
    setErrorMessage('');
    console.log('handleSubmit value1' + JSON.stringify(values));
    // console.log('handleSubmit orgForm value1' + JSON.stringify(orgForm.values));
    // console.log('signupform ' + JSON.stringify(signupform.values));
    let isRegisterUser = true;
    values.emailId = values.emailId || signupform.emailId;

    if (values.organisationId === '0' && !orgForm.values.organisationName) {
      if (values.emailId && values.mob) {
        let result1 = await props.checkUseremailId(values.emailId);
        let result2 = await props.checkUserphoneNumber(values.phoneNumber);
        console.log('checkUseremailId result1 ' + JSON.stringify(result1));
        console.log('checkUserphoneNumber result2 ' + JSON.stringify(result2));
        if (
          result1 &&
          result1.status === 200 &&
          result2 &&
          result2.status === 200
        ) {
          let data1 = result1.data && result1.data.data;
          let data2 = result1.data && result2.data.data;
          if (data1.length && data2.length) {
            if (
              data1[0].emailId ||
              data2[0].emailId ||
              data1[0].phoneNumber ||
              data2[0].phoneNumber
            ) {
              isRegisterUser = false;
              setErrorMessage('User is Already registered');
              ToastShow(
                t('User is Already registered'),
                'error',
                'long',
                'top',
              )
            } else {
              setIsOtherOrganisation(true);
              isRegisterUser = false;
            }
          } else if (data1.length) {
            if (data1[0].emailId || data1[0].phoneNumber) {
              isRegisterUser = false;
              setErrorMessage('User is Already registered');
              ToastShow(
                t('User is Already registered'),
                'error',
                'long',
                'top',
              )
            } else {
              setIsOtherOrganisation(true);
              isRegisterUser = false;
            }
          } else if (data2.length) {
            if (data2[0].emailId || data2[0].phoneNumber) {
              isRegisterUser = false;
              setErrorMessage('User is Already registered');
              ToastShow(
                t('User is Already registered'),
                'error',
                'long',
                'top',
              )
            } else {
              setIsOtherOrganisation(true);
              isRegisterUser = false;
            }
          } else {
            setIsOtherOrganisation(true);
            isRegisterUser = false;
          }
          // props.getAllRegion();
        } else if (result1?.status === 500) {
          isRegisterUser = false;
          setErrorMessage(result1?.data?.message);
          ToastShow(
            t(result1?.data?.message),
            'error',
            'long',
            'top',
          )
        } else if (result2?.status === 500) {
          isRegisterUser = false;
          setErrorMessage(result2?.data?.message);
          ToastShow(
            t(result1?.data?.message),
            'error',
            'long',
            'top',
          )
        } else {
          isRegisterUser = false;
          setErrorMessage('User is Already registered');
          ToastShow(
            t('User is Already registered'),
            'error',
            'long',
            'top',
          )
        }
      } else if (values.emailId) {
        let result3 = await props.checkUseremailId(values.emailId);
        console.log('checkUseremailId result3 ' + JSON.stringify(result3));
        if (result3 && result3.status === 200 && result3.data) {
          let data3 = result3.data && result3.data.data;
          if (data3.length) {
            if (data3[0].emailId || data3[0].phoneNumber) {
              isRegisterUser = false;
              setErrorMessage('User is Already registered');
              ToastShow(
                t('User is Already registered'),
                'error',
                'long',
                'top',
              )
            } else {
              setIsOtherOrganisation(true);
              isRegisterUser = false;
            }
          } else {
            setIsOtherOrganisation(true);
            isRegisterUser = false;
          }
        } else if (result3?.status === 500) {
          isRegisterUser = false;
          setErrorMessage(result3?.data?.message);
          ToastShow(
            t(result3?.data?.message),
            'error',
            'long',
            'top',
          )
        } else {
          isRegisterUser = false;
          setErrorMessage('User is Already registered');
          ToastShow(
            t('User is Already registered'),
            'error',
            'long',
            'top',
          )
        }
      } else {
        let result4 = await props.checkUserphoneNumber(values.phoneNumber);
        console.log('signupform result4 ' + JSON.stringify(result4.data));

        if (result4 && result4.status === 200 && result4.data) {
          let data4 = result4.data && result4.data.data;
          if (data4.length) {
            if (data4[0].emailId || data4[0].phoneNumber) {
              isRegisterUser = false;
              setErrorMessage('User is Already registered');
              ToastShow(
                t('User is Already registered'),
                'error',
                'long',
                'top',
              )
            } else {
              setIsOtherOrganisation(true);
              isRegisterUser = false;
            }
          } else {
            setIsOtherOrganisation(true);
            isRegisterUser = false;
          }
        } else if (result4?.status === 500) {
          isRegisterUser = false;
          setErrorMessage(result4?.data?.message);
          ToastShow(
            t(result4?.data?.message),
            'error',
            'long',
            'top',
          )
        } else {
          isRegisterUser = false;
          setErrorMessage('User is Already registered');
          ToastShow(
            t('User is Already registered'),
            'error',
            'long',
            'top',
          )
        }
      }

    } else if (orgForm.values.organisationName) {
      let newValue = orgForm.values;
      let newSignupValue = signupform.values;
      newSignupValue.organisationName = orgForm.values.organisationName;
      newSignupValue.address = newValue;
      newSignupValue.emailId = signupform.values?.emailId;
      values = newSignupValue;
      // console.log('signupform.organisationName' + JSON.stringify(values));
    }
    // console.log('handleSubmit final' + JSON.stringify(values));
    // console.log('handleSubmit final isRegisterUser' + isRegisterUser);


    if (isRegisterUser) {
      const res = await props.saveUser(values);
      console.log('isRegisterUser res' + JSON.stringify(res));

      if (res?.status === 200) {
        setAdminAwaiting(true);
      } else if (res?.status === 500) {
        setErrorMessage(res?.data?.message);
        ToastShow(
          t(res?.data?.message),
          'error',
          'long',
          'top',
        )
      } else {
        const err = res?.data?.data[0];
        setErrorMessage(err?.msg);
        ToastShow(
          t(err?.msg),
          'error',
          'long',
          'top',
        )
      }
    }
  };

  const hideLocationPicker = () => {
    setIsLocationPicker(false);
  };
  const handleLocationPicked = Loc => {
    console.log('handleLocationPicked Loc ' + JSON.stringify(Loc));
    orgForm.handleChange({ location: Loc });
    hideLocationPicker();
  };
  const onRegionChange = item => {
    orgForm.setValues({
      ...orgForm.values,
      region: item.value,
      country: '',
      state: '',
      city: '',
      countryId: '',
      stateId: '',
      cityId: '',
    });
    props.getAllCountries(item.value);
  };
  const onOrgChange = item => {
    // console.log('onOrgChange ' + JSON.stringify(item))
    signupform.setValues({
      ...signupform.values,
      type: item?.value,
      organisationName: '',
      organisationId: '',
    })
    let neworgList = organisationsList.filter(each => {
      if (each.type === item?.value) {
        return each;
      }
    });
    let Other = { id: '0', name: 'Other' };
    neworgList.push(Other);
    // console.log('onOrgChange neworgList' + JSON.stringify(neworgList))

    setOrganisations(neworgList);
  };
  const onOrgNameChange = item => {
    console.log('onOrgNameChange ' + JSON.stringify(item))
    signupform.setValues({
      ...signupform.values,
      organisationId: item?.value,
      organisationName: item?.label,
    })
  };
  const onCountryChange = item => {
    console.log('onCountryChange ' + JSON.stringify(item))
    orgForm.setValues({
      ...orgForm.values,
      country: item.label,
      countryId: item.value,
      stateId: '',
      cityId: '',
      state: '',
      city: '',
    });
    props.getAllState(item.value);
  };
  const onStateChange = item => {
    orgForm.setValues({
      ...orgForm.values,
      stateId: item.value,
      state: item.label,
      cityId: '',
      city: '',
    });
    props.getAllCities(item.value);
  };
  const onCityChange = item => {
    orgForm.setValues({
      ...orgForm.values,
      cityId: item.value,
      city: item.label,
    });
  };
  const AdminWaintingComponentOld = () => {
    return <>

      <Text
        style={{
          marginTop: verticalScale(20),
          fontSize: scale(20),
          color: '#000000',
          fontWeight: 'bold',
        }}>
        {t('Signup')}
      </Text>
      <View style={{ width: '20%', marginLeft: '90%', marginTop: verticalScale(-35), }}>
        <TouchableOpacity
          style={{
            borderRadius: 10,
            height: scale(30),
          }}
          onPress={() =>
            props.navigation.navigate('Login')
          }>
          <AntDesign
            name={'closecircleo'}
            color={Colors.blueE9}
            size={25}
          />
        </TouchableOpacity>
      </View>
      <Image
        style={{
          width: scale(150),
          height: scale(240),
          borderWidth: 0,
        }}
        source={require('../../assets/waiting.png')}
        resizeMode="contain"
      />
      <Text
        style={{
          marginBottom: verticalScale(60),
          color: '#000000',
        }}>
        {t('Waiting for Admin’s Approval…')}
      </Text>
    </>
  }
  const AdminWaintingComponent = () => {
    return <View>
      <TouchableOpacity onPress={() =>
        props.navigation.navigate('Login')
      } style={{ flexDirection: 'row', marginTop: verticalScale(40), marginLeft: 15 }}>
        <AntDesign
          name={'arrowleft'}
          color={Colors.grey9A}
          size={25}
        />
        <Text
          style={{

            fontSize: scale(16),
            color: Colors.grey9A,
            marginLeft: 5
          }}>
          {t('Back')}
        </Text>
      </TouchableOpacity>

      <View style={{ justifyContent: 'center', alignItems: 'center' }}>
        <Text
          style={{
            marginTop: verticalScale(20),
            fontSize: scale(20),
            color: '#000000',
            fontWeight: 'bold',
          }}>
          {t('Signup')}
        </Text>

        <Image
          style={{
            width: scale(150),
            height: scale(240),
            borderWidth: 0,
          }}
          source={require('../../assets/waiting.png')}
          resizeMode="contain"
        />
        <Text
          style={{
            marginBottom: verticalScale(60),
            color: '#000000',
          }}>
          {t('Waiting for Admin’s Approval…')}
        </Text>
      </View>
    </View>
  }
  const otherOrgComponent = () => {
    return <View>
      <TouchableOpacity onPress={() =>
        setIsOtherOrganisation(!isOtherOrganisation)
      } style={{ flexDirection: 'row', marginTop: verticalScale(40), marginLeft: 15 }}>
        <AntDesign
          name={'arrowleft'}
          color={Colors.grey9A}
          size={25}
        />
        <Text
          style={{

            fontSize: scale(16),
            color: Colors.grey9A,
            marginLeft: 5
          }}>
          {t('Back')}
        </Text>
      </TouchableOpacity>
      <View style={{ width: '100%' }}>
        <View style={{ width: '90%' }}>
          <Text
            style={{
              marginTop: verticalScale(20),
              fontSize: scale(16),
              fontWeight: 'bold',
              marginLeft: 10,
            }}>
            {t('Register your Organization')}
          </Text>
        </View>
        <Text
          style={{
            marginTop: verticalScale(12),
            fontSize: scale(14),
            color: Colors.gray9,
            marginLeft: 10,
          }}>
          {t('Almost there, Please provide the below details to regsiter your organization with us')}
        </Text>

      </View>
      <View style={{ justifyContent: 'center', alignItems: 'center' }}>

        <View style={{ width: '100%' }}>
          <CustomInputTextImage
            label={"Organization Name"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={orgForm.handleChange('organisationName')}
            val={orgForm?.values?.organisationName}
            errorMsg={orgForm?.errors?.organisationName}
            mandatory={ismanDatory}
            img={require('../../assets/organization.png')}
            imgStyle={{ width: scale(15), height: scale(13), }}

          />

        </View>
        <View style={{ width: '96%', }}>
          {global.defaultCountrycode === 'cr' ? null :
            <View>
              <SearchableDropdown
                eachRow={NewLocation.region}
                state={orgForm.values}
                image={require('../../assets/location.png')}
                dropDowndata={props.regions}
                onChangeValue={onRegionChange}

              />
              <SearchableDropdown
                eachRow={NewLocation.countryId}
                state={orgForm.values}
                image={require('../../assets/location.png')}
                dropDowndata={props.countries}
                label={'name'}
                mapKey={'id'}
                onChangeValue={onCountryChange}

              />
            </View>}
          <SearchableDropdown
            eachRow={NewLocation.stateId}
            state={orgForm.values}
            image={require('../../assets/location.png')}
            dropDowndata={props.states}
            label={'name'}
            mapKey={'id'}
            onChangeValue={onStateChange}
          />
          <SearchableDropdown
            eachRow={NewLocation.cityId}
            state={orgForm.values}
            image={require('../../assets/location.png')}
            dropDowndata={props.cities}
            label={'name'}
            mapKey={'id'}
            onChangeValue={onCityChange}

          />

          <View style={{ width: '100%', }}>
            <CustomInputTextImage
              label={"Address"}
              labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
              onChange={orgForm.handleChange('line1')}
              val={orgForm?.values?.line1}
              errorMsg={orgForm?.errors?.line1}
              mandatory={ismanDatory}
              img={require('../../assets/location.png')}
              imgStyle={{ width: scale(11), height: scale(15) }}

            />
            {/* <View style={{ width: '100%', marginLeft: 5 }}>
            <Input
              placeholder={t("Address")}
              leftIcon={() => (
                <Icon name="location-pin" color="black" size={16} />
              )}
              value={orgForm?.values?.line1}
              onChangeText={orgForm.handleChange('line1')}
              errorMessage={orgForm?.errors?.line1}
            />
          </View> */}

          </View>
          <View style={{ width: '100%' }}>
            <CustomInputTextImage
              label={"Pin"}
              labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
              onChange={orgForm.handleChange('pincode')}
              val={orgForm?.values?.pincode}
              errorMsg={orgForm?.errors?.pincode}
              mandatory={ismanDatory}
              img={require('../../assets/location.png')}
              imgStyle={{ width: scale(11), height: scale(15) }}
            />
            {/* <View style={{ width: '100%', marginLeft: 5, marginTop: scale(-20) }}>
            <Input
              placeholder={t("Pin")}
              leftIcon={() => (
                <Icon name="location-pin" color="black" size={16} />
              )}
              keyboardType={'numeric'}
              value={orgForm?.values?.pincode}
              onChangeText={orgForm.handleChange('pincode')}
              errorMessage={orgForm?.errors?.pincode}
            />
          </View> */}
          </View>
        </View>


        <TouchableOpacity
          onPress={_AddOrg}
          disabled={isNewOrgEnable ? false : true}
          style={{
            width: scale(133),
            height: scale(40),
            borderRadius: 8,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 30
          }}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={
              isNewOrgEnable
                ? ['#0EBA86', '#0EBA86']
                : ['gray', 'gray']
            }
            style={{
              width: scale(133),
              height: scale(40),
              borderRadius: 8,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: scale(20),
                color: '#FFFFFF',
                fontWeight: 'bold',
              }}>
              {t('Register')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
        <View style={{ height: scale(10) }} />
      </View>
    </View>
  }
  const RegisterComponent = () => {
    return <>
      <Text
        style={{
          marginTop: verticalScale(20),
          fontSize: scale(20),
          color: '#000000',
          fontWeight: 'bold',
        }}>
        {t('Signup')}
      </Text>
      <View style={{ width: '100%', }}>
        <CustomInputTextImage
          label={"First Name"}
          labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
          onChange={signupform.handleChange(`firstName`)}
          val={signupform?.values?.firstName}
          errorMsg={signupform?.errors?.firstName}
          mandatory={ismanDatory}
          img={require('../../assets/user1.png')}
          imgStyle={{ width: 15, height: 17 }}
        />

      </View>
      <View style={{ width: '100%' }}>
        <CustomInputTextImage
          label={"Last Name"}
          labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
          onChange={signupform.handleChange(`lastName`)}
          val={signupform?.values?.lastName}
          errorMsg={signupform?.errors?.lastName}
          mandatory={ismanDatory}
          img={require('../../assets/user1.png')}
          imgStyle={{ width: 15, height: 17 }}
        />

      </View>

      <View style={{ width: '100%', }}>
        <CustomInputTextImage
          label={"Email"}
          labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
          onChange={signupform.handleChange(`emailId`)}
          val={signupform?.values?.emailId}
          errorMsg={signupform?.errors?.emailId}
          mandatory={ismanDatory}
          img={require('../../assets/mail.png')}
          imgStyle={{ width: 17, height: 12 }}
        />

      </View>
      <View style={{ width: '100%', flexDirection: 'row', marginTop: 10 }}>
        <Text style={{ fontWeight: '400', marginLeft: scale(10) }}>{t('Phone')}</Text>
        {/* {signupform?.values?.mob ? <Text style={{ fontWeight: '400', marginLeft: scale(10) }}>{t('Phone')}</Text> : null} */}
      </View>
      <View style={{ width: '100%', flexDirection: 'row', borderWidth: 1, borderRadius: 15, }}>
        <Image
          source={require('../../assets/phone.png')}
          style={{
            width: 18,
            height: 18,
            marginLeft: 10,
            marginRight: 5,
            alignSelf: 'center',
          }}
        />

        <PhoneInput
          textStyle={{ fontSize: 14, marginBottom: 5 }}
          initialCountry={global.defaultCountrycode}
          value={signupform?.values?.mob}
          layout="first"
          onChangeFormattedText={signupform.handleChange(
            'phoneNumber',
          )}
          onChangeText={signupform.handleChange('mob')}
        />
      </View>
      {signupform?.values?.mob ? (
        <Text style={{ color: 'red', marginLeft: -100 }}>
          {ismanDatory && signupform?.errors?.phoneNumber}
        </Text>
      ) : null}
      <View style={{ width: '100%', marginTop: verticalScale(7) }}>
        <SearchableDropdown
          eachRow={SiginupScreenForm.type}
          state={signupform.values}
          image={require('../../assets/organization.png')}
          dropDowndata={orgTypeList}
          label={'name'}
          mapKey={'name'}
          onChangeValue={onOrgChange}

          mandatory={ismanDatory}
        />
      </View>
      <View style={{ width: '100%', marginTop: verticalScale(7) }}>

        <SearchableDropdown
          eachRow={SiginupScreenForm.organisationId}
          state={signupform.values}
          image={require('../../assets/organization.png')}
          dropDowndata={organisations}
          label={'name'}
          mapKey={'id'}
          onChangeValue={onOrgNameChange}

          mandatory={ismanDatory}
        />
      </View>

      <View style={{ height: scale(10) }} />
      {/* <Text style={{ marginBottom: 10, color: 'red' }}>
        {errorMessage}
      </Text> */}
      <TouchableOpacity
        onPress={_signup}
        disabled={isEnable ? false : true}
        style={{
          width: scale(133),
          height: scale(40),
          borderRadius: 8,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={
            isEnable ? ['#0093E9', '#36C2CF'] : ['gray', 'gray']
          }
          style={{
            width: scale(133),
            height: scale(40),
            borderRadius: 8,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: scale(20),
              color: '#FFFFFF',
              fontWeight: 'bold',
            }}>
            {t('SIGNUP')}
          </Text>
        </LinearGradient>
      </TouchableOpacity>

      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
          marginTop: verticalScale(17),
        }}>
        <Text
          style={{
            fontSize: scale(14),
            color: '#707070',
          }}>
          {t('Already have an Account?')}
        </Text>
        <TouchableOpacity
          onPress={() => props.navigation.navigate('Login')}>
          <Text
            style={{
              marginLeft: scale(7),
              fontSize: scale(14),
              color: '#0B65C1',
              fontWeight: 'bold',
            }}>
            {t('Log In')}
          </Text>
        </TouchableOpacity>
      </View>

      <View style={{ height: scale(10) }} />
    </>
  }

  const RegisterComponentNew = () => {
    return <View>
      <TouchableOpacity onPress={() =>
        props.navigation.navigate('Login')
      } style={{ flexDirection: 'row', marginTop: verticalScale(40), marginLeft: 15 }}>
        <AntDesign
          name={'arrowleft'}
          color={Colors.grey9A}
          size={25}
        />
        <Text
          style={{

            fontSize: scale(16),
            color: Colors.grey9A,
            marginLeft: 5
          }}>
          {t('Back')}
        </Text>
      </TouchableOpacity>
      <View style={{ width: '100%' }}>
        <View style={{ width: '90%' }}>
          <Text
            style={{
              marginTop: verticalScale(20),
              fontSize: scale(16),
              fontWeight: 'bold',
              marginLeft: 10,
            }}>
            {t('Create your Account')}
          </Text>
        </View>
        <Text
          style={{
            marginTop: verticalScale(14),
            fontSize: scale(12),
            color: Colors.gray9,
            marginLeft: 10,
          }}>
          {t('Join VaccineLedger to ensure quality and safety of your Vaccines using Blockchain')}
        </Text>

      </View>
      <TouchableOpacity onPress={() => gSignIn()} style={{ margin: 15, borderWidth: 1, borderRadius: 10, borderColor: '#296ACC' }}>
        <View style={{ padding: 5, flexDirection: 'row', justifyContent: 'center' }}>
          <Image source={localImage.GoogleLogo} style={{ width: 30, height: 30, alignItems: 'center' }} />

          <Text style={{ fontSize: 16, marginLeft: 10, textAlign: 'center', marginTop: 5 }}>{t('Sigup with Google')}</Text>
        </View>

      </TouchableOpacity>
      <View style={{ justifyContent: 'center', alignItems: 'center' }}>

        <View style={{ width: '100%', }}>
          <CustomInputTextImage
            label={"First Name"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={signupform.handleChange(`firstName`)}
            val={signupform?.values?.firstName}
            errorMsg={signupform?.errors?.firstName}
            mandatory={ismanDatory}
            img={require('../../assets/user1.png')}
            imgStyle={{ width: 15, height: 17 }}
          />

        </View>
        <View style={{ width: '100%' }}>
          <CustomInputTextImage
            label={"Last Name"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={signupform.handleChange(`lastName`)}
            val={signupform?.values?.lastName}
            errorMsg={signupform?.errors?.lastName}
            mandatory={ismanDatory}
            img={require('../../assets/user1.png')}
            imgStyle={{ width: 15, height: 17 }}
          />

        </View>

        <View style={{ width: '100%', }}>
          <CustomInputTextImage
            label={"Email"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={signupform.handleChange(`emailId`)}
            val={signupform?.values?.emailId}
            errorMsg={signupform?.errors?.emailId}
            mandatory={ismanDatory}
            img={require('../../assets/mail.png')}
            imgStyle={{ width: 17, height: 12 }}
          />

        </View>
        <View style={{ width: '100%', flexDirection: 'row', marginTop: 10 }}>

          <Text style={{ fontWeight: '400', marginLeft: scale(10) }}>{t('Phone')}</Text>
        </View>
        <View style={{ width: '97%', flexDirection: 'row', borderWidth: 1, borderRadius: 10, marginTop: 5 }}>
          <Image
            source={require('../../assets/phone.png')}
            style={{
              width: 18,
              height: 18,
              marginLeft: 10,
              marginRight: 5,
              alignSelf: 'center',
            }}
          />

          <PhoneInput
            textStyle={{ fontSize: 14, marginBottom: 5 }}
            initialCountry={global.defaultCountrycode}
            value={signupform?.values?.mob}
            layout="first"
            onChangeFormattedText={signupform.handleChange(
              'phoneNumber',
            )}
            onChangeText={signupform.handleChange('mob')}
          />
        </View>
        {signupform?.values?.mob ? (
          <Text style={{ color: 'red', marginLeft: -100 }}>
            {ismanDatory && signupform?.errors?.phoneNumber}
          </Text>
        ) : null}
        <View style={{ flexDirection: 'row', marginTop: 40, width: '90%' }}>
          <RadioForm
            radio_props={ props.userLang === 'es' ? orgRadio1 : orgRadio }
            initial={existOrg}
            onPress={(val) => checkOrg(val)}
            buttonColor={'#0364FF'}
            buttonSize={10}
            buttonOuterColor={'red'}
            buttonInnerColor={'green'}
            formHorizontal={true}
          />
        </View>

        {existOrg === 'E' ? (<View style={{ width: '97%' }}>
          <View style={{ width: '100%', marginTop: verticalScale(7) }}>
            <SearchableDropdown
              eachRow={SiginupScreenForm.type}
              state={signupform.values}
              image={require('../../assets/organization.png')}
              dropDowndata={orgTypeList}
              label={'name'}
              mapKey={'name'}
              onChangeValue={onOrgChange}

              mandatory={ismanDatory}
            />
          </View>
          <View style={{ width: '100%', marginTop: verticalScale(7) }}>

            <SearchableDropdown
              eachRow={SiginupScreenForm.organisationId}
              state={signupform.values}
              image={require('../../assets/organization.png')}
              dropDowndata={organisations}
              label={'name'}
              mapKey={'id'}
              onChangeValue={onOrgNameChange}

              mandatory={ismanDatory}
            />
          </View>
        </View>)
          : null}
        <View style={{ height: scale(10) }} />
        <TouchableOpacity
          onPress={_signup}
          disabled={isEnable ? false : true}
          style={{
            width: scale(133),
            height: scale(40),
            borderRadius: 8,
            backgroundColor: 'red',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={
              isEnable ? ['#0EBA86', '#0EBA86'] : ['gray', 'gray']
            }
            style={{
              width: scale(133),
              height: scale(40),
              borderRadius: 8,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: scale(20),
                color: '#FFFFFF',
                fontWeight: 'bold',
              }}>
              {t('Continue')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
            marginTop: verticalScale(17),
          }}>
          <Text
            style={{
              fontSize: scale(14),
              color: '#707070',
            }}>
            {t('Already have an Account?')}
          </Text>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Login')}>
            <Text
              style={{
                marginLeft: scale(7),
                fontSize: scale(14),
                color: '#0B65C1',
                fontWeight: 'bold',
              }}>
              {t('Log In')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{ height: scale(10) }} />
    </View>
  }
  const checkOrg = (val) => {
    setExistOrg(val);
    if (val === 'N') {
      onOrgNameChange({ value: '0', label: 'Other' })
    }
  }
  const isEnable =
    (signupform?.values?.emailId || signupform?.values?.mob) &&
    signupform?.values?.firstName &&
    signupform?.values?.lastName &&
    (signupform?.values?.organisationId ||
      signupform?.values?.organisationId === 0);
  const isNewOrgEnable =
    orgForm?.values?.organisationName &&
    orgForm?.values?.line1 &&
    orgForm?.values?.city &&
    orgForm?.values?.pincode &&
    (orgForm?.values?.state && orgForm?.values?.country);
  console.log('orgTypeList ' + JSON.stringify(orgTypeList));
  return (
    <ImageBackground
      style={styles.imgBackground}
      resizeMode="stretch"
      source={localImage.loginBackgraund}>
      <KeyboardAwareScrollView extraScrollHeight={50} >

        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
            justifyContent: 'center',
          }}>
          <StatusBar hidden />

          <View
            style={{
              // flex: 1,
              backgroundColor: 'transparent',
              justifyContent: 'center',

            }}>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              nestedScrollEnabled={true}
              showsVerticalScrollIndicator={false}>
              {/* <Image
                style={{
                  marginTop: verticalScale(79),
                  marginLeft: scale(30),
                  width: scale(198),
                  height: scale(24),
                }}
                resizeMode="cover"
                source={require('../../assets/VACCINELEDGER.png')}
              /> */}
              {/* <Text
                style={{
                  marginLeft: scale(30),
                  marginTop: verticalScale(8),
                  fontSize: scale(24),
                  color: '#000000',
                }}>
                {t('Welcome')}
              </Text>
              <Text
                style={{
                  marginLeft: scale(30),
                  marginTop: verticalScale(9),
                  fontSize: scale(13),
                  color: '#000000',
                }}>
                {t('Signup to continue')}
              </Text> */}
              {/* <View
                style={[
                  {
                    marginLeft: scale(30),
                    marginRight: scale(30),
                    flex: 1,
                    borderRadius: 10,
                    backgroundColor: '#FFFFFF',
                    marginTop: verticalScale(40),
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                  shadowStyle,
                ]}>
                {props.loder ? (
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      flex: 1,
                    }}>
                    <ActivityIndicator color="#0000ff" />
                  </View>
                ) : null}
                {adminAwaiting ? (
                  AdminWaintingComponent()
                ) : isOtherOrganisation ? (
                  otherOrgComponent()
                ) : (
                  RegisterComponent()
                )}
              </View> */}
              {adminAwaiting ? (
                AdminWaintingComponent()
              ) : isOtherOrganisation ? (
                otherOrgComponent()
              ) : (
                RegisterComponentNew()
              )}
              <View style={{ height: scale(10) }} />
            </ScrollView>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
    backgroundColor: 'white'
  },
  wrapper: {
    flex: 1,
  },
});
function mapStateToProps(state) {
  return {
    userdata: state.auth.userdata,
    regions: state.productdetail.regions,
    countries: state.productdetail.countries,
    states: state.productdetail.states,
    cities: state.productdetail.cities,
    userLang: state.userinfo.userLang,
  };
}

export default connect(
  mapStateToProps,
  {
    saveUser,
    checkUser,
    checkUseremailId,
    checkUserphoneNumber,
    getAllRegion,
    getAllCountries,
    getAllState,
    getAllCities,
    checkGoogleLogin,
    registerTwillio
  },
)(Signup);
