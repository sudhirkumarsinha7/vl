import React, { useEffect } from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
} from '../../Redux/Action/inventory'
import {
  getProductNames
} from '../../Redux/Action/shipment';
import _ from 'lodash';
import { backgroundColor,DeviceWidth } from '../../Style';
import Empty_Card from '../../components/Empty_Card';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { compose } from 'redux';
const OutOfStockProduct =(props)=> {
  const { t, i18n } = useTranslation();


  
  const eachProductList=(item)=> {
    let unit = item.products && item.products.unitofMeasure && item.products.unitofMeasure.name || ' '
    let qty = item.inventoryDetails && item.inventoryDetails.quantity || 0
    return (
      <View
        style={{
          marginTop: verticalScale(15),
          width: scale(328),
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginLeft: scale(15),
            marginTop: scale(10)
          }}>
          <Text
            style={{
              fontSize: scale(12),
             
              color: '#717171',
              width: '40%',
            }}>
            {t('Product Name')}
          </Text>
          <Text
            style={{
              marginLeft: scale(11),
              fontSize: scale(12),
             
              marginBottom: 5,
            }}
            numberOfLines={1}>
            {item.products && item.products.name}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginLeft: scale(15),
          }}>
          <Text
            style={{
              fontSize: scale(12),
             
              color: '#717171',
              width: '40%',
            }}>
           { t('Product ID')}
          </Text>
          <Text
            style={{
              marginLeft: scale(11),
              fontSize: scale(12),
             
              marginBottom: 5,
            }}
            numberOfLines={1}>
            {item.products && item.products.id}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginLeft: scale(15),
          }}>
          <Text
            style={{
              fontSize: scale(12),
             
              color: '#717171',
              width: '40%',
              marginBottom: 5,
            }}>
            {t('Manufacturer')}
          </Text>
          <Text
            style={{
              marginLeft: scale(11),
              fontSize: scale(12),
             
            }}
            numberOfLines={1}>
            {item.products && item.products.manufacturer}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginLeft: scale(15),
          }}>
          <Text
            style={{
              fontSize: scale(12),
             
              color: '#717171',
              width: '40%',
              marginBottom: 15,
            }}>
            {t('Quantity')}
          </Text>
          <Text
            style={{
              marginLeft: scale(11),
              fontSize: scale(12),
             
            }}
            numberOfLines={1}>
            {'' + qty + ' ' + unit}
          </Text>
        </View>
      </View>
    );
  }


    let { inventoryout=[] } =props;
  
    let inventorydataOutOfStock = inventoryout.filter(each => {
      if (each.inventoryDetails && each.inventoryDetails.quantity <= 0) {
        return each;
      }
    });
    return (
      <View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('product_out_of_stock')}
          navigation={props.navigation}
        />
        <View
          style={{
            borderRadius: 8,
           marginTop: verticalScale(-DeviceWidth/7),
            padding: verticalScale(10),
          }}>
          {inventorydataOutOfStock.length === 0 ? (
            <View style={{ marginTop: verticalScale(0) }}>
              <Empty_Card Text="No Inventory Yet." />
            </View>
          ) : (
            <FlatList
              onEndReachedThreshold={0.7}
              data={inventorydataOutOfStock}
              keyExtractor={(item, index) => index.toString()}
              enableEmptySections={true}
              renderItem={({ item, index }) => eachProductList(item)}
            />
          )}
          <View style={{ height: scale(185) }} />
        </View>
      </View>
    );
}
function mapStateToProps(state) {
  return {
    product_name: state.productdetail.product_name,
    loder: state.loder,
    inventoryout: state.inventory.inventoryout,
  };
}

export default connect(
  mapStateToProps,
  { getProductNames },
)(OutOfStockProduct);
