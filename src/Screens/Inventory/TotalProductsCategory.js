import React, { useEffect } from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';

import _ from 'lodash';
import { backgroundColor,DeviceWidth } from '../../Style';
import Empty_Card from '../../components/Empty_Card';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
  getBatchWarehouse,
  searchProductByType,
} from '../../Redux/Action/inventory'
import {
  getProductNames
} from '../../Redux/Action/shipment';

const TotalProductsCategory =(props)=> {
  const { t, i18n } = useTranslation();

 
  useEffect(() => {
    async function fetchData() {
      await props.getProductNames();
    }
    fetchData();
  }, []);
 


  const eachProductType=(item)=> {
    return (
      <TouchableOpacity
        onPress={() => {
          props.searchProductByType(item.type);
          props.navigation.navigate('ProductListByCategory', {
            selectedProductType: item.type,
          });
        }}
        style={{
          width: scale(99),
          height: scale(110),
          justifyContent: 'center',
          alignItems: 'center',
          margin: scale(5),
          borderRadius: 8,
          backgroundColor:
            backgroundColor[Math.floor(Math.random() * backgroundColor.length)],
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{ width: scale(40), height: scale(40) }}
            source={require('../../assets/product.png')}
            resizeMode="contain"
          />
          <Text
            style={{
              margin: scale(8),
              fontSize: scale(11),
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {item && item.type}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

    let ProductType = _.uniqBy(props.product_name, function (x) {
      return x.type;
    });
    return (
      <View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('Product Category')}
          navigation={props.navigation}
        />
        <ScrollView
          style={{
            backgroundColor: '#FFFFFF',
            borderRadius: 8,
            flexDirection: 'row',
           marginTop: verticalScale(-DeviceWidth/7),
 
            padding: verticalScale(10),
          }}>
          {ProductType.length === 0 ? (
            <View style={{marginTop: verticalScale(-DeviceWidth/7) }}>
              <Empty_Card Text="No Inventory Yet." />
            </View>
          ) : (
            <FlatList
              onEndReachedThreshold={0.7}
              data={ProductType}
              enableEmptySections={true}
              keyExtractor={(item, index) => index.toString()}
              numColumns={3}
              renderItem={({ item, index }) => eachProductType(item)}
            />
          )}
        </ScrollView>
      </View>
    );
}
function mapStateToProps(state) {
  return {
    product_name: state.productdetail.product_name,
    loder: state.loder,
  };
}

export default connect(
  mapStateToProps,
  { getProductNames, searchProductByType },
)(TotalProductsCategory);
