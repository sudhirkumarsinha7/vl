import React,{useEffect} from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  StatusBar,
  ImageBackground,
  Image,
  FlatList,
  ScrollView,
  Dimensions,
  BackHandler,
  KeyboardAvoidingView,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';

import { connect } from 'react-redux';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { backgroundColor,DeviceWidth } from '../../Style';
import { InventoryProductList } from '../../components/Common/InventoryHelper';

import {
    getOrderDetails,
    searchProductByType,
    searchProductByName,
  } from '../../Redux/Action/order';
  import {
    getProductNames,
    getViewShipment,
    getchainOfCustody,
  
  } from '../../Redux/Action/shipment';
  import {

    getProductListCounts,

  } from '../../Redux/Action/inventory'
import { useTranslation } from 'react-i18next';
import { compose } from 'redux';
import {ToastShow} from '../../components/Toast'
const ProductListCount =(props)=> {
    const { t, i18n } = useTranslation();

    useEffect(() => {
        async function fetchData() {
            getData();
        }
        fetchData();
      }, []);
 
  const getData=() =>{
    props.getProductListCounts();
  }
 

  const getProductList = item => {
    return <InventoryProductList navigation={props.navigation} item={item} />;
  };
  
    return (
      <View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={'Product List'}
          navigation={props.navigation}
        />

        <View
          style={{
            marginTop: verticalScale(-DeviceWidth/7),
            alignItems: 'center',
            backgroundColor: 'white',
            borderRadius:10,
            padding: 10
          }}>
          
       
        {props.loder ? (
          <View
            style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
            <ActivityIndicator color="#0000ff" />
          </View>
        ) : null}
        {/* <ScrollView
          style={{
            flex: 1,
            marginLeft: 10,
            marginRight: 10,
            marginTop: 20,
          }}> */}
          <FlatList
            onEndReachedThreshold={0.7}
            data={props.productCountList}
            enableEmptySections={true}
            keyExtractor={(item, index) => index.toString()}
            numColumns={3}
            renderItem={({ item, index }) =>
              getProductList(item)
            }
          />
        {/* </ScrollView> */}
        </View>
      </View>
    );
  
}

function mapStateToProps(state) {
  return {
    inventorydata: state.inventory.inventorydata.reverse(),
    inventorycounts: state.inventory.inventorycounts,
    productCountList: state.inventory.productCountList,
    loder: state.loder,

  };
}

export default connect(
  mapStateToProps,
  {
    getchainOfCustody,
    getViewShipment,
    searchProductByType,
    searchProductByName,
    getOrderDetails,
    getProductListCounts,
  },
)(ProductListCount);
