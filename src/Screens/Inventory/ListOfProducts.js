import React, { useEffect,useState } from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import {
  getProductListCounts,
  getAllEventsWithFilter,
  getBatchWarehouseLatest
} from '../../Redux/Action/inventory'
import {
  getInventoryAnalytics
} from '../../Redux/Action/analytics';
import _ from 'lodash';
import Empty_Card from '../../components/Empty_Card';
import { Colors, DeviceWidth } from '../../Style';

import {
  InventoryProductList1,
  ProductListNew,
} from '../../components/Common/InventoryHelper';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';

import { useTranslation } from 'react-i18next';
const ProductListByCategory =(props)=> {
  const { t, i18n } = useTranslation();
  const [selectedProduct,setSelectedPRoduct]=useState(props.route.params.product)
  const isFocused = useIsFocused();

  useEffect(() => {
    async function fetchData() {
      let { user, activeWareHouse = 0 } = props;
      let { warehouses = [] } = user;
      let curentWarehouse = warehouses[activeWareHouse]
      props.getBatchWarehouseLatest(
        selectedProduct.productId,
      );
    }
    fetchData();
  }, [isFocused]);

  const GetData = async () => {
    await props.getAllEventsWithFilter(0, 50, props.filterInventory);

    props.getProductListCounts();
    props.getInventoryAnalytics();
  };
  const eachProductListByType = item => {
 
    return (
      <ProductListNew item={item} navigation={props.navigation} ShowMore={false} t={t} selectedProduct={selectedProduct} getlatestRecord={GetData} />
    );
  };

  const navigateProductList=async(item)=> {
    let { user, activeWareHouse = 0 } = props;
    let { warehouses = [] } = user;
    let curentWarehouse = warehouses[activeWareHouse]
    props.getBatchWarehouseLatest(
      // curentWarehouse.warehouseInventory,
      item.productId,
    );
    props.navigation.navigate('ListOfProducts', {
      product: item,
    });
    setSelectedPRoduct(item)
  }
  const product=()=> {
    let {productCountList } = props;

    let newProductList = [];
    newProductList = productCountList.filter(item => item.productName !== selectedProduct.productName);
    newProductList.unshift(selectedProduct);
    return (
      <FlatList
        onEndReachedThreshold={0.7}
        data={newProductList}
        keyExtractor={(_item, index) => index.toString()}
        horizontal={true}
        renderItem={({ item, index }) => (
          <InventoryProductList1 nextScreen1={navigateProductList} item={item} navigation={props.navigation} t={t} selectedProduct={selectedProduct.productName} />
        )}
      />
    );

  }



    let { productCountList, productsByInv = [] } = props;

    console.log('productsByInv  ' + JSON.stringify(productsByInv));
 
    return (
      <View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('Product List')}
          navigation={props.navigation}
        />

        <View style={{ marginTop: verticalScale(-DeviceWidth/7), backgroundColor: 'white', margin: 10, borderRadius: 7 }}>

        </View>
        <ScrollView>

          <View
            style={{
              backgroundColor: '#FFFFFF',
              borderRadius: 8,
              padding: verticalScale(10),
            }}>
            {product()}
          </View>
          {props.loder ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}>
              <ActivityIndicator color="#0000ff" />
            </View>
          ) : null}
          {productsByInv.length === 0 ? (
            <View style={{ marginTop: verticalScale(0) }}>
              <Empty_Card Text="No Inventory Yet." />
            </View>
          ) : (
            <FlatList
              onEndReachedThreshold={0.7}
              data={productsByInv}
              keyExtractor={(item, index) => index.toString()}
              enableEmptySections={true}
              renderItem={({ item, index }) => eachProductListByType(item)}
            />
          )}
        </ScrollView>
      </View>
    );
}
function mapStateToProps(state) {
  return {
    inventorydata: state.inventory.inventorydata,
    productCountList: state.inventory.productCountList,
    productsByType: state.productdetail.productsByType,
    productsByName: state.productdetail.productsByName,
    productsByInv: state.productdetail.productsByInv,
    loder: state.loder,
    user: state.userinfo.user,
    activeWareHouse: state.userinfo.activeWareHouse,
    filterInventory: state.inventory.filterInventory,
  };
}

export default  connect(
  mapStateToProps,
  {
    getProductListCounts,
  getAllEventsWithFilter,
  getBatchWarehouseLatest,
  getInventoryAnalytics

  },
)(ProductListByCategory);
