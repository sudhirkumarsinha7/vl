
import React, { useState, useEffect } from 'react';
import {
  LayoutAnimation,
  UIManager,
  Platform,
  Button,
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  userinfo,
  getOrderDetails,
  searchProductByType,
  searchProductByName,
} from '../../Redux/Action/order';
import {
  getInventoryAnalytics
} from '../../Redux/Action/analytics';
import {
  getProductNames,
  getViewShipment,
  getchainOfCustody,

} from '../../Redux/Action/shipment';
import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
} from '../../Redux/Action/inventory'
import Header from '../../components/Header';
import Card from '../../components/Card';
import Empty_Card from '../../components/Empty_Card';
import _ from 'lodash';
import { compareValues, NetworkUtils } from '../../Util/utils';
import { Colors, DeviceWidth } from '../../Style';
import { CustomButton1 } from '../../components/Common/common';
import { useTranslation } from 'react-i18next';
import { queryAllTodoLists, queryAllInventryData, queryDeleteAllInventryData } from '../../databases/allSchemas'
import { ToastShow } from '../../components/Toast';
import Search from '../../components/Search'
import { InventoryList, InventoryProductList } from '../../components/Common/InventoryHelper'
import { NavigationContainer, useIsFocused } from '@react-navigation/native';

const InventoryScreen = (props) => {
  const { t, i18n } = useTranslation();
  const isFocused = useIsFocused();

  useEffect(() => {
    async function fetchData() {
      GetData();
    }
    fetchData();
  }, [isFocused]);


  const GetData = async () => {
    const isConnected = await NetworkUtils.isNetworkAvailable()

    if (isConnected) {
      await syncInventoryData();
      await props.getProductListCounts();
      await props.getAllEventsWithFilter(0, 50, props.filterInventory);
      await props.getInventoryAnalytics();
      await props.getProductNames();
    }


  };
  const syncInventoryData = async () => {
    queryAllInventryData().then((todoLists) => {
      let data = todoLists;
      console.log('syncInventoryData 334 ' + JSON.stringify(data));
      if (data.length) {
        for (var i = 0; i < data.length; i++) {
          let productData = data[i].products;

          props.add_inventory(productData)
        }
        queryDeleteAllInventryData()
      }


    }).catch((error) => {
      console.log('syncInventoryData error' + JSON.stringify(error));

    });
  }
  const InventoryAnalyticsCard = () => {
    return <View><ScrollView
      horizontal={true}
      contentContainerStyle={{
        margin: verticalScale(10),
      }}
      showsHorizontalScrollIndicator={true}>
      <Card
        image={require('../../assets/Total_Product_Category.png')}
        bgcolor={Colors.blue96}
        textcolor={Colors.blueAE}
        nextScreen={navigateTotalProduct}
        isDisable={false}
        quantity={inventoryAnalytics.totalProductCategory || 0}
        card_name={t("total_product_category")}
      />
      <View>
        <Text>{'  '}</Text>
      </View>
      <Card
        image={require('../../assets/Total_Vaccine_near_Expiration.png')}
        nextScreen={navigateNearExpiredProduct}
        isDisable={false}
        bgcolor={Colors.orange1D}
        textcolor={Colors.red23}
        quantity={inventoryAnalytics.batchNearExpiration || 0}
        card_name={t("batch_near_expiration")}
      />
      <View>
        <Text>{'  '}</Text>
      </View>

      <Card
        image={require('../../assets/Products_out_of_stock.png')}
        bgcolor="#89D1F0"
        textcolor="#0093E9"
        quantity={inventoryAnalytics.stockOut || 0}
        nextScreen={navigateOutOfStockProduct}
        isDisable={false}
        card_name={t("product_out_of_stock")}
      />
      <View>
        <Text>{'  '}</Text>
      </View>
      <Card
        image={require('../../assets/Total_Vaccine_Expired.png')}
        bgcolor="#0A69C5"
        textcolor="#0159EA"
        nextScreen={navigateExpiredProduct}
        isDisable={false}
        quantity={inventoryAnalytics.batchExpired || 0}
        card_name={t("batch_expired")}
      />
      <View style={{ marginRight: 20 }}>
        <Text>{'  '}</Text>
      </View>
    </ScrollView></View>
  }

  const navigateTotalProduct = async () => {
    props.navigation.navigate('TotalProductsCategory');
  }
  const navigateExpiredProduct = async () => {
    props.navigation.navigate('ExpiredProduct');
  }
  const navigateOutOfStockProduct = async () => {
    await props.getInventory(0, 1000)
    props.navigation.navigate('OutOfStockProduct');
  }
  const navigateNearExpiredProduct = async () => {
    props.navigation.navigate('NearExpiredProduct');
  }
  const ActionButton = () => {
    return <View
      style={{
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: verticalScale(5),
      }}>
      {inventory.addInventory ?
        <CustomButton1
          backgroundColor={'#FA7923'}
          imgWidth={scale(13.14)}
          imgHight={scale(15)}
          label1={'+ ' + t('add_inventory')}
          navigation={props.navigation}
          Press={() => props.navigation.navigate('AddInventory')}
        /> : null}
    </View>
  }
  const FilterDesign = () => {
    return (<View
      style={{
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: verticalScale(10),
        marginRight: scale(10)
      }}>
      <Text
        style={{
          fontSize: scale(16),
          color: '#0093E9',
          fontWeight: 'bold',
        }}>
        {t('Recent Transactions')}
      </Text>
      <TouchableOpacity
        style={{
          borderRadius: 10,
          padding: scale(7),
          paddingLeft: scale(10),
          paddingRight: scale(10),
          backgroundColor: Colors.blueCF,
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}
        onPress={() => props.navigation.navigate('RecentInventory')}>
        <Text
          style={{
            fontSize: scale(16),
            color: Colors.blueE9,
          }}>
          {t('view_all')}
        </Text>
      </TouchableOpacity>
    </View>)
  }
  const eachInventoryData = (item, index) => {
    return (
      <InventoryList
        item={item}
        index={index}
        ViewShipment={() =>
          getDetails(
            item.payloadData && item.payloadData.data,
            item.payloadData &&
            item.payloadData.data &&
            item.payloadData.data.id,
          )
        }
        shipmentPersmission={shipment}
        t={t}
      />
    );
  };
  const getDetails = async (item, id) => {
    await props.getchainOfCustody(id);
    await props.getViewShipment(id);
    props.navigation.navigate('Shipment', { screen: 'ViewShipment' });
  };

  const ProductCountListView = () => {
    return <View>
      <View
        style={{
          backgroundColor: 'transparent',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          margin: 15,
        }}>
        <Text
          style={{
            fontSize: scale(16),
            color: Colors.blueE9,
            fontWeight: 'bold',
          }}>
          {t('product_list')}
        </Text>
        {productCountList.length ? <TouchableOpacity
          onPress={
            () => props.navigation.navigate('ProductListCount')
          }>
          <Text
            style={{
              fontSize: scale(10),
              color: '#707070',
            }}>
            {t('show_more')}
          </Text>
        </TouchableOpacity> : null}
      </View>

      <View
        style={{
          flexDirection: 'row',
          marginLeft: 10,
          marginRight: 10,
        }}>
        <FlatList
          onEndReachedThreshold={0.7}
          data={productCountList}
          enableEmptySections={true}
          keyExtractor={(item, index) => index.toString()}
          horizontal={true}
          renderItem={({ item, index }) =>
            getProductList(item)
          }
        />
      </View>
    </View>
  }
  const getProductList = item => {
    return (
      <InventoryProductList item={item} navigation={props.navigation} />
    );
  };
  let { inventoryAnalytics = {}, productCountList = [], inventorydata = [], userpermission = {} } = props;
  let { inventory = {}, shipment = {} } = userpermission;
  inventorydata = inventorydata.sort(compareValues('createdAt', 'desc'));
  const InventoryScroll = (item) => {
    return <View>
      {inventory.inventoryAnalytics ? InventoryAnalyticsCard() : null}
      {ActionButton()}
      {ProductCountListView()}
      {FilterDesign()}

      {inventorydata.length === 0 ? (
        <View style={{ marginTop: verticalScale(0) }}>
          <Empty_Card Text="No Inventory Yet." />
        </View>
      ) : (
        inventory.viewInventory ? (<View>
          <FlatList
            onEndReachedThreshold={0.7}
            data={inventorydata}
            enableEmptySections={true}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) =>
              eachInventoryData(item, index)
            }
          />
          <View style={{ height: scale(10) }} />
        </View>) : (
          <View style={{ marginTop: verticalScale(0) }}>
            <Empty_Card Text="ViewInventory Permission Denied" />
          </View>
        )
      )}
    </View>
  }
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#0093E9" />
      <Header navigation={props.navigation}
        name={t('inventory')}
      />
      <Search
        navigation={props.navigation}
        fromScreen={'Inventory'}
      />

      <FlatList
        onEndReachedThreshold={0.7}
        style={{ width: '100%' }}
        keyExtractor={(item, index) => index.toString()}
        data={['test']}
        renderItem={({ item }) => InventoryScroll(item)}
        refreshControl={
          <RefreshControl
            refreshing={false}
            onRefresh={()=>GetData()}
            tintColor={Colors.blueE9}
            titleColor={Colors.blueE9}
            colors={['#a076e8', '#5dc4dd']}
            progressBackgroundColor={Colors.blueAE}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
function mapStateToProps(state) {
  return {
    inventorydata: state.inventory.inventorydata,
    inventorycounts: state.inventory.inventorycounts,
    inventoryAnalytics: state.inventory.inventoryAnalytics,
    productCountList: state.inventory.productCountList,
    filterInventory: state.inventory.filterInventory,
    product_name: state.productdetail.product_name,
    loder: state.loder,
    userpermission: state.userinfo.userpermission,
  };
}
export default connect(mapStateToProps, {
  userinfo,
  getInventoryAnalytics,
  getProductNames,
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
  getchainOfCustody,
  getViewShipment,
  searchProductByType,
  searchProductByName,
  getOrderDetails,
},
)(InventoryScreen);