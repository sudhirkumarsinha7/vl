
import React, { useState, useEffect } from 'react';
import {
 
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
  Image,
  Keyboard
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  userinfo,
  getOrderDetails,
  searchProductByType,
  searchProductByName,
} from '../../Redux/Action/order';
import {
  getInventoryAnalytics
} from '../../Redux/Action/analytics';
import {
  getProductNames,
  getchainOfCustody,
  getViewShipment
} from '../../Redux/Action/shipment';
import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
} from '../../Redux/Action/inventory'
import { HeaderWithBackBlue } from '../../components/Header';
import _ from 'lodash';
import { Colors, DeviceWidth } from '../../Style';
import { CustomDropdown, CustomInputText, MonthYearPickerView } from '../../components/Common/common';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../components/Toast';
import Search from '../../components/Search'
import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';

const AddNewProduct = () => {
  const add = {
    productId: '',
    manufacturer: '',
    productName: '',
    quantity: '',
    isMoreData: false,
    mfgDate: '',
    expDate: '',
    type: '',
    batchNumber: '',
    serialNumbersRange: '',
    unitofMeasure: {
      id: '',
      name: '',
    }
  }
  return add
}
const AddInventory = (props) => {
  const { t, i18n } = useTranslation();
  const [mandatory, setIsmantory] = useState(false)
  const [ProductType, setProductType] = useState([])
  const [ProductNameList, setProductNameList] = useState([])
  useEffect(() => {
    async function fetchData() {
      await props.getProductNames();
      let { product_name } = props;
      let getProductNames = _.uniqBy(product_name, function (x) {
        return x.type;
      });
      setProductType(getProductNames)
    }
    fetchData();
  }, []);


  var validationSchema = Yup.object().shape(
    {
      products: Yup.array()
        .of(
          Yup.object().shape({
            manufacturer: Yup.string().required(t('Required')),
            productName: Yup.string().required(t('Required')),
            quantity: Yup.string().required(t('Required')),
            batchNumber: Yup.string().required(t('Required')),
            type: Yup.string().required(t('Required')),
          })
        ).required(t('Required'))
    },
    [],
  ); // <-- HERE!!!!!!!!

  const InventoryForm = useFormik({
    initialValues: {
      products: [
        {
          productId: '',
          manufacturer: '',
          productName: '',
          quantity: '',
          isMoreData: false,
          mfgDate: '',
          expDate: '',
          type: '',
          batchNumber: '',
          serialNumbersRange: '',
          unitofMeasure: {
            id: '',
            name: '',
          }
        }
      ]
    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _Review = () => {
    setIsmantory(true);
    Keyboard.dismiss();
    InventoryForm.handleSubmit();
  };
  const AddAnotherProduct = () => {
    const data = [...InventoryForm.values.products]
    const newData = AddNewProduct()
    const finalData = [...data, newData]
    let allProduct = {
      products: finalData
    }

    InventoryForm.setValues(allProduct)
  }
  const cancel = index => {
    const productClone = JSON.parse(JSON.stringify(InventoryForm?.values?.products));
    // console.log(Object.keys(productClone));
    productClone.splice(index, 1);
    let allProduct = {
      products: productClone
    }
    InventoryForm.setValues(allProduct)
  };
  const DeleteIcon = index => {
    return <View
      style={{
        flexDirection: 'row',
        marginLeft: verticalScale(5),
        alignItems: 'center',

      }}>
      <Text style={{ width: '80%', fontWeight: 'bold' }}>
        {t('Product') + ' ' + (index + 1)}
      </Text>
      <View
        style={{
          width: scale(35),
          height: scale(35),
          borderRadius: 400,
          backgroundColor: Colors.whiteF2,
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 10,
          marginTop: 5,
        }}
        opacity={5}>
        <TouchableOpacity
          style={{
            justifyContent: 'flex-end',
          }}
          onPress={() => cancel(index)}
          disabled={InventoryForm?.values?.products.length === 1 ? true : false}>

          {InventoryForm?.values?.products.length === 1 ? (
            <Image
              style={{ width: scale(13.5), height: scale(35) }}
              source={require('../../assets/deletegrey.png')}
              resizeMode="contain"
            />
          ) : (
            <Image
              style={{ width: scale(13.5), height: scale(35) }}
              source={require('../../assets/deleteblue.png')}
              resizeMode="contain"
            />
          )}

        </TouchableOpacity>
      </View>
    </View>
  };
  const updateMoreValue = index => {
    const productClone = JSON.parse(JSON.stringify(InventoryForm?.values?.products));
    productClone[index].isMoreData = true;
    let allProduct = {
      products: productClone
    }
    InventoryForm.setValues(allProduct)
  };
  const  handleSubmit = async values =>{
    console.log('handleSubmit '+JSON.stringify(values))
    props.navigation.navigate('ReviewInventory', {
      values:values,
    });
  };
  const ProductListComponent = () => {
    return (<View>
      {InventoryForm?.values?.products.map((each, index) => {
        return <View style={{ backgroundColor: Colors.whiteFF, margin: 15, borderRadius: 5 }}>
          {DeleteIcon(index)}
          <CustomDropdown
            displayName={'Product type'}
            stateName={InventoryForm?.values?.products[index]?.type}
            dropDowndata={ProductType}
            onChangeValue={(val) => onChangeProductType(val, index)}
            label={'type'}
            mapKey={'type'}
            mandatory={mandatory}
          />
          <CustomDropdown
            displayName={'Product name'}
            stateName={InventoryForm?.values?.products[index]?.productId}
            dropDowndata={ProductNameList}
            onChangeValue={(val) => onChangeProductName(val, index)}
            label={'name'}
            mapKey={'id'}
            mandatory={mandatory}
          />
          {/* <View style={{ marginTop: scale(15) }} /> */}
          <CustomInputText
            label={"Manufacturer"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={InventoryForm.handleChange(`products[${index}].manufacturer`)}
            val={InventoryForm?.values?.products[index]?.manufacturer}
            errorMsg={InventoryForm.errors && InventoryForm.errors.products && InventoryForm.errors.products[index] && InventoryForm.errors.products[index].manufacturer}
            disabled={true}
            mandatory={mandatory}

          />

          <CustomInputText
            // label={"Quantity"}
            label={each.unitofMeasure && each.unitofMeasure.name? "Quantity (" +  each.unitofMeasure.name + ')':'Quantity'}

            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={InventoryForm.handleChange(`products[${index}].quantity`)}
            val={InventoryForm?.values?.products[index]?.quantity}
            keyboardType={"numeric"}
            errorMsg={InventoryForm.errors && InventoryForm.errors.products && InventoryForm.errors.products[index] && InventoryForm.errors.products[index].quantity}
            mandatory={mandatory}
          />
          <CustomInputText
            label={"Batch number"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={InventoryForm.handleChange(`products[${index}].batchNumber`)}
            val={InventoryForm?.values?.products[index]?.batchNumber}
            errorMsg={InventoryForm.errors && InventoryForm.errors.products && InventoryForm.errors.products[index] && InventoryForm.errors.products[index].batchNumber}
            mandatory={mandatory}

          />


          {each.isMoreData ? (
            <View>
              <CustomInputText
                label={"Serial number"}
                labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                onChange={(val) => onChangeSerialNo(val, index)}
                val={InventoryForm?.values?.products[index]?.serialNumbersRange}
                mandatory={mandatory}
              />
              <View style={{flexDirection:'row',margin:scale(10),marginTop:scale(10)}}>
                  <View style={{flex:0.45}}>
                  <MonthYearPickerView 
                   label={"Mfg. Date"}
                   labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                   onChange={(val) => onChangeMfg(val, index)}
                   val={InventoryForm?.values?.products[index]?.mfgDate}
                   mandatory={mandatory}
                   isMfgDate={true}
                  />
                  </View>
                  <View style={{flex:0.1}}/>
                  <View style={{flex:0.45}}>
                  <MonthYearPickerView 
                   label={"Exp. Date"}
                   labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                   onChange={(val) => onChangeExp(val, index)}
                   val={InventoryForm?.values?.products[index]?.expDate}
                   mandatory={mandatory}
                   minDate={InventoryForm?.values?.products[index]?.mfgDate}
                   isMfgDate={false}
                  />

                  </View>
                   
                </View>
            </View>
          ) : (
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                marginBottom: verticalScale(10),
                borderRadius: 10,
                marginLeft: scale(10),
              }}
              onPress={() => updateMoreValue(index)}>
              <View style={{
                backgroundColor: '#FFAB1D',
                padding: 5,
                marginTop: 10,
                borderRadius: 5,
                paddingLeft: scale(10),
                paddingRight: scale(10)
              }}>
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                    elevation: 5,
                  }}>
                  {t('+ Add More Details')}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        </View>

      })}
    </View>)
  }
  const onChangeProductType = (item, i) => {
    InventoryForm.handleChange({ target: { name: `products[${i}].type`, value: item?.value } })
    InventoryForm.handleChange({ target: { name: `products[${i}].productId`, value: '' } })
    InventoryForm.handleChange({ target: { name: `products[${i}].productName`, value: '' } })
    InventoryForm.handleChange({ target: { name: `products[${i}].manufacturer`, value: '' } })
    const getProducts = props.product_name.filter(each => {
      if (each.type == item?.value) {
        return each
      }
    })
    setProductNameList(getProducts)
  };
  const onChangeProductName = (item, i) => {
    InventoryForm.handleChange({ target: { name: `products[${i}].productId`, value: item?.value } })
    InventoryForm.handleChange({ target: { name: `products[${i}].productName`, value: item.label } })
    InventoryForm.handleChange({ target: { name: `products[${i}].manufacturer`, value: item.eachItem.manufacturer } })
    InventoryForm.handleChange({ target: { name: `products[${i}].manufacturer`, value: item.eachItem.manufacturer } })
    InventoryForm.handleChange({
      target: {
        name: `products[${i}].unitofMeasure`, value: item.eachItem.unitofMeasure
      }
    })
  };
  const onChangeSerialNo = (val, i) => {
    if (!/[^a-zA-Z0-9-]/.test(val)) {
      InventoryForm.handleChange({ target: { name: `products[${i}].serialNumbersRange`, value: val } })
    } else {

      ToastShow(
        t('Special charactets not allowed.'),
         'error',
         'long',
         'top',
       )
    }
  }
  const onChangeMfg= (val, i) => {
    InventoryForm.handleChange({ target: { name: `products[${i}].mfgDate`, value: val } })
  }
  const onChangeExp= (val, i) => {
    InventoryForm.handleChange({ target: { name: `products[${i}].expDate`, value: val } })
  }
  return (
    <View style={styles.container}>
      <HeaderWithBackBlue
        name={t('add_inventory')}
        navigation={props.navigation}
      />
      <View style={{ marginTop: verticalScale(-DeviceWidth / 7) }}>

        <KeyboardAwareScrollView extraScrollHeight={50} >
          {ProductListComponent()}
          <TouchableOpacity
            style={{
              marginTop: verticalScale(10),
              borderRadius: 10,
              flexDirection: 'row',
              margin: 10,
            }}
            onPress={AddAnotherProduct}>
            <View style={{
              backgroundColor: '#FFFFFF',
              borderRadius: 5,
              marginLeft: scale(5),
              padding: 5,
              elevation: 5,
              paddingLeft: scale(10),
              paddingRight: scale(10)
            }}>
              <Text
                style={{
                  fontSize: scale(12),
                  color: '#0093E9',
                  fontWeight: 'bold',
                }}>
                {t('+ Add Another Product')}
              </Text>
            </View>
          </TouchableOpacity>
          <View
                style={{
                  flexDirection: 'row',
                  margin: verticalScale(15),
                  justifyContent: 'center',
                  alignItems: 'center',
                  flex: 1,
                }}>
                <TouchableOpacity
                  style={{
                    height: scale(40),
                    backgroundColor: '#FFFFFF',
                    borderRadius: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderColor: '#0093E9',
                    borderWidth: 1,
                    flex: 0.5,
                    margin: 10,
                  }}
                  onPress={() => props.navigation.goBack()}>
                  <Text
                    style={{
                      marginLeft: scale(13),
                      fontSize: scale(16),
                      fontWeight: 'bold',
                      color: '#0093E9',
                      alignSelf: 'center'
                    }}>
                    {t('Cancel')}
                  </Text>
                </TouchableOpacity>
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 1 }}
                  colors={['#0093E9', '#36C2CF']}
                  style={{
                    // marginTop: verticalScale(45),
                    // width: scale(90),
                    // height: scale(35),
                    borderRadius: 8,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 0.5,
                  }}>
                  <TouchableOpacity
                    style={{
                      // height: scale(40),
                      // backgroundColor: '#FFAB1D',
                      borderRadius: 10,
                      alignItems: 'center',
                      justifyContent: 'center',
                      margin: 10,


                    }}
                    onPress={() => _Review()}>
                    <Text
                      style={{
                        marginLeft: scale(13),
                        fontSize: scale(16),
                        color: '#FFFFFF',
                        fontWeight: 'bold',
                      }}>
                      {t('Review')}
                    </Text>
                  </TouchableOpacity>
                </LinearGradient>
              </View>

          <View style={{ marginBottom: verticalScale(DeviceWidth / 2) }} />

        </KeyboardAwareScrollView>
      </View>

    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
function mapStateToProps(state) {
  return {
    inventorydata: state.inventory.inventorydata,
    inventorycounts: state.inventory.inventorycounts,
    inventoryAnalytics: state.inventory.inventoryAnalytics,
    productCountList: state.inventory.productCountList,
    filterInventory: state.inventory.filterInventory,
    product_name: state.productdetail.product_name,
    loder: state.loder,
    userpermission: state.userinfo.userpermission,
  };
}
export default connect(mapStateToProps, {
  userinfo,
  getInventoryAnalytics,
  getProductNames,
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
  getchainOfCustody,
  getViewShipment,
  searchProductByType,
  searchProductByName,
  getOrderDetails,
},
)(AddInventory);
