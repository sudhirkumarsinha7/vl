import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
  ActivityIndicator,
} from 'react-native';
import Header, {
  HeaderWithBack,
} from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  CustomDropDownFilter,
} from '../../components/Common/common';
import { FilterData } from '../../components/Common/defaultValue';
import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  filterInverntoryData,
  getAllEventsWithFilter,
} from '../../Redux/Action/inventory'
import {
  getProductNames
} from '../../Redux/Action/shipment';

import _, { set } from 'lodash';

import { FilterByDate, CustomDateRange } from '../../components/Common/Filter';
import { useTranslation } from 'react-i18next';
import { Colors, DeviceWidth } from '../../Style';

export const STATUS = [
  { label: 'SENT', id: 'CREATE' },
  { label: 'RECEIVED', id: 'RECEIVE' },
  { label: 'ADDED', id: 'ADD' },
  { label: 'SOLD', id: 'BUY' },
];
export const STATUS1 = [
  { label: 'ENVIADA', id: 'CREATE' },
  { label: 'RECIBIÓ', id: 'RECEIVE' },
  { label: 'ADICIONAL', id: 'ADD' },
  { label: 'VENDIDA', id: 'BUY' },
];
const FilterInvenorty = (props) => {
  const { t, i18n } = useTranslation();
  const [date, setDate] = useState('')
  const [manufacturer, setManufacturer] = useState('')
  const [dateFilter, setDateFilter] = useState('')
  const [status, setStatus] = useState('')
  const [statusId, setStatusId] = useState('')
  const [productName, setProductName] = useState('')
  const [type, setType] = useState('')
  const [productID, setProductID] = useState('')
  const [fromDate, setFromDate] = useState('')
  const [toDate, setToDate] = useState('')
  useEffect(() => {
    async function fetchData() {
      const { filterInventory = {} } = props;
      let { date = '', manufacturer = '', dateFilter = '', status = '', statusId = '', productName = '', type = '', productID = '', fromDate = '', toDate = '' } = filterInventory;
      setDate(date)
      setManufacturer(manufacturer)
      setDateFilter(dateFilter)
      setStatus(status)
      setStatusId(statusId)
      setProductID(productID)
      setProductName(productName)
      setType(type)
      setFromDate(fromDate)
      setToDate(toDate)
    }
    fetchData();
  }, []);

  const onProdChange = (item) => {
    setProductID(item.value)
    setProductName(item.value)
  }
  const onStatusChange = (item) => {
    setStatus(item.value)
    setStatusId(item.value)
  }

  const onManufactureCahnge = (item) => {
    setManufacturer(item.value)

  }
  const onProdTypeChange = (item) => {
    setType(item.value)
  }
  const onChangeDateRange = (val) => {
    setFromDate(val.startDate)
    setToDate(val.endDate)

  }
  const onChangeDateFilter = (val) => {
    setDateFilter(val)
  }

  const apply = async () => {
    let Filter = {
      type: type || '',
      productName: productName || '',
      productID: productID || '',
      dateFilter: dateFilter || '',
      status: status || '',
      statusId: statusId || '',
      manufacturer: manufacturer || '',
      fromDate: fromDate || '',
      toDate: toDate || '',
    };
    props.filterInverntoryData(Filter);
    await props.getAllEventsWithFilter(0, 50, Filter);
    props.navigation.goBack();
  }
  const clearALL = () => {
    let Filter = {
      date: '',
      manufacturer: '',
      dateFilter: '',
      status: '',
      statusId: '',
      productName: '',
      type: '',
      productID: '',
      fromDate: '',
      toDate: '',
    };
    setDate('')
    setManufacturer('')
    setDateFilter('')
    setStatus('')
    setStatusId('')
    setProductID('')
    setProductName('')
    setType('')
    setFromDate('')
    setToDate('')
    props.filterInverntoryData(Filter);
    props.getAllEventsWithFilter(0, 50, Filter);
  }



  const { userLang } = props;
  let ProductType = _.uniqBy(props.product_name, function (x) {
    return x.type;
  });
  let manufacturerList = _.uniqBy(props.product_name, function (x) {
    return x.manufacturer;
  });
  console.log(
    'filterInventory ' + JSON.stringify(props.filterInventory),
  );
  return (
    <View
      style={{
        flex: 1,
      }}>
      <StatusBar backgroundColor="#0093E9" />
      <HeaderWithBack
        name={t('Filter')}
        navigation={props.navigation}
        isfilter={true}
        onPress={clearALL}
        clearFilter={t('Clear Filter')}
      />

      <ScrollView
        nestedScrollEnabled={true}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{ marginTop: verticalScale(-DeviceWidth / 7) }}>
        <View
          style={{
            margin: verticalScale(10),
            borderRadius: 10,
            padding: 5,
          }}>

          <CustomDropDownFilter
            displayName={'Product Name'}
            stateName={productID}
            image={require('../../assets/from_filter.png')}
            dropDowndata={props.product_name}
            label={'name'}
            mapKey={'id'}
            onChangeValue={onProdChange}

          />



          <CustomDropDownFilter
            displayName={'Product Type'}
            stateName={type}
            image={require('../../assets/Total_Product_Category.png')}
            dropDowndata={ProductType}
            label={'type'}
            mapKey={'type'}
            onChangeValue={onProdTypeChange}

          />
          <CustomDropDownFilter
            displayName={'manufacturer'}
            stateName={manufacturer}
            image={require('../../assets/user.png')}
            dropDowndata={manufacturerList}
            label={'manufacturer'}
            mapKey={'manufacturer'}
            onChangeValue={onManufactureCahnge}

          />
          <CustomDropDownFilter
            displayName={'Status'}
            stateName={status}
            image={require('../../assets/status.png')}
            dropDowndata={userLang === 'es' ? STATUS1 : STATUS}
            label={'label'}
            mapKey={'id'}
            onChangeValue={onStatusChange}
            search={false}

          />

          <CustomDateRange
            displayName={'Order Date'}
            fromDate={fromDate}
            toDate={toDate}
            onChangeValue={onChangeDateRange}
            image={require('../../assets/calendar.png')}

          />
          <FilterByDate dateFilter={dateFilter} onChangeValue={onChangeDateFilter} />
        </View>

        {props.loder ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
            }}>
            <ActivityIndicator color="#0000ff" />
          </View>
        ) : null}
      </ScrollView>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignContent: 'center',
          marginBottom: 20,
          width: scale(328),
          bottom: 0,
        }}>
        <TouchableOpacity
          style={{
            padding: 8,
            borderRadius: 10,
            borderWidth: 1,
            borderColor: '#58b1da',
            height: scale(45),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            width: '30%',
            marginRight: 10,
          }}
          onPress={() => props.navigation.goBack()}>
          <Text
            style={{
              color: '#58b1da',
              fontSize: 16,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {'  '}
            {t('Cancel')}{' '}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            height: scale(45),
            borderRadius: 10,
            backgroundColor: Colors.blueE9,
            flexDirection: 'row',
            padding: 8,
            justifyContent: 'space-evenly',
            width: '50%',
          }}
          onPress={() => apply()}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{ width: scale(12.35), height: scale(12.68) }}
              source={require('../../assets/Filter.png')}
            />
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: scale(13),
                color: '#FFFFFF',
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              {t('Apply Filter')}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );

}

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    filterInventory: state.inventory.filterInventory,
    loder: state.loder,
    product_name: state.productdetail.product_name,
    userLang: state.userinfo.userLang,
  };
}

export default connect(
  mapStateToProps,
  {
    filterInverntoryData,
    getAllEventsWithFilter,
  },
)(FilterInvenorty);
