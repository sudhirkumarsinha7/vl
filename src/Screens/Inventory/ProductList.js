import React, { useEffect } from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
} from '../../Redux/Action/inventory'
import {
  getProductNames
} from '../../Redux/Action/shipment';
import _ from 'lodash';
import { backgroundColor } from '../../Style';
import Empty_Card from '../../components/Empty_Card';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { compose } from 'redux';
import {
  InventoryList,
  InventoryListOld,
  InventoryProductList,
  InventoryProducList,
  InventoryProductListNew,
} from '../../components/Common/InventoryHelper';
import { Colors, DeviceWidth } from '../../Style';

const ProductList =(props)=> {
  const { t, i18n } = useTranslation();
  const selectedProductType = props.route.params.selectedProductType

  

 
  const eachProductListByType = item => {
    return (
      <InventoryProductListNew item={item} navigation={props.navigation} ShowMore={false} />
    );
  };
  const product=(item) =>{
    return (
      <View
        style={{
          width: scale(99),
          height: scale(110),
          justifyContent: 'center',
          alignItems: 'center',
          margin: verticalScale(5),
          borderRadius: 8,
          backgroundColor:
            backgroundColor[Math.floor(Math.random() * backgroundColor.length)],
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{ width: scale(40), height: scale(40) }}
            source={require('../../assets/product.png')}
            resizeMode="contain"
          />
          <Text
            style={{
              fontSize: scale(11),
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {item}
          </Text>
        </View>
      </View>
    );
  }

    
    let { productsByInv = [] } = props;
    
    if (props.loder) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <ActivityIndicator color="#0000ff" />
        </View>
      );
    }
    return (
      <View
        style={{
          flex: 1,
          // alignItems: 'center',
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('product_list')}
          navigation={props.navigation}
        />
        <ScrollView style={{
          marginTop: verticalScale(-41),
        }}>
          {selectedProductType ? <View
            style={{
              backgroundColor: '#FFFFFF',
              borderRadius: 8,
              padding: verticalScale(10),
            }}>
            {product(selectedProductType)}
          </View> : null}
          {productsByInv.length === 0 ? (
            <View style={{ marginTop: verticalScale(0) }}>
              <Empty_Card Text="No Inventory Yet." />
            </View>
          ) : (
            <FlatList
              onEndReachedThreshold={0.7}
              data={productsByInv}
              keyExtractor={(item, index) => index.toString()}
              enableEmptySections={true}
              renderItem={({ item, index }) => eachProductListByType(item)}
            />
          )}
        </ScrollView>
      </View>
    );
}
function mapStateToProps(state) {
  return {
    product_name: state.productdetail.product_name,
    inventorydata: state.inventory.inventorydata,
    productCountList: state.inventory.productCountList,
    productsByInv: state.productdetail.productsByInv,
    loder: state.loder,
  };
}

export default connect(
  mapStateToProps,
  { getProductNames, getProductListCounts },
)(ProductList);
