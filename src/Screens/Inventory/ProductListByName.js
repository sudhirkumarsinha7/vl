import React, { Component } from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';

import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
  getBatchWarehouse,
} from '../../Redux/Action/inventory'
import {
  getProductNames
} from '../../Redux/Action/shipment';

import _ from 'lodash';
import Empty_Card from '../../components/Empty_Card';
import { InventoryProductListNew } from '../../components/Common/InventoryHelper';
import { useTranslation } from 'react-i18next';

const ProductListByName =(props)=> {
  const { t, i18n } = useTranslation();


  const nextScreen=(item)=>{
    console.log('getBatchWarehouse nextScreen ' + JSON.stringify(item));

    props.getBatchWarehouse(
      item.id,
      item.inventoryDetails && item.inventoryDetails.productId,
    );
    props.navigation.navigate('ProductList', {
      selectedProductType: item.inventoryDetails.productName,
    });
  }
  const eachProductListByType = item => {
    return (
      <InventoryProductListNew
        item={item}
        onChangeScreen={nextScreen}
        ShowMore={true}
      />
    );
  };


    let { productsByName = [], } = props;

    console.log('productsByName ' + JSON.stringify(productsByName));
    if (props.loder) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <ActivityIndicator color="#0000ff" />
        </View>
      );
    }

    return (
      <View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('Product List')}
          navigation={props.navigation}
        />
        <View
          style={{
            marginTop: verticalScale(-41),
            padding: verticalScale(10),
          }}>
          {productsByName.length === 0 ? (
            <View style={{ marginTop: verticalScale(-30) }}>
              <Empty_Card Text="No Inventory Yet." />
            </View>
          ) : (
            <FlatList
              onEndReachedThreshold={0.7}
              data={productsByName}
              keyExtractor={(item, index) => index.toString()}
              enableEmptySections={true}
              renderItem={({ item, index }) => eachProductListByType(item)}
            />
          )}
          <View style={{ height: scale(185) }} />
        </View>
      </View>
    );
}
function mapStateToProps(state) {
  return {
    productsByName: state.productdetail.productsByName,
    loder: state.loder,
  };
}

export default connect(
  mapStateToProps,
  { getProductNames, getProductListCounts, getBatchWarehouse },
)(ProductListByName);
