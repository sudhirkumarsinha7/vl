import React, { useEffect,useState } from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';

import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchWarehouse,
  searchProductByType,
  getAllEventsWithFilter,
} from '../../Redux/Action/inventory'
import _ from 'lodash';
import Empty_Card from '../../components/Empty_Card';
import {
  InventoryList,
  InventoryProductType,
  InventoryProductList,
  InventoryProducList,
  InventoryProductListNew,
} from '../../components/Common/InventoryHelper';
import { useTranslation } from 'react-i18next';
const ProductListByCategory =(props)=> {
  const { t, i18n } = useTranslation();

  const [selectedProductType,setSelectedProductType] = useState(props.route.params.selectedProductType)
  useEffect(() => {
    async function fetchData() {
      await  props.getProductName()
    }
    fetchData();
  }, []);




  const eachProductListByType = item => {

    return (
      <InventoryProductListNew
        item={item}
        onChangeScreen={nextScreen}
        ShowMore={true}
      />
    );
  };
  const nextScreen = async(item)=>{
    await props.getBatchWarehouse(
      item.id,
      item.inventoryDetails && item.inventoryDetails.productId,
    );
    props.navigation.navigate('ProductList', {
      selectedProductType: selectedProductType,
    });
  }
  const  navigateProductListByCategory =async(item)=> {

    props.searchProductByType(item.type);
    props.navigation.navigate('ProductListByCategory', {
      selectedProductType: item.type,
    });
    setSelectedProductType(item.type)
  }
  const product=() =>{
    let newObj = {
      type: selectedProductType,
    };
    let ProductType = _.uniqBy(props.product_name, function (x) {
      return x.type;
    });
    ProductType = ProductType.filter(item => item.type !== selectedProductType);
    ProductType.unshift(newObj);
    return (
      <FlatList
        onEndReachedThreshold={0.7}
        data={ProductType}
        keyExtractor={(_item, index) => index.toString()}
        horizontal={true}
        renderItem={({ item, index }) => (
          <InventoryProductType
            item={item}
            selectedProductType={selectedProductType}
            nextScreen1={navigateProductListByCategory}
          />
        )}
      />
    );

  }

   
    let { productCountList, productsByType = [] } = props;

    if (props.loder) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <ActivityIndicator color="#0000ff" />
        </View>
      );
    }
    return (
      <View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('Product Category')}
          navigation={props.navigation}
        />
        <ScrollView
          style={{
            marginTop: verticalScale(-41),
          }}>
          <View
            style={{
              backgroundColor: '#FFFFFF',
              borderRadius: 8,
              padding: verticalScale(10),
            }}>
            {product()}
          </View>
          {productsByType.length === 0 ? (
            <View style={{ marginTop: verticalScale(-30) }}>
              <Empty_Card Text="No Inventory Yet." />
            </View>
          ) : (
            <FlatList
              onEndReachedThreshold={0.7}
              data={productsByType}
              keyExtractor={(item, index) => index.toString()}
              enableEmptySections={true}
              renderItem={({ item, index }) => eachProductListByType(item)}
            />
          )}
        </ScrollView>
      </View>
    );

}
function mapStateToProps(state) {
  return {
    product_name: state.productdetail.product_name,
    inventorydata: state.inventory.inventorydata,
    productCountList: state.inventory.productCountList,
    productsByType: state.productdetail.productsByType,
    loder: state.loder,
  };
}

export default connect(
  mapStateToProps,
  {
    getBatchWarehouse,
    searchProductByType,
  },
)(ProductListByCategory);
