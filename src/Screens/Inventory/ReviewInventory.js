
import React, { useState, useEffect } from 'react';
import {
  LayoutAnimation,
  FlatList,
  Platform,
  Button,
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  ActivityIndicator,
  Keyboard
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  userinfo,
  getOrderDetails,
  searchProductByType,
  searchProductByName,
} from '../../Redux/Action/order';

import {
  getInventoryAnalytics
} from '../../Redux/Action/analytics';
import {
  getProductNames,
  getchainOfCustody,
  getViewShipment,
} from '../../Redux/Action/shipment';
import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
} from '../../Redux/Action/inventory'
import { HeaderWithBackBlue } from '../../components/Header';
import {ViewProdutList } from '../../components/Common/shipmentHelper';

import _ from 'lodash';
import { compareValues, NetworkUtils } from '../../Util/utils';
import { Colors, DeviceWidth } from '../../Style';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { queryAllTodoLists, queryAllInventryData, queryDeleteAllInventryData } from '../../databases/allSchemas'
import { ToastShow } from '../../components/Toast';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';



const ReviewInventory = (props) => {
  const { t, i18n } = useTranslation();
  const InventoryFormData = props.route.params.values
  console.log('InventoryFormData review'+ JSON.stringify(InventoryFormData))
  const _SaveInventory = async () => {

  
    const isConnected = await NetworkUtils.isNetworkAvailable()
    let response={}
    if(isConnected){
      response = await props.add_inventory(InventoryFormData.products);
    }else{
      response = await addInventorylocal(InventoryFormData.products);
    }
   
    if (response.status === 200) {
      const smsg = 'Success.'
      ToastShow(
        t(smsg),
         'success',
         'long',
         'top',
       )
       await props.getProductListCounts();
       await props.getAllEventsWithFilter(0, 50, props.filterInventory);
       await props.getInventoryAnalytics();
       props.navigation.navigate('Inventory');
    } else {
     
      const emsg = response && response.data && response.data.message || 'somethingWrong'
      ToastShow(
        t(emsg),
         'error',
         'long',
         'top',
       )
    }
  };
  return (
    <View style={styles.container}>
       <HeaderWithBackBlue
        name={t('review_inventory')}
        navigation={props.navigation}
      />
      <Text
          style={{
            fontSize: scale(14),
           marginTop: verticalScale(-DeviceWidth/7),
            alignSelf: 'flex-start',
            marginLeft: scale(16),
            color: '#FFFFFF',

          }}>
          {t('Description Of Goods')}
        </Text>
        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        >
      <ViewProdutList
            Products={InventoryFormData.products}
            isOrderView={true}
            isReviewOrder={true} 
            />
      </ScrollView>
      <View
          style={{
            flexDirection: 'row',
            margin: verticalScale(15),
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            bottom: 10
          }}>
          {/* position: 'absolute', bottom: 10, */}
          <TouchableOpacity
            style={{
              height: scale(40),
              flex: 0.5,
              borderRadius: 10,
              backgroundColor: '#FFFFFF',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#0093E9',
              borderWidth: 1,
              margin: 10,

            }}
            onPress={() => props.navigation.navigate('AddInventory')}>
            <Image
              style={{ width: scale(13.73), height: scale(13.73) }}
              source={require('../../assets/edit.png')}
            />
            <Text
              style={{
                fontSize: scale(13),
                color: '#0093E9',
                fontWeight: 'bold',
                marginLeft: scale(10),
              }}>
              {t('edit')}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => _SaveInventory()}
            style={{
              height: scale(40),
              borderRadius: 8,
              margin: 10,
              flex: 0.5,
              flexDirection: 'row',
              backgroundColor: '#FFAB1D',
              alignItems: 'center',
              justifyContent: 'space-evenly',
            }}>
           
            <Text
              style={{
                fontSize: scale(15),
                color: '#FFFFFF',
                fontWeight: 'bold',
              }}>
              {t('save')}
            </Text>
          </TouchableOpacity>
        </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
function mapStateToProps(state) {
  return {
    inventorydata: state.inventory.inventorydata,
    inventorycounts: state.inventory.inventorycounts,
    inventoryAnalytics: state.inventory.inventoryAnalytics,
    productCountList: state.inventory.productCountList,
    filterInventory: state.inventory.filterInventory,
    product_name: state.productdetail.product_name,
    loder: state.loder,
    userpermission: state.userinfo.userpermission,
  };
}
export default connect(mapStateToProps, {
  userinfo,
  getInventoryAnalytics,
  getProductNames,
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
  getchainOfCustody,
  getViewShipment,
  searchProductByType,
  searchProductByName,
  getOrderDetails,
},
)(ReviewInventory);
