import React, { useEffect } from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
  getBatchWarehouse,
} from '../../Redux/Action/inventory'
import Search from '../../components/Search'

import {
  getProductNames,
  getchainOfCustody,
  getViewShipment
} from '../../Redux/Action/shipment';
import _ from 'lodash';
import Empty_Card from '../../components/Empty_Card';
import {
  InventoryList,
  InventoryListOld,
} from '../../components/Common/InventoryHelper';
import { compareValues } from '../../Util/utils';
import { Colors, DeviceWidth } from '../../Style';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';

import { useTranslation } from 'react-i18next';
const RecentTransaction = (props) => {
  const { t, i18n } = useTranslation();
  const isFocused = useIsFocused();



  const onRefresh = async () => {
    await props.getAllEventsWithFilter(0, 50, props.filterInventory);
  };
  const eachInventoryData = (item, index) => {
    let { userpermission } = props;
    let { shipment = {} } = userpermission;
    return (
      <InventoryList
        item={item}
        index={index}
        ViewShipment={() =>
          getDetails(
            item.payloadData && item.payloadData.data,
            item.payloadData &&
            item.payloadData.data &&
            item.payloadData.data.id,
          )
        }
        shipmentPersmission={shipment}
        t={t}
      />
    );
  };
  const getDetails = async (item, id) => {
    await props.getchainOfCustody(id);
    await props.getViewShipment(id);
    props.navigation.navigate('Shipment', { screen: 'ViewShipment' });
  };

  let { inventorydata, filterInventory, userpermission } = props;
  let { inventory = {} } = userpermission;

  inventorydata = inventorydata.sort(compareValues('createdAt', 'desc'));
  let filterInventoryList = [];
  filterInventoryList = Object.keys(filterInventory).map((key) => { if (filterInventory[key]) { if (key != 'productName' && key != 'status') return [key, filterInventory[key]] } });
  let filterTransactionCount = filterInventoryList.filter(each => {
    if (each) {
      return each
    }
  })

  let count = 0;
  count = filterTransactionCount.length;
  const _listEmptyComponent= ()=>{
    return  <View>
    <Empty_Card Text="No Inventory Yet." />
  </View>
  }
  return (
    <View
      style={{
        // flex: 1,
      }}>
      <StatusBar backgroundColor="#0093E9" />
      <HeaderWithBackBlue
        name={t('Recent Transactions')}
        navigation={props.navigation}
      />


      <View
        style={{
          marginTop: verticalScale(-DeviceWidth / 7),
        }}
      >

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 50,
            marginRight: 20,
            marginLeft: 20
          }}>
          <Search
            navigation={props.navigation}
            fromScreen={'Inventory'}
            isRecentTrasaction={true}
          />
          {inventory.inventoryFilters ? (
            <View style={{
              marginTop: -80,
              marginBottom: 8,
              marginLeft: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <TouchableOpacity
                style={{

                  padding: 10,
                  borderRadius: 10,
                  backgroundColor: Colors.red23,
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                }}
                onPress={() =>
                  props.navigation.navigate('FilterInventory')
                }>
                <Image
                  style={{ width: scale(9.35), height: scale(11.68) }}
                  source={require('../../assets/Filter.png')}
                />
                <Text
                  style={{
                    fontSize: scale(12),
                    color: '#FFFFFF',
                  }}>
                  {count ? t('Filter') + '(' + count + ')' : t('Filter')}
                </Text>
                <Image
                  style={{ width: scale(10.01), height: scale(5.72) }}
                  resizeMode="cover"
                  source={require('../../assets/downarrow.png')}
                />
              </TouchableOpacity>
            </View>) : null}
        </View>
        
          <FlatList
            onEndReachedThreshold={0.7}
            data={inventorydata}
            enableEmptySections={true}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) =>
              eachInventoryData(item, index)
            }
            refreshControl={
              <RefreshControl
              refreshing={false}
                onRefresh={()=>onRefresh()}
                tintColor={Colors.blueE9}
                titleColor={Colors.blueE9}
                colors={['#a076e8', '#5dc4dd']}
                progressBackgroundColor={Colors.blueAE}
              />
            }
            ListEmptyComponent={_listEmptyComponent}
          />
      </View>
      {props.loder ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <ActivityIndicator color="#0000ff" />
        </View>
      ) : null}
    </View>
  );
}
function mapStateToProps(state) {
  return {
    product_name: state.productdetail.product_name,
    inventorydata: state.inventory.inventorydata,
    loder: state.loder,
    filterInventory: state.inventory.filterInventory,
    userpermission: state.userinfo.userpermission,
  };
}

export default connect(
  mapStateToProps,
  {
    getProductNames,
    getchainOfCustody,
    getViewShipment,
    getAllEventsWithFilter
  },
)(RecentTransaction);
