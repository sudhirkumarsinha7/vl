import React, { useEffect } from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import {
  add_inventory,
  getInventory,
  getProductListCounts,
  getBatchNearExpiration,
  getBatchExpired,
  getAllEventsWithFilter,
} from '../../Redux/Action/inventory'
import {
  getProductNames
} from '../../Redux/Action/shipment';

import _ from 'lodash';
import { backgroundColor,DeviceWidth } from '../../Style';
import Empty_Card from '../../components/Empty_Card';
import { InventoryProductListExpired } from '../../components/Common/InventoryHelper';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { compose } from 'redux';
const NearExpiredProduct =(props)=> {
  const { t, i18n } = useTranslation();
  useEffect(() => {
    async function fetchData() {
      await props.getBatchNearExpiration()
    }
    fetchData();
  }, []);


  const eachProductList=(item)=> {

    return (
      <InventoryProductListExpired
        item={item}
        navigation={props.navigation}
      />
    );
  }

    const { inventoryNearExpired = [] } =props
    return (
      <View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('batch_near_expiration')}
          navigation={props.navigation}
        />
        <View
          style={{
            borderRadius: 8,
           marginTop: verticalScale(-DeviceWidth/7),
            padding: verticalScale(10),
          }}>
          {props.loder ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}>
              <ActivityIndicator color="#0000ff" />
            </View>
          ) : null}
          {inventoryNearExpired.length === 0 ? (
            <View style={{ marginTop: verticalScale(0) }}>
              <Empty_Card Text="No Inventory Yet." />
            </View>
          ) : (
            <FlatList
              onEndReachedThreshold={0.7}
              data={inventoryNearExpired}
              keyExtractor={(item, index) => index.toString()}
              enableEmptySections={true}
              renderItem={({ item, index }) => eachProductList(item)}
            />
          )}
          <View style={{ height: scale(185) }} />
        </View>
      </View>
    );
}
function mapStateToProps(state) {
  return {
    product_name: state.productdetail.product_name,
    inventoryNearExpired: state.inventory.inventoryNearExpired,
    loder: state.loder,
  };
}

export default connect(
  mapStateToProps,
  { getProductNames, getBatchNearExpiration },
)(NearExpiredProduct);
