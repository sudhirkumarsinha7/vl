import React,{useState,useEffect} from 'react';
import {
  View,
  ScrollView,
  RefreshControl,
  StyleSheet,
  StatusBar,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { scale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  getTransactionNotifications,
  getAlertNotifications,
  getRequestById,
  getMoreAlertNotifications,
  getMoreTransactionNotifications,
} from '../../Redux/Action/alert';
import {
  getViewShipment,
  searchProductByType,
  searchProductByName,
  getOrderDetails,
} from '../../Redux/Action';

import {
  userinfo,
  trackJourney,
} from '../../Redux/Action/order';

import {
  getchainOfCustody,

}from '../../Redux/Action/shipment';
import {
  HeaderWithBackBlue,
  HeaderWithBackShipmentScreen,
} from '../../components/Header';
import Card from '../../components/Card';
import Empty_Card from '../../components/Empty_Card';
import Section_Select from '../../components/Section_Select';
import { NotificationHelper } from '../../components/Common/NotificationHelper';
import { alert } from '../../components/Common/alertData';
import { Colors } from '../../Style';
import PopUp from '../../components/PopUp';
import _ from 'lodash';
import Alert from './Alert';
import { useTranslation } from 'react-i18next';
import { compose } from 'redux';
import { ToastShow } from '../../components/Toast';
import Transaction from './Transaction';

const radioItems = [
  {
    label: 'Alerts',
    selected: true,
  },
  {
    label: 'Transactions',
    selected: false,
  },
]
const Notifications = (props) => {

  const { t, i18n } = useTranslation();
  const [selectedItem, setTab] = useState('Alerts');
  const [isLoadingState,setLoadingState] = useState(false)
  useEffect(() => {
    async function fetchData() {
      GetData();

    }
    fetchData();
  }, []);

  const GetData = async () => {
    radioItems.map(item => {
      if (item.selected == true) {
        setTab(item.label)
      }
    });
    props.getAlertNotifications();
    props.getTransactionNotifications();
  };
  const onRefresh = async () => {
    props.getAlertNotifications();
    props.getTransactionNotifications();
  };
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });
    radioItems[index].selected = true;
    const label = radioItems[index].label
    console.log('radioItems[index].label ' + label)
    setTab(label)
  }


    let { alerts = [], transaction = [], newAlert, newTransaction } = props;
    // console.log('render Notifications transaction' + JSON.stringify(transaction));
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#0093E9" />

        <HeaderWithBackShipmentScreen
          name={t('Notifications')}
          navigation={props.navigation}
          noOfAlert={selectedItem === 'Transactions' ? newTransaction : newAlert}
          t={t}
        />
      
        <View style={{ flexDirection: 'row', marginTop: verticalScale(-60) }}>
          {radioItems.map((item, key) => (
            <Section_Select
              key={key}
              button={item}
              selectedTextColor={Colors.whiteFF}
              fontSize={scale(16)}
              onClick={()=>changeActiveRadioButton(key)}
            />
          ))}
        </View>
        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              //refresh control used for the Pull to Refresh
              onRefresh={()=>onRefresh()}
              colors={['#a076e8', '#5dc4dd']}
              refreshing={false}
            />
          }
        >
         

          <View style={{ marginTop: scale(30) }} />
          {selectedItem === 'Transactions' ? (
            transaction.length === 0 ? (
              <View style={{ marginTop: verticalScale(10) }}>
                <Empty_Card Text={t("No Transactions Yet.")} />
              </View>
            ) : (
              <View>
                <Transaction
                  navigation={props.navigation}
                />
              </View>
            )
          ) : alerts.length === 0 ? (
            <View style={{ marginTop: verticalScale(10) }}>
              <Empty_Card Text={t("No Alerts Yet. ")} />
            </View>
          ) : (
            <View>
              <Alert
                navigation={props.navigation} />
            </View>
          )}

          <View style={{ height: scale(10) }} />
        </ScrollView>
      </View>
    );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
  lottie: {
    width: scale(100),
    height: scale(100),
  },
});
function mapStateToProps(state) {
  return {
    loder: state.loder,
    alerts: state.alert.alerts,
    transaction: state.alert.transaction,
    alertPage: state.alert.alertPage,
    transactionPage: state.alert.transactionPage,
    alertCount: state.alert.alertCount,
    transactionCount: state.alert.transactionCount,
    newAlert: state.alert.newAlert,
    newTransaction: state.alert.newTransaction,
  };
}

export default connect(
  mapStateToProps,
  {
    userinfo,
    getAlertNotifications,
    getTransactionNotifications,
    trackJourney,
    getViewShipment,
    getchainOfCustody,
    searchProductByType,
    searchProductByName,
    getOrderDetails,
    getRequestById,
    getMoreAlertNotifications,
    getMoreTransactionNotifications,
  },
)(Notifications);
