import React,{useEffect,useState} from 'react';
import {
  View,
  ScrollView,
  RefreshControl,
  StyleSheet,
  StatusBar,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { scale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';

import {
  getTransactionNotifications,
  getAlertNotifications,
  getRequestById,
  getMoreAlertNotifications,
  getMoreTransactionNotifications,
  readNotification
} from '../../Redux/Action/alert';
import {
  getViewShipment,
  searchProductByType,
  searchProductByName,
  getOrderDetails,
} from '../../Redux/Action';

import {
  userinfo,
  trackJourney,
} from '../../Redux/Action/order';

import {
  getchainOfCustody,

}from '../../Redux/Action/shipment';
import { NotificationHelper } from '../../components/Common/NotificationHelper';
import PopUp from '../../components/PopUp';
import _ from 'lodash';
import { useTranslation } from 'react-i18next';
import {ToastShow} from '../../components/Toast'
const Alert =(props)=> {
  const { t, i18n } = useTranslation();
  const [isLoadingState,setLoadingState] = useState(false)
  const onRefresh = async () => {
   props.getAlertNotifications();
  };
  const loadMore = async () => {
    //Call the Service to get the latest data
  };


  const eachAlert = item => {
    return (
      <NotificationHelper
        item={item}
        navigateNextScreen={navigateNextScreen}
      />
    );
  };
  const navigateNextScreen=async (item)=> {
    // console.log('navigateNextScreen item' + JSON.stringify(item));
   props.readNotification(item.id)
    if (item.eventType === 'SHIPMENT' && item.transactionId) {
     props.getchainOfCustody(item.transactionId);
     props.getViewShipment(item.transactionId);
     props.navigation.navigate('Shipment', { screen: 'ViewShipment' });;

    } else if (item.eventType === 'ORDER' && item.transactionId) {
      SearchOrder(item.transactionId);
    } else if (item.eventType === 'SHIPMENT_TRACKING' && item.transactionId) {
     await props.trackJourney(item.transactionId);
     props.navigation.navigate('Track', { screen: 'TrackDeatils' });;
    } else if (item.eventType === 'INVENTORY') {
     props.navigation.navigate('Inventory', { screen: 'Inventory' });;

    } else if (item.eventType === 'PRODUCT' && item.transactionId) {
     await props.searchProductByName(item.transactionId);
     props.navigation.navigate('Inventory', { screen: 'ProductListByName' });;

    } else if (item.eventType === 'PRODUCTTYPE' && item.transactionId) {
     await props.searchProductByType(item.transactionId);
     props.navigation.navigate('Inventory', { screen: 'ProductListByCategory',params: { selectedProductType:  item.transactionId } });;

    } else if (item.eventType === 'REQUEST' && item.transactionId) {
     await props.getRequestById(item.transactionId);
     props.navigation.navigate('Notifications', { screen: 'UserPermission',params: { item:  item } });;
    } else if ((item.eventType === 'REJECTED' || item.eventType === 'ACCEPTED')) {
      props.navigation.navigate('Shipment', { screen: 'ReceiveShipment',params: { optionName: 'shipment',shipmentId: null, } });;
    } else {
      const errorMessage = item.transactionId
      ? 'Invalid EventType'
      : 'transactionId is not Available';
          ToastShow(
          t(errorMessage),
            'error',
            'long',
            'top',
          )
    }
  }
  const SearchOrder = async text => {
    setLoadingState(true)
    const result = await props.getOrderDetails(text);
    console.log(
      'SearchOrder getOrderDetails result' + JSON.stringify(result),
    );
    if (result.length) {
      setLoadingState(false)

      const orderData = result[0];
      props.navigation.navigate('Order', { screen: 'ViewOrder',params: { item: orderData } });;
    } else {
      ToastShow(
        t('Please Check your order Id :') + text,
         'error',
         'long',
         'top',
       )
    }
    setLoadingState(false)

  };
  const handleLoadMoreAlert=() =>{
    let { alerts = [], alertPage, alertCount = 0 } =props
    console.log('alerts.length ' + alerts.length + ' page ' + alertPage)

    if (alertCount > alerts.length && alertPage) {
     props.getMoreAlertNotifications(alertPage, alerts.length);
    }
  }

  const renderFooter=()=> {
    if (!props.loder) return null
    return (<View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
      }}>
      <ActivityIndicator color="#0000ff" />
    </View>);
  }

    let { alerts = [], transaction = []} =props;
    // console.log('render Alert transaction' + JSON.stringify(transaction));
    return (
      <View style={styles.container}>

     

        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              //refresh control used for the Pull to Refresh
              onRefresh={()=>onRefresh()}
              colors={['#a076e8', '#5dc4dd']}
              refreshing={false}
            // refreshing={props.loder}
            />
          }
        >
          <FlatList
            onEndReachedThreshold={0.7}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={alerts}
            renderItem={({ item }) => eachAlert(item)}
            ListFooterComponent={renderFooter}
            onEndReached={handleLoadMoreAlert}
          />
          <View style={{ height: scale(10) }} />
        </ScrollView>
      </View>
    );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
  lottie: {
    width: scale(100),
    height: scale(100),
  },
});
function mapStateToProps(state) {
  return {
    loder: state.loder,
    alerts: state.alert.alerts,
    alertPage: state.alert.alertPage,
    alertCount: state.alert.alertCount,
  };
}

export default connect(
  mapStateToProps,
  {
    userinfo,
    getAlertNotifications,
    trackJourney,
    getViewShipment,
    getchainOfCustody,
    searchProductByType,
    searchProductByName,
    getOrderDetails,
    getRequestById,
    getMoreAlertNotifications,
    readNotification,
  },
)(Alert);
