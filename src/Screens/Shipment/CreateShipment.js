
import React, { useState, useEffect } from 'react';
import {
  Vibration,
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
  Image,
  Keyboard
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  getRegions,
  fetch_Purchase_Orders_ids
} from '../../Redux/Action/order';
import {
  getInventoryAnalytics,
  getShipmentAnalytics,
} from '../../Redux/Action/analytics';
import {CustomScanner} from '../../components/Scanner'
import {
  getProductNames,
  fetchOrganizations,
  getOrganizationsByType,
  getWarehouseByOrgId,
  getViewShipment,
  getProductsByInventory,
  fetchInboundShipments,
  fetchOutboundShipments,
  create_new_shipment
} from '../../Redux/Action/shipment';
import axios from 'axios';
import { HeaderWithBackBlue } from '../../components/Header';
import _ from 'lodash';
import { Colors, DeviceWidth,DeviceHeight } from '../../Style';
import { CustomDropdown, CustomInputText, MonthYearPickerView, CustomDropDownLocation, CustomeDatePicker } from '../../components/Common/common';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../components/Toast';
import { BatchPoup } from '../../components/PopUp'
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { compareValues, NetworkUtils } from '../../Util/utils';
import {CameraScreen} from 'react-native-camera-kit';
import LinearGradient from 'react-native-linear-gradient';
import { global, config, } from '../../Util/config'
export function replaceAll(string, search, replace) {
  return string.split(search).join(replace);
}
const AddNewProduct = () => {
  const add = {
    productID: '',
    manufacturer: '',
    productName: '',
    productQuantity: '',
    isMoreData: false,
    mfgDate: '',
    expDate: '',
    productCategory: '',
    batchNumber: '',
    serialNumbersRange: [],
    unitofMeasure: {
      id: '',
      name: '',
    },
    MaxQty: '',
  }
  return add
}
const CreateShipment = (props) => {
  const { t, i18n } = useTranslation();
  const [opneScanner,setOpneScanner] = useState(false);
  const [currentIndx,setCurrentIndx] = useState(null);

  const [mandatory, setIsmantory] = useState(false)
  const [loading, setLoading] = useState(false)
  const [shipmentOrderDetailsData, setShipmenOrderDetails] = useState({})
  const [fromLoc, setFromLoct] = useState([])
  const [ProductType, setProductType] = useState([])
  const [ProductNameList, setProductNameList] = useState([])
  const [TotalProductNameList, setTotalProductNameList] = useState([])
  const [toOrganisations, setToOrganisations] = useState([])
  const [toOrgLocations, setToOrgLocations] = useState([])
  const [isBatchModal, setBAtchModal] = useState(false)
  const [batchDetails, setBatchDetails] = useState([])
  const [selectedProductIndex, setSelectedProductIndex] = useState(null)

  useEffect(() => {
    async function fetchData() {

      await props.fetchOrganizations();
      await props.fetch_Purchase_Orders_ids();
      const orgSplit = props.user.organisation?.split('/');
      const ownOrg = orgSplit[0];
      const orgId = orgSplit[1];
      const res = await props.getWarehouseByOrgId(orgId);
      ShipmentForm.handleChange({ target: { name: `fromOrgName`, value: ownOrg } })
      ShipmentForm.handleChange({ target: { name: `fromOrgId`, value: orgId } })
      setFromLoct(res)
      const res1 = await props.getOrganizationsByType(
        (props.user && props.user.configuration_id) ||
        global.defaultConfigID,
      );
    }
    fetchData();
  }, []);

  const OrgTypeList = props.OrgTypeList?.filter(each => {
    if (each.name) {
      return each
    }
  })


  var validationSchema = Yup.object().shape(
    {
      fromOrgId: Yup.string().required(t('Required')),
      fromOrgLocid: Yup.string().required(t('Required')),
      ToOrgType: Yup.string().required(t('Required')),
      toOrgId: Yup.string().required(t('Required')),
      toOrgLocId: Yup.string().required(t('Required')),
      airWayBillNo: Yup.string().required(t('Required')),
      shipmentDate: Yup.string().required(t('Required')),
      products: Yup.array()
        .of(
          Yup.object().shape({
            manufacturer: Yup.string().required(t('Required')),
            productName: Yup.string().required(t('Required')),
            productQuantity: Yup.string().required(t('Required')),
            productCategory: Yup.string().required(t('Required')),
          })
        ).required(t('Required'))
    },
    [],
  ); // <-- HERE!!!!!!!!

  const ShipmentForm = useFormik({
    initialValues: {
      products: [
        {
          productID: '',
          manufacturer: '',
          productName: '',
          productQuantity: '',
          productCategory: '',
          unitofMeasure: {
            id: '',
            name: '',
          },
          mfgDate: '',
          expDate: '',
          serialNumbersRange: [],
          batchNumber: '',
          MaxQty: '',
          isMoreData:false,
        }
      ],
      shippingOrderId: '',
      taggedShipments: '',
      ToOrgType: '',
      toOrgId: '',
      toOrgLocId: '',
      toOrgLoc: '',
      fromOrgLocid: '',
      fromOrgId: '',
      fromOrgName: '',
      labelCode: '',
      airWayBillNo: '',
      shipmentDate: '',
      estimateDeliveryDate: '',

    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _CreateShipment = () => {
    setIsmantory(true);
    Keyboard.dismiss();
    ShipmentForm.handleSubmit();
  };
  const AddAnotherProduct = () => {
    const data = [...ShipmentForm.values.products]
    const newData = AddNewProduct()
    const finalData = [...data, newData]
    let allProduct = {
      products: finalData
    }
    ShipmentForm.setValues({
      ...ShipmentForm.values,
      products: finalData,
    });
    // ShipmentForm.setValues(allProduct)
  }
  const cancel = index => {
    const productClone = JSON.parse(JSON.stringify(ShipmentForm?.values?.products));
    // console.log(Object.keys(productClone));
    productClone.splice(index, 1);
    let allProduct = {
      products: productClone
    }
    ShipmentForm.setValues(allProduct)
  };
  const DeleteIcon = index => {
    return <View
      style={{
        flexDirection: 'row',
        marginLeft: verticalScale(5),
        alignItems: 'center',

      }}>
      <Text style={{ width: '80%', fontWeight: 'bold' }}>
        {t('Product') + ' ' + (index + 1)}
      </Text>
      <View
        style={{
          width: scale(35),
          height: scale(35),
          borderRadius: 400,
          backgroundColor: Colors.whiteF2,
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 10,
          marginTop: 5,
        }}
        opacity={5}>
        <TouchableOpacity
          style={{
            justifyContent: 'flex-end',
          }}
          onPress={() => cancel(index)}
          disabled={ShipmentForm?.values?.products.length === 1 ? true : false}>

          {ShipmentForm?.values?.products.length === 1 ? (
            <Image
              style={{ width: scale(13.5), height: scale(35) }}
              source={require('../../assets/deletegrey.png')}
              resizeMode="contain"
            />
          ) : (
            <Image
              style={{ width: scale(13.5), height: scale(35) }}
              source={require('../../assets/deleteblue.png')}
              resizeMode="contain"
            />
          )}

        </TouchableOpacity>
      </View>
    </View>
  };

  const handleSubmit = async values => {
    console.log('handleSubmit ' + JSON.stringify(values))
    addShipment(values)

  };

  const addShipment = async (values) => {

    const {
      airWayBillNo,
      labelCode,
      shipmentDate,
      fromOrgLocid,
      taggedShipments,
      toOrgLocId,
      fromOrgId,
      toOrgId,
      estimateDeliveryDate = '',
      shippingOrderId,

      products
    } = values;


    const data = {
      airWayBillNo,
      label: {
        labelId: labelCode,
        labelType: 'QR_2DBAR',
      },
      externalShipmentId: '',
      supplier: {
        id: fromOrgId,
        locationId: fromOrgLocid,
      },
      receiver: {
        id:  toOrgId,
        locationId: toOrgLocId,
      },
      shippingDate: new Date().toISOString(shipmentDate),
      expectedDeliveryDate: estimateDeliveryDate
        ? new Date().toISOString(estimateDeliveryDate)
        : '',
      actualDeliveryDate: estimateDeliveryDate
        ? new Date().toISOString(estimateDeliveryDate)
        : '',
      status: 'CREATED',
      products: products,
      poId: shippingOrderId ? shippingOrderId : null,
      taggedShipments: taggedShipments || '',
    };
    console.log('setCreateShipment data' + JSON.stringify(data));
    const isConnected = await NetworkUtils.isNetworkAvailable();
    let result = {};
    if (isConnected) {
      result = await await props.create_new_shipment(data);
    } else {
      result = await addShipmentlocal(data);
    }

    console.log('setCreateShipment result ' + JSON.stringify(result));
    //   queryAllShipment().then((todoLists) => {
    //   let data = todoLists;
    //               console.log('  queryAllShipment test '+ JSON.stringify(data));

    //   }).catch((error) => {
    //     console.log('syncInventoryData error'+ JSON.stringify(error));

    //   });
    if (result && result.status === 200) {

      const smsg = result.data && result.data.data && result.data.data.id
        ? t('Shipment ') +
        result.data.data.id +
        t(' has been created successfully!')
        : t('Shipment has been created successfully!');
      ToastShow(
        smsg,
        'success',
        'long',
        'top',
      )
      await props.fetchInboundShipments(0, 50, props.filterInbound);
      await props.fetchOutboundShipments(0, 50, props.filterOutBound);
      await props.getShipmentAnalytics();
      props.navigation.navigate('Shipment');
    } else if (result && result.status === 500) {
      // this.setState({
      //   isModalVisible2: true,
      //   errorMessage:
      //     (result &&
      //       result.data &&
      //       result.data.message &&
      //       typeof result.data.message === 'string') ||
      //     '500 Internal server error',
      // });
      const emsg = (result &&
        result.data &&
        result.data.message &&
        typeof result.data.message === 'string') ||
        '500 Internal server error';
      ToastShow(
        emsg,
        'error',
        'long',
        'top',
      )
    } else {
      // this.setState({
      //   isModalVisible2: true,
      //   errorMessage:
      //     (result && result.data && result.data.message) ||
      //     'something is wrong',
      // });
      const emsg1 = (result && result.data && result.data.message) ||
        'something is wrong';
      ToastShow(
        emsg1,
        'error',
        'long',
        'top',
      )
    }


  };

  const ShipmentFromOrderComponent = () => {
    return (<View
      style={{
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
      }}>
      <View style={{ marginTop: scale(5) }} />

      <CustomDropdown


        displayName={'Order ID'}
        stateName={ShipmentForm?.values?.shippingOrderId}
        dropDowndata={props.purchaseorderids || []}
        onChangeValue={(val) => ChangeOrderId(val)}
        label={'id'}
        mapKey={'id'}
      />
               {/* <View style={{ marginTop: scale(15) }} /> */}
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 0.9 }}>
          <CustomInputText
            label={"Ref Shipment ID"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={ShipmentForm.handleChange('taggedShipments')}
            val={ShipmentForm?.values?.taggedShipments}
            errorMsg={ShipmentForm.errors && ShipmentForm.errors.taggedShipments}

          />
        </View>

        <TouchableOpacity
        style={{justifyContent:'center'}}
          disabled={ShipmentForm?.values?.taggedShipments ? false : true}
          onPress={() =>
            getShipmentIdDetails(ShipmentForm?.values?.taggedShipments, true,)
          }>
          <Text style={{ color: ShipmentForm?.values?.taggedShipments ? Colors.blueE9 : Colors.gray9, borderWidth: 1, padding: 5, borderColor: ShipmentForm?.values?.taggedShipments ? Colors.blueE9 : Colors.gray9, borderRadius: 5 }}>
            {t('Fetch')}
          </Text>
        </TouchableOpacity>
      </View>




    </View>)
  }
  const ChangeOrderId = async (item) => {
    // console.log('ChangeOrderId item' + JSON.stringify(item));
    ShipmentForm.handleChange({ target: { name: `shippingOrderId`, value: item?.value } })
    getOrderIdDetails(item.value);
  }

  const getOrderIdDetails = async (shipmentID) => {
    setLoading(true)
    try {
      console.log(
        'fyry shipmentID,' +
        JSON.stringify(`${config().getOrderDetails}` + shipmentID),
      );
      let response = await axios.get(
        `${config().getOrderDetails}${shipmentID}&skip=0&limit=1`,
      );
      // console.log(
      //   'getOrderIdDetails response.data.data' +
      //     JSON.stringify(response.data.data),
      // );
      let shipmentDetails = response.data.data.poDetails[0];
      let { products = [], productDetails = [], supplier = {}, customer = {} } = shipmentDetails;
      let { organisation = {}, warehouse = {} } = customer;
      const Whloc = (warehouse && warehouse.warehouseAddress
        ? warehouse.title + ' / ' + warehouse.warehouseAddress.firstLine + ' ' + warehouse.warehouseAddress.city
        : organisation && organisation.postalAddress) || ''



      if (products.length) {
        products = products.map(each => {
          each.productID = each.productId || each.id;
          if (each.productQuantityShipped && each.productQuantityDelivered) {
            
            each.productQuantity = Number(each.productQuantity) - Number(each.productQuantityShipped) - Number(each.productQuantityDelivered);

          } else if (each.productQuantityShipped) {
            each.productQuantity = Number(each.productQuantity) - Number(each.productQuantityShipped);

          } else if (each.productQuantityDelivered) {
            each.productQuantity = Number(each.productQuantity) - Number(each.productQuantityDelivered);

          } else {
            each.productQuantity = each.productQuantity;

          }

          each.productName = each.name;
          each.productCategory = each.type;
          each.productCategory = each.productCategory;
          each.serialNumbersRange = each.serialNumbersRange || [];
          return each;
        });
        console.log(
          'getOrderIdDetails shipmentDetails products' +
          JSON.stringify(products),
        );
        ShipmentForm.handleChange({ target: { name: `products`, value: products } })
      }
      ShipmentForm.handleChange({ target: { name: `fromOrgLocid`, value: '' } })
      ShipmentForm.handleChange({ target: { name: `ToOrgType`, value: organisation.productCategory } })
      ShipmentForm.handleChange({ target: { name: `toOrgId`, value: organisation.id } })
      ShipmentForm.handleChange({ target: { name: `toOrgName`, value: organisation.name } })
      ShipmentForm.handleChange({ target: { name: `toOrgLocId`, value: warehouse.id } })
      ShipmentForm.handleChange({ target: { name: `toOrgLoc`, value: Whloc } })

      setShipmenOrderDetails(shipmentDetails)
    } catch (e) {
      console.log('Action_getOrderIdDetails_error', e);
      setShipmenOrderDetails([])
    } finally {
      setLoading(false)
    }
  }
  const getShipmentIdDetails = async (shipmentID, refShipment = false) => {
    let response = await props.getViewShipment(shipmentID);
    console.log(
      'getShipmentIdDetails response.data.data jj' +
      JSON.stringify(response),
    );
    let shipmentDetails = response && response.data && response.data;
    console.log(
      'getShipmentIdDetails response.data.data jj' +
      JSON.stringify(response),
    );
    if (shipmentDetails.id) {
      if (shipmentDetails.status === 'RECEIVED') {
        let { products = [] } = shipmentDetails;

        if (products.length) {
          products = products.map(each => {
            each.productID = each.productID || each.productID;
            if (each.productQuantityTaggedSent) {
              let Qty = Number(each.productQuantity) -
                Number(each.productQuantityTaggedSent);
              each.productQuantity = Qty
              each.MaxQty = Qty;

            } else {
              each.productQuantity = each.productQuantity;
              each.MaxQty = each.productQuantity;



            }
         
            return each;
          });
          ShipmentForm.handleChange({ target: { name: `products`, value: products } })
        }
        console.log('getShipmentIdDetails products' + JSON.stringify(products));
        setShipmenOrderDetails(shipmentDetails)
        ShipmentForm.handleChange({ target: { name: `shippingOrderId`, value: '' } })

      } else {
        ToastShow(
          t('Shipment has to be delivered: ') + shipmentID,
          'error',
          'long',
          'top',
        )
      }
    } else {
      ShipmentForm.handleChange({ target: { name: `taggedShipments`, value: '' } })

      // alert(t('Please Enter Valid Ref ShipmentId :') + shipmentID);
      ToastShow(
        t('Please Enter Valid Ref ShipmentId :') + shipmentID,
        'error',
        'long',
        'top',
      )
    }
  }

  const ShipmentFromLocComponent = () => {
    return (<View
      style={{
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
      }}>
      <Text
        style={{
          marginLeft: verticalScale(10),
          fontWeight: 'bold',
          marginTop: verticalScale(10),
          color: '#0093E9',
        }}>
        {t('From')}
      </Text>
               {/* <View style={{ marginTop: scale(15) }} /> */}

      <CustomInputText
        label={"Organization Name"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        val={ShipmentForm?.values?.fromOrgName}
        errorMsg={ShipmentForm.errors && ShipmentForm.errors.fromOrgName}
        disabled={true}
        mandatory={mandatory}

      />

      <CustomDropDownLocation
        displayName={'Organization Location'}
        stateName={ShipmentForm?.values?.fromOrgLocid}
        dropDowndata={fromLoc}
        onChangeValue={(val) => selectFromLoc(val)}
        label={'id'}
        mapKey={'id'}
        mandatory={mandatory}
      />

               {/* <View style={{ marginTop: scale(15) }} /> */}



    </View>)
  }

  const selectFromLoc = async (item1) => {
    ShipmentForm.handleChange({ target: { name: `fromOrgLocid`, value: item1.value } })

    let item = item1.eachItem;
    const res = await props.getProductsByInventory(
      item.warehouseInventory,
    );
    console.log('selectFromLoc' + JSON.stringify(item));
    if (res.length) {
      const ProductType = _.uniqBy(res, function (x) {
        return x.productCategory;
      });
      setProductType(ProductType);
      console.log('selectFromLoc ProductType' + JSON.stringify(ProductType));

      setTotalProductNameList(res);
      ShipmentForm.handleChange({ target: { name: `fromOrgLoc`, value: item1.label } })

    } else {
      ToastShow(
        t('Product is not Available in Inventory'),
        'error',
        'long',
        'top',
      )
    }
  };


  const ShipmentToLocComponent = () => {
    return (<View
      style={{
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
      }}>
      <Text
        style={{
          marginLeft: verticalScale(10),
          fontWeight: 'bold',
          marginTop: verticalScale(10),
          color: '#0093E9',
        }}>
        {t('To')}
      </Text>

      {ShipmentForm?.values?.shippingOrderId ?
        <View>
          <CustomInputText
            label={"Organization Type"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            val={ShipmentForm?.values?.ToOrgType}
            errorMsg={ShipmentForm.errors && ShipmentForm.errors.ToOrgType}
            disabled={true}
            mandatory={mandatory}

          />
          <CustomInputText
            label={"Organization Name"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            val={ShipmentForm?.values?.toOrgName}
            errorMsg={ShipmentForm.errors && ShipmentForm.errors.toOrgName}
            disabled={true}
            mandatory={mandatory}

          />
          <CustomInputText
            label={"Delivery location"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            val={ShipmentForm?.values?.toOrgLoc}
            errorMsg={ShipmentForm.errors && ShipmentForm.errors.toOrgLoc}
            disabled={true}
            mandatory={mandatory}

          />
        </View>
        : <View>
                   {/* <View style={{ marginTop: scale(15) }} /> */}
          <CustomDropdown
            displayName={'Organization Type'}
            stateName={ShipmentForm?.values?.ToOrgType}
            dropDowndata={OrgTypeList}
            onChangeValue={(val) => selectToOrgType(val)}
            label={'name'}
            mapKey={'name'}
            mandatory={mandatory}
          />
          <CustomDropdown
            displayName={'Organization Name'}
            stateName={ShipmentForm?.values?.toOrgId}
            dropDowndata={toOrganisations}
            onChangeValue={(val) => selectToOrg(val)}
            label={'name'}
            mapKey={'id'}
            mandatory={mandatory}
          />

          <CustomDropDownLocation
            displayName={'Delivery location'}
            stateName={ShipmentForm?.values?.toOrgLocId}
            dropDowndata={toOrgLocations}
            onChangeValue={(val) => selectToLoc(val)}
            label={'id'}
            mapKey={'id'}
            mandatory={mandatory}
          />

                   {/* <View style={{ marginTop: scale(15) }} /> */}

        </View>}


    </View>)
  }

  const selectToOrgType = async item1 => {
    let item = item1.eachItem;
    ShipmentForm.handleChange({ target: { name: `ToOrgType`, value: item1?.value } })
    ShipmentForm.handleChange({ target: { name: `toOrgLoc`, value: '' } })
    ShipmentForm.handleChange({ target: { name: `toOrgId`, value: '' } })
    ShipmentForm.handleChange({ target: { name: `toOrgName`, value: '' } })
    
    let filterOrg = props.organizations.filter(each => {
      if (each.type === item1.label) {
        return each;
      }
    });

    setToOrganisations(filterOrg);
    setToOrgLocations([])
  };

  const selectToOrg = async item => {
    ShipmentForm.handleChange({ target: { name: `toOrgId`, value: item?.value } })
    ShipmentForm.handleChange({ target: { name: `toOrgLoc`, value: '' } })
    ShipmentForm.handleChange({ target: { name: `toOrgName`, value: item.label } })
    const res = await props.getWarehouseByOrgId(item.value);
    setToOrgLocations(res)
  };

  const selectToLoc = async item1 => {
    let item = item1.eachItem;
    ShipmentForm.handleChange({ target: { name: `toOrgLocId`, value: item1?.value } })
    ShipmentForm.handleChange({ target: { name: `toOrgLoc`, value: item1.label } })
  };
  const DeliveryDetailsComponent = () => {
    return <View
      style={{
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
      }}>
      <Text
        style={{
          fontSize: scale(13),
          color: '#0093E9',
          fontWeight: 'bold',
          marginLeft: 10,
          marginTop: 10,
          marginBottom: 10,
        }}
        numberOfLines={1}>
        {t('Deliver Details')}
      </Text>
      <CustomInputText
        label={"Transit Number"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={ShipmentForm.handleChange(`airWayBillNo`)}
        val={ShipmentForm?.values?.airWayBillNo}
        errorMsg={ShipmentForm.errors && ShipmentForm.errors.airWayBillNo}
        mandatory={mandatory}

      />
      <CustomInputText
        label={"Label Code"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={ShipmentForm.handleChange(`labelCode`)}

        val={ShipmentForm?.values?.labelCode}
        errorMsg={ShipmentForm.errors && ShipmentForm.errors.labelCode}

      />
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 0.4 }}>

          <CustomeDatePicker
            label={"Shipment Date"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={ShipmentForm.handleChange(`shipmentDate`)}
            val={ShipmentForm?.values?.shipmentDate}
            errorMsg={ShipmentForm.errors && ShipmentForm.errors.shipmentDate}
            mandatory={mandatory}
          />
        </View>
        <View style={{ flex: 0.2 }}/>
        <View style={{ flex: 0.4 }}>

          <CustomeDatePicker
            label={"Delivery Date"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={ShipmentForm.handleChange(`estimateDeliveryDate`)}
            val={ShipmentForm?.values?.estimateDeliveryDate}
            minDate={ShipmentForm?.values?.shipmentDate}

          />
        </View>

      </View>

    </View>
  }
  const ProductListComponent = () => {
    return (<View>
      {ShipmentForm?.values?.products.map((each, index) => {
        return <View style={{ backgroundColor: Colors.whiteFF, margin: 15, borderRadius: 5 }}>
          {DeleteIcon(index)}

          {ShipmentForm?.values?.shippingOrderId ||ShipmentForm?.values?.taggedShipments ? <View>
            <CustomInputText
              label={"Product Type"}
              labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
              onChange={ShipmentForm.handleChange(`products[${index}].productID`)}
              val={ShipmentForm?.values?.products[index]?.productCategory}
              errorMsg={ShipmentForm.errors && ShipmentForm.errors.products && ShipmentForm.errors.products[index] && ShipmentForm.errors.products[index].productCategory}
              mandatory={mandatory}
              disabled={true}
            />
            <CustomInputText
              label={"Product Name"}
              labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
              onChange={ShipmentForm.handleChange(`products[${index}].productName`)}
              val={ShipmentForm?.values?.products[index]?.productName}
              errorMsg={ShipmentForm.errors && ShipmentForm.errors.products && ShipmentForm.errors.products[index] && ShipmentForm.errors.products[index].productName}
              mandatory={mandatory}
              disabled={true}

            />
          </View> : <View>
            <CustomDropdown
              displayName={'Product Type'}
              stateName={ShipmentForm?.values?.products[index]?.productCategory}
              dropDowndata={ProductType}
              onChangeValue={(val) => onChangeProductType(val, index)}
              label={'productCategory'}
              mapKey={'productCategory'}
              mandatory={mandatory}
            />
            <CustomDropdown
              displayName={'Product Name'}
              stateName={ShipmentForm?.values?.products[index]?.productID}
              dropDowndata={ProductNameList}
              onChangeValue={(val) => onChangeProductName(val, index)}
              label={'productName'}
              mapKey={'_id'}
              mandatory={mandatory}
            />
                     {/* <View style={{ marginTop: scale(15) }} /> */}
          </View>}


          <CustomInputText
            label={"manufacturer"}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={ShipmentForm.handleChange(`products[${index}].manufacturer`)}
            val={ShipmentForm?.values?.products[index]?.manufacturer}
            errorMsg={ShipmentForm.errors && ShipmentForm.errors.products && ShipmentForm.errors.products[index] && ShipmentForm.errors.products[index].manufacturer}
            disabled={true}
            mandatory={mandatory}

          />

          <CustomInputText
            label={each.unitofMeasure && each.unitofMeasure.name? "Quantity (" +  each.unitofMeasure.name + ')':'Quantity'}
            // label={"Quantity " + ShipmentForm?.values?.products[index]?.MaxQty + " (" +  each.unitofMeasure && each.unitofMeasure.name + ')'}
            labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
            onChange={(val) => onChangeQty(val, index)}
            val={ShipmentForm?.values?.products[index]?.productQuantity + ''}
            keyboardType={"numeric"}
            errorMsg={ShipmentForm.errors && ShipmentForm.errors.products && ShipmentForm.errors.products[index] && ShipmentForm.errors.products[index].productQuantity}
            mandatory={mandatory}
          />


          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.9 }}>
              <CustomInputText
                label={"Batch Number"}
                labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                onChange={ShipmentForm.handleChange(`products[${index}].batchNumber`)}
                val={ShipmentForm?.values?.products[index]?.batchNumber}
                errorMsg={ShipmentForm.errors && ShipmentForm.errors.products && ShipmentForm.errors.products[index] && ShipmentForm.errors.products[index].batchNumber}
                mandatory={mandatory}

              />
            </View>

            <TouchableOpacity
             style={{justifyContent:'center'}}
              disabled={ShipmentForm?.values?.products[index]?.productID && ShipmentForm?.values?.fromOrgLocid ? false : true}
              onPress={() =>
                fetchBatchNo(index, ShipmentForm?.values?.products[index]?.productID)
              }>
              <Text style={{ color: ShipmentForm?.values?.products[index]?.productID ? Colors.blueE9 : Colors.gray9, borderWidth: 1, padding: 5, borderColor: ShipmentForm?.values?.products[index]?.productID ? Colors.blueE9 : Colors.gray9, borderRadius: 5 }}>
                {t('Fetch')}
              </Text>
            </TouchableOpacity>
          </View>
          {ShipmentForm?.values?.products[index]?.productID?ShipmentForm?.values?.products[index]?.isMoreData ? (
            <View>
              <CustomInputText
                label={"Serial Number"}
                labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                onChange={(val) => onChangeSerialNo(val, index)}
                val={ShipmentForm?.values?.products[index]?.serialNumbersRange.toString()}
                mandatory={mandatory}
              />
              <View style={{ flexDirection: 'row', margin: scale(10), marginTop: scale(10) }}>
                <View style={{ flex: 0.45 }}>
                  <MonthYearPickerView
                    label={"Mfg Date"}
                    labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                    onChange={(val) => onChangeMfg(val, index)}
                    val={ShipmentForm?.values?.products[index]?.mfgDate}
                    mandatory={mandatory}
                    minDate={new Date(2020, 1)}
                    isMfgDate={true}
                  />
                </View>
                <View style={{ flex: 0.1 }} />
                <View style={{ flex: 0.45 }}>
                  <MonthYearPickerView
                    label={"Exp Date"}
                    labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                    onChange={(val) => onChangeExp(val, index)}
                    val={ShipmentForm?.values?.products[index]?.expDate}
                    mandatory={mandatory}
                    minDate={ShipmentForm?.values?.products[index]?.mfgDate}
                    isMfgDate={false}
                  />

                </View>

              </View>
            </View>
          ) : (
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                marginBottom: verticalScale(10),
                borderRadius: 10,
                marginLeft: scale(10),
              }}
              onPress={() => updateMoreValue(index)}>
              <View style={{
                backgroundColor: '#FFAB1D',
                padding: 5,
                marginTop: 10,
                borderRadius: 5,
                paddingLeft: scale(10),
                paddingRight: scale(10)
              }}>
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                    elevation: 5,
                  }}>
                  {t('+ Add More Details')}
                </Text>
              </View>
            </TouchableOpacity>
          ):null}
           {ShipmentForm?.values?.products[index]?.productID?<View  style={{
      flexDirection: 'row',
      alignSelf: 'flex-end',
      marginBottom: 10,
    }}>
    <TouchableOpacity
      style={{
        marginTop: verticalScale(5),
        width: scale(90),
        height: scale(35),
        marginRight: scale(20),
        borderRadius: 8,
        borderWidth: 1,
        borderColor:props.enable ? '#D6D6D6' : '#0093E9',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        alignSelf: 'flex-end',
      }}
      disabled={ShipmentForm?.values?.products.length === 1 ? true : false}
      onPress={() => cancel(index)}
      >
      <Text
        style={{
          fontSize: scale(14),
          color: props.enable ? '#D6D6D6' : '#0093E9',
          fontWeight: 'bold',
        }}>
        {t('Cancel')}
      </Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={{
        marginTop: verticalScale(5),
        width: scale(90),
        height: scale(35),
        borderRadius: 8,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        alignSelf: 'flex-end',
      }}
      onPress={() => onOpneScanner(index)}>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        colors={['#0093E9', '#36C2CF']}
        style={{
          marginTop: verticalScale(45),
          width: scale(90),
          height: scale(35),
          borderRadius: 8,
          borderWidth: 1,
          borderColor: '#0093E9',
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          alignSelf: 'flex-end',
        }}>
        <Image
          style={{ width: scale(15), height: scale(15), tintColor: 'white' }}
          source={require('../../assets/Scan.png')}
        />
        <Text
          style={{
            fontSize: scale(14),
            color: '#FFFFFF',
            fontWeight: 'bold',
            marginLeft: 5
          }}>
          {t('SCAN')}
        </Text>
      </LinearGradient>
    </TouchableOpacity>
    </View>:null}
         {/* <CustomScanner
          enable={ShipmentForm?.values?.products[index]?.productID} 
          updatePRoduct={updateScannedProduct}
          index={index}
          products={ShipmentForm?.values?.products}
          /> */}
        </View>

      })}
       
    </View>)
  }
  const onChangeProductType = (item, i) => {
    ShipmentForm.handleChange({ target: { name: `products[${i}].productCategory`, value: item?.value } })
    ShipmentForm.handleChange({ target: { name: `products[${i}].productID`, value: '' } })
    ShipmentForm.handleChange({ target: { name: `products[${i}].productName`, value: '' } })
    ShipmentForm.handleChange({ target: { name: `products[${i}].manufacturer`, value: '' } })
    ShipmentForm.handleChange({ target: { name: `products[${i}].productQuantity`, value: '' } })
    ShipmentForm.handleChange({ target: { name: `products[${i}].MaxQty`, value: ''} })
    console.log('ProductNameList '+JSON.stringify(TotalProductNameList))
    const getProducts = TotalProductNameList.filter(each => {
      if (each.productCategory == item?.value) {
        return each
      }
    })
    console.log('ProductNameList getProducts'+JSON.stringify(getProducts))

    setProductNameList(getProducts)
  };
  const onChangeProductName = (item, i) => {
    ShipmentForm.handleChange({ target: { name: `products[${i}].productID`, value: item?.value } })
    ShipmentForm.handleChange({ target: { name: `products[${i}].productName`, value: item.label } })
    ShipmentForm.handleChange({ target: { name: `products[${i}].manufacturer`, value: item.eachItem.manufacturer } })
    ShipmentForm.handleChange({ target: { name: `products[${i}].productQuantity`, value: item.eachItem.productQuantity } })
    ShipmentForm.handleChange({ target: { name: `products[${i}].MaxQty`, value: item.eachItem.productQuantity } })

    ShipmentForm.handleChange({
      target: {
        name: `products[${i}].unitofMeasure`, value: item.eachItem.unitofMeasure
      }
    })

  };
  const onChangeQty = (val, i) => {
    if (ShipmentForm?.values?.products[i]?.MaxQty >= Number(val)) {
      ShipmentForm.handleChange({ target: { name: `products[${i}].productQuantity`, value: Number(val) } })

      
    } else {
      ToastShow(
        t('Quantity cannot be more than Stock quantity ' + ShipmentForm?.values?.products[i]?.MaxQty),
        'error',
        'long',
        'top',
      )
    }

  }
  const onChangeSerialNo = (val, i) => {
    if (!/[^a-zA-Z0-9-]/.test(val)) {
      ShipmentForm.handleChange({ target: { name: `products[${i}].serialNumbersRange`, value: [val] } })
    } else {
      ShipmentForm.handleChange({ target: { name: `products[${i}].serialNumbersRange`, value: [''] } })

      ToastShow(
        t('Special charactets not allowed.'),
         'error',
         'long',
         'top',
       )
    }
  }
  const onChangeMfg = (val, i) => {
    ShipmentForm.handleChange({ target: { name: `products[${i}].mfgDate`, value: val } })
  }
  const onChangeExp = (val, i) => {
    ShipmentForm.handleChange({ target: { name: `products[${i}].expDate`, value: val } })
  }
  const updateScannedProduct = (val, i) => {

    ShipmentForm.handleChange({ target: { name: `products`, value: val } })
  }
  const fetchBatchNo = async (index, id) => {
    setSelectedProductIndex(index)
    await fetchBatchData(id)
    setBAtchModal(true)
  };
  const updateMoreValue = index => {
    const productClone = JSON.parse(JSON.stringify(ShipmentForm?.values?.products));
    productClone[index].isMoreData = true;
    ShipmentForm.setValues({
      ...ShipmentForm.values,
      products: productClone,
    });
  };
  const fetchBatchData = async (id) => {
    const url = `${config().fetchBatchesOfInventory}` + id + '&wareId=' + ShipmentForm?.values?.fromOrgLocid;

    // console.log('BatchPoup url ' + JSON.stringify(url));

    const response = await axios.get(url);
    // console.log('response ' + JSON.stringify(response));

    const result = response.data && response.data.data || [];
    setBatchDetails(result)
  }
  const updateProducts = async (products) => {
    ShipmentForm.handleChange({ target: { name: `products`, value: products } })

  };
  const onOpneScanner=(i)=> {
    setCurrentIndx(i)
    setOpneScanner(true)
    console.log('setCurrentIndx '+i)
  }
  const onBarcodeScan =async(qrvalue) =>{
    let products = ShipmentForm.values.products
    console.log('qrvalue' + qrvalue);
    let baseUrl = 'http://';
    qrvalue = qrvalue.replace(baseUrl, '');
    //called after te successful scanning of QRCode/Barcode

    let newIndex = currentIndx;
    console.log('setCurrentIndx newIndex '+newIndex)

    // console.log('onBarcodeScan result baseUrl' + qrvalue);
    // console.log('onBarcodeScan products' + JSON.stringify(products));

    // qrvalue = qrvalue.replaceAll('\u001d', '')
    qrvalue = replaceAll(qrvalue, '\u001d', '');
    console.log('onBarcodeScan result qrvalue ' + qrvalue);
    Vibration.vibrate(30);

    let comp = qrvalue.startsWith('01');
    if (comp) {
      let compId = qrvalue.slice(2, 16);
      let rem1 = qrvalue.slice(16);
      let check = rem1.substr(0, 2);
      let expdateId = '';
      let serialId = '';
      let rem3;
      let rem2;
      let batch = '';
      let check2;
      let check3;
      let MM;
      let YYYY;
      let expD;
      if (check === '17') {
        expdateId = rem1.slice(2, 8);
        rem2 = rem1.slice(8);
        check2 = rem2.substr(0, 2);
        if (check2 === '21') {
          serialId = rem2.slice(2, 22);
          rem3 = rem2.slice(22);
          check3 = rem3.substr(0, 2);
          console.log('onBarcodeScan result check3 ' + rem3);
          batch = rem3.slice(2);
          if (check3 === '10') {
            batch = rem3.slice(2);
            // console.log('onBarcodeScan result check3 10 ' + batch);
          }
        } else if (check2 === '10') {
          batch = rem3.slice(2, 7);
          rem3 = rem2.slice(7);
          check3 = rem3.substr(0, 2);
          if (check3 === '21') {
            serialId = rem3.slice(2);
          }
        }
      } else if (check === '21') {
        serialId = rem1.slice(2, 22);
        rem2 = rem1.slice(22);
        check2 = rem2.substr(0, 2);
        if (check2 === '17') {
          expdateId = rem2.slice(2, 8);
          rem3 = rem2.slice(8);
          check3 = rem3.substr(0, 2);
          batch = rem3.slice(2);
          if (check3 === '10') {
            batch = rem3.slice(2);
          }
        } else if (check2 === '10') {
          batch = rem3.slice(2, 7);
          rem3 = rem2.slice(7);
          check3 = rem3.substr(0, 2);
          expdateId = rem3.slice(2);
          if (check3 === '17') {
            expdateId = rem3.slice(2);
          }
        }
      }
      if (expdateId) {
        YYYY = expdateId.slice(0, 2);
        YYYY = '20' + YYYY;
        MM = expdateId.slice(2, 4);
        expD = MM + '/' + YYYY;
      }
      console.log('company id ' + compId);
      console.log('expire date ' + expdateId);
      console.log('serialId  ' + serialId);
      console.log('batch ' + batch);
      let checkBatch = false;
      let index = newIndex;
      if (batch) {
        for (let j = 0; j < products.length; j++) {
          console.log('products[j].batchNumber ' + products[j].batchNumber);
          if (products[j].batchNumber && (products[j].batchNumber === batch)) {
            console.log('find batch number ' + products[j].batchNumber);
            checkBatch = true;
            index = j;
          } else {
          }
        }
      }
      console.log('index ' + index);
      if (checkBatch) {
        if (products[index].batchNumber === batch) {
          let existSerialNo = products[index].serialNumbersRange;
          let newSerial = [serialId];
          let newSerialNumlist = [...existSerialNo, ...newSerial];
          newSerialNumlist = _.uniqBy(newSerialNumlist, function(x) {
            return x;
          });
          products[index].serialNumbersRange = newSerialNumlist;
          products[index].quantity = newSerialNumlist.length;
          products[index].productQuantity = newSerialNumlist.length;
          console.log('products 1st ' + JSON.stringify(products));
          updateProducts(products)
        } else if (products[index].batchNumber !== batch) {
          let newList = [];
          let addData = {
            productCategory: products[index].productCategory,
            productID: products[index].productID,
            productName: products[index].productName,
            manufacturer: products[index].manufacturer,
            batchNumber: products[index].batchNumber,
            quantity: products[index].batchNumber,
            serialNumbersRange: [serialId],
            expDate: expD,
          };
          newList.push(addData);
          let newDataList = [...products, ...newList];
          let nindx = index + 1;
          console.log('products 2nd ' + JSON.stringify(products));
          updateProducts(newDataList)
          setCurrentIndx(nindx)

        } else {
          products[index].quantity = '1';
          products[index].productQuantity = '1';
          products[index].batchNumber = batch;
          products[index].serialNumbersRange = [serialId];
          products[index].expDate = expD;
          console.log('products 3rd ' + JSON.stringify(products));

          updateProducts(products)
        }
      } else {
        if (products[index].batchNumber != null) {
          index = newIndex + 1;
        }
        console.log('new index ' + index);
        // console.log('products 5th+1 ' + JSON.stringify(products[index].productID))
        if (index === 0) {
          products[index].quantity = '1';
          products[index].productQuantity = '1';
          products[index].batchNumber = batch;
          products[index].expDate = expD;
          products[index].serialNumbersRange = [serialId];
          console.log('products 4th ' + JSON.stringify(products));
          updateProducts(products)

        } else {
          // console.log('products 5th-1 ' + JSON.stringify(products[index - 1].productID))

          if (products[index] && products[index].productID) {
            products[index].quantity = '1';
            products[index].productQuantity = '1';
            products[index].batchNumber = batch;
            products[index].expDate = expD;
            products[index].serialNumbersRange = [serialId];
            console.log('products 5th ' + JSON.stringify(products));
          } else {
            let newList = [];
            let addData = {
              productCategory: products[index - 1].productCategory,
              productID: products[index - 1].productID,
              productName: products[index - 1].productName,
              manufacturer: products[index - 1].manufacturer,
              batchNumber: batch,
              quantity: '1',
              expDate: expD,
              productQuantity: '1',
              serialNumbersRange: [serialId],
            };
            newList.push(addData);
            let newDataList = [...products, ...newList];
            let nindx = index;
            console.log('products 6th ' + JSON.stringify(products));
            updateProducts(newDataList)
            setCurrentIndx(nindx)
          }
        }
      }
    } else {
      setOpneScanner(false)
      alert(t('Invalid QR code'));
    }
  
  }
  if(!opneScanner){
  return (
    <View style={styles.container}>
      <HeaderWithBackBlue
        name={t('create_shipment')}
        navigation={props.navigation}
      />
              <KeyboardAwareScrollView extraScrollHeight={50} style={{ marginTop: verticalScale(-DeviceWidth / 5) }} >

      <View >

          {ShipmentFromOrderComponent()}
          {ShipmentFromLocComponent()}
          {ShipmentToLocComponent()}
          {DeliveryDetailsComponent()}
          {/* <View
            style={{
              marginLeft: verticalScale(15),

            }}>
            <Text style={{ fontWeight: 'bold' }}>
              {t('Product')}
            </Text>
          </View> */}
          {/* {ProductNameList.length ? ProductListComponent() : null} */}
          {ProductListComponent()}
          {ProductNameList.length ? ShipmentForm?.values?.shippingOrderId ? null : <TouchableOpacity
            style={{
              borderRadius: 10,
              flexDirection: 'row',
              marginLeft: 10,
            }}
            onPress={AddAnotherProduct}>
            <View style={{
              backgroundColor: '#FFFFFF',
              borderRadius: 5,
              marginLeft: scale(5),
              padding: 5,
              elevation: 5,
              paddingLeft: scale(10),
              paddingRight: scale(10)
            }}>
              <Text
                style={{
                  fontSize: scale(12),
                  color: '#0093E9',
                  fontWeight: 'bold',
                }}>
                {t('+ Add Another Product')}
              </Text>
            </View>
          </TouchableOpacity> : null}
          </View>
          <View
            style={{
              flexDirection: 'row',
              // margin: verticalScale(15),
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
              marginTop: scale(20)
            }}>
            <TouchableOpacity
              style={{
                height: scale(40),
                backgroundColor: '#FFFFFF',
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#0093E9',
                borderWidth: 1,
                flex: 0.3,
                margin: 15,
              }}
              onPress={() => props.navigation.goBack()}>
              <Text
                style={{
                  marginLeft: scale(13),
                  fontSize: scale(16),
                  fontWeight: 'bold',
                  color: '#0093E9',
                  alignSelf: 'center'
                }}>
                {t('Cancel')}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{

                backgroundColor: '#FF4500',
                flexDirection: 'row',
                height: scale(40),
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#FF4500',
                borderWidth: 1,
                flex: 0.7,
                margin: 15,

              }}
              onPress={() => _CreateShipment()}>
              <Image
                style={{
                  marginLeft: scale(10),
                  width: scale(18.73),
                  height: scale(18.73),
                  tintColor: 'white',
                  resizeMode: 'contain',
                }}
                source={require('../../assets/CreateShipment2.png')}
              />
              <Text
                style={{
                  marginLeft: scale(13),
                  fontSize: scale(16),
                  color: '#FFFFFF',
                  fontWeight: 'bold',
                }}>
                {t('create_shipment')}
              </Text>
            </TouchableOpacity>
          </View>
          </KeyboardAwareScrollView>

         
          <BatchPoup
            isModalVisible={isBatchModal}
            text={t('Success!')}
            label={t('Cancel')}
            selectedIndx={selectedProductIndex}
            selectedProductData={ShipmentForm?.values?.products[selectedProductIndex]}
            updateState={updateProducts}
            list={ShipmentForm?.values?.products}
            batchDetails={batchDetails}
            Ok={() => setBAtchModal(false)}
          />
          <View style={{ marginBottom: verticalScale(25) }} />


    </View>
  );
              }
  return (
    <View style={{backgroundColor: 'black', flex: 1}}>
      {/* <ScannerHeaderBack
              name={'Scan'}
              back={() => this.setState({opneScanner: false})}
              /> */}
      <CameraScreen
        actions={{
          leftButtonText: (
            <Icon name="close" style={{color: 'white', fontSize: 70}} />
          ),
          rightButtonText: (
            <Icon name="images" style={{color: 'white', fontSize: 40}} />
          ),
        }}
        style={{height: Platform.OS === 'ios' ? DeviceHeight / 1.2 : '1%'}}
        onBottomButtonPressed={event =>
          setOpneScanner(false)
        }
        showFrame={true}
        //Show/hide scan frame
        scanBarcode={true}
        //Can restrict for the QR Code only
        laserColor={'green'}
        //Color can be of your choice
        frameColor={'green'}
        //If frame is visible then frame color
        colorForScannerFrame={'green'}
        //Scanner Frame color
        onReadCode={event =>
          onBarcodeScan(event.nativeEvent.codeStringValue)
        }
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    loder: state.loder,
    ShippingOrderIds: state.shipment.shipmentOrderids,
    shipmentOrderDetails: state.shipment.shipmentOrderDetails,
    organizations: state.productdetail.Organizations,
    purchaseorderids: state.purchaseorder.purchaseorderids,
    OrgTypeList: state.userinfo.OrgTypeList,
    filterOutBound: state.shipment.filterOutBound,
    filterInbound: state.shipment.filterInbound,
  };
}
export default connect(mapStateToProps, {
  getProductNames,
  fetchOrganizations,
  getOrganizationsByType,
  getWarehouseByOrgId,
  getViewShipment,
  fetch_Purchase_Orders_ids,
  getProductsByInventory,
  getShipmentAnalytics,
  fetchInboundShipments,
  fetchOutboundShipments,
  create_new_shipment
},
)(CreateShipment);
