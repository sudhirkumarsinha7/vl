import React, { useState,useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  ActivityIndicator,
} from 'react-native';
import Header, {
  HeaderWithBack,
} from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  CustomDropDownFilter,
} from '../../components/Common/common';
import {
  fetchSupplierAndReceiverList,
  InboundFilter,
  fetchInboundShipments,
  fetchOutboundShipments,
  outboundFilter,
  fetchShipmentIds,
} from '../../Redux/Action/shipment';


import _ from 'lodash';

import { FilterByDate, CustomDateRange } from '../../components/Common/Filter';
import { Colors, DeviceWidth } from '../../Style';
import { useTranslation } from 'react-i18next';

export const STATUS = [
  { label: 'SHIPPED', id: 'CREATED' },
  { label: 'DELIVERED', id: 'RECEIVED' },
];
export const STATUS1 = [
  { label: 'ENVIADA', id: 'CREATED' },
  { label: 'ENTREGADA', id: 'RECEIVED' },
];
const ShipmentFilter = (props) => {

  const selectedTab = props.route.params.selectedTab

  const { t, i18n } = useTranslation();
  const [shipmentId, setShipmentId] = useState('')
  const [dateFilter, setDateFilter] = useState('')
  const [status, setStatus] = useState('')
  const [statusId, setStatusId] = useState('')
  const [from, setFrom] = useState('')
  const [toId, setToID] = useState('')
  const [to, setTo] = useState('')
  const [fromDate, setFromDate] = useState('')
  const [toDate, setToDate] = useState('')
  const [fromId, setFromId] = useState('')

  useEffect(() => {
    async function fetchData() {
      await props.fetchShipmentIds();
      await props.fetchSupplierAndReceiverList();
      let { filterInbound, filterOutBound } = props;
      if (selectedTab === 'Inbound') {
        setFromId(filterInbound.shipmentId || '')
        setToID(filterInbound.toId || '')
        setFrom(filterInbound.from || orderSentFilter.fromId || '')
        setTo(filterInbound.to || orderSentFilter.toId || '')
        setShipmentId(filterInbound.orderId || '')
        setDateFilter(filterInbound.dateFilter || '')
        setStatusId(filterInbound.statusId || '')
        setStatus(filterInbound.status || '')
        setFromDate(filterInbound.fromDate || '')
        setToDate(filterInbound.toDate || '')
      } else {
        setFromId(filterOutBound.fromId || '')
        setToID(filterOutBound.toId || '')
        setFrom(filterOutBound.from || filterOutBound.fromId || '')
        setTo(filterOutBound.to || filterOutBound.toId || '')
        setShipmentId(filterOutBound.orderId || '')
        setDateFilter(filterOutBound.dateFilter || '')
        setStatusId(filterOutBound.statusId || '')
        setStatus(filterOutBound.status || '')
        setFromDate(filterOutBound.fromDate || '')
        setToDate(filterOutBound.toDate || '')
      }

    }
    fetchData();
  }, []);


  const onFromChange = (item) => {
    setFrom(item.value)
    setFromId(item.value)


  }
  const onShipmentChange = (item) => {
    setShipmentId(item.value)

  }
  const onToChange = (item) => {
    setTo(item.value)
    setToID(item.value)

  }
  const onStatusChange = (item) => {
    setStatus(item.value)
    setStatusId(item.value)
  }
  const onChangeDateRange = (val) => {
    setFromDate(val.startDate)
    setToDate(val.endDate)

  }
  const onChangeDateFilter = (val) => {
    setDateFilter(val)
  }
  const apply = async () => {
    let Filter = {
      dateFilter: dateFilter || '',
      status: status || '',
      statusId: statusId || '',
      shipmentId: shipmentId || '',
      fromId: fromId || '',
      toId: toId || '',
      from: from || '',
      to: to || '',
      fromDate: fromDate || '',
      toDate: toDate || '',
    };
    if (selectedTab === 'Inbound') {
      props.InboundFilter(Filter);
      await props.fetchInboundShipments(0, 50, Filter);
    } else {
      props.outboundFilter(Filter);
      await props.fetchOutboundShipments(0, 50, Filter);
    }
    props.navigation.goBack();
  }

  const clearALL = () => {
    let { filterInbound, filterOutBound } = props;
    let Filter = {
      shipmentId: '',
      dateFilter: '',
      status: '',
      from: '',
      to: '',
      statusId: '',
      fromId: '',
      toId: '',
      fromDate: '',
      toDate: '',
    };
    setFromId('')
    setToID('')
    setFrom('')
    setTo('')
    setShipmentId('')
    setDateFilter('')
    setStatusId('')
    setStatus('')
    setFromDate('')
    setToDate('')
    if (selectedTab === 'Inbound') {
      props.InboundFilter(Filter);
      props.fetchInboundShipments(0, 50, Filter);
    } else {
      props.outboundFilter(Filter);
      props.fetchOutboundShipments(0, 50, Filter);
    }
  }


  let { ShipmentIDList = [], userLang } = props;
  return (
    <View
      style={{
        flex: 1,
      }}>
      <StatusBar backgroundColor="#0093E9" />
      <HeaderWithBack
        name={t('Filter')}
        navigation={props.navigation}
        isfilter={true}
        onPress={clearALL}
        clearFilter={t('Clear Filter')}
      />

      <ScrollView
        nestedScrollEnabled={true}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{ marginTop: verticalScale(-DeviceWidth / 7) }}>
        <View
          style={{
            // backgroundColor: '#FFFFFF',
            margin: verticalScale(10),
            borderRadius: 10,
            padding: 5,
            // height: scale(340),
          }}>


          <CustomDropDownFilter
            displayName={'From'}
            stateName={fromId}
            image={require('../../assets/from_filter.png')}
            dropDowndata={props.supplierAndReceivers}
            label={'name'}
            mapKey={'id'}
            onChangeValue={onFromChange}
            t={t}
          />
          <CustomDropDownFilter
            displayName={'To'}
            stateName={toId}
            image={require('../../assets/from_filter.png')}
            dropDowndata={props.supplierAndReceivers}
            label={'name'}
            mapKey={'id'}
            onChangeValue={onToChange}
            t={t}
          />
          <CustomDropDownFilter

            displayName={'Shipment ID'}
            stateName={shipmentId}
            image={require('../../assets/Inventory1.png')}
            dropDowndata={ShipmentIDList}
            label={'id'}
            mapKey={'id'}
            onChangeValue={onShipmentChange}
            t={t}
          />
          <CustomDropDownFilter
            displayName={'Status'}
            stateName={status}
            image={require('../../assets/status.png')}
            dropDowndata={userLang === 'es' ? STATUS1 : STATUS}
            label={'label'}
            mapKey={'id'}
            onChangeValue={onStatusChange}
            search={false}
            t={t}
          />

          <CustomDateRange
            displayName={'Order Date'}
            fromDate={fromDate}
            toDate={toDate}
            onChangeValue={onChangeDateRange}
            image={require('../../assets/calendar.png')}

          />
          <FilterByDate dateFilter={dateFilter} onChangeValue={onChangeDateFilter} />
        </View>
      </ScrollView>
      {props.loder ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <ActivityIndicator color="#0000ff" size={'large'} />
        </View>
      ) : null}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          // marginTop: 10,
          alignContent: 'center',
          marginBottom: 20,
          width: scale(328),
          bottom: 0,
          // position: 'absolute',
        }}>
        <TouchableOpacity
          style={{
            padding: 8,
            borderRadius: 10,
            borderWidth: 1,
            borderColor: '#58b1da',
            height: scale(45),
            flexDirection: 'row',
            alignItems: 'center',
            width: '30%',
            justifyContent: 'center',
            marginRight: 10,
          }}
          onPress={() => props.navigation.goBack()}>
          <Text
            style={{
              color: '#58b1da',
              fontSize: 16,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {'  '}
            {t('Cancel')}{' '}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            height: scale(45),
            borderRadius: 10,
            backgroundColor: Colors.blueE9,
            flexDirection: 'row',
            padding: 8,
            justifyContent: 'space-evenly',
            width: '50%',
          }}
          onPress={() => apply()}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{ width: scale(12.35), height: scale(12.68) }}
              source={require('../../assets/Filter.png')}
            />
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: scale(13),
                color: '#FFFFFF',
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              {t('Apply Filter')}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    supplierAndReceivers: state.shipment.supplierAndReceivers,
    filterOutBound: state.shipment.filterOutBound,
    filterInbound: state.shipment.filterInbound,
    ShipmentIDList: state.shipment.ShipmentIDList,
    userLang: state.userinfo.userLang,
  };
}

export default connect(
  mapStateToProps,
  {
    fetchSupplierAndReceiverList,
    InboundFilter,
    fetchInboundShipments,
    fetchOutboundShipments,
    outboundFilter,
    fetchShipmentIds,
  },
)(ShipmentFilter);
