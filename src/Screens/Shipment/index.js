import React, { useState, useEffect } from 'react';
import {
  LayoutAnimation,
  UIManager,
  Platform,
  Button,
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  ActivityIndicator,
  FlatList,
  PermissionsAndroid

} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  userinfo,
  trackJourney,
} from '../../Redux/Action/order';
import {
  getShipmentAnalytics
} from '../../Redux/Action/analytics';
import {
  fetchInboundShipments,
  fetchOutboundShipments,
  getProductNames,
  getOrganizationsByType,
  getWarehouseByOrgId,
  getchainOfCustody,
  getViewShipment,
  fetchOrganizations
} from '../../Redux/Action/shipment';
import {
  getAlertNotifications,
  getTransactionNotifications
} from '../../Redux/Action/alert';
import Header from '../../components/Header';
import Card from '../../components/Card';
import Empty_Card from '../../components/Empty_Card';
import Section_Select from '../../components/Section_Select';
import { InboundList } from '../../components/Common/shipmentHelper';
import _ from 'lodash';
import { compareValues, NetworkUtils } from '../../Util/utils';
import { Colors, DeviceWidth } from '../../Style';
import { CustomButton } from '../../components/Common/common';
import { useTranslation } from 'react-i18next';
import { queryAllorder, queryDeleteAllOrder } from '../../databases/allSchemas'
import { ToastShow } from '../../components/Toast';
import Search from '../../components/Search'
import { NavigationContainer, useIsFocused } from '@react-navigation/native';

const radioItems = [
  {
    label: 'Inbound',
    selected: false,
  },
  {
    label: 'Outbound',
    selected: true,
  },]
const ShipmentScreen = (props) => {
  const { t, i18n } = useTranslation();
  const [selectedItem, setTab] = useState('Outbound');
  const isFocused = useIsFocused();

  useEffect(() => {
    async function fetchData() {
      GetData();
      requestCameraPermission();
    }
    fetchData();
  }, [isFocused]);
  const GetData = async () => {
    radioItems.map(item => {
      if (item.selected == true) {
        setTab(item.label)
      }
    });
    await props.userinfo();
    props.getShipmentAnalytics();
    props.fetchInboundShipments(0, 50, props.filterInbound);
    props.fetchOutboundShipments(0, 50, props.filterOutBound);
    props.getAlertNotifications();
    props.getTransactionNotifications();
    props.getProductNames();
    props.getOrganizationsByType(
      props.user && props.user.configuration_id || global.defaultConfigID,
    );
    props.fetchOrganizations()
    const orgSplit = props.user.organisation?.split('/');
    const ownOrg = orgSplit[0];
    const orgId = orgSplit[1];
    const res = await props.getWarehouseByOrgId(orgId);
    // const location = await getCurrenntLocation();
    // console.log('location', location);
  };
  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "App Camera Permission",
          message:"App needs access to your camera ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Camera permission given");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });
    radioItems[index].selected = true;
    const label = radioItems[index].label
    console.log('radioItems[index].label ' + label)
    setTab(label)
  }
  let {
    userpermission = {},
    shipmentAnalytics,
    inboundShipments,
    outboundShipments,
    filterInbound,
    filterOutBound
  } = props;
  let { shipment = {}, track = {} } = userpermission;
  let newInBoundFilter = filterInbound
  let newOutBoundFilter = filterOutBound
  delete newInBoundFilter.from;
  delete newInBoundFilter.to;
  delete newOutBoundFilter.from;
  delete newOutBoundFilter.to;
  let inboundFilterList = [];
  let outBoundFilterList = [];
  inboundFilterList = Object.keys(newInBoundFilter).map((key) => { if (newInBoundFilter[key]) { if (key != 'status') return [key, newInBoundFilter[key]] } });
  outBoundFilterList = Object.keys(newOutBoundFilter).map((key) => { if (newOutBoundFilter[key]) { if (key != 'status') return [key, filterOutBound[key]] } });
  let inBoundcount = inboundFilterList.filter(each => {
    if (each) {
      return each
    }
  })
  let outBoundcount = outBoundFilterList.filter(each => {
    if (each) {
      return each
    }
  })
  let count = 0;
  count = selectedItem === 'Inbound' ? inBoundcount.length : outBoundcount.length

  // console.log('userpermission userpermission' + JSON.stringify(userpermission))
  // console.log('inboundFilter filterInbound' + JSON.stringify(inboundFilterList))

  inboundShipments = _.uniqBy(inboundShipments, function (x) {
    return x.id;
  });
  outboundShipments = _.uniqBy(outboundShipments, function (x) {
    return x.id;
  });
  inboundShipments = inboundShipments.sort(
    compareValues('createdAt', 'desc'),
  );
  outboundShipments = outboundShipments.sort(
    compareValues('createdAt', 'desc'),
  );
  const ShipmentCard = () => {
    return (
      <View>
        <ScrollView
          horizontal={true}
          contentContainerStyle={{
            margin: verticalScale(10),
          }}
          showsHorizontalScrollIndicator={true}>
          <Card
            image={require('../../assets/inbound.png')}
            bgcolor={Colors.blue96}
            textcolor={Colors.blueAE}
            nextScreen={() => changeActiveRadioButton(0)}
            isDisable={false}
            quantity={shipmentAnalytics.inboundShipments || 0}
            card_name={t('total_inbound_shipments')}
          />
          <View>
            <Text>{'  '}</Text>
          </View>
          <Card
            image={require('../../assets/outbound.png')}
            bgcolor={Colors.orange1D}
            textcolor={Colors.red23}
            nextScreen={() => changeActiveRadioButton(1)}
            isDisable={false}
            quantity={shipmentAnalytics.outboundShipments || 0}
            card_name={t('total_outbound_shipments')}
          />
          <View>
            <Text>{'  '}</Text>
          </View>
          <Card
            image={require('../../assets/inboundAlert.png')}
            bgcolor={Colors.blueF0}
            textcolor={Colors.blueE9}
            quantity={shipmentAnalytics.inboundAlerts || 0}
            card_name={t('total_inbound_alerts')}
          />
          <View>
            <Text>{'  '}</Text>
          </View>
          <Card
            image={require('../../assets/outboundAlert.png')}
            bgcolor={'#0A69C5'}
            textcolor={Colors.blueEA}
            quantity={shipmentAnalytics.outboundAlerts || 0}
            card_name={t('total_outbound_alerts')}
          />
          <View style={{ marginRight: 20 }}>
            <Text>{'  '}</Text>
          </View>

        </ScrollView>
      </View>)
  }
  const ActionButton = () => {
    return <View
      style={{
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: verticalScale(5),
      }}>
      {shipment.createShipment ?
        <CustomButton
          img={require('../../assets/CreateShipment2.png')}
          backgroundColor={Colors.orange1D}
          imgWidth={scale(22.73)}
          label1={t('CREATE')}
          label2={t('SHIPMENTCAP')}
          Press={() => props.navigation.navigate('ShipmentOption')}
        /> : null}
      <CustomButton
        img={require('../../assets/receive.png')}
        backgroundColor={'#0093E9'}
        label1={t('RECEIVE')}
        label2={t('SHIPMENTCAP')}
        Press={
          () =>
            props.navigation.navigate('ReceiveShipment', {
              optionName: 'shipment',
              shipmentId: null,
            })
        }
      />
      {shipment.updateShipment ? <CustomButton
        backgroundColor={'#FA7923'}
        img={require('../../assets/update.png')}
        label1={t('UPDATE')}
        label2={t('SHIPMENTCAP')}
        Press={
          () =>
            props.navigation.navigate('UpdateShipment', {
              shipmentDetails: null,
            })
        }
      /> : null}
    </View>
  }
  const FilterDesign = () => {
    return (<View
      style={{
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: verticalScale(10),
        marginRight: scale(10)
      }}>
      <Text
        style={{
          fontSize: scale(16),
          color: '#0093E9',
          fontWeight: 'bold',
        }}>
        {t('Recent Shipments')}
      </Text>
      {shipment.shipmentFilters ? <TouchableOpacity
        style={{
          borderRadius: 10,
          padding: scale(7),
          paddingLeft: scale(10),
          paddingRight: scale(10),
          backgroundColor: Colors.blueCF,
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}
        onPress={() => props.navigation.navigate('FilterShipment', { selectedTab: selectedItem })}>
        <Image
          style={{ width: scale(9.35), height: scale(11.68) }}
          source={require('../../assets/Filter.png')}
        />
        <Text
          style={{
            fontSize: scale(12),
            color: '#FFFFFF',
            fontWeight: '900',
            marginLeft: scale(5),
            marginRight: scale(5)
          }}>
          {count ? t('Filter') + '(' + count + ')' : t('Filter')}
        </Text>
        <Image
          style={{ width: scale(10.01), height: scale(5.72), }}
          resizeMode="cover"
          source={require('../../assets/downarrow.png')}
        />
      </TouchableOpacity> : null}
    </View>)
  }

  const getTrackDetails = async (item, id) => {
    await props.trackJourney(id);
    props.navigation.navigate('Track', { screen: 'TrackDeatils' });

  };
  const getDetails = async (item, id) => {
    const isConnected = await NetworkUtils.isNetworkAvailable()

    if (isConnected) {
      props.getchainOfCustody(id);
      props.getViewShipment(id);
    } else {
      props.getShipmentFromList(item)
    }
    // console.log('getDetails item' + JSON.stringify(item));

    props.navigation.navigate('ViewShipment', { item });
  };
  const eachBound = item => {
    return (
      <InboundList
        item={item}
        onClickFunction={() => getTrackDetails(item, item.id)}
        onView={() => getDetails(item, item.id)}
        viewShipment={shipment.viewShipment}
        trackPermission={track}
      />
    );
  };
  const loadMore = async () => {
    //Call the Service to get the latest data
    // inboundShipments = _.uniqBy(inboundShipments, function (x) {
    //   return x.id;
    // });
    // outboundShipments = _.uniqBy(outboundShipments, function (x) {
    //   return x.id;
    // });
    if (inboundShipments.length > 50) {
      props.fetchInboundShipments(0, 50, props.filterInbound);
    } else {
      props.fetchInboundShipments(
        inboundShipments.length,
        inboundShipments.length + 50,
        props.filterInbound,
      );
    }
    if (outboundShipments.length > 50) {
      props.fetchOutboundShipments(0, 50, props.filterOutBound);
    } else {
      props.fetchOutboundShipments(
        outboundShipments.length,
        outboundShipments.length + 100,
        props.filterOutBound,
      );
    }
  };
  const ShipmnetScroll = () => {
    return <View>
      {shipment.shipmentAnalytics ? ShipmentCard() : null}
      {ActionButton()}
      {FilterDesign()}
      <View style={{ flexDirection: 'row' }}>
        {radioItems.map((item, key) => (
          <Section_Select
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
          />
        ))}
        <View style={{ height: scale(10) }} />

      </View>
      {selectedItem === 'Inbound' ? (
        inboundShipments.length === 0 ? (
          <View style={{ marginTop: verticalScale(0) }}>
            <Empty_Card Text="No Inbound Yet. " />
          </View>
        ) : (
          shipment.inboundShipments ? (<View>
            <FlatList
              onEndReachedThreshold={0.7}
              style={{ width: '100%' }}
              keyExtractor={(item, index) => index.toString()}
              data={inboundShipments}
              renderItem={({ item }) => eachBound(item)}
            />
            {props.inboundCount != inboundShipments.length ? (
              <TouchableOpacity
                style={{ justifyContent: 'center', alignItems: 'center' }}
                onPress={() => loadMore()}>
                <Text
                  style={{
                    justifyContent: 'center',
                    backgroundColor: '#0093E9',
                    alignItems: 'center',
                    color: '#ffffff',
                    padding: 10,
                    borderRadius: 10,
                    fontWeight: 'bold',
                  }}>
                  {t('load more')}

                </Text>
              </TouchableOpacity>
            ) : null}
          </View>) : (
            <View style={{ marginTop: verticalScale(0) }}>
              <Empty_Card Text="Inbound Permission Denied" />
            </View>
          )
        )
      ) : outboundShipments.length === 0 ? (
        <View style={{ marginTop: verticalScale(0) }}>
          <Empty_Card Text="No Outbounds Yet. " />
        </View>
      ) : (
        shipment.outboundShipments ? (<View>
          <FlatList
            onEndReachedThreshold={0.7}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={outboundShipments}
            renderItem={({ item }) => eachBound(item)}
          />
          {props.outboundCount != outboundShipments.length ? (
            <TouchableOpacity
              style={{ justifyContent: 'center', alignItems: 'center' }}
              onPress={() => loadMore()}>
              <Text
                style={{
                  justifyContent: 'center',
                  backgroundColor: '#0093E9',
                  alignItems: 'center',
                  color: '#ffffff',
                  padding: 10,
                  borderRadius: 10,
                  fontWeight: 'bold',
                }}>
                {t('load more')}
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>) : (
          <View style={{ marginTop: verticalScale(0) }}>
            <Empty_Card Text="Outbound Permission Denied" />
          </View>
        )
      )}
    </View>
  }
  onRefresh = async () => {
   await props.fetchInboundShipments(0, 50,props.filterInbound);
   await props.fetchOutboundShipments(0, 50,props.filterOutBound);
   await props.getShipmentAnalytics();
   props.getAlertNotifications();
   props.getTransactionNotifications();
  };
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#0093E9" />
      <Header navigation={props.navigation}
        name={t('Shipment')}
      // scan={true}
      // onPress={() =>onOpneScanner()} 
      />
      <Search
        navigation={props.navigation}
        fromScreen={'Shipment'} />
      <FlatList
        // onEndReachedThreshold={0.7}
  
        showsVerticalScrollIndicator={false}
        style={{ width: '100%' }}
        keyExtractor={(item, index) => index.toString()}
        data={['test']}
        renderItem={({ item }) => ShipmnetScroll(item)}
        refreshControl={
          <RefreshControl
          refreshing={false}
            onRefresh={()=>onRefresh()}
            tintColor={Colors.blueE9}
            titleColor={Colors.blueE9}
            colors={['#a076e8', '#5dc4dd']}
            progressBackgroundColor={Colors.blueAE}
          />
        }
      />

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    loder: state.loder,
    shipment: state.shipment.shipmentdata,
    inboundShipments: state.shipment.inboundShipments,
    outboundShipments: state.shipment.outboundShipments,
    shipmentAnalytics: state.shipment.shipmentAnalytics,
    filterOutBound: state.shipment.filterOutBound,
    filterInbound: state.shipment.filterInbound,
    inboundCount: state.shipment.inboundCount,
    outboundCount: state.shipment.outboundCount,
    userpermission: state.userinfo.userpermission,
  };
}
export default connect(mapStateToProps, {
  userinfo,
  getShipmentAnalytics,
  fetchInboundShipments,
  fetchOutboundShipments,
  getProductNames,
  getOrganizationsByType,
  getWarehouseByOrgId,
  getAlertNotifications,
  getTransactionNotifications,
  trackJourney,
  getchainOfCustody,
  getViewShipment,
  fetchOrganizations,
},
)(ShipmentScreen);