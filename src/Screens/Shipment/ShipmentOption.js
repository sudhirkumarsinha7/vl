import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { useTranslation } from 'react-i18next';
import { Colors, DeviceWidth } from '../../Style';

const ShipmentOption =(props)=> {

  const { t, i18n } = useTranslation();

    return (
      <View
        style={{
          // flex: 1,
          justifyContent: 'center',
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('create_shipment')}
          navigation={props.navigation}
        />
        <View
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{ marginTop: verticalScale(-DeviceWidth / 7),alignItems: 'center', }}>
          <View
            style={{
              backgroundColor: '#FFFFFF',
              marginTop: verticalScale(10),
              borderRadius: 10,
              padding: 10,
              width: scale(328),
              height: scale(420),

            }}>
            <View
              style={{
                marginTop: 70,
              }}>
              <Text
                style={{
                  fontSize: scale(11),
                  color: '#707070',
                  marginLeft: 10,
                }}>
                {t('To send a bulk shipment')}
              </Text>
              <TouchableOpacity
                style={{
                  marginTop: 10,
                  backgroundColor: '#FA7923',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 15,
                  borderRadius: 15,
                }}
                onPress={() => props.navigation.navigate('CreateShipment')}>
                <Text
                  style={{
                    fontSize: scale(16),
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                  }}>
                  {t('CREATE SHIPMENT')}
                </Text>
              </TouchableOpacity>
            </View>
            <Text
              style={{
                fontSize: scale(16),
                color: '#0093E9',
                fontWeight: 'bold',
                margin: 20,
                marginTop: 40,
                textAlign: 'center',
              }}>
              {t('OR')}
            </Text>
            <View
              style={{
                marginTop: 50,
              }}>
              <Text
                style={{
                  fontSize: scale(11),
                  color: '#707070',
                  marginLeft: 10,
                }}>
                {t('To administer a single Vaccine to a Patient')}
              </Text>
              <TouchableOpacity
                style={{
                  marginTop: 10,
                  backgroundColor: '#FFAB1D',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 15,
                  borderRadius: 15,
                }}
                onPress={() =>
                  props.navigation.navigate('Patient')
                }>
                <Text
                  style={{
                    fontSize: scale(16),
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                  }}>
                  {t('SEND SINGLE VIAL')}
                </Text>
              </TouchableOpacity>
            </View>

          </View>
        </View>
      </View>
    );
}

export default ShipmentOption;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
});
