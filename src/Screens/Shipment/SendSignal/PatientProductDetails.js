
import React, { useState, useEffect } from 'react';
import {

  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
  Image,
  Keyboard
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import { connect } from 'react-redux';
import {
  fetchOrganizations,
  AddNewEOL
} from '../../../Redux/Action/shipment';
import { HeaderWithBackBlue } from '../../../components/Header';
import _ from 'lodash';
import { Colors, DeviceWidth } from '../../../Style';
import { CustomDropdown, CustomInputText, CustomTextView } from '../../../components/Common/common';
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../../components/Toast';
import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';
import { global } from '../../../Util/config'
import { SendSigleVialElement } from '../../../components/Common/defaultValue';

const PatientProductDetails = (props) => {
  const { t, i18n } = useTranslation();
  const Patientdata = props.route.params.Patientdata
  const ProductData = props.route.params.ProductData

  useEffect(() => {
    async function fetchData() {
      await props.fetchOrganizations()
    }
    fetchData();
  }, []);





 
 


  const API_Call = async () => {
    let { user } = props;
    const orgSplit = user.organisation?.split('/');
    const ownOrg = orgSplit[0];
    const orgId = orgSplit[1];
    let organisationDetails = props.Organizations.filter(
      eachOrganisation => {
        if (eachOrganisation.id === orgId) {
          return eachOrganisation;
        }
      },
    );
    let selectedOrg = organisationDetails[0];

    let data = {
      productAdministeredInfo: [
        {
          productId: ProductData.productId,
          productName: ProductData.productName,
          productMfg: ProductData.productMfg,
          productBatchNo: ProductData.batchNumber,
          locationInfo: {
            warehouseId: props.user.warehouseId[0],
            warehouseTitle: props.user.title,
          },
          labelId: ProductData.serialNumber,
          atomId: 'atom_id 2',
          administeredData: new Date(),
          administeredBy: selectedOrg.primaryContactId,
        },
      ],
      eol_id: 'eol' + Math.floor(1000 + Math.random() * 9000),
      eol_info: {
        first_name: Patientdata.name,
        last_name: Patientdata.name,
        gender: Patientdata.gender.gender,
        age: Patientdata.age,
        contact_number: Patientdata.mob,
        contact_address: {
          firstLine: user.warehouseAddress_firstline,
          secondLine: user.location,
          district: user.warehouseAddress_city,
          state: user.warehouseAddress_state,
          country: user.warehouseAddress_country,
          landmark: user.location,
          zipcode: user.warehouseAddress_zipcode,
        },
        idProof: {
          idType: Patientdata.idType,
          idNo: Patientdata.aadhar,
        },
      },
    };
    const result = await props.AddNewEOL(data);
    console.log('AddNewEOL result ' + JSON.stringify(result));
    if (result.status === 200) {
      
      ToastShow(
        t('Patient Details Added successfully !'),
        'success',
        'long',
        'top',
      )
      props.navigation.navigate('Shipment');

    } else {
      ToastShow(
        t(result && result.data && result.data.message),
        'error',
        'long',
        'top',
      )
    }
   

  };
 
  const ProductFromComponent = () => {
    return (<View
      style={{
        backgroundColor: '#FFFFFF',
        margin: verticalScale(10),
        borderRadius: 10,
        padding: 10,
      }}>
      <Text
        style={{
          fontSize: scale(13),
          color: '#000000',
          fontWeight: 'bold',
          marginLeft: 10,
          marginTop: verticalScale(10),
        }}>
        {t('Product Details')}
      </Text>
      <CustomTextView
        leftText={t(SendSigleVialElement.cardView2.productName.displayName)}
        rightText={ProductData.productName}
      />
      <CustomTextView
        leftText={t(SendSigleVialElement.cardView2.batchNumber.displayName)}
        rightText={ProductData.batchNumber}
      />
      <CustomTextView
        leftText={t(SendSigleVialElement.cardView2.serialNumber.displayName)}
        rightText={ProductData.serialNumber}
      />
    </View>)
  }

 const PaitentDetails = ()=>{
  return (<View
    style={{
      backgroundColor: '#FFFFFF',
      margin: verticalScale(10),
      borderRadius: 10,
      padding: 10,
    }}>
    <Text
      style={{
        fontSize: scale(13),
        color: '#000000',
        fontWeight: 'bold',
        marginLeft: 10,
        marginTop: verticalScale(10),
      }}>
      {t('Patient Details')}
    </Text>
    <CustomTextView
      leftText={t(SendSigleVialElement.cardView1.name.displayName)}
      rightText={Patientdata.name}
    />
    <CustomTextView
      leftText={t(SendSigleVialElement.cardView1.gender.displayName)}
      rightText={Patientdata.gender}
    />
    <CustomTextView
      leftText={t(SendSigleVialElement.cardView1.age.displayName)}
      rightText={Patientdata.age}
    />
    <CustomTextView
      leftText={t(SendSigleVialElement.cardView1.idType.displayName)}
      rightText={Patientdata.idType}
    />
    <CustomTextView
      leftText={t(SendSigleVialElement.cardView1.aadhar.displayName)}
      rightText={Patientdata.aadhar}
    />
    <CustomTextView
      leftText={t(SendSigleVialElement.cardView1.mob.displayName)}
      rightText={Patientdata.mob}
    />
  </View>)
 } 
  return (
    <View style={styles.container}>
      <HeaderWithBackBlue
        name={t('Send Single Vial')}
        navigation={props.navigation}
      />
      <View style={{ marginTop: verticalScale(-DeviceWidth / 7) }}>

        <KeyboardAwareScrollView extraScrollHeight={50} >
         {PaitentDetails()}
          {ProductFromComponent()}
          <View
            style={{
              flexDirection: 'row',
              margin: verticalScale(15),
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
            }}>
            <TouchableOpacity
              style={{
                height: scale(40),
                backgroundColor: '#FFFFFF',
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#0093E9',
                borderWidth: 1,
                flex: 0.5,
                margin: 10,
              }}
              onPress={() => props.navigation.goBack()}>
              <Text
                style={{
                  marginLeft: scale(13),
                  fontSize: scale(16),
                  fontWeight: 'bold',
                  color: '#0093E9',
                  alignSelf: 'center'
                }}>
                {t('Cancel')}
              </Text>
            </TouchableOpacity>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 1 }}
              colors={['#0093E9', '#36C2CF']}
              style={{
                borderRadius: 8,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                flex: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  margin: 10,

                }}
                onPress={() => API_Call()}>
                <Text
                  style={{
                    marginLeft: scale(13),
                    fontSize: scale(16),
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                  }}>
                  {t('SEND OTP')}
                </Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <View style={{ marginBottom: verticalScale(DeviceWidth / 2) }} />

        </KeyboardAwareScrollView>
      </View>

    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
function mapStateToProps(state) {
  return {
    Organizations: state.productdetail.Organizations,
    product_name: state.productdetail.product_name,
    user: state.userinfo.user,

  };
}
export default connect(mapStateToProps, {
  fetchOrganizations,
  AddNewEOL
},
)(PatientProductDetails);
