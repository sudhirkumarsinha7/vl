
import React, { useState, useEffect } from 'react';
import {

  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
  Image,
  Keyboard
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBackBlue } from '../../../components/Header';
import _ from 'lodash';
import { Colors, DeviceWidth } from '../../../Style';
import { CustomDropdown, CustomInputText, MonthYearPickerView } from '../../../components/Common/common';
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../../components/Toast';
import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
const CELL_COUNT = 4

const genderList = [
  { label: 'Male', gender: 'Male' },
  { label: 'Female', gender: 'Female' },
  { label: 'Other', gender: 'Other' },
];
const shadowStyle = {
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowOpacity: 0.3,
  shadowRadius: 4.65,
  elevation: 8,
};
const Patient = (props) => {
  const { t, i18n } = useTranslation();
  const [mandatory, setIsmantory] = useState(false)
  const [showOtp, setOTP] = useState(false)
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [propsc, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  useEffect(() => {
    async function fetchData() {

    }
    fetchData();
  }, []);
  var validationSchema = Yup.object().shape(
    {
      name: Yup.string().required(t('Required')),
      gender: Yup.string().required(t('Required')),
      age: Yup.string().required(t('Required')),
      mob: Yup.string().required(t('Required')),
      idType: Yup.string().required(t('Required')),
    },
    [],
  ); // <-- HERE!!!!!!!!



  const PatientForm = useFormik({
    initialValues: {
      name: '',
      gender: '',
      age: '',
      mob: '',
      idType: '',
    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _Review = () => {
    setIsmantory(true);
    Keyboard.dismiss();
    PatientForm.handleSubmit();
  };
 
 


  const handleSubmit = async values => {
    console.log('handleSubmit ' + JSON.stringify(values))
    setOTP(true)
   
  };
  // console.log('PatientForm ' + JSON.stringify(PatientForm))

  const PatientViewComponent = () => {
    return (<View
      style={{
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
      }}>
               {/* <View style={{ marginTop: scale(15) }} /> */}
     


      <CustomInputText
        label={"Patient Name"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={PatientForm.handleChange('name')}
        val={PatientForm?.values?.name}
        errorMsg={PatientForm.errors && PatientForm.errors.name}
        mandatory={mandatory}
      />
            {/* <View style={{ marginTop: scale(-10) }} /> */}

      <CustomDropdown
        displayName={'Gender'}
        stateName={PatientForm?.values?.gender}
        dropDowndata={genderList}
        onChangeValue={(val) => selectGender(val)}
        label={'label'}
        mapKey={'gender'}
        mandatory={mandatory}
      />
      {/* <View style={{ marginTop: scale(15) }} />  */}
      <CustomInputText
        label={"Age"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={PatientForm.handleChange('age')}
        KeyboardType={'numeric'}
        val={PatientForm?.values?.age}
        errorMsg={PatientForm.errors && PatientForm.errors.age}
        mandatory={mandatory}
      />
      <CustomInputText
        label={"ID Proof Type"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={PatientForm.handleChange('idType')}
        val={PatientForm?.values?.idType}
        errorMsg={PatientForm.errors && PatientForm.errors.idType}
        mandatory={mandatory}
      />
      <CustomInputText
        label={"ID Proof"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={PatientForm.handleChange('aadhar')}
        val={PatientForm?.values?.aadhar}
        errorMsg={PatientForm.errors && PatientForm.errors.aadhar}
        mandatory={mandatory}
      />
       <CustomInputText
        label={"Mobile"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={PatientForm.handleChange('mob')}
        val={PatientForm?.values?.mob}
        errorMsg={PatientForm.errors && PatientForm.errors.mob}
        mandatory={mandatory}
        KeyboardType={'numeric'}
      />
    </View>)
  }
  const selectGender = (item) => {
    PatientForm.handleChange({ target: { name: `gender`, value: item?.value } })
  };

const OTPScreen =()=>{
  return <View
  style={[
    {
      marginLeft: scale(30),
      marginRight: scale(30),
      height: scale(380),
      borderRadius: 10,
      backgroundColor: '#FFFFFF',
      marginTop: verticalScale(20),
      justifyContent: 'center',
      alignItems: 'center',
    },
    shadowStyle,
  ]}>
  <Text
    style={{
      marginTop: verticalScale(20),
      fontSize: scale(20),
      color: '#000000',
      fontWeight: 'bold',
      marginBottom: verticalScale(20),
    }}>
    {t('Enter OTP')}
  </Text>
  <View style={{ width: '90%' }}>
  <CodeField
          ref={ref}
          {...propsc}
          // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
          value={value}
          onChangeText={(text)=>changeCode(text)}
          cellCount={CELL_COUNT}
          rootStyle={styles.codeFieldRoot}
          keyboardType="number-pad"
          textContentType="oneTimeCode"
          renderCell={({ index, symbol, isFocused }) => (
            <Text
              key={index}
              style={[styles.cell, isFocused && styles.focusCell]}
              onLayout={getCellOnLayoutHandler(index)}>
              {symbol || (isFocused ? <Cursor /> : null)}
            </Text>
          )}
        />
  </View>
</View>
}
const changeCode=(code)=>{
  setValue(code);
}
const continueAPI=()=> {
  props.navigation.navigate('ProductDetails', { Patientdata:PatientForm?.values });
}
  return (
    <View style={styles.container}>
      <HeaderWithBackBlue
       name={t('Send Single Vial')}
        navigation={props.navigation}
      />
      <View style={{ marginTop: verticalScale(-DeviceWidth / 7) }}>

        <KeyboardAwareScrollView extraScrollHeight={50} >
         
          {showOtp?OTPScreen():PatientViewComponent()}
          <View
            style={{
              flexDirection: 'row',
              margin: verticalScale(15),
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
            }}>
            <TouchableOpacity
              style={{
                height: scale(40),
                backgroundColor: '#FFFFFF',
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#0093E9',
                borderWidth: 1,
                flex: 0.5,
                margin: 10,
              }}
              onPress={() => showOtp?setOTP(false):props.navigation.goBack()}>
              <Text
                style={{
                  marginLeft: scale(13),
                  fontSize: scale(16),
                  fontWeight: 'bold',
                  color: '#0093E9',
                  alignSelf: 'center'
                }}>
                {t('Cancel')}
              </Text>
            </TouchableOpacity>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 1 }}
              colors={['#0093E9', '#36C2CF']}
              style={{
                borderRadius: 8,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                flex: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  margin: 10,


                }}
                onPress={() => showOtp?continueAPI():_Review()}>
                <Text
                  style={{
                    marginLeft: scale(13),
                    fontSize: scale(16),
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                  }}>
                  {t('SEND OTP')}
                </Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <View style={{ marginBottom: verticalScale(DeviceWidth / 2) }} />

        </KeyboardAwareScrollView>
      </View>

    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
  codeFieldRoot: { marginTop: 20 },
  cell: {
    width: 60,
    height: 55,
    color: '#000',
    borderColor: '#D3D3D3',
    borderWidth: 1,
    borderRadius: 10,
    textAlign: 'center',
    fontSize: scale(20),
    paddingTop: scale(10),
  },
  focusCell: {
    borderColor: Colors.blueE9,
  },
});
function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
  };
}
export default connect(mapStateToProps, {

},
)(Patient);
