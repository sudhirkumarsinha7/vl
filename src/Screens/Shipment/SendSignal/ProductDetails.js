
import React, { useState, useEffect } from 'react';
import {

  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
  Image,
  Keyboard
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import { connect } from 'react-redux';
import {
  getProductNames,
  fetchShipmentsByQRCode
} from '../../../Redux/Action/shipment';
import { HeaderWithBackBlue } from '../../../components/Header';
import _ from 'lodash';
import { Colors, DeviceWidth } from '../../../Style';
import { CustomDropdown, CustomInputText, MonthYearPickerView } from '../../../components/Common/common';
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../../components/Toast';
import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';
import { global } from '../../../Util/config'
import {CameraScreen} from 'react-native-camera-kit';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

const ProductDetails = (props) => {
  const { t, i18n } = useTranslation();
  const [mandatory, setIsmantory] = useState(false)
  const Patientdata = props.route.params.Patientdata
  const [opneScanner, setOpneScanner] = useState(false);

  useEffect(() => {
    async function fetchData() {
      props.getProductNames()
    }
    fetchData();
  }, []);


  var validationSchema = Yup.object().shape(
    {
      productName: Yup.string().required(t('Required')),
      batchNumber: Yup.string().required(t('Required')),
      serialNumber: Yup.string().required(t('Required')),
      productId: Yup.string().required(t('Required')),
    },
    [],
  );



  const ProductForm = useFormik({
    initialValues: {
      productName: '',
      batchNumber: '',
      serialNumber: '',
      productId: '',
      productMfg: ''
    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _Review = () => {
    setIsmantory(true);
    Keyboard.dismiss();
    ProductForm.handleSubmit();
  };




  const handleSubmit = async values => {
    console.log('handleSubmit ' + JSON.stringify(values))
    props.navigation.navigate('PatientProductDetails', {
      Patientdata: Patientdata,
      ProductData: values,
    });

  };

  const ProductFromComponent = () => {
    return (<View
      style={{
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
      }}>

      {/* <View style={{ marginTop: scale(15) }} /> */}


      <CustomDropdown
        displayName={'Product Name'}
        stateName={ProductForm?.values?.productId}
        dropDowndata={props.product_name}
        onChangeValue={(val) => selectProduct(val)}
        label={'name'}
        mapKey={'id'}
        mandatory={mandatory}
      />
      {/* <View style={{ marginTop: scale(15) }} /> */}


      <CustomInputText
        label={"Batch Number"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={ProductForm.handleChange(`batchNumber`)}
        val={ProductForm?.values?.batchNumber}
        errorMsg={ProductForm.errors && ProductForm.errors.batchNumber}
        mandatory={mandatory}

      />
      <CustomInputText
        label={"Serial Number"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={ProductForm.handleChange(`serialNumber`)}
        val={ProductForm?.values?.serialNumber}
        errorMsg={ProductForm.errors && ProductForm.errors.serialNumber}
        mandatory={mandatory}

      />
      <View style={{ height: scale(50) }} />
      <Text
        style={{
          textAlign: 'center',
          color: '#36C2CF',
          fontWeight: 'bold',
        }}>
        {t('OR')}
      </Text>
      <Text
        style={{
          textAlign: 'center',
          marginTop: verticalScale(20),
          color: 'black',
        }}>
        {t('Scan the QR code to get the product details')}
      </Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 8,
            borderRadius: 10,
          }}
          onPress={() => onOpneScanner()}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}
            colors={['#0093E9', '#36C2CF']}
            style={{
              marginTop: verticalScale(45),
              width: scale(90),
              height: scale(35),
              borderRadius: 8,
              borderColor: '#0093E9',
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignItems: 'center',
              alignSelf: 'flex-end',
            }}>
            <Image
              style={{ width: scale(15), height: scale(15) }}
              source={require('../../../assets/Scan.png')}
            />
            <Text
              style={{
                fontSize: scale(14),
                color: '#FFFFFF',
                fontWeight: 'bold',
              }}>
              {t('SCAN')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      <View style={{ height: scale(25) }} />
    </View>)
  }
  const ScannerScreen =()=>{
    return (
      <View style={{
        alignItems: 'center',
        justifyContent: 'center',
        height: Platform.OS === 'ios' ? 300: 100,
      }}>
        
        <CameraScreen
          actions={{
            leftButtonText: (
              <Icon name="close" style={{color: 'red', fontSize: 20}} />
            ),
            rightButtonText: (
              <Icon name="images" style={{color: 'red', fontSize: 20}} />
            ),
          }}
          // style={{height: Platform.OS === 'ios' ? 300: '1%',width:300,}}
          style={{height: Platform.OS === 'ios' ? 300: 300,width:300,marginTop:Platform.OS==='ios'?0: -200,}}
          onBottomButtonPressed={event =>
           setOpneScanner(false)
          }
          showFrame={true}
          //Show/hide scan frame
          scanBarcode={true}
          //Can restrict for the QR Code only
          laserColor={'green'}
          //Color can be of your choice
          frameColor={'green'}
          //If frame is visible then frame color
          colorForScannerFrame={'green'}
          //Scanner Frame color
          onReadCode={event =>
            onBarcodeScan(event.nativeEvent.codeStringValue)
          }
          />
          </View>
          )
  }
  const selectProduct = item => {
    ProductForm.handleChange({ target: { name: `productId`, value: item?.value } })
    ProductForm.handleChange({ target: { name: `productName`, value: item?.label } })
    ProductForm.handleChange({ target: { name: `productMfg`, value: item.eachItem.manufacturer } })

  };
  const onOpneScanner = () => {
    setOpneScanner(true)
  }

  const onBarcodeScan = async (qrvalue) => {

    let baseUrl = 'http://'
    qrvalue = qrvalue.replace(baseUrl, "");
    console.log('onBarcodeScan vvqrvalue' + JSON.stringify(qrvalue));
    // const result = await props.GetEOLInfoByProductId(qrvalue);
    const result = await props.fetchShipmentsByQRCode(qrvalue);
    console.log(
      'onBarcodeScan GetEOLInfoByProductId ' + JSON.stringify(result),
    );
    setOpneScanner(false)
    if (result.length) {

      let value = result && result[0];
      ProductForm.handleChange({ target: { name: `batchNumber`, value: value.batchNumber } })
      ProductForm.handleChange({ target: { name: `productName`, value: value.productName } })
      ProductForm.handleChange({ target: { name: `serialNumber`, value: value.serialNumber } })
      ProductForm.handleChange({ target: { name: `productId`, value: value.productID } })
      ProductForm.handleChange({ target: { name: `productMfg`, value: value.manufacturer } })
    } else {
      alert(t('Invalid QR code'));
    }

  }

  return (
    <View style={styles.container}>
      <HeaderWithBackBlue
        name={t('Send Single Vial')}
        navigation={props.navigation}
      />
      <View style={{ marginTop: verticalScale(-DeviceWidth / 7) }}>

        <KeyboardAwareScrollView extraScrollHeight={50} >

          {!opneScanner? ProductFromComponent():ScannerScreen()}
         

          <View style={{ marginBottom: verticalScale(DeviceWidth / 3) }} />

        </KeyboardAwareScrollView>
     
      </View>
     {!opneScanner? <View
            style={{
              flexDirection: 'row',
              margin: verticalScale(15),
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
            }}>
            <TouchableOpacity
              style={{
                height: scale(40),
                backgroundColor: '#FFFFFF',
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#0093E9',
                borderWidth: 1,
                flex: 0.5,
                margin: 10,
              }}
              onPress={() => props.navigation.goBack()}>
              <Text
                style={{
                  marginLeft: scale(13),
                  fontSize: scale(16),
                  fontWeight: 'bold',
                  color: '#0093E9',
                  alignSelf: 'center'
                }}>
                {t('Cancel')}
              </Text>
            </TouchableOpacity>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 1 }}
              colors={['#0093E9', '#36C2CF']}
              style={{
                borderRadius: 8,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                flex: 0.5,
              }}>
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  margin: 10,

                }}
                onPress={() => _Review()}>
                <Text
                  style={{
                    marginLeft: scale(13),
                    fontSize: scale(16),
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                  }}>
                  {t('SEND OTP')}
                </Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>:null}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
function mapStateToProps(state) {
  return {
    organizations: state.productdetail.Organizations,
    product_name: state.productdetail.product_name,
  };
}
export default connect(mapStateToProps, {
  getProductNames,
  fetchShipmentsByQRCode
},
)(ProductDetails);
