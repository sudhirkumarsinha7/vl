import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
  TextInput,
  Alert,
  Linking,
} from 'react-native';
import Header, {
  HeaderWithBackBlue,
} from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import {
  CustomInputText1,
  CustomInputText,
  CustomTextView,
  CustomDropdown,
} from '../../components/Common/common';
import {
  UpdateShipment,
  commentDropDown,
} from '../../components/Common/defaultValue';
import {
  fetchOrganizations,
  getWarehouseByOrgId,
  getProductsByInventory,
  updateShipment,
  getchainOfCustody,
  getViewShipment,

} from '../../Redux/Action/shipment';
import {CustomImagePicker} from '../../components/ImageSelect'

import Icon1 from 'react-native-vector-icons/SimpleLineIcons';
import { config } from '../../Util/config';
import _ from 'lodash';
import axios from 'axios';
import { Colors, DeviceHeight } from '../../Style';
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../components/Toast';
var genderList = [
  { label: 'Male', gender: 'M' },
  { label: 'Female', gender: 'F' },
  { label: 'Other', gender: 'Other' },
];
const  UpdateStatus =(props) =>{
  const { t, i18n } = useTranslation();
  const shipmentDetails = props.route.params.shipmentDetails
  const [anotherOrder,setAnotherOrder] = useState(true)

  const [otherComment, setOtherComment] = useState('');
  const [comment, setComment] = useState('');
  const [isLoadingState, setLoadingState] = useState(false);
  const [orderID, setShipmentID] = useState('');
  const [filePath, setFilePath] = useState({});
  const [orgLocation, setOrgLocation] = useState('');
  const [uniqueID, setUniqueID] = useState('');
  const [uniqueID1, setUniqueID1] = useState('');

  const [fromOrgName, setfromOrgName] = useState(false);
  const [fromOrgLocid, setFromOrgLocid] = useState('');

  const [isModalVisible2, setModalVissiable] = useState(false)
  const [errorMessage2, setErrorMsg] = useState(false)
 
  useEffect(() => {
    async function fetchData() {

      if (shipmentDetails && shipmentDetails.id) {
        setAnotherOrder(false)
      }
      getData();
    }
    fetchData();
  }, []);
  const getData = async () => {

    const orgSplit = props.user.organisation?.split('/');
    const ownOrg = orgSplit[0];
    const orgId = orgSplit[1];
    const res = await props.getWarehouseByOrgId(orgId);
    setfromOrgName(ownOrg)
  };


  const onChageComment=(item)=> {
    setComment(item.value)
    setOtherComment('')
  }

  
 
  const updateSipmentAPI =async()=> {
    let { user } = props;

    let formData = new FormData();
    // formData.append("photo", photo, photo.name);
    formData.append("id", (shipmentDetails && shipmentDetails.id) || orderID,);
    formData.append("updatedBy", user.firstName + ' ' + user.lastName);
    formData.append("orgId", props.user.organisation);
    formData.append("orgLocation", fromOrgLocid);
    formData.append("updatedAt", orgLocation);
    formData.append("isAlertTrue", true);
    if (otherComment || comment) {
      // data.updateComment = otherComment || comment;
      formData.append("updateComment", otherComment || comment);
    }
    // console.log('User filePath ' + JSON.stringify(filePath));
    if (filePath.uri) {
      formData.append('photo', {
        name: filePath.fileName || filePath.uri,
        type: filePath.type,
        uri:
          Platform.OS === 'android'
            ? filePath.uri
            : filePath.uri.replace('file://', ''),
      }, 'Image');
      // formData.append("photo", filePath, filePath.fileName);
    }
    console.log('User data ' + JSON.stringify(formData));
    const result = await props.updateShipment(
      formData,
      (shipmentDetails && shipmentDetails.id) || orderID,
    );
    if (result.status === 200) {
      let id = (shipmentDetails && shipmentDetails.id) || orderID || '';

      await props.getchainOfCustody(
        (shipmentDetails && shipmentDetails.id) || orderID,
      );
      await props.getViewShipment(
        (shipmentDetails && shipmentDetails.id) || orderID,
      );
      const smsg = t('Shipment ') + id + t(' has been Updated!');
      ToastShow(
        smsg,
         'success',
         'long',
         'top',
       )
       props.navigation.goBack();

    } else {

      const emsg = result.message || result.data && result.data.message || t("something is wrong");
      ToastShow(
        emsg,
         'error',
         'long',
         'top',
       )
    }
    
  }
 


  const getShipment = async (text, lablecode = false) => {
    setLoadingState(true)
    // const result = await props.getViewShipment(text);
    let result;
    let status;
    if (lablecode) {
      let response = await axios.get(config().fetchShipmentsByQRCode + text);
      result =
        response && response.data &&
        response.data.data &&
        response.data.data.shipments &&
        response.data.data.shipments[0];
    } else {
      let res = await props.getViewShipment(text);
      result = res.data;
    }
    setLoadingState(false)

    // console.log(
    //   'SearchFilterFunction getViewShipment result' + JSON.stringify(result),
    // );
    if (result && result.status) {
      if (result.status === 'RECEIVED') {
        ToastShow(
          t('Shipment  is already received :') + text,
           'error',
           'long',
           'top',
         )
      } else {
        setShipmentID(result.id)
        setAnotherOrder(false)
        setLoadingState(false)
      }
    } else {
      let errorMsg=lablecode? 'Please Check your QR code label :' + text: 'Please Enter valid Shipment id/Transit Number'
      ToastShow(
        t(errorMsg),
         'error',
         'long',
         'top',
       )
    }
  };

    let { shipment } = props;
    shipment = _.uniqBy(shipment, function (x) {
      return x.id;
    });
    console.log('render user ' + JSON.stringify(props.user));
   

     return (<View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('Update Status')}
          navigation={props.navigation}
        />
        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{ marginTop: verticalScale(-70) }}>
          <View
            style={{
              backgroundColor: '#FFFFFF',
              margin: verticalScale(10),
              borderRadius: 10,
              padding: 5,
              //   height: scale(350),
            }}>
            {anotherOrder ? (
              <View style={{ justifyContent: 'center', }}>
                                <View style={{marginTop:scale(10)}}/>

                <CustomInputText
                  label={"Shipment ID"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  onChange={(val) => setUniqueID(val)}
                  val={uniqueID}
                  disabled={uniqueID1}

                />
                <Text
                  style={{
                    alignSelf: 'center',
                    color: '#0093E9',
                    paddingBottom: 10,
                    marginTop:10
                  }}>
                  {t('OR')}
                </Text>
                <CustomInputText
                  label={"Transit Number"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  onChange={(val) => setUniqueID1(val)}
                  val={uniqueID1}
                  disabled={uniqueID}

                />
                <TouchableOpacity
                  style={{
                    marginLeft:'50%',
                    padding: 4,
                    borderRadius: 10,
                    height: scale(45),
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginBottom:scale(15),
                    marginTop:10,
                    width: '30%',
                    backgroundColor:
                        !uniqueID && !uniqueID1
                          ? 'gray'
                          : Colors.blueE9,
                  }}
                  disabled={!uniqueID && !uniqueID1}
                  onPress={() =>
                    getShipment(uniqueID || uniqueID1)
                  }
                 >
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 16,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      
                    }}>
                    {t('Next')}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
           
              <View>
                <CustomTextView
                  leftText={(UpdateShipment.orderID.displayName)}
                  rightText={
                    (shipmentDetails && shipmentDetails.id) || uniqueID||uniqueID1
                  }
                />

                <Text
                  style={{
                    marginLeft: 10,
                    fontWeight: 'bold',
                    color: Colors.black0,
                  }}>
                  {t('Account Holder Details')}
                </Text>
                <CustomTextView
                  leftText={UpdateShipment.name.displayName}
                  rightText={
                    props.user.firstName + ' ' + props.user.lastName
                  }
                />
                <CustomTextView
                  leftText={UpdateShipment.orgName.displayName}
                  rightText={fromOrgName}
                />
                <View style={{marginTop:scale(10)}}/>
                <CustomInputText
                  label={"Update Status Location"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  onChange={(val) => setOrgLocation(val)}
                  val={orgLocation}

                />
               
              </View>
            )}
          </View>
          {anotherOrder ? null : (
            <View>
              <View
                style={{
                  backgroundColor: '#FFFFFF',
                  margin: verticalScale(10),
                  borderRadius: 10,
                  padding: 5,
                  //   height: scale(350),
                }}>
                  <CustomDropdown
                displayName={'Comment'}
                stateName={comment}
                dropDowndata={commentDropDown}
                onChangeValue={(val) => onChageComment(val)}
              />
               
                {comment && comment === 'Others' ? (
                  <View
                    style={{
                      borderBottomColor: '#E8E8E8',
                    }}>
                    <TextInput
                      style={{
                        borderBottomWidth: 1,
                        borderBottomColor: '#E8E8E8',
                      }}
                      placeholder={t('Enter Comment')}
                      value={otherComment}
                      onChangeText={text => setOtherComment(text)}
                      multiline={true}
                    />
                  </View>
                ) : null}
                <CustomImagePicker 
                setImage={(val)=> setFilePath(val)}
                filePath={filePath}
                />
        
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  marginTop: 10,
                  marginBottom: 20,
                }}>
                <TouchableOpacity
                  style={{
                    padding: 8,
                    borderRadius: 10,
                    borderWidth: 1,
                    borderColor: '#58b1da',
                    height: scale(45),
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: '40%',
                  }}
                  onPress={() => props.navigation.goBack()}>
                  <Text
                    style={{
                      color: '#58b1da',
                      fontSize: scale(16),
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}>
                    {'  '}
                    {t('cancel')}{' '}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    height: scale(45),
                    borderRadius: 10,
                    backgroundColor:
                      (anotherOrder && orderID) ||
                        !anotherOrder
                        ? '#FA7923'
                        : 'gray',
                    flexDirection: 'row',
                    padding: 8,
                    width: '55%',
                    justifyContent: 'space-evenly'
                  }}
                  disabled={
                    (anotherOrder && orderID) ||
                      !anotherOrder
                      ? false
                      : true
                  }
                  onPress={() => updateSipmentAPI()}>
                  <View
                    style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                      style={{
                        width: scale(18.73),
                        height: scale(18.73),
                      }}
                      source={require('../../assets/update.png')}
                    />
                    {/* <VIcon name="sync" color="white" size={30} /> */}
                  </View>
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: scale(12),
                        color: '#FFFFFF',
                        fontWeight: 'bold',
                        textAlign: 'center',
                      }}>
                      {t('UPDATE STATUS')}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </ScrollView>
      </View>)
}

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    organizations: state.productdetail.Organizations,
    shipment: state.shipment.shipmentdata,
    token: state.userinfo.token,
  };
}

export default connect(
  mapStateToProps,
  {
    fetchOrganizations,
    getWarehouseByOrgId,
    getProductsByInventory,
    updateShipment,
    getchainOfCustody,
    getViewShipment,
  },
)(UpdateStatus);
