import React, { useEffect,useState,useRef } from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  BackHandler,
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import MapView, { Marker, Polyline,PROVIDER_GOOGLE,Polygon } from 'react-native-maps';

import Section_Select from '../../components/Section_Select';
import { connect } from 'react-redux';
import { LocationIcon } from '../../components/Common/common';
import EntypoIcon from 'react-native-vector-icons/FontAwesome';
import Svg, { Line } from 'react-native-svg';

import {
  ShimpmetViewCard,
  ViewProdutList,
  DeliveryDetails,
  ChainOfStudy,
  ShipmentGraph,
} from '../../components/Common/shipmentHelper';
import { global } from '../../Util/config';
import { getIotEnabledStatus } from '../../Redux/Action/shipment';
import { useTranslation } from 'react-i18next';
import { Colors, DeviceWidth } from '../../Style';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const origin1 = { latitude: 17.4295865, longitude: 78.368776 };
const destination1 = { latitude: 28.6448, longitude: 77.216721 };
const defaltCoordinate = [
  { latitude: 17.4295865, longitude: 78.368776 },
  { latitude: 28.6448, longitude: 77.216721 },
  { latitude: 38.6448, longitude: 13.847747 },
];
const GOOGLE_MAPS_APIKEY = global.GoogleMapsApiKey;
const radioItems = [
  {
    label: 'Shipment Details',
    selected: true,
  },
  {
    label: 'Chain of Custody',
    selected: false,
  },
  {
    label: 'Product List',
    selected: false,
  },]
const ViewShipment =(props)=> {
  const { t, i18n } = useTranslation();
  const [selectedItem, setTab] = useState('Shipment Details');
  const mapRef = useRef(null);

  useEffect(() => {
    async function fetchData() {
      GetData();
    }
    fetchData();
  }, []);
  const GetData = async () => {
    radioItems.map(item => {
      if (item.selected == true) {
        setTab(item.label)
      }
    });
   
  };
 
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });
    radioItems[index].selected = true;
    const label = radioItems[index].label
    console.log('radioItems[index].label ' + label)
    setTab(label)
  }
  const shipmentViewSummary=()=> {
    return (
      <View>
        <ShimpmetViewCard item={props.shipment}/>
        <DeliveryDetails item={props.shipment} />

      </View>
    );
  }

  const chainOfStudy=()=> {
    return (
      <ChainOfStudy
        shipmentDetails={props.shipment}
        shipmentChain={props.shipmentChain}
      />
    );
  }

    // console.log('render shipment ' + JSON.stringify(props.shipment));
    let newCordinate = [];
    // let newCordinate = [];
    const { shipment, IotEnabled, userpermission = {} } = props;
    let shipmentPermission = userpermission.shipment;
    const { supplier={},receiver={}} = shipment;
    const { warehouse : supplierWarehose={},} = supplier;
    const { location : origin={}} = supplierWarehose;
    const { warehouse : receiverWarehose={},} = receiver;
    const { location : destination={},} = receiverWarehose;
    const {coordinates:supplierCordinate=[]} =origin
    const {coordinates:receiverCordinate=[]} =destination

    if(supplierCordinate.length){
      newCordinate.push(supplierCordinate); 
    }
    if(receiverCordinate.length){
      newCordinate.push(receiverCordinate); 
    }
    const polygonnCordinate = newCordinate.length && newCordinate.map(coordsArr => {
        let coords = {
          latitude: coordsArr[0],
          longitude: coordsArr[1],
        }
        return coords;
    });
// const polygonnCordinate = [{latitude:27.88169,longitude:0.27904},{latitude:9.1754,longitude:83.32771}]
    // console.log('render supplierCordinate ' + JSON.stringify(supplierCordinate));
    // console.log('render receiverCordinate ' + JSON.stringify(receiverCordinate));
    // console.log('render newCordinate ' + JSON.stringify(newCordinate));

    // console.log('render polygonnCordinate ' + JSON.stringify(polygonnCordinate));
    // console.log('render destination ' + JSON.stringify(defaltCoordinate));
    return (
      <KeyboardAwareScrollView extraScrollHeight={50} >
        <View
          style={{
            flex: 1,
            // alignItems: 'center',
          }}>
          <HeaderWithBackBlue
            name={t('view_shipment')}
            navigation={props.navigation}
          />

          <ScrollView
            nestedScrollEnabled={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{
              borderRadius: 18,
              marginTop: verticalScale(-DeviceWidth / 5),
            }}>
              <View style={{ height: DeviceWidth / 1.12, margin: 8 }}>
              <MapView
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  borderRadius: 10,
                }}
                showsUserLocation={false}
                zoomEnabled={true}
                zoomControlEnabled={true}
                initialRegion={{
                  latitude:
                  receiverCordinate && receiverCordinate.length
                      ? parseFloat(parseFloat(receiverCordinate[0]).toFixed(6))
                      : 17.4295865,
                  longitude:
                  receiverCordinate && receiverCordinate.length
                      ? parseFloat(parseFloat(receiverCordinate[1]).toFixed(6))
                      : 78.368776,
                  latitudeDelta: 100,
                  longitudeDelta: 100,
                }}
                // ref={mapRef}
              >
                {supplierCordinate && supplierCordinate.length ? (
                  <Marker
                    coordinate={{
                      latitude: parseFloat(
                        parseFloat(supplierCordinate[0]).toFixed(6),
                      ),
                      longitude: parseFloat(
                        parseFloat(supplierCordinate[1]).toFixed(6),
                      ),
                    }}
                    title={
                      shipment.supplier &&
                      shipment.supplier.org &&
                      shipment.supplier.org.name
                    }
                    description={
                      shipment.supplier &&
                        shipment.supplier.warehouse &&
                        shipment.supplier.warehouse.warehouseAddress
                        ? shipment.supplier.warehouse.title +
                        ' / ' +
                        shipment.supplier.warehouse.warehouseAddress.firstLine +
                        ' ' +
                        shipment.supplier.warehouse.warehouseAddress.city
                        : ''
                    }
                    >
                    <LocationIcon />
                  </Marker>
                ) : null}
                {receiverCordinate && receiverCordinate.length ? (
                  <Marker
                    coordinate={{
                      latitude: parseFloat(
                        parseFloat(receiverCordinate[0]).toFixed(6),
                      ),
                      longitude: parseFloat(
                        parseFloat(receiverCordinate[1]).toFixed(6),
                      ),
                    }}
                    title={
                      shipment.receiver &&
                      shipment.receiver.org &&
                      shipment.receiver.org.name
                    }
                    description={
                      shipment.receiver &&
                        shipment.receiver.warehouse &&
                        shipment.receiver.warehouse.warehouseAddress
                        ? shipment.receiver.warehouse.title +
                        ' / ' +
                        shipment.receiver.warehouse.warehouseAddress.firstLine +
                        ' ' +
                        shipment.receiver.warehouse.warehouseAddress.city
                        : ''
                    }
                    >
                    <LocationIcon />
                  </Marker>
                ) : null}
                            
                {polygonnCordinate && polygonnCordinate.length  ? (
                  <Polygon
                    coordinates={polygonnCordinate}
                    strokeColor="#0093E9"
                    strokeWidth={3}
                  />
                ) : null}
                
              </MapView>
            </View>
            {props.loder  ? (
              <View
                style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                <ActivityIndicator color="#0000ff" />
              </View>
            ) : null}
            <View
              style={{
                flex: 1,
                margin: 10,
                borderRadius: 10,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  width: scale(41),
                  height: scale(41),
                  borderRadius: 400,
                  backgroundColor: Colors.whiteFF,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                opacity={1}>
                <Image
                  style={{
                    width: scale(21.7),
                    height: scale(16.06),
                    borderWidth: 0,
                  }}
                  source={require('../../assets/Shipment1.png')}
                  resizeMode="contain"
                />
              </View>
              <View style={{ flex: 0.4, marginLeft: 10 }}>
                <Text
                  style={{
                    color: Colors.black70,
                    fontSize: scale(12),
                  }}>
                  {t('Shipment ID')}
                </Text>
                <Text
                  style={{
                    color: Colors.blueE9,
                    fontWeight: 'bold',
                    fontSize: scale(16),
                  }}>
                  {shipment.id}
                </Text>
              </View>
              <View style={{ flex: 0.6, marginRight: 15 }}>
                <Svg>
                  <Line
                    x1="5"
                    y1="5"
                    x2="5"
                    y2="40"
                    strokeDasharray="1, 5"
                    stroke-linecap="round"
                    stroke={Colors.blueE9}
                    strokeWidth="1"
                  />
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <EntypoIcon name="circle" color={Colors.black70} size={10} />
                    <Text
                      style={{
                        color: '#000000',
                        fontWeight: 'bold',
                        fontSize: scale(9),
                        marginLeft: 5,
                      }}>
                      {shipment.supplier &&
                        shipment.supplier.org &&
                        shipment.supplier.org.name}
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        color: Colors.black70,
                        fontSize: scale(7),
                        marginLeft: 15,
                      }}>
                      {shipment.supplier &&
                        shipment.supplier.warehouse &&
                        shipment.supplier.warehouse.warehouseAddress
                        ? shipment.supplier.warehouse.title +
                        ' / ' +
                        shipment.supplier.warehouse.warehouseAddress.firstLine +
                        ' ' +
                        shipment.supplier.warehouse.warehouseAddress.city
                        : ''}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <EntypoIcon name="circle" color={Colors.blueFB} size={10} />
                    <Text
                      style={{
                        color: '#000000',
                        fontWeight: 'bold',
                        fontSize: scale(9),
                        marginLeft: 5,
                      }}>
                      {shipment.receiver &&
                        shipment.receiver.org &&
                        shipment.receiver.org.name}
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        color: Colors.black70,
                        fontSize: scale(7),
                        marginLeft: 15,
                      }}>
                      {shipment.receiver &&
                        shipment.receiver.warehouse &&
                        shipment.receiver.warehouse.warehouseAddress
                        ? shipment.receiver.warehouse.title +
                        ' / ' +
                        shipment.receiver.warehouse.warehouseAddress.firstLine +
                        ' ' +
                        shipment.receiver.warehouse.warehouseAddress.city
                        : ''}
                    </Text>
                  </View>
                </Svg>
              </View>
            </View>
            <View
              style={{
                backgroundColor: 'transparent',
                flexDirection: 'row',
                margin: verticalScale(5),
                justifyContent: 'space-evenly'
              }}>
              {shipmentPermission.updateShipment ? (<TouchableOpacity
                style={{
                  flex: 0.47,
                  borderRadius: 10,
                  backgroundColor:
                    shipment.status === 'RECEIVED' ? 'gray' : Colors.red23,
                  flexDirection: 'row',
                  margin: verticalScale(5),
                  padding: verticalScale(10),
                }}
                disabled={shipment.status === 'RECEIVED' ? true : false}
                onPress={() =>
                  props.navigation.navigate('UpdateShipment', {
                    shipmentDetails: shipment,
                  })
                }>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 10,
                  }}>
                  <Image
                    style={{
                      width: scale(18.73),
                      height: scale(18.73),
                    }}
                    source={require('../../assets/update.png')}
                  />
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginLeft: scale(10),
                  }}>
                  <Text
                    style={{
                      fontSize: scale(13),
                      color: '#FFFFFF',
                      fontWeight: 'bold',
                    }}>
                    {t('UPDATE')}
                  </Text>
                  <Text
                    style={{
                      fontSize: scale(13),
                      color: '#FFFFFF',
                      fontWeight: 'bold',
                    }}>
                    {t('STATUS')}
                  </Text>
                </View>
              </TouchableOpacity>) : null}
              <TouchableOpacity
                style={{
                  flex: 0.47,
                  borderRadius: 10,
                  backgroundColor:
                    shipment.status === 'RECEIVED' ? 'gray' : Colors.blueE9,
                  flexDirection: 'row',
                  margin: verticalScale(5),
                  padding: verticalScale(10),
                }}
                disabled={shipment.status === 'RECEIVED' ? true : false}
                onPress={
                  () =>
                    props.navigation.navigate('ReceiveShipment', {
                      optionName: 'shipment',
                      shipmentId: shipment.id,
                    })
                }>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 10,
                  }}>
                
                  <Image
                    style={{
                      width: scale(18.73),
                      height: scale(18.73),
                    }}
                    source={require('../../assets/receive.png')}
                  />
                </View>
                <View
                  style={{
                    alignItems: 'flex-start',
                    justifyContent: 'center',
                    marginLeft: scale(10),
                  }}>
                  <Text
                    style={{
                      fontSize: scale(13),
                      color: '#FFFFFF',
                      fontWeight: 'bold',
                    }}>
                    {t('RECEIVE')}
                  </Text>
                  <Text
                    style={{
                      fontSize: scale(13),
                      color: '#FFFFFF',
                      fontWeight: 'bold',
                    }}>
                    {t('SHIPMENT')}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            {IotEnabled && IotEnabled.iot_enabled ? <ShipmentGraph t={t} /> : null}
            <View style={{ flexDirection: 'row' }}>
              {radioItems.map((item, key) => (
                <Section_Select
                  key={key}
                  button={item}
                  onClick={()=>changeActiveRadioButton(key)}
                />
              ))}
            </View>
            {selectedItem === 'Product List' ? (
              <ViewProdutList
                Products={props.shipment.products}
              />
            ) : selectedItem === 'Chain of Custody' ? (
              chainOfStudy()
            ) : (

              shipmentViewSummary()
            )}
          </ScrollView>
        </View>
      </KeyboardAwareScrollView>
    );
}

function mapStateToProps(state) {
  return {
    TrackingData: state.shipment.TrackingData,
    shipmentChain: state.shipment.shipmentChain,
    shipment: state.shipment.shipment,
    token: state.userinfo.token,
    IotEnabled: state.shipment.IotEnabled,
    loder: state.loder,
    userpermission: state.userinfo.userpermission,
  };
}

export default connect(
  mapStateToProps,
  { getIotEnabledStatus },
)(ViewShipment);
