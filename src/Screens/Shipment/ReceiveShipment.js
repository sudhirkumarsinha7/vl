import React, { useState, useEffect } from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  ActivityIndicator,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  StatusBar,
  Switch,
  Alert,
  Linking,
} from 'react-native';
import { CameraScreen } from 'react-native-camera-kit';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import PopUp, { PopUp1 } from '../../components/PopUp';
import Permissions from 'react-native-permissions';
import {
  receiveShipment,
  getchainOfCustody,
  getViewShipment,
  fetchInboundShipments,
  fetchOutboundShipments,
} from '../../Redux/Action/shipment';
import {
  CustomDropdown,
  CustomInputText
} from '../../components/Common/common';
import {
  ViewProdutList,
  ReceiveShipmentDetails,
  ReceiveShipmetHepler,
} from '../../components/Common/shipmentHelper';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { CustomImagePicker } from '../../components/ImageSelect'
import axios from 'axios';
import { config } from '../../Util/config';
import { CustomQrcodeScanner } from '../../components/Scanner'
import {
  UpdateShipment,
  commentDropDown,
  uniqueID
} from '../../components/Common/defaultValue';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { Colors, DeviceWidth, DeviceHeight } from '../../Style';
import { ToastShow } from '../../components/Toast'
const ReceiveShipmentScreen = (props) => {

  const { t, i18n } = useTranslation();
  const [scanObj, setScanObj] = useState({});
  const [otherComment, setOtherComment] = useState('');
  const [comment, setComment] = useState('');
  const [reqData, setReqData] = useState({});

  const [filePath, setFilePath] = useState({});
  const [OnReceiveOption, setOnReceiveOption] = useState(true);
  const [opneScanner, setScanner] = useState(false);
  const [qrvalue, setQrvalue] = useState('');

  const [isModalVisible2, setModalVissiable] = useState(false)
  const [errorMessage2, setErrorMsg] = useState(false)
  const [uniqueID, setUniqueID] = useState('');
  const [uniqueID1, setUniqueID1] = useState('');
  const shipmentID = props.route.params.shipmentId
  useEffect(() => {
    async function fetchData() {

      if (shipmentID) {
        setOnReceiveOption(false)
        getDetails(shipmentID);
      }
    }
    fetchData();
  }, []);



  const getDetails = async (id) => {

    console.log('getDetails id', id);
    try {
      console.log('AScan uri' + (config().getShipment + id));
      const response = await axios.get(config().getShipment + id);
      if (response.status === 200) {
        console.log('Action_getDetails' + JSON.stringify(response));
        let ShipmentDetails = response.data.data;
        if (ShipmentDetails.createdAt) {
          if (ShipmentDetails.status !== 'RECEIVED') {
          delete ShipmentDetails.__v;
          delete ShipmentDetails.updatedAt;
          delete ShipmentDetails.createdAt;
          delete ShipmentDetails._id;
          let modifiedProduct = ShipmentDetails.products.map(each => {
            each.isEditQty = true;
            each.isUserQty = false;
            each.productQuantityDelivered = each.productQuantity
              ? each.productQuantity + ''
              : '';
            each.productQuantitySent = each.productQuantity
              ? each.productQuantity + ''
              : '';
            return each;
          });
          ShipmentDetails.products = modifiedProduct;
          setScanObj(ShipmentDetails)
          setOnReceiveOption(false)
        } else {
          ToastShow(
            t('Shipment  is already received') + ' Id ' + ShipmentDetails.id,
            'error',
            'long',
            'top',
          )
        }
        } else {
          // alert(t('Please Enter valid Shipment id/Transit Number'));
          ToastShow(
            t('Please Enter valid Shipment id/Transit Number'),
            'error',
            'long',
            'top',
          )
        }
      } else {
        // alert(t('Please Enter valid Shipment id/Transit Number'));
        ToastShow(
          t('Please Enter valid Shipment id/Transit Number'),
          'error',
          'long',
          'top',
        )
      }
    } catch (e) {
      // alert(t('Please Enter valid Shipment id/Transit Number'));
      ToastShow(
        t('Please Enter valid Shipment id/Transit Number'),
        'error',
        'long',
        'top',
      )
    }
  }
  const send_Shipment = async () => {
    let formData = new FormData();
    // formData.append("photo", photo, photo.name);
    formData.append("id", scanObj.id || shipmentID);
    formData.append("supplier", JSON.stringify(scanObj.supplier));
    formData.append("receiver", JSON.stringify(scanObj.receiver));
    formData.append("products", JSON.stringify(scanObj.products));
    formData.append("poId", scanObj.poId);
    formData.append("status", "RECEIVED");

    if (otherComment || comment) {
      formData.append("comment", otherComment || comment);
    }
    if (filePath.uri) {
      formData.append('photo', {
        name: filePath.fileName || filePath.uri,
        type: filePath.type,
        uri:
          Platform.OS === 'android'
            ? filePath.uri
            : filePath.uri.replace('file://', ''),
      }, 'Image');
      // formData.append("photo", filePath, filePath.fileName);
    }

    console.log('receive shipment formData' + JSON.stringify(formData))

    const result = await props.receiveShipment(formData);
    console.log('receive shipment' + JSON.stringify(result))
    if (result.status === 200) {
      await props.fetchInboundShipments(0, 50, props.filterOutBound);
      await props.fetchOutboundShipments(0, 50, props.filterInbound);
      await props.getchainOfCustody(shipmentID);
      await props.getViewShipment(shipmentID);

      const smsg = result.data && result.data.message || t('Shipment has been Received successfully!');
      ToastShow(
        smsg,
        'success',
        'long',
        'top',
      )
      props.navigation.goBack();

    } else {
      const emsg = result.message || result.data && result.data.message || t("Unable to receive shipment");
      ToastShow(
        emsg,
        'error',
        'long',
        'top',
      )
    }
  };
  const onChageComment = (item) => {
    setOtherComment('')
    setComment(item.value)
  }


  const opneScanScreen = () => {
    return (

      <CameraScreen
        actions={{
          leftButtonText: (
            <MaterialCommunityIcons name="close" style={{ color: 'white', fontSize: 50 }} />
          ),
        }}
        style={{ height: Platform.OS === 'ios' ? DeviceHeight / 1.2 : '1%', }}
        onBottomButtonPressed={event => setScanner(false)}
        showFrame={true}
        //Show/hide scan frame
        scanBarcode={true}

        //Can restrict for the QR Code only
        laserColor={'green'}
        //Color can be of your choice
        frameColor={'green'}
        //If frame is visible then frame color
        colorForScannerFrame={'green'}

        //Scanner Frame color
        onReadCode={event =>
          onBarcodeScan(event.nativeEvent.codeStringValue)
        }
      />
    );
  };
  const onOpneScanner = () => {

    async function requestCameraPermission() {
      try {
        const checkCameraPermission = await Permissions.request('camera', {
          rationale: {
            title: t('App Camera Permission'),
            message: t('App needs access to your camera '),
          },
        });
        if (checkCameraPermission === 'authorized') {
          setQrvalue('')
          setScanner(true)

        } else {
          Alert.alert(
            t('App Camera Permission'),
            t('App needs access to your camera '),
            [
              {
                text: t('No way'),
                onPress: () => console.log('Permission denied'),
                style: t('Cancel'),
              },
              checkCameraPermission == 'undetermined'
                ? { text: t('OK'), onPress: () => _requestPermission }
                : {
                  text: t('Open Settings'), onPress: () => Linking.openSettings()
                },
            ],
          );
        }
        console.log('checkCameraPermission ' + JSON.stringify(checkCameraPermission))
      } catch (err) {
        alert(t('Camera permission err'), err);
        console.warn(err);
      }



      // const isCameraAuthorized = await Camera.checkDeviceCameraAuthorizationStatus();
      // console.log('isCameraAuthorized ' + JSON.stringify(isCameraAuthorized))

    }
    //Calling the camera permission function
    requestCameraPermission();

  }
  const _requestPermission = () => {

    Permissions.request('camera', {
      title: t('App Camera Permission'),
      message: t('App needs access to your camera '),
    }).then(response => {
      console.log('response ' + JSON.stringify(response))
      setQrvalue('')
      setScanner(true)
    });
  };


  const onBarcodeScan = (qrvalue) => {
    //called after te successful scanning of QRCode/Barcode
    console.log('qr', qrvalue);
    setQrvalue('')
    setScanner(false)
    let baseUrl = 'http://'
    let newval = qrvalue.replace(baseUrl, "");
    console.log('qr', newval);
    getQRDetails(newval, true);
    // }
  }
  const getQRDetails = async (id, lablecode = false) => {
    const { t } = props
    try {
      console.log('ScanHelper getQRDetails' + (config().fetchShipmentsByQRCode + id));
      // const response = await axios.get(config().getShipment + id);

      let shimentId = id
      if (lablecode) {
        response = await axios.get(config().fetchShipmentsByQRCode + id);
      } else {
        response = await axios.get(config().getShipment + id);
      }

      // fetchShipmentsByQRCode
      console.log('ScanHelper getDetails' + JSON.stringify(response));
      if (response.status === 200) {
        let ShipmentDetails;
        if (lablecode) {
          ShipmentDetails = (response.data && response.data.data &&
            response.data.data.shipments && response.data.data.shipments.length &&
            response.data.data.shipments[0] || {});
        } else {
          ShipmentDetails = response.data && response.data.data
        }
        if (ShipmentDetails.createdAt) {
          if (ShipmentDetails.status !== 'RECEIVED') {

            delete ShipmentDetails.__v;
            delete ShipmentDetails.updatedAt;
            delete ShipmentDetails.createdAt;
            delete ShipmentDetails._id;
            if (lablecode) {
              shimentId = ShipmentDetails.id;
            }
            let modifiedProduct = ShipmentDetails.products.map(each => {
              each.isEditQty = true;
              each.isUserQty = false;
              each.productQuantityDelivered = each.productQuantity
                ? each.productQuantity + ''
                : '';
              each.productQuantitySent = each.productQuantity
                ? each.productQuantity + ''
                : '';
              return each;
            });
            ShipmentDetails.products = modifiedProduct;
            setScanObj(ShipmentDetails)
            setOnReceiveOption(false)
          } else {
            ToastShow(
              t('Shipment  is already received') + ' Id ' + ShipmentDetails.id,
              'error',
              'long',
              'top',
            )
          }
        } else {
          ToastShow(
            t('Please Enter/Scan valid Shipment  id'),
            'error',
            'long',
            'top',
          )
        }
      } else {
        if (response.data && response.data.message && response.data.message.message && response.data.message.requestType) {
          let reqData1 = {
            "labelId": id,
            "type": response.data.message.requestType
          }
          setReqData(reqData1)
          setErrorMsg(response.data.message.message)
          setModalVissiable(true)
        } else {
          let invalid = (response.data && response.data.message && response.data.message.errmsg) ||
            (response.data && response.data.message && response.data.message.name) ||
            'Invalid Shipment'
          ToastShow(
            t(invalid),
            'error',
            'long',
            'top',
          )
        }

      }
    } catch (e) {
      let result = e.response
      console.log('ScanHelper result' + JSON.stringify(result));

      if (result && result.data && result.data.message && result.data.message.message && result.data.message.requestType) {
        let reqNewData = {
          "labelId": id,
          "type": result.data.message.requestType
        }

        setReqData(reqNewData)
        setErrorMsg(response.data.message.message)
        setModalVissiable(true)
      } else {
        ToastShow(
          t('Please Enter valid Shipment id/Transit Number'),
          'error',
          'long',
          'top',
        )
      }

    }
  }

  const fail = () => {
    setModalVissiable(false)
  }
  const sendRequest = async () => {
    try {
      console.log(
        'Action_sendRequest_reqData' + JSON.stringify(reqData),
      );
      const response = await axios.post(config().createRequest, reqData);
      console.log(
        'Action_sendRequest_response' + JSON.stringify(response),
      );
      const smsg1 = "Request sended succcuessfully"
      ToastShow(
        smsg1,
        'success',
        'long',
        'top',
      )

    } catch (e) {
      ToastShow(
        t(JSON.stringify(e.response)),
        'error',
        'long',
        'top',
      )
    }
  }
  // console.log('scanObj shipmentID filePath' + JSON.stringify(filePath));
  const updatePdtQty = (val) => {
    setScanObj(val)
  }
  let {
    userpermission = {},
  } = props;
  let { shipment = {} } = userpermission;
  if (!OnReceiveOption) {
    return (

      <View>
        {/* <StatusBar backgroundColor="#0093E9" /> */}
        <HeaderWithBackBlue
          name={t('Receive Shipment')}
          navigation={props.navigation}
        />
        <KeyboardAwareScrollView extraScrollHeight={50} style={{ marginTop: verticalScale(-DeviceWidth / 7) }} >

          {/* <ScrollView
            nestedScrollEnabled={true}
            style={{ marginTop: verticalScale(-DeviceWidth / 7) }}> */}
          {scanObj ? <ReceiveShipmetHepler item={scanObj} /> : null}
          {scanObj ? <ReceiveShipmentDetails item={scanObj} /> : null}
          {scanObj && scanObj.products ? (
            <ViewProdutList
              Products={scanObj.products}
              receiveProduct={true}
              updateProductQty={(value) => updatePdtQty(value)}
              scanObj={scanObj}
            />
          ) : null}
          <View
            style={{
              backgroundColor: '#FFFFFF',
              margin: verticalScale(10),
              borderRadius: 10,
              padding: 5,
            }}>
            <CustomDropdown
              displayName={'Comment'}
              stateName={comment}
              dropDowndata={commentDropDown}
              onChangeValue={(val) => onChageComment(val)}
            />

            {comment === 'Others' ? (
              <View
                style={{
                  borderBottomColor: '#E8E8E8',
                  margin: 15

                }}>
                <TextInput
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#E8E8E8',
                    fontSize: scale(16)
                  }}
                  placeholder={t('Enter Comment')}
                  value={otherComment}
                  onChangeText={text => setOtherComment(text)}
                  multiline={true}
                />
              </View>
            ) : null}
            <CustomImagePicker
              setImage={(val) => setFilePath(val)}
              filePath={filePath}
            />
          </View>

          {props.loder ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}>
              <ActivityIndicator color="#0000ff" size="large" />
            </View>
          ) : null}
          {props.loder ? null : (
            <View
              style={{
                flexDirection: 'row',
                height: scale(40),
                borderRadius: 8,
                magin: 10,
                justifyContent: 'space-evenly',
                marginBottom: 20,
              }}>
              <TouchableOpacity
                style={{
                  width: scale(111),
                  height: scale(40),
                  borderRadius: 10,
                  backgroundColor: '#FFFFFF',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderColor: '#0093E9',
                  borderWidth: 1,
                }}
                onPress={() => props.navigation.goBack()}>

                <Text
                  style={{
                    fontSize: scale(13),
                    color: '#0093E9',
                    fontWeight: 'bold',
                    marginLeft: scale(10),
                  }}>
                  {t('cancel')}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  send_Shipment();
                }}
                style={{
                  width: scale(195),
                  height: scale(40),
                  borderRadius: 8,
                  flexDirection: 'row',
                  backgroundColor: '#FA7923',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                <Image
                  style={{ width: scale(15), height: scale(15) }}

                  source={require('../../assets/receive.png')}
                />
                <Text
                  style={{
                    fontSize: scale(15),
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                  }}>
                  {t('Receive Shipment')}
                </Text>
              </TouchableOpacity>

            </View>
          )}
          <View style={{ height: DeviceWidth }} />

        </KeyboardAwareScrollView>

      </View>)
  } else {
    return (
      <View>
        {!opneScanner ? (
          <KeyboardAwareScrollView extraScrollHeight={50} >
            <View
              style={{
                // alignItems: 'center',
              }}>
              <StatusBar backgroundColor="#0093E9" />
              <HeaderWithBackBlue
                name={t('Receive Shipment')}
                navigation={props.navigation}
              />
              <PopUp1
                isModalVisible={isModalVisible2}
                image={require('../../assets/fail.png')}
                text={t("Unauthorized Scan!")}
                text2={
                  errorMessage2 ||
                  t('You don’t have permission to scan this shipment.Would you like to send an access request?')
                }
                label={t("Cancel")}
                label1={t("SEND REQUEST")}
                Ok={() => sendRequest()}
                cancel={() => fail()}
              />


              <View
                style={{
                  backgroundColor: '#FFFFFF',
                  borderRadius: 10,
                  paddingTop: 20,
                  marginTop: verticalScale(-DeviceWidth / 6),
                  // alignItems: 'center',
                  margin: verticalScale(10),
                }}>

                <CustomInputText
                  label={"Shipment ID"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  onChange={(val) => setUniqueID(val)}
                  val={uniqueID}
                  disabled={uniqueID1}

                />
                <Text
                  style={{
                    alignSelf: 'center',
                    color: '#0093E9',
                    paddingBottom: 10
                  }}>
                  {t('OR')}
                </Text>
                <CustomInputText
                  label={"Transit Number"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  onChange={(val) => setUniqueID1(val)}
                  val={uniqueID1}
                  disabled={uniqueID}

                />
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    marginBottom: 15,
                    marginTop: 10
                  }}>
                  <View style={{ flex: 0.7 }} />
                  <View
                    style={{
                      flexDirection: 'row',
                      // paddingTop: 15,
                      // marginRight: 15
                    }}>
                    <TouchableOpacity
                      style={{
                        width: scale(100),
                        height: scale(40),
                        borderRadius: 10,
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'space-evenly',
                        alignItems: 'center',
                        borderColor: '#58b1da',
                        borderWidth: 1,
                        marginRight: 15
                      }}
                      onPress={() => props.navigation.goBack()}>
                      <Text
                        style={{
                          fontSize: scale(13),
                          color: '#0093E9',
                          fontWeight: 'bold',
                          marginLeft: scale(10),
                        }}>
                        {t('Cancel')}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        width: scale(100),
                        height: scale(40),
                        borderRadius: 10,
                        backgroundColor:
                          !uniqueID && !uniqueID1
                            ? 'gray'
                            : Colors.blueE9,
                        justifyContent: 'space-evenly',
                        alignItems: 'center',
                      }}
                      disabled={!uniqueID && !uniqueID1}
                      onPress={() =>
                        getDetails(uniqueID || uniqueID1)
                      }>
                      <Text
                        style={{
                          fontSize: scale(13),
                          color: '#FFFFFF',
                          fontWeight: 'bold',
                          marginLeft: scale(10),
                        }}>
                        {t('Next')}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <CustomQrcodeScanner
                updateProductQty={(value) => updatePdtQty(value)}
                closeScanner={() => setOnReceiveOption(false)}
                token={props.token}
              />

            </View>
          </KeyboardAwareScrollView>
        ) : (
          opneScanScreen()
        )}
      </View>)
  }

}


function mapStateToProps(state) {
  return {
    loder: state.loder,
    filterOutBound: state.shipment.filterOutBound,
    filterInbound: state.shipment.filterInbound,
    token: state.userinfo.token,
    userpermission: state.userinfo.userpermission,
  };
}

export default connect(
  mapStateToProps,
  {
    receiveShipment,
    getchainOfCustody,
    getViewShipment,
    fetchInboundShipments,
    fetchOutboundShipments,
  },
)(ReceiveShipmentScreen);
