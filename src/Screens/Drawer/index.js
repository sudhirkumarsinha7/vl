import React, { useEffect, useState } from 'react';
import {
  Button,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import Logout_PopUp from '../../components/Logout_PopUp';
import Icon from 'react-native-vector-icons/FontAwesome';
import '../../languages/i18n'
import {
  fetchProfileImage
} from '../../Redux/Action';
import { useTranslation } from 'react-i18next';
import { Colors, DeviceHeight, DeviceWidth } from '../../Style';
import setAuthToken from '../../Util/setAuthToken';
import { GoogleSignin, } from '@react-native-google-signin/google-signin';

const MenuListComponent = (props) => {
  const { img = '', lable } = props
  const { t, i18n } = useTranslation();

  return <TouchableOpacity
    style={{
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: verticalScale(30),
    }}
    onPress={() => props.navigateScreen()}>
    <Image
      style={{ width: scale(22), height: scale(22) }}
      source={img}
      resizeMode="contain"
    />
    <Text
      style={{
        fontSize: scale(16),
        marginLeft: scale(15),
        color: '#0093E9',
      }}>
      {t(lable)}
    </Text>
  </TouchableOpacity>
}

const Custom_Side_Menu = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const { t, i18n } = useTranslation();

  useEffect(() => {
    async function fetchData() {
      getImage();
    }
    fetchData();
  }, []);

  const getImage = async () => {
    const { user = {} } = props;
    props.fetchProfileImage(user.photoId)
  }
  const _signout = async () => {
    setIsModalVisible(true)
  };
  const _DeleteProf = async () => {
    alert('Not Implemented yet')
  }
  const Cancel = () => {
    setIsModalVisible(false)
  };
  const Logout = async () => {
    setIsModalVisible(false)
    await AsyncStorage.clear();
    await GoogleSignin.signOut();
    setAuthToken('')
    props.navigation.navigate('Login');
  };
  const { user = {}, activeWareHouse = 0, userProfPic } = props;
  let { warehouses = [] } = user;
  warehouses = warehouses.filter(each => {
    if (each.status == 'ACTIVE') {
      return each;
    }
  })
  const { warehouseAddress } =
    warehouses.length &&
    activeWareHouse < warehouses.length &&
    warehouses[activeWareHouse];
  const selecteWh =
    warehouses.length &&
    activeWareHouse < warehouses.length &&
    warehouses[activeWareHouse];
  // console.log('Custom_Side_Menu user' + JSON.stringify(user));
  const location =
    warehouseAddress &&
      warehouseAddress.country &&
      warehouseAddress &&
      warehouseAddress.city
      ? warehouseAddress.city + ',' + warehouseAddress.country
      : (warehouseAddress && warehouseAddress.city) ||
      (warehouseAddress && warehouseAddress.country) ||
      '';


  return (
    <ScrollView>
      <SafeAreaView
        style={{ alignItems: 'center' }}
        forceInset={{ top: 'always', horizontal: 'never' }}>
        <Logout_PopUp
          Cancel={Cancel}
          Logout={Logout}
          isModalVisible={isModalVisible}
        />
        <View
          style={{
            marginTop: verticalScale(70),
            justifyContent: 'center',
            alignItems: 'center',
            width: scale(91),
            height: scale(91),
            borderRadius: userProfPic ? 400 : 0,
          }}>
          {userProfPic ? (
            <Image
              style={{ width: scale(91), height: scale(91), borderRadius: 400 }}
              resizeMode="cover"
              source={{
                uri: userProfPic
              }}
            />
          ) : (
            <Image
              style={{ width: 91, height: 91 }}
              resizeMode="cover"
              source={require('../../assets/user.png')}
            />
          )}
        </View>
        <Text
          style={{
            fontSize: scale(13),
            color: '#0B65C1',
            fontWeight: 'bold',
            marginTop: verticalScale(7),
          }}>
          {user.firstName + ' ' + user.lastName}
        </Text>
        <Text
          style={{
            fontSize: scale(8),
            color: '#0B65C1',
            marginTop: verticalScale(3),
          }}>
          {user.organisation}
        </Text>
        <View style={{ width: '100%', marginLeft: scale(36) }}>
          <MenuListComponent
            img={require('../../assets/user.png')}
            lable={'profile'}
            navigateScreen={() => {
              props.navigation.navigate('Profile');
              props.navigation.closeDrawer();
            }}
          />
          <MenuListComponent
            img={require('../../assets/contactUs.png')}
            lable={'Contact_us'}
            navigateScreen={() => {
              props.navigation.navigate('Contact');
              props.navigation.closeDrawer();
            }}
          />
          <MenuListComponent
            img={require('../../assets/lang.png')}
            lable={'Change language'}
            navigateScreen={() => {
              props.navigation.navigate('CangeLang');
              props.navigation.closeDrawer();
            }}
          />
          <MenuListComponent
            img={require('../../assets/Aboutus.png')}
            lable={'about_us'}
            navigateScreen={() => {
              props.navigation.navigate('AboutUs');
              props.navigation.closeDrawer();
            }}
          />
          <MenuListComponent
            img={require('../../assets/deleteblue.png')}

            lable={'Delete Profile'}
            navigateScreen={() => {
              _DeleteProf();
              props.navigation.closeDrawer();
            }}
          />
          <MenuListComponent
            img={require('../../assets/logout.png')}
            lable={'logout'}
            navigateScreen={() => {
              _signout();
              props.navigation.closeDrawer();
            }}
          />


          <TouchableOpacity
            style={{
              marginTop: DeviceWidth / 4,
              marginRight: 30,
            }}
            onPress={() => {
              props.navigation.navigate('SwitchLocation');
              props.navigation.closeDrawer();
            }}>
            <LinearGradient
              start={{ x: 0.1, y: 1.0 }}
              end={{ x: 1.0, y: 0.0 }}
              // locations={[0, 0.5, 0.6]}
              colors={['#0093E9', '#36C2CF']}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 30,
                flexDirection: 'row',
                flex: 1,
                padding: 10,
              }}
              opacity={1}>
              <View style={{ flex: 0.1 }}>
                <Image
                  style={{ width: scale(25), height: scale(25) }}
                  source={require('../../assets/locationwhite.png')}
                  resizeMode="contain"
                />
              </View>
              <View style={{ flex: 0.8, marginLeft: 10 }}>
                <Text
                  style={{
                    fontSize: scale(15),
                    color: 'white',
                  }}>
                  {selecteWh.title}
                </Text>
                <Text
                  style={{
                    fontSize: scale(10),
                    color: 'white',
                  }}>
                  {location}
                </Text>
              </View>
              <View style={{ flex: 0.1 }}>
                <Icon name="angle-right" color="white" size={50} />
              </View>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </ScrollView>
  );

}

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    token: state.userinfo.token,
    activeWareHouse: state.userinfo.activeWareHouse,
    userProfPic: state.userinfo.userProfPic,
  };
}


export default connect(
  mapStateToProps,
  { fetchProfileImage },
)(Custom_Side_Menu);