import React, { useState, useEffect } from 'react';
import {
  Button,
  View,
  Text,
  StatusBar,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  ScrollView,
  ActivityIndicator,
  FlatList,
  KeyboardAvoidingView,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  RefreshControl,
} from 'react-native';
import { connect } from 'react-redux';
import {
  userinfo,
  upload_Image,
  user_update,
  fetchOrganizations,
  addWarehouse,
  updateWarehouse,
  getAllRegion,
  getAllCountries,
  getAllState,
  getAllCities,
  fetchProfileImage
} from '../../../Redux/Action';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Header, {
  HeaderWithBackShipmentScreen,
} from '../../../components/Header';
import ImagePicker from 'react-native-image-picker';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import { WareHouseDetails } from '../../../components/Common/profileHelper';
import moment from 'moment';
import { Colors, DeviceWidth } from '../../../Style';
import { ToastShow } from '../../../components/Toast';
import {
  profileData,
} from '../../../components/Common/defaultValue';
import {
  CustomInputText
} from '../../../components/Common/common';
import { config, GoogleMapsApiKey, global } from '../../../Util/config';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Geocoder from 'react-native-geocoder';
import PhoneInput, { isValidNumber } from 'react-native-phone-number-input';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useTranslation } from 'react-i18next';
import { formatDateMMYYYY } from '../../../Util/utils';
import { SelectOptionPopUp } from '../../../components/ImageSelect';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';


// simply add your google key
Geocoder.fallbackToGoogle(GoogleMapsApiKey);
const Profile = (props) => {
  const { t, i18n } = useTranslation();
  const [isModalVisible, setModalVisible] = useState(false);

  const [edit, SetEdit] = useState(false)
  const [userImg, setUserImage] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')

  useEffect(() => {
    async function fetchData() {
      _data();
      requestCameraPermission()
    }
    fetchData();

  }, []);

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "App Camera Permission",
          message: "App needs access to your camera ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Camera permission given");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };
  const _data = async () => {
    const token = await AsyncStorage.getItem('token');
    console.log('renderprofile token', token);
    const { user } = props;
    await props.fetchOrganizations();
    props.getAllRegion();
    const orgSplit = props.user.organisation?.split('/');
    const ownOrg = orgSplit[0];
    const orgId = orgSplit[1];
    // getWarehouse(orgId);
    //let {role,organisation,email,phone} = state;
    // let userImage = user.photoId
    //   ? config().fetchProfileImage + user.photoId
    //   : '';
    let organisationDetails = props.Organizations.filter(
      eachOrganisation => {
        if (eachOrganisation.id === orgId) {
          return eachOrganisation;
        }
      },
    );
    let userImage = ''
    if (user.photoId) {
      userImage = await props.fetchProfileImage(user.photoId)
    }
    let selectedOrg = organisationDetails[0];
    setFirstName(user.firstName)
    setLastName(user.lastName)
    setPhoneNumber(user.phoneNumber)


  };

  const _edit = () => {
    SetEdit(true)
    requestCameraPermission()
  }
  const _cancel = () => {
    _data();
    SetEdit(false)
  }

  const _save = async () => {
    const { user } = props;

    let phoneNumber1 = phoneNumber ? phoneNumber.replace('+', '') : '';
    phoneNumber1 = phoneNumber1 ? '+' + phoneNumber1 : '';

    // console.log('phoneNumber1 ' + phoneNumber1);
    const data = {
      firstName: firstName,
      lastName: lastName,
      phoneNumber: phoneNumber1 || '',
      organisation: user.organisation,
      location: user.location,
      warehouseId: user.warehouseId,
    };

    const re = /^(?!\s*$|\s).*$/;

    if (!re.test(firstName) || !re.test(lastName)) {
      ToastShow(
        t('All Fields Are Mandatory Or Input Should not start with blank'),
        'error',
        'long',
        'top',
      )
    } else {
      let result;

      result = await props.user_update(data);
      if (result.status === 200) {
        SetEdit(false)
        const smsg = t('Success');
        ToastShow(
          smsg,
          'success',
          'long',
          'top',
        )
      } else if (result.status === 500) {

        ToastShow(
          'API Filed with 500',
          'error',
          'long',
          'top',
        )
      } else {

        const emsg = (result && result.data && result.data.message) ||
          t('Please Check All fileld');

        ToastShow(
          emsg,
          'error',
          'long',
          'top',
        )
      }
    }
  };

  const chooseFile = () => {
    setModalVisible(true)
  };
  const chooseFileFromCamera = () => {
    var options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchCamera(options, response => {
      console.log('User response' + JSON.stringify(response));
      if (response.didCancel) {
        console.log('User canceled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        // alert(response.customButton);
      } else if (response.assets) {
        handleUploadPhoto(response.assets[0])
      }
      setModalVisible(false)

    });
  };
  const chooseFileFromGallary = () => {
    var options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, response => {
      console.log('User response' + JSON.stringify(response));
      if (response.didCancel) {
        console.log('User canceled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        // alert(response.customButton);
      } else if (response.assets) {
        handleUploadPhoto(response.assets[0])
      }
      setModalVisible(false)

    });
  };
  const createFormData = (filePath, body) => {
    const data = new FormData();

    setUserImage(filePath.uri)
    data.append('photo', {
      name: filePath.fileName || filePath.uri,
      type: filePath.type,
      uri:
        Platform.OS === 'android'
          ? filePath.uri
          : filePath.uri.replace('file://', ''),
    });

    Object.keys(body).forEach(key => {
      data.append(key, body[key]);
    });
    console.log('image data', data);
    return data;
  };
  const handleUploadPhoto = async (filePath) => {
    const { user } = props
    const result = await props.upload_Image(
      createFormData(filePath, { userId: '123' })
    );
    await props.userinfo();
    await props.fetchProfileImage(user.photoId)
  };


  const onRefresh = async () => {
    _data();
  };

  const eachWareHouse = (item, index) => {
    return (
      <WareHouseDetails
        item={item}
        index={index}
        navigation={props.navigation}
      />
    );

  }

  const { user, userProfPic } = props;

  return (

      <View
        style={{
          // flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackShipmentScreen
          name={t('profile')}
          navigation={props.navigation}
          t={t}
        />
        <SelectOptionPopUp
          TakePic={chooseFileFromCamera}
          selectPic={chooseFileFromGallary}
          isModalVisible={isModalVisible}
          cancel={() => setModalVisible(false)}
        />
        <View style={{ marginTop: verticalScale(-DeviceWidth / 7) }} />
        <KeyboardAwareScrollView extraScrollHeight={50} >

        <ScrollView>
          <View
            style={{
              // width: DeviceWidth / 1.1,
              margin: 15,
              borderRadius: 8,
              backgroundColor: '#FFFFFF',
            }}
            refreshControl={
              <RefreshControl
                //refresh control used for the Pull to Refresh
                onRefresh={onRefresh}
                colors={['#a076e8', '#5dc4dd']}
              />
            }>
            {!edit ? (
              <TouchableOpacity
                style={{
                  width: scale(57),
                  height: scale(20),
                  borderRadius: 8,
                  borderColor: '#0093E9',
                  borderWidth: 1,
                  backgroundColor: 'transparent',
                  marginTop: verticalScale(10),
                  alignSelf: 'flex-end',
                  marginRight: scale(10),
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                }}
                onPress={() => _edit()}>
                <Image
                  style={{ width: scale(10.89), height: scale(10.89) }}
                  source={require('../../../assets/edit.png')}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    color: '#0093E9',
                    fontSize: scale(10),
                  }}>
                  {t('edit')}
                </Text>
              </TouchableOpacity>
            ) : (
              <View style={{ alignSelf: 'flex-end', flexDirection: 'column' }}>
                <TouchableOpacity
                  style={{
                    width: scale(57),
                    height: scale(18),
                    borderRadius: 8,
                    backgroundColor: 'transparent',
                    marginTop: verticalScale(10),
                    alignSelf: 'flex-end',
                    marginRight: scale(10),
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}
                  onPress={() => _save()}>
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={['#0093E9', '#36C2CF']}
                    style={{
                      width: scale(57),
                      height: scale(18),
                      borderRadius: 8,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: '#FFFFFF',
                        fontSize: scale(10),
                        fontWeight: 'bold',
                      }}>
                      {t('save')}
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{
                    position: 'relative',
                    width: scale(57),
                    height: scale(18),
                    borderRadius: 8,
                    borderColor: '#0093E9',
                    borderWidth: 1,
                    backgroundColor: 'transparent',
                    marginTop: verticalScale(10),
                    alignSelf: 'flex-end',
                    marginRight: scale(10),
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}
                  onPress={() => _cancel()}>
                  <Text
                    style={{
                      color: '#0093E9',
                      fontSize: scale(10),
                    }}>
                    {t('Cancel')}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
            <View style={{ alignItems: 'center' }}>
              <View
                style={{
                  marginTop: verticalScale(20),
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: scale(114),
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    width: scale(114),
                    height: scale(114),
                    borderRadius: 200,
                  }}>
                  {userImg ? (
                    <Image
                      style={{
                        width: scale(114),
                        height: scale(114),
                        borderRadius: 400,
                      }}
                      resizeMode="cover"
                      source={{ uri: userImg }}
                    />
                  ) : (
                    <Image
                      style={{
                        width: scale(114),
                        height: scale(114),
                        borderRadius: 400,
                      }}
                      resizeMode="cover"
                      source={
                        userProfPic
                          ? {
                            uri: userProfPic,
                            // headers: {
                            //   'Content-Type': 'application/json',
                            //   Authorization: 'Bearer ' + state.token,
                            // },
                            // method: 'GET',
                          }
                          : require('../../../assets/user.png')
                      }
                    />
                  )}
                  {edit ? (
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => chooseFile()}
                      style={{
                        position: 'absolute',
                        left: scale(75),
                        alignSelf: 'flex-start',
                        width: scale(20),
                        height: scale(20),
                        borderRadius: 400,
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderColor: '#0093E9',
                        borderWidth: 1,
                      }}>
                      <Image
                        style={{ height: scale(9.79), width: scale(13.18) }}
                        source={require('../../../assets/edit.png')}
                      />
                    </TouchableOpacity>
                  ) : null}
                </View>
              </View>
            </View>

            {!edit ? (
              <View>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: scale(13),
                      marginTop: verticalScale(5),
                    }}>
                    {user.role}
                  </Text>
                  <Text
                    style={{
                      fontSize: scale(14),
                      fontWeight: 'bold',
                      marginTop: verticalScale(5),
                    }}>
                    {user.firstName + ' ' + user.lastName}
                  </Text>
                  <Text style={{
                    fontSize: scale(14),
                    marginTop: verticalScale(5),
                  }}>{t('joined_on') + ' ' + formatDateMMYYYY(user.signup_date)}</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    margin: 10,
                  }}>
                  <Image
                    source={require('../../../assets/organization.png')}
                    style={{ width: 19, height: 16 }}
                  />
                  <Text
                    style={{
                      fontSize: scale(13),
                      marginLeft: 10
                    }}>
                    {user.organisation}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    margin: 10,
                  }}>
                  <Image
                    source={require('../../../assets/mail.png')}
                    style={{ width: 22, height: 16 }}
                  />
                  <Text
                    style={{
                      fontSize: scale(13),
                      marginLeft: 10

                    }}>
                    {user.emailId}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    margin: 10,
                  }}>
                  <Icon name={'call'} color={'gray'} size={25} />
                  <Text
                    style={{
                      fontSize: scale(13),
                    }}>
                    {user.phoneNumber ? '  ' + user.phoneNumber : ' N/A'}
                  </Text>
                </View>
              </View>
            ) : (
              <View>
                <CustomInputText
                  label={"First Name"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  onChange={(text) => setFirstName(text)}
                  val={firstName}

                />
                <CustomInputText
                  label={"Last Name"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  onChange={(text) => setLastName(text)}
                  val={lastName}

                />
                <CustomInputText
                  label={profileData.organisationId.displayName}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  onChange={console.log('organisationId')}
                  val={user.organisation}
                  disabled={true}

                />
                <CustomInputText
                  label={"Phone"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  onChange={(text) => setPhoneNumber(text)}
                  val={phoneNumber}

                />
                {/* <PhoneInput
         
          textStyle={{ fontSize: 14, marginBottom: 5 }}
          initialCountry={global.defaultCountrycode}
          initialValue={signinform?.values?.formattedmob}
          onChangeFormattedText={(text)=>setPhoneNumber(text)}
          onChange={(test)=>console.log('test')}
         
        /> */}
              </View>
            )}
          </View>
          {!edit ? (
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 15,
                marginRight: 15,
                justifyContent: 'space-between',
                flex: 1,
              }}>
              <Text
                style={{
                  fontSize: scale(13),
                  fontWeight: 'bold',
                  margin: verticalScale(7),
                  flex: 0.7,
                }}>
                {t('my_locations')}
              </Text>
              <TouchableOpacity
                onPress={() => props.navigation.navigate('AddWareHouse')}
                style={{
                  backgroundColor: 'orange',
                  borderRadius: 10,
                  // flex: 0.3,
                  justifyContent: 'center'
                }}>
                <Text
                  style={{
                    fontSize: scale(13),
                    fontWeight: 'bold',
                    margin: verticalScale(7),
                    color: '#FFFFFF',
                  }}>
                  {'+ ' + t('add_new_location')}
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
          {props.loder ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}>
              <ActivityIndicator color="#0000ff" size="large" />
            </View>
          ) : null}
          <FlatList
            onEndReachedThreshold={0.7}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={user && user.warehouses || []}
            renderItem={({ item, index }) => eachWareHouse(item, index)}
          />

        </ScrollView>
        <View style={{ height: DeviceWidth/2 }} />

        </KeyboardAwareScrollView>

      </View>
    );
}

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    loder: state.loder,
    Organizations: state.productdetail.Organizations,
    token: state.userinfo.token,
    regions: state.productdetail.regions,
    countries: state.productdetail.countries,
    states: state.productdetail.states,
    cities: state.productdetail.cities,
    userProfPic: state.userinfo.userProfPic,
  };
}

export default connect(mapStateToProps, {
  upload_Image,
  user_update,
  fetchOrganizations,
  addWarehouse,
  userinfo,
  updateWarehouse,
  getAllRegion,
  getAllCountries,
  getAllState,
  getAllCities,
  fetchProfileImage
},
)(Profile);
