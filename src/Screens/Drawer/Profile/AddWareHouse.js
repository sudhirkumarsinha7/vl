import React, { useState, useEffect } from 'react';
import {
  LayoutAnimation,
  UIManager,
  Platform,
  Button,
  View,
  ScrollView,
  Text,
  RefreshControl,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  ActivityIndicator,
  FlatList,
  Keyboard,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import { connect } from 'react-redux';
import {
  addWarehouse,
  updateWarehouse,
  getAllRegion,
  getAllCountries,
  getAllState,
  getAllCities,
} from '../../../Redux/Action/auth';
import {
  userinfo,
} from '../../../Redux/Action/order';
import { config, GoogleMapsApiKey, global } from '../../../Util/config';
import LinearGradient from 'react-native-linear-gradient';
import Header, { HeaderWithBackBlue } from '../../../components/Header';
import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';

import _ from 'lodash';
import {
  CountriesData,
  Region,
} from '../../../components/Common/defaultValue';
import { getCurrenntLocation, getAddress, getLatLong } from '../../../Util/utils';
import Geocoder from 'react-native-geocoder';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useTranslation } from 'react-i18next';
import { Colors, DeviceWidth } from '../../../Style';
import { ToastShow } from '../../../components/Toast'
import { CustomDropdown, CustomInputText, MonthYearPickerView, CustomDropDownLocation } from '../../../components/Common/common';

// simply add your google key
Geocoder.fallbackToGoogle(GoogleMapsApiKey);
const AddWareHouse = (props) => {
  const { t, i18n } = useTranslation();

  const [isUserLoc, setUSerLoc] = useState(false)
  const [isLoading, setLoading] = useState(false)
  const [location, setLocation] = useState({})
  const [mandatory, setIsmantory] = useState(false)

  useEffect(() => {
    async function fetchData() {
      GetData();
    }
    fetchData();
  }, []);

  var validationSchema = Yup.object().shape(
    {
      title: Yup.string().required(t('Required')),
      zipCode: Yup.string().required(t('Required')),
      firstLine: Yup.string().required(t('Required')),
      city: Yup.string().required(t('Required')),
      state: Yup.string().required(t('Required')),
      city: Yup.string().required(t('Required')),
      country: Yup.string().required(t('Required')),
      region: Yup.string().required(t('Required')),
    },
    [],
  ); // <-- HERE!!!!!!!!
  const WarehoueseForm = useFormik({
    initialValues: {
      title: '',
      zipCode: '',
      firstLine: '',
      secondLine: '',
      landmark: '',
      city: '',
      cityId: '',
      state: '',
      stateId: '',
      country: '',
      countryId: '',
      region: '',

    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _Save = () => {
    setIsmantory(true);
    Keyboard.dismiss();
    WarehoueseForm.handleSubmit();
  };
  GetData = async () => {
    props.userinfo();
    const orgSplit = props.user.organisation?.split('/');
    const ownOrg = orgSplit[0];
    const orgId = orgSplit[1];
    props.getAllRegion();
    if (global.defaultCountrycode === 'cr') {
      let costa = { "value": 53, "label": "Costa Rica" }
      WarehoueseForm.handleChange({ target: { name: `region`, value: 'Americas' } })
      WarehoueseForm.handleChange({ target: { name: `country`, value: 'Costa Rica' } })

      onCountryChange(costa)
    }
  };
  const handleSubmit = async values => {
    AddLocation(values)
  };
  const onRegionChange = item => {
    WarehoueseForm.handleChange({ target: { name: `region`, value: item.value } })
    WarehoueseForm.handleChange({ target: { name: `country`, value: '' } })
    WarehoueseForm.handleChange({ target: { name: `countryId`, value: '' } })
    WarehoueseForm.handleChange({ target: { name: `state`, value: '' } })
    WarehoueseForm.handleChange({ target: { name: `stateId`, value: '' } })
    WarehoueseForm.handleChange({ target: { name: `city`, value: '' } })
    WarehoueseForm.handleChange({ target: { name: `cityId`, value: '' } })
    setUSerLoc(false)

    props.getAllCountries(item.value);
  };
  const onCountryChange = item => {
    WarehoueseForm.handleChange({ target: { name: `country`, value: item.label } })
    WarehoueseForm.handleChange({ target: { name: `countryId`, value: item.value } })
    WarehoueseForm.handleChange({ target: { name: `state`, value: '' } })
    WarehoueseForm.handleChange({ target: { name: `stateId`, value: '' } })
    WarehoueseForm.handleChange({ target: { name: `city`, value: '' } })
    WarehoueseForm.handleChange({ target: { name: `cityId`, value: '' } })
    props.getAllState(item.value);
  };
  const onStateChange = item => {

    WarehoueseForm.handleChange({ target: { name: `state`, value: item.label } })
    WarehoueseForm.handleChange({ target: { name: `stateId`, value: item.value } })
    WarehoueseForm.handleChange({ target: { name: `city`, value: '' } })
    WarehoueseForm.handleChange({ target: { name: `cityId`, value: '' } })
    props.getAllCities(item.value);
  };
  const onCityChange = item => {
    WarehoueseForm.handleChange({ target: { name: `city`, value: item.label } })
    WarehoueseForm.handleChange({ target: { name: `cityId`, value: item.value } })
  };

  const getCurrentLocation = async () => {
    setUSerLoc(true);
    setLoading(true)
    const loc = await getCurrenntLocation();
    let coords = loc.coords;

    var location = {
      latitude: coords.latitude,
      longitude: coords.longitude,
    };
    console.log('getCurrenntLocation location ' + JSON.stringify(location));
    setLocation(location)

    Geocoder.geocodePosition({
      lat: location.latitude,
      lng: location.longitude,
    })
      .then(ret => {
        // res is an Array of geocoding object (see below)
        console.log('getAddress ret' + JSON.stringify(ret[0]));
        let res = ret[0];
        let list = CountriesData.filter(each => {
          if (res.country === each.country) {
            return each;
          }
        });
        WarehoueseForm.handleChange({ target: { name: `zipCode`, value: res.postalCode } })
        WarehoueseForm.handleChange({ target: { name: `firstLine`, value: res.formattedAddress } })
        WarehoueseForm.handleChange({ target: { name: `landmark`, value: res.streetName } })
        WarehoueseForm.handleChange({ target: { name: `region`, value: list[0].continent } })
        WarehoueseForm.handleChange({ target: { name: `country`, value: res.country } })
        WarehoueseForm.handleChange({ target: { name: `state`, value: res.adminArea } })
        WarehoueseForm.handleChange({ target: { name: `city`, value: res.subAdminArea } })
      })
      .catch(err => {
        console.log('screen2:error' + JSON.stringify(err));
      });
    setLoading(false)
  }
  const AddLocation = async (values) => {
    let {
      title,
      zipCode,
      firstLine,
      secondLine,
      landmark,
      city,
      state,
      country,
      region,
    } = values;
    const orgSplit = props.user.organisation?.split('/');
    const ownOrg = orgSplit[0];
    const orgId = orgSplit[1];
    let organisationDetails = props.Organizations.filter(
      eachOrganisation => {
        if (eachOrganisation.id === orgId) {
          return eachOrganisation;
        }
      },
    );
    let selectedOrg = organisationDetails[0];
    let cordinate = await getLatLong(firstLine);
    let latLang = cordinate || location;
    console.log('getAddress:latLang123 ' + JSON.stringify(latLang));
    let data = {
      title,
      organisationId: orgId,
      postalAddress: selectedOrg.postalAddress,
      region: region || selectedOrg.region,
      country: selectedOrg.country,
      location: {
        longitude: latLang && latLang.latitude,
        latitude: latLang && latLang.longitude,
      },
      warehouseAddress: {
        firstLine,
        secondLine,
        city: city,
        region,
        state: state,
        country: country,
        landmark,
        zipCode,
      },
      supervisors: selectedOrg.supervisors,
      employeess: [selectedOrg.primaryContactId],
    };
    // console.log('addWarehouse data ' + JSON.stringify(data));
    const result = await props.addWarehouse(data);
    console.log('addWarehouse result ' + JSON.stringify(result));
    if (result.status === 200) {
      WarehoueseForm.resetForm()
      ToastShow(
        t('Location added. Awaiting for admin approval.'),
        'success',
        'long',
        'top',
      )
    } else if (result.status === 500) {
      ToastShow(
        t('API Filed with 500'),
        'error',
        'long',
        'top',
      )
    } else {
      let errorMessage = result && result.data && result.data.message || 'Somthing went wrong'
      ToastShow(
        t(errorMessage),
        'error',
        'long',
        'top',
      )
    }
    await props.userinfo();

  }

  return (
    <KeyboardAwareScrollView extraScrollHeight={50} >

      <View style={styles.container}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          navigation={props.navigation}
          name={t('add_new_location')}
        />

        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{ marginTop: verticalScale(-DeviceWidth / 7) }}>

          <View
            style={{ backgroundColor: '#FFFFFF', margin: 15, borderRadius: 10 }}>
            <View style={{ marginTop: 30, }} />
            <CustomInputText
              label={"Address Title"}
              labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
              onChange={WarehoueseForm.handleChange(`title`)}
              val={WarehoueseForm?.values?.title}
              errorMsg={WarehoueseForm.errors && WarehoueseForm.errors.title}
              mandatory={mandatory}

            />
            <View style={{ marginTop: -10, }} />

            {!isUserLoc ? (
              <View>
                {global.defaultCountrycode === 'cr' ? null : <View>
                  <CustomDropdown
                    displayName={'Region'}
                    stateName={WarehoueseForm?.values?.region}
                    dropDowndata={props.regions}
                    onChangeValue={(val) => onRegionChange(val)}
                    mandatory={mandatory}
                  />
                  <CustomDropdown
                    displayName={'Country'}
                    stateName={WarehoueseForm?.values?.countryId}
                    dropDowndata={props.countries}
                    label={'name'}
                    mapKey={'id'}
                    onChangeValue={(val) => onCountryChange(val)}
                    mandatory={mandatory}
                  />


                </View>}

                <CustomDropdown
                  displayName={'State'}
                  stateName={WarehoueseForm?.values?.stateId}
                  dropDowndata={props.states}
                  label={'name'}
                  mapKey={'id'}
                  onChangeValue={(val) => onStateChange(val)}
                  mandatory={mandatory}
                />
                <CustomDropdown
                  displayName={'City/Town'}
                  stateName={WarehoueseForm?.values?.cityId}
                  dropDowndata={props.cities}
                  label={'name'}
                  mapKey={'id'}
                  onChangeValue={(val) => onCityChange(val)}
                  mandatory={mandatory}
                />
                <View style={{ marginTop: 20, }} />

              </View>
            ) : (
              <View>

                <CustomInputText
                  label={"Region"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  val={WarehoueseForm?.values?.region}
                  errorMsg={WarehoueseForm.errors && WarehoueseForm.errors.region}
                  disabled={true}
                  mandatory={mandatory}

                />
                <CustomInputText
                  label={"Country"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  val={WarehoueseForm?.values?.country}
                  errorMsg={WarehoueseForm.errors && WarehoueseForm.errors.country}
                  disabled={true}
                  mandatory={mandatory}

                />
                <CustomInputText
                  label={"State"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  val={WarehoueseForm?.values?.state}
                  errorMsg={WarehoueseForm.errors && WarehoueseForm.errors.state}
                  disabled={true}
                  mandatory={mandatory}

                />
                <CustomInputText
                  label={"City/Town"}
                  labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
                  val={WarehoueseForm?.values?.city}
                  errorMsg={WarehoueseForm.errors && WarehoueseForm.errors.city}
                  disabled={true}
                  mandatory={mandatory}

                />

              </View>
            )}

            <CustomInputText
              label={"Address Line"}
              labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
              onChange={WarehoueseForm.handleChange(`firstLine`)}
              val={WarehoueseForm?.values?.firstLine}
              errorMsg={WarehoueseForm.errors && WarehoueseForm.errors.firstLine}
              mandatory={mandatory}

            />
            <CustomInputText
              label={"Pincode"}
              labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
              onChange={WarehoueseForm.handleChange(`zipCode`)}
              val={WarehoueseForm?.values?.zipCode}
              errorMsg={WarehoueseForm.errors && WarehoueseForm.errors.zipCode}
              mandatory={mandatory}

            />

            {props.loder ? (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  flex: 1,
                }}>
                <ActivityIndicator color="#0000ff" size={'large'} />
              </View>
            ) : null}
            <View>
              <TouchableOpacity
                onPress={() => getCurrentLocation()}
                style={{
                  borderRadius: 10,
                  margin: 20,
                }}>
                <LinearGradient
                  start={{ x: 0.1, y: 1.0 }}
                  end={{ x: 1.0, y: 0.0 }}
                  locations={[0, 0.5, 0.6]}
                  colors={['#0093E9', '#36C2CF']}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10,
                  }}>
                  <Text
                    style={{
                      fontSize: scale(13),
                      fontWeight: 'bold',
                      textAlign: 'center',
                      padding: 10,
                      color: '#FFFFFF',
                    }}>
                    {t('use_current_location')}
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => _Save()}
                style={{
                  backgroundColor: 'orange',
                  borderRadius: 10,
                  margin: 20,
                }}>
                <Text
                  style={{
                    fontSize: scale(13),
                    fontWeight: 'bold',
                    padding: 10,
                    color: '#FFFFFF',
                    textAlign: 'center',
                  }}>
                  {t('admin_aprove')}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ height: scale(10) }} />
        </ScrollView>
      </View>
    </KeyboardAwareScrollView>);
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent',
  },
  lottie: {
    width: scale(100),
    height: scale(100),
  },
});
function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    loder: state.loder,
    Organizations: state.productdetail.Organizations,
    token: state.userinfo.token,
    regions: state.productdetail.regions,
    countries: state.productdetail.countries,
    states: state.productdetail.states,
    cities: state.productdetail.cities,
  };
}


export default connect(mapStateToProps, {
  addWarehouse,
  userinfo,
  updateWarehouse,
  getAllRegion,
  getAllCountries,
  getAllState,
  getAllCities,
},
)(AddWareHouse);