import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
  Linking,
} from 'react-native';
import Header, {
  HeaderWithBackBlue,
} from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import { global } from '../../Util/config';
import { Colors,DeviceWidth } from '../../Style'
import { useTranslation } from 'react-i18next';

const Contact =(props)=> {
  const { t, i18n } = useTranslation();


    return (
      <View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('Contact_us')}
          navigation={props.navigation}
        />

        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{ marginTop: verticalScale(-DeviceWidth/7) }}>
          <View
            style={{
              margin: verticalScale(10),
              borderRadius: 10,
              padding: 5,
              flex: 1,
            }}>
            <View
              style={{
                // backgroundColor: '#FFFFFF',
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                resizeMode={'contain'}
                source={require('../../assets/contact_us_pic.png')}
                style={{ height: 280, width: 300 }}
              />
            </View>
            <Text
              style={{
                color: '#0093E9',
                fontSize: 15,
                alignSelf: 'center',
                fontWeight: 'bold',
              }}>
              {t("GET IN TOUCH")}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 12,
                marginLeft: 16,
                backgroundColor: '#FFFFFF',
                padding: 10,
                borderRadius: 10,
                // width: verticalScale(300),
              }}>
              <View
                style={{
                  width: scale(41),
                  height: scale(41),
                  borderRadius: 400,
                  backgroundColor: '#0093E9',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                opacity={0.3}>
                <TouchableOpacity
                  onPress={() => Linking.openURL('mailto:support@statwig.com')}>
                  <Image
                    resizeMode={'contain'}
                    source={require('../../assets/email_blue.png')}
                    style={{ height: 15, width: 15, tintColor: 'black' }}
                  />
                </TouchableOpacity>
              </View>
              <View>
                <Text style={{ fontSize: 16, marginLeft: 8 }}>{t('Email')}</Text>
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL('mailto:' + global.supportMail)
                  }>
                  <Text style={{ color: 'blue', marginLeft: 8, fontSize: 16, }}>
                    {global.supportMail}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 12,
                marginLeft: 16,
                backgroundColor: '#FFFFFF',
                padding: 10,
                borderRadius: 10,
                // width: verticalScale(300),
              }}>
              <View
                style={{
                  width: scale(41),
                  height: scale(41),
                  borderRadius: 400,
                  backgroundColor: '#0093E9',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                opacity={0.3}>
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL('tel:' + global.supportPh)
                  }>
                  <Image
                    resizeMode={'contain'}
                    source={require('../../assets/contactUs.png')}
                    style={{ height: 15, width: 20, tintColor: 'black' }}
                  />
                </TouchableOpacity>
              </View>
              <View>
                <Text style={{ fontSize: 16, marginLeft: 8 }}>
                  {t('Phone/WhatsApp')}
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL('tel:' + global.supportPh)
                  }>
                  <Text style={{ color: 'blue', marginLeft: 8, fontSize: 16, }}>
                    {global.supportPh}
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    'http://api.whatsapp.com/send?phone=' +
                    global.supportPh,
                  )
                }>
                <Image
                  resizeMode={'contain'}
                  source={require('../../assets/whatsapp.png')}
                  style={{ height: 50, width: 50 }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
}

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
  };
}

export default connect(
  mapStateToProps,
  {},
)(Contact);
