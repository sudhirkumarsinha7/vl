import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
  Linking,
} from 'react-native';
import Header, {
  HeaderWithBackBlue,
} from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { Colors, DeviceHeight, DeviceWidth } from '../../Style';

const AboutUS =(props)=> {
  const { t, i18n } = useTranslation();

    return (
      <View
        style={{
          flex: 1,
        }}>
        <StatusBar backgroundColor="#0093E9" />
        <HeaderWithBackBlue
          name={t('about_us')}
          navigation={props.navigation}
        />

        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{ marginTop: verticalScale(-DeviceWidth / 7) }}>
          <View
            style={{
              margin: verticalScale(10),
              borderRadius: 10,
              padding: 10,
              flex: 1,
              backgroundColor: '#FFFFFF',
            }}>
            <Text style={{ fontSize: scale(16) }}>
              {'       VaccineLedger leverages serialization of vaccines and other life sciences products at the manufacturing stage to create digital IDs and record vital data such as chain of custody, temperature, live location, timestamps, purchase orders, release orders and verification workflows at a dose level.VaccineLedger can tag, track and trace every dose of vaccine and it can be used to record the complete journey of the vaccine from manufacturer to the beneficiary.'}
            </Text>
            <Text style={{ fontSize: scale(16), fontWeight: 'bold', marginTop: scale(15), marginBottom: scale(15) }}>
              About Company
            </Text>
            <Text style={{ fontSize: scale(16) }}>
              {'       StaTwig, based in India and Singapore, is powering a new generation of supply chain solutions. Our blockchain-enabled technology lets investors and supply chain managers gain unparalleled insight into the tracking and tracing of products at the unit level in a multi-stakeholder environment.'}
            </Text>
          </View>
        </ScrollView>
      </View>
    );
}

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
  };
}

export default connect(
  mapStateToProps,
  {},
)(AboutUS);
