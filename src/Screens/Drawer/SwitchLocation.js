import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
  FlatList,
  BackHandler,
  Dimensions,
} from 'react-native';
import Header, {
  HeaderWithBack,
} from '../../components/Header';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import { WareHouseList } from '../../components/Common/profileHelper';

import {
  userinfo,
  fetchInboundPurchaseOrders,
  fetchOutboundPurchaseOrders
} from '../../Redux/Action/order';
import {
  getShipmentAnalytics,
  getOrderAnalytics,
  getInventoryAnalytics
} from '../../Redux/Action/analytics';
import {
  fetchInboundShipments,
  fetchOutboundShipments,

} from '../../Redux/Action/shipment';
import {
  switchLocation,
  getProductListCounts,
  getAllEventsWithFilter,
} from '../../Redux/Action/inventory'
import { Colors,DeviceWidth,CommonStyle } from '../../Style';
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../../components/Toast';

const SwitchLocation =(props)=>{
  const { t, i18n } = useTranslation();

  const onChangeLoc1 =async(Id, index)=> {
    console.log('onChangeLocation ' + JSON.stringify(Id));
    const result = await props.switchLocation({ warehouseId: Id }, index, props.userLang);
    console.log('onChangeLocation result ' + JSON.stringify(result));
    if (result && result.status === 200) {
      
      // ToastShow(
      //   t((result && result.data && result.data.message) ||
      //   'Success.'),
      //   'success',
      //   'long',
      //   'top',
      // )
      ToastShow(
        t('Success.'),
        'success',
        'long',
        'top',
      )
      props.fetchInboundShipments(0, 50, props.filterOutBound);
      props.fetchOutboundShipments(0, 50, props.filterInbound);
      props.getShipmentAnalytics();
      props.fetchInboundPurchaseOrders(0, 50, props.orderSentFilter);
      props.fetchOutboundPurchaseOrders(
        0,
        50,
        props.orderReceiveFilter,
      );
      props.getOrderAnalytics();
      props.getAllEventsWithFilter(0, 50, props.filterInventory);
      props.getProductListCounts();
      props.getInventoryAnalytics();
    } else if (result.status === 500) {
      ToastShow(
        t('API Filed with 500'),
        'error',
        'long',
        'top',
      )
    } else {
      ToastShow(
        t(result && result.data && result.data.message || 'Error'),
        'error',
        'long',
        'top',
      )
     
    }
  }

  const eachWareHouse=(item, index)=> {
    return (
      <WareHouseList
        item={item}
        index={index}
        navigation={props.navigation}
        activeWareHouse={props.activeWareHouse || 0}
        onChangeValue={onChangeLoc1}
      />
    );
  }

    // console.log('users ' + JSON.stringify(props.user));
    const { user = {} } = props;
    let { warehouses = [] } = user;
    warehouses = warehouses.filter(each => {
      if (each.status == 'ACTIVE') {
        return each;
      }
    })
    
    return (
      <View
        style={{
          flex: 1,
          // alignItems: 'center',
          backgroundColor: Colors.grayF5,
        }}
        opacity={1}>
        <HeaderWithBack
          name={t('select_location')}
          navigation={props.navigation}
        />

        <ScrollView
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{marginTop: verticalScale(-DeviceWidth/7) }}>
          
          <View
            style={{
              width: scale(328),
              borderRadius: 10,
              padding: 10,
              marginTop: 20,
            }}>
            <FlatList
              onEndReachedThreshold={0.7}
              style={{ width: '100%' }}
              keyExtractor={(item, index) => index.toString()}
              data={warehouses}
              renderItem={({ item, index }) => eachWareHouse(item, index)}
            />
          </View>
        </ScrollView>
      </View>
    );
}

function mapStateToProps(state) {
  return {
    user: state.userinfo.user,
    activeWareHouse: state.userinfo.activeWareHouse,
    filterOutBound: state.shipment.filterOutBound,
    filterInbound: state.shipment.filterInbound,
    orderReceiveFilter: state.purchaseorder.orderReceiveFilter,
    orderSentFilter: state.purchaseorder.orderSentFilter,
    filterInventory: state.inventory.filterInventory,
    userLang: state.userinfo.userLang,
  };
}

export default connect(
  mapStateToProps,
  {
    userinfo,
    switchLocation,
    fetchInboundShipments,
    fetchOutboundShipments,
    getShipmentAnalytics,
    fetchOutboundPurchaseOrders,
    fetchInboundPurchaseOrders,
    getOrderAnalytics,
    getProductListCounts,
    getInventoryAnalytics,
    getAllEventsWithFilter,
  },
)(SwitchLocation);
