import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StatusBar, FlatList } from 'react-native';
import { useTranslation } from 'react-i18next';
import '../../languages/i18n'
import Header, {
    HeaderWithBackShipmentScreen,
} from '../../components/Header';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { changeLang } from '../../Redux/Action/auth';
import { connect } from 'react-redux';
import { config, global } from '../../Util/config';
import { Colors, DeviceHeight, DeviceWidth } from '../../Style';

const List = [
    { label: 'English', id: 'en' },
    { label: 'Español', id: 'es' }
]
const changeLanguageScreen = (props) => {

    const { t, i18n } = useTranslation();

    const [currentLanguage, setLanguage] = useState(global.defaultCountrycode === 'cr'?'es':'en');
    useEffect(() => {
        async function fetchData() {
            const lang = await AsyncStorage.getItem('lang');
            console.log('lang ' + lang)
            if (lang) {
                changeLanguage(lang)
            }

        }
        fetchData();
    }, []);
    const changeLanguage = async value => {
        await AsyncStorage.setItem('lang', value);
        const token = await AsyncStorage.getItem('token');
        props.changeLang(value, token)
        // console.log(value)
        i18n
            .changeLanguage(value)
            .then(() => {
                setLanguage(value);
            })
            .catch(err => console.log(err));

    };

    getItem = (item) => {
        return (
            <TouchableOpacity
                onPress={() => changeLanguage(item.id)}
                style={{
                    marginLeft: scale(10),
                    marginRight: scale(10),
                    backgroundColor: item.id == currentLanguage ? Colors.blueE9 : '#d3d3d3',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10,
                    padding: 5,
                    paddingBottom: scale(15),
                    paddingTop: scale(15)
                }}>
                <Text
                    style={{
                        fontSize: scale(14),
                    }}>
                    {item.label}
                </Text>
            </TouchableOpacity>
        );
    }
    return (
        <View
            style={{
                flex: 1,
            }}>
            <StatusBar backgroundColor="#0093E9" />
            <HeaderWithBackShipmentScreen
                name={t('Change language')}
                navigation={props.navigation}
            />
            <View
                style={{
                    marginTop: scale(-DeviceWidth/7)
                }}>
                <FlatList
                    onEndReachedThreshold={0.7}
                    data={List}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => getItem(item)} />

            </View>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        loder: state.loder,
    };
}

export default connect(mapStateToProps, { changeLang })(changeLanguageScreen);
