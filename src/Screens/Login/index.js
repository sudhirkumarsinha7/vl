import React, { useState, useEffect, useRef } from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  Keyboard,
  ActivityIndicator,
  Alert,
  BackHandler,
  SafeAreaView,
  StatusBar,
  Pressable
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { login, sendOtp, verify_otp, changeLang,checkGoogleLogin } from '../../Redux/Action/auth';
import { registerTwillio } from '../../Redux/Action/alert';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Input } from '@rneui/base';
import LinearGradient from 'react-native-linear-gradient';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { ToastShow } from '../../components/Toast';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import PhoneInput, { isValidNumber } from 'react-native-phone-number-input';
import { Colors, DeviceHeight, DeviceWidth } from '../../Style';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import {
  CustomInputTextImage
} from '../../components/Common/common';
import Section_Select from '../../components/Section_Select';

import AppIntroSlider from 'react-native-app-intro-slider';
import { localImage } from '../../Util/imges';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
import { global } from '../../Util/config'

const data1 = [
  {
    title: "Bringing visibility for your products in emerging markets",
    text: "Revolutionize your Supply Chain with Blockchain Technology",
    image: localImage.Saving,
    imageStyle: {
      width: 200,
      height: 200,
      alignSelf: 'center',
      marginBottom: scale(40)
    },
    
  },
  {
    title: "Connecting various stakeholders on VaccineLedger",
    text: "Built on the foundation of Trust, Transaparency and Authenticity",
    image: localImage.Safety,
    bg: "#febe29",
    imageStyle: {
      width: 200,
      height: 200,
      alignSelf: 'center',
      marginBottom: scale(40)
    },
  },
  {
    title: "Resolve bottlenecks in your supply chain with Cognitive Network",
    text: "Revolutionize your Supply Chain with Blockchain Technology",
    imageStyle: {
      width: 260,
      height: 210,
      alignSelf: 'center',
      marginBottom: scale(40)
    },
    image: localImage.graph,

    bg: "#22bcb5",
  },
];
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;
const shadowStyle = {
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowOpacity: 0.3,
  shadowRadius: 4.65,
  elevation: 8,
};
const radioItems = [
  {
    label: '✉️ Use Email  Address',
    selected: true,
  },
  {
    label: '📲 Use Mobile Number',
    selected: false,
  },
];
const CELL_COUNT = 4
export const RenderPagination = (props) => {
  const { activeIndex,
    slider,
    data,
    skip } = props
  const handleIntroCompleted = () => {
    console.log('error')
  };

  return (
    <View style={styles.paginationContainer}>
      <SafeAreaView>
        <View style={styles.paginationDots}>
          {data.length > 1 &&
            data.map((_, i) => (
              <View>
                <Pressable
                  key={i}
                  style={[
                    styles.dot,
                    i === activeIndex
                      ? { backgroundColor: "#296ACC" }
                      : { backgroundColor: Colors.grey8O },
                    i === activeIndex
                      ? { width: 50 }
                      : {},
                  ]}
                  onPress={() => slider?.goToSlide(i, true)}
                />
              </View>
            ))}
        </View>

        <View style={styles.buttonContainer}>
          <TouchableOpacity onPress={skip} style={{ padding: 10, flex: activeIndex === data.length - 1 ? 0.5 : 0 }}>
            <Text style={{ color: Colors.grey4A, fontSize: 18 }}>Skip</Text>
          </TouchableOpacity>
          {activeIndex === data.length - 1 && (<TouchableOpacity onPress={skip} style={{ flex: 0.5, paddingRight: 5, paddingLeft: 5, padding: 15, backgroundColor: '#04BA72', borderRadius: scale(10) }}>
            <Text style={styles.buttonText}>{'Get Started'}</Text>
          </TouchableOpacity>
          )}
        </View>
      </SafeAreaView>
    </View>
  );
};
const Login = (props) => {
  const sliderEl = useRef(null);
  const [selectedItem, setTab] = useState('✉️ Use Email  Address')
  const keyExtractor = (item) => item.title;
  const [errorMessage, setErrorMessage] = useState('');
  const [showOtp, setshowOtp] = useState(false);
  const [landingPage, setlandingPage] = useState(true);

  const { t, i18n } = useTranslation();
  const [currentLanguage, setLanguage] = useState(global.defaultCountrycode === 'cr' ? 'es' : 'en');
  const [value, setValue] = useState('');
  // const phoneInput = useRef<PhoneInput>(null);

  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [propsc, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  useEffect(() => {
    async function fetchData() {
      changeLanguage(global.defaultCountrycode === 'cr' ? 'es' : 'en')
    }
    googleConfig()
    fetchData();
  }, []);
  const changeLanguage = async value => {
    i18n
      .changeLanguage(value)
      .then(() => {
        setLanguage(value);
      })
      .catch(err => console.log(err));
    await AsyncStorage.setItem('lang', value);
    props.changeLang(value);


  };
  const googleConfig = async () => {
    GoogleSignin.configure({
      scopes: ['profile', 'email'], // what API you want to access 
      // scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      webClientId: global.GAPI_WEB_CLIENT_ID, // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      hostedDomain: '', // specifies a hosted domain restriction
      forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      accountName: '', // [Android] specifies an account name on the device that should be used
      iosClientId: global.GOIS_WEB_CLIENT_ID, // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
      googleServicePlistPath: '', // [iOS] if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
      openIdRealm: '', // [iOS] The OpenID2 realm of the home web server. This allows Google to include the user's OpenID Identifier in the OpenID Connect ID token.
      profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
    });
  }
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });
    radioItems[index].selected = true;
    // console.log('radioItems[index].label ' + radioItems[index].label)
    setTab(radioItems[index].label)
  }
  var validationSchema = Yup.object().shape(
    {
      emailId: Yup.string().when('mob', {
        is: mob => !mob,
        then: Yup.string().matches(reg_email, t('Please enter a valid email.')),
      }),
      // formattedmob: Yup.string().when('emailId', {
      //   is: emailId => !emailId,
      //   then: Yup.string().test('Mobile', t('Please Enter valid number'), val =>
      //     isValidNumber(val),
      //   ),
      // }),

    },
    ['emailId',],
  ); // <-- HERE!!!!!!!!
  const signinform = useFormik({
    initialValues: {
      emailId: '',
      formattedmob: '',
      mob: '',
    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _signin = () => {
    Keyboard.dismiss();
    signinform.handleSubmit();
  };

  const handleSubmit = async values => {
    // console.log('val', values);
    otpform.setValues({
      ...otpform.values,
      emailId: values?.emailId || values?.formattedmob.slice(1),
    });
    values.emailId = values?.emailId || values?.formattedmob.slice(1);

    const result = await props.sendOtp(values);
    // console.log('res result' + JSON.stringify(result));
    if (result?.status === 200) {
      //do something
      setshowOtp(true);
      setValue('');
      // changeCode('');
    } else if (result.status === 500) {
      const err = result?.data?.message;
      setErrorMessage(err);
      ToastShow(
        t(err),
        'error',
        'long',
        'top',
      )
    } else if (result?.status === 401) {
      const err = result?.data?.message;
      setErrorMessage(err);
      ToastShow(
        t(err),
        'error',
        'long',
        'top',
      )
    } else {
      // const err = result?.data?.data[0];
      // setErrorMessage(err?.msg);
      const err = result.data;
      setErrorMessage(err?.message);
      ToastShow(
        t((err?.message)),
        'error',
        'long',
        'top',
      )
    }
  };

  const otp = Yup.object({
    otp: Yup.string()
      .min(4, 'Too Short!')
      .max(4, 'Too Lonng!')
      .required('Required'),
  });
  const otpform = useFormik({
    initialValues: {
      emailId: signinform?.values?.emailId,
      otp: '',
    },
    validationSchema: otp,
    validateOnChange: false,
    onSubmit: (values, actions) => {
      handleSubmitotp({ ...values });
    },
  });

  const handleSubmitotp = async values => {
    // console.log('valuuu', values);
    // console.log('values.otp.length ', values.otp.length);

    if (values.otp.length === 4) {
      const result = await props.verify_otp(values, currentLanguage);
      console.log('res', result);
      if (result?.status === 200) {
        //do something
        // setshowOtp(false);
        signinform.resetForm()
        changeCode('');
        RemotePushService(values.emailId);
      } else if (result.status === 500) {
        let err = (result.data && result.data.message) || 'API failed';
        if (typeof err === 'string') {
          err = err;
        } else {
          err = 'API failed 500 Internal Server Error';
        }
        setErrorMessage(err);
        ToastShow(
          t(err),
          'error',
          'long',
          'top',
        )
      } else {
        const err = result.data.message;
        setErrorMessage(err);
        ToastShow(
          t(err),
          'error',
          'long',
          'top',
        )
      }
    } else {
      setErrorMessage('Please Enter 4 digit otp');
      ToastShow(
        t('Please Enter 4 digit otp'),
        'error',
        'long',
        'top',
      )
    }
  };

  const RemotePushService = userIDPushNotification => {
    PushNotification.configure({
      onRegister: function (data) {
        var reqParamsRaw = {
          token_id: data.token,
          device_type: Platform.OS,
          username: userIDPushNotification,
        };
        console.log('reqParamsRaw' + JSON.stringify(reqParamsRaw));
        props.registerTwillio(reqParamsRaw);
        // console.log(reqParamsRaw);
      },
      onNotification: function (notification) {
        PushNotification.localNotification(notification);
        if (Platform.OS === 'ios') {
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        }
      },
      onAction: function (notification) {
        //  console.log("ACTION:", notification.action);
        console.log('NOTIFICATION:', notification);
      },
      onRegistrationError: function (err) {
        console.log('Not Registered:' + err.message);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      // Android only: GCM or FCM Sender ID
      senderID: '937115966227',
      popInitialNotification: true,
      requestPermissions: true,
    });
    setshowOtp(false);
    props.navigation.navigate('App');
  };
  const _signinOTP = () => {
    Keyboard.dismiss();
    otpform.handleSubmit();
  };
  const langauageComponent = () => {
    return <View style={{ marginTop: scale(30), justifyContent: 'center', alignItems: 'center' }}>
      <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#04BA72', borderRadius: scale(5) }}>

        <TouchableOpacity
          onPress={() => changeLanguage('en')}
          style={{
            backgroundColor:
              currentLanguage === 'en' ? '#04BA72' : Colors.whiteFF,
            borderTopLeftRadius: scale(5),
            borderBottomLeftRadius: scale(5),
            padding: scale(5),
          }}>
          <Text style={{
            marginLeft: scale(15), marginRight: scale(15), color:
              currentLanguage === 'en' ? Colors.whiteFF : '#04BA72',
          }}>English</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => changeLanguage('es')}
          style={{
            backgroundColor:
              currentLanguage === 'es' ? '#04BA72' : Colors.whiteFF,
            borderTopRightRadius: scale(5),
            borderBottomRightRadius: scale(5),
            padding: scale(5),
          }}>
          <Text style={{
            marginLeft: scale(15), marginRight: scale(15), color:
              currentLanguage === 'es' ? Colors.whiteFF : '#04BA72',
          }}>Español</Text>
        </TouchableOpacity>
      </View>

    </View>
  }
  const setEmail = (val) => {
    signinform.handleChange({ target: { name: `emailId`, value: val } })
    signinform.handleChange({ target: { name: `formattedmob`, value: '' } })
    signinform.handleChange({ target: { name: `mob`, value: '' } })


  }
  const setPhone = (val) => {
    signinform.handleChange({ target: { name: `formattedmob`, value: val } })
    signinform.handleChange({ target: { name: `emailId`, value: '' } })


  }
  const loginComponent = () => {
    return (<>
      <Text
        style={{

          fontSize: scale(20),
          color: '#000000',
          fontWeight: 'bold',
        }}>
        {t('Login')}
      </Text>
      <View style={{ width: '100%' }}>
        <CustomInputTextImage
          label={"Email"}
          labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
          onChange={(text) => setEmail(text)}
          val={signinform?.values?.emailId}
          errorMsg={signinform?.errors?.emailId}
          mandatory={true}
          img={require('../../assets/mail.png')}
          imgStyle={{ width: 17, height: 12 }}
        // disabled={signinform?.values?.formattedmob ? true : false}
        />
        <Text
          style={{
            alignSelf: 'center',
            marginBottom: 20,
            color: '#0093E9',
          }}>
          {t('OR')}
        </Text>
      </View>
      <View style={{ width: '100%', flexDirection: 'row' }}>
        <Image
          source={require('../../assets/phone.png')}
          style={{
            width: 18,
            height: 18,
            marginLeft: 15,
            marginRight: 10,
            alignSelf: 'center',
          }}
        />

        {/* <PhoneInput
          textStyle={{ fontSize: 14, marginBottom: 5 }}
          value={signinform?.values?.mob}
          layout="first"
          defaultCode={global.defaultCountrycode}
          onChangeFormattedText={(text)=>setPhone(text) }
          onChangeText={signinform.handleChange('mob')}
        /> */}
      </View>
      {signinform.values &&
        signinform.values.emailId ? null : signinform.values &&
          signinform.values.mob ? (
        <Text style={{ color: 'red', marginLeft: -100 }}>
          {signinform?.errors?.formattedmob}
        </Text>
      ) : null}

      <TouchableOpacity
        onPress={() => _signin()}
        disabled={
          signinform?.values?.mob ||
            signinform?.values?.emailId
            ? false
            : true
        }
        style={{
          width: scale(133),
          height: scale(40),
          borderRadius: 8,
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 15,
        }}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={
            signinform?.values?.mob ||
              signinform?.values?.emailId
              ? ['#0093E9', '#36C2CF']
              : ['gray', 'gray']
          }
          style={{
            width: scale(133),
            height: scale(40),
            borderRadius: 8,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: scale(20),
              color: '#FFFFFF',
              fontWeight: 'bold',
            }}>
            {t('SEND OTP')}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
      {langauageComponent()}

      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
          marginTop: verticalScale(10),
          marginBottom: 40,

        }}>
        <Text
          style={{
            fontSize: scale(14),
            color: '#707070',
          }}>
          {t("Don’t have an account?")}
        </Text>
        <TouchableOpacity
          onPress={() => props.navigation.navigate('Signup', { data: {} })}>
          <Text
            style={{
              marginLeft: scale(7),
              fontSize: scale(14),
              color: '#0B65C1',
              fontWeight: 'bold',
            }}>
            {t('Sign Up')}
          </Text>
        </TouchableOpacity>
      </View>

    </>)
  }
  const loginComponentNew = () => {
    return (<View style={{ marginTop: DeviceWidth / 7, }}>
      {langauageComponent()}
      <TouchableOpacity onPress={() => gSignIn()} style={{ margin: 20, borderWidth: 1, borderRadius: 10, borderColor: '#296ACC' }}>
        <View style={{ padding: 5, flexDirection: 'row', justifyContent: 'center', alignContent: 'center' }}>
          <Image source={localImage.GoogleLogo} style={{ width: 30, height: 30, }} />

          <Text style={{ fontSize: 16, marginLeft: 10, textAlign: 'center', marginTop: 5 }}>{t('Sigin with Google')}</Text>
        </View>

      </TouchableOpacity>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottomWidth: 1, margin: 15 }}>
        {radioItems.map((item, key) => (
          <Section_Select
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
          />
        ))}
        <View style={{ height: scale(10) }} />
      </View>

      {selectedItem === '✉️ Use Email  Address' ? emailComponent() : selectedItem === '📲 Use Mobile Number' ? PhoneComponent() : null}
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: verticalScale(10),
          marginBottom: 40,

        }}>
        <TouchableOpacity
          onPress={() => _signin()}
          disabled={
            signinform?.values?.mob ||
              signinform?.values?.emailId
              ? false
              : true
          }
          style={{
            width: scale(133),
            height: scale(40),
            borderRadius: 0,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            // marginTop: verticalScale(30),

          }}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={
              signinform?.values?.mob ||
                signinform?.values?.emailId
                ? ['#04BA72', '#04BA72']
                : ['gray', 'gray']
            }
            style={{
              width: scale(133),
              height: scale(40),
              borderRadius: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: scale(16),
                color: '#FFFFFF',
                fontWeight: 'bold',
              }}>
              {t('Sign In')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
        <View style={{ flexDirection: 'row', marginTop: 40 }}>


          <Text
            style={{
              fontSize: scale(14),
              color: '#707070',
            }}>
            {t("Don’t have Account?")}
          </Text>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Signup', { data: {} })}>
            <Text
              style={{
                marginLeft: scale(7),
                fontSize: scale(14),
                color: '#0B65C1',
                fontWeight: 'bold',
              }}>
              {t('Create Account')}
            </Text>
          </TouchableOpacity>
        </View>

      </View>
    </View>)
  }
  const gSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log('gSignIn userInfo ' + JSON.stringify(userInfo))
      let userData = userInfo.user;
      let data = {
        emailId: userData.email,
        firstName: userData.givenName,
        lastName: userData.familyName,
        avatar: userData.photo,
        loginType: 'Google',
        token: userInfo.idToken,
      };
      const data1 = {
				tokenId: userInfo.idToken,
			};
      const gRes= await props.checkGoogleLogin(data1,currentLanguage)
      if (gRes?.status === 200) {
        //do something
        signinform.resetForm()
        changeCode('');
        RemotePushService(userData.email);
      } else if (gRes.status === 500) {
        let err = (gRes.data && gRes.data.message) || 'API failed';
        if (typeof err === 'string') {
          err = err;
        } else {
          err = 'API failed 500 Internal Server Error';
        }
        setErrorMessage(err);
        ToastShow(
          t(err),
          'error',
          'long',
          'top',
        )
      }  else {
        const err = gRes.data.message;
        setErrorMessage(err);
        ToastShow(
          t(err),
          'error',
          'long',
          'top',
        )
        props.navigation.navigate('Signup', { data })
      }
    
    } catch (error) {

      console.log('gSignIn error ' + JSON.stringify(error))
      console.log('gSignIn statusCodes ' + JSON.stringify(statusCodes))

      if (error.code === statusCodes.SIGN_IN_CANCELLED) {

      } else if (error.code === statusCodes.IN_PROGRESS) {

      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      } else {
      }
    }
  }
  const emailComponent = () => {
    return (
      <CustomInputTextImage
        label={"Email"}
        labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
        onChange={(text) => setEmail(text)}
        val={signinform?.values?.emailId}
        errorMsg={signinform?.errors?.emailId}
        mandatory={true}
        img={require('../../assets/mail.png')}
        imgStyle={{ width: 17, height: 12 }}
      />
    )
  }
  const PhoneComponent = () => {
    return (
      <View >
        <Text style={{ fontWeight: '400', marginLeft: scale(15), marginTop: 20 }}>{t('Phone')}</Text>

        <View style={{
          flexDirection: 'row', borderWidth: 1, margin: 15, 
          padding: 2,
          borderRadius: 15,
          marginTop:5,

        }}>
          <Image
            source={require('../../assets/phone.png')}
            style={{
              width: 18,
              height: 18,
              marginLeft: 15,
              marginRight: 10,
              alignSelf: 'center',
            }}
          />
          <PhoneInput
            textStyle={{ fontSize: 14, marginBottom: 5 }}
            initialCountry={global.defaultCountrycode}
            value={signinform?.values?.mob}
            layout="first"
            onChangeFormattedText={(text) => setPhone(text)}
            onChangeText={signinform.handleChange('mob')}
          />

        </View>
        {signinform.values &&
          signinform.values.emailId ? null : signinform.values &&
            signinform.values.mob ? (
          <Text style={{ color: 'red', marginLeft: -100 }}>
            {signinform?.errors?.formattedmob}
          </Text>
        ) : null}
      </View>
    )
  }
  const changeCode = (code) => {
    setValue(code);
    otpform.handleChange('otp')
    otpform.setValues({
      ...otpform.values,
      otp: code,
    });
  }
  const otpComponent = () => {
    return (<>
      <View style={{ width: '20%', marginLeft: '100%', }}>
        <TouchableOpacity
          style={{
            borderRadius: 10,
            height: scale(30),
          }}
          onPress={() => {
            changeCode('')
            setshowOtp(false)
          }
          }>
          {/* <Text
            style={{
              textAlign: 'right',
              marginRight: 25,
            }}>
            {'X'}
          </Text> */}
          <AntDesign
            name={'closecircleo'}
            color={Colors.blueE9}
            size={25}
          />
        </TouchableOpacity>
      </View>
      <Text
        style={{
          fontSize: scale(20),
          color: '#000000',
          fontWeight: 'bold',
          marginBottom: verticalScale(20),
        }}>
        {t('Enter OTP')}
      </Text>

      <View style={{ width: '90%' }}>
        <CodeField
          ref={ref}
          {...propsc}
          // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
          value={value}
          onChangeText={(text) => changeCode(text)}
          cellCount={CELL_COUNT}
          rootStyle={styles.codeFieldRoot}
          keyboardType="number-pad"
          textContentType="oneTimeCode"
          renderCell={({ index, symbol, isFocused }) => (
            <Text
              key={index}
              style={[styles.cell, isFocused && styles.focusCell]}
              onLayout={getCellOnLayoutHandler(index)}>
              {symbol || (isFocused ? <Cursor /> : null)}
            </Text>
          )}
        />
      </View>
      <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => _signin()}>
        <Text style={{ color: Colors.grayA8, marginBottom: scale(5) }}>{t("Didn't receive the OTP?")}</Text>

        <Text style={{ fontWeight: 'bold', color: Colors.blueE9 }}>{t('RESEND CODE')}</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => _signinOTP()}
        style={{
          width: scale(133),
          height: scale(40),
          borderRadius: 8,
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: verticalScale(15),
          marginBottom: verticalScale(40),
        }}
      >
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={
            otpform?.values?.otp.length == 4
              ? ['#0093E9', '#36C2CF']
              : ['gray', 'gray']
          }
          style={{
            width: scale(133),
            height: scale(40),
            borderRadius: 8,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: scale(20),
              color: '#FFFFFF',
              fontWeight: 'bold',
            }}>
            {t('LOGIN')}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    </>)
  }
  const newOtpComponent = () => {
    return (<View style={{ justifyContent: 'center', alignItems: 'center', marginTop: scale(40) }}>
      <View style={{ justifyContent: 'center', alignItems: 'center' }}>
        <Image source={otpform?.values?.otp.length == 4 ? localImage.unlock : localImage.lock} style={{ ...styles.image, width: 150 }} />
        <Image source={localImage.dot} style={{ height: 30, width: 150 }} />
      </View>
      <Text
        style={{
          fontSize: scale(20),
          color: '#000000',
          fontWeight: 'bold',
          marginTop: 20,
          marginBottom: verticalScale(10),
        }}>
        {t('OTP Verification')}
      </Text>
      <Text
        style={{
          fontSize: scale(14),
          color: '#000000',
          marginBottom: verticalScale(20),
        }}>
        {t('Enter the OTP sent to') + ' ' + otpform?.values?.emailId}
      </Text>
      <View style={{ width: '90%' }}>
        <CodeField
          ref={ref}
          {...propsc}
          value={value}
          onChangeText={(text) => changeCode(text)}
          cellCount={CELL_COUNT}
          rootStyle={styles.codeFieldRoot}
          keyboardType="number-pad"
          textContentType="oneTimeCode"

          renderCell={({ index, symbol, isFocused }) => (
            <Text
              key={index}
              style={[styles.cell, isFocused && styles.focusCell]}
              onLayout={getCellOnLayoutHandler(index)}>
              {symbol || (isFocused ? <Cursor /> : null)}
            </Text>
          )}
        />
      </View>
      <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }} onPress={() => _signin()}>
        <Text style={{ color: Colors.grayA8, marginBottom: scale(5) }}>{t("Didn't receive the OTP?")}  <Text style={{ fontWeight: 'bold', color: Colors.blueE9 }}>{t('RESEND CODE')}</Text></Text>
      </TouchableOpacity>

      <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }} onPress={() => {
        changeCode('')
        setshowOtp(false)
      }}>
        <Text style={{ color: '#04BA72', marginBottom: scale(5) }}>{t("Try with another account")}</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => _signinOTP()}
        style={{
          width: scale(133),
          height: scale(40),
          borderRadius: 8,
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: verticalScale(15),
          marginBottom: verticalScale(40),
        }}
      >
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={
            otpform?.values?.otp.length == 4
              ? ['#04BA72', '#04BA72']
              : ['gray', 'gray']
          }
          style={{
            width: scale(200),
            height: scale(50),
            borderRadius: 8,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: scale(20),
              color: '#FFFFFF',
              fontWeight: 'bold',
            }}>
            {t('Verify')}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
    )
  }
  console.log('orgTypeList ' + JSON.stringify(signinform));
  const onIntroCompleted = () => {
    // navigation.navigate("Root");
  };
  const landingScreen = () => {
    return (<View>
      <AppIntroSlider
        keyExtractor={keyExtractor}
        renderItem={({ item }) => renderItem(item)}
        renderPagination={(activeIndex) => (
          <RenderPagination
            data={data1}
            activeIndex={activeIndex}
            slider={sliderEl.current}
            onIntroCompleted={onIntroCompleted}
            skip={() => setlandingPage(false)}
          />
        )}
        data={data1}
        ref={sliderEl}
      />
    </View>)
  }
  const renderItem = (item) => (
    <View
      style={[
        styles.slide,
      ]}>
      <View style={{ justifyContent: 'center', alignItems: 'center' }}>
        <Image source={item.image} style={item.imageStyle} />

      </View>
      <View style={{}}>

        <Text style={styles.title}>{item.title}</Text>

        <Text style={styles.text}>{item.text}</Text>
      </View>
    </View>
  );



  return (
    <ImageBackground
      style={styles.imgBackground}
      resizeMode="stretch"
      source={localImage.loginBackgraund}>
      <KeyboardAwareScrollView extraScrollHeight={50} >

        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            justifyContent: 'center',
          }}>
          <StatusBar hidden />

          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              justifyContent: 'center',
            }}>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              nestedScrollEnabled={true}
              showsVerticalScrollIndicator={false}>
              {!showOtp ? <View style={{ marginTop: verticalScale(50), alignSelf: 'center', flexDirection: 'row', marginBottom: verticalScale(50) }}>
                <Text style={{ color: '#098BFE', textAlign: 'center', fontSize: 24, fontWeight: 'bold' }}>VACCINE</Text>
                <Text style={{ color: '#000000', textAlign: 'center', fontSize: 24, fontWeight: 'bold' }}>LEDGER</Text>
              </View> : null}
              {props.loder ? (
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                  }}>
                  <ActivityIndicator color="#0000ff" />
                </View>
              ) : null}
              {/* {loginComponentNew()} */}
              {landingPage ? landingScreen() : showOtp ? newOtpComponent() : loginComponentNew()}
              {/* {newOtpComponent()} */}
              {/* {landingPage?landingScreen():showOtp ? (<View
                style={[
                  {
                    marginLeft: scale(30),
                    marginRight: scale(30),
                    flex: 1,
                    borderRadius: 10,
                    backgroundColor: '#FFFFFF',
                    marginTop: verticalScale(40),
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                  shadowStyle,
                ]}>
                 { otpComponent()}
                  </View>) :  (<View
                style={[
                  {
                    marginLeft: scale(30),
                    marginRight: scale(30),
                    flex: 1,
                    borderRadius: 10,
                    backgroundColor: '#FFFFFF',
                    marginTop: verticalScale(40),
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                  shadowStyle,
                ]}>
                {  loginComponent()}
                  </View>)} */}



              <View style={{ height: scale(10) }} />
            </ScrollView>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </ImageBackground>
  );
};
const styles = StyleSheet.create({
  imgBackground: {
    width: '100%',
    height: '100%',
    // flex: 1,
    // position: 'absolute',
  },
  wrapper: {
    flex: 1,
  },
  root: { flex: 1, padding: 20 },
  codeFieldRoot: { marginTop: 20, },
  cell: {
    width: 60,
    height: 55,
    color: '#000',
    borderColor: '#D3D3D3',
    borderWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 10,
    textAlign: 'center',
    fontSize: scale(20),
    paddingTop: scale(10),
  },
  focusCell: {
    borderColor: Colors.blueE9,
  },
  slide: {
    // alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
  image: {
    width: 200,
    height: 200,
    alignSelf: 'center',
    marginBottom: scale(40)
  },
  text: {
    color: '#00407B',
    fontSize: 24,
    marginBottom: 100,
    marginTop: 30,
    textAlign: 'center',
  },
  title: {
    fontSize: 36,
    color: "#296ACC",
    fontWeight: 'bold',
    textAlign: 'center',
  },
  paginationContainer: {
    // bottom: 16,
    // left: 16,
    // right: 16,
    // margin:10
  },
  paginationDots: {
    height: 16,
    margin: 16,
    flexDirection: "row",
    justifyContent: 'center',
    alignItems: 'center',
  },
  dot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 4,
    marginTop: 20

  },
  buttonContainer: {
    flexDirection: "row",
    // marginTop: 24,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 50,
    marginRight: 50
  },
  button: {
    flex: 1,
    paddingVertical: 20,
    marginHorizontal: 8,
    borderRadius: 24,
    backgroundColor: "#1cb278",
  },
  buttonText: {
    color: "white",
    fontWeight: "600",
    textAlign: "center",
  },
});

function mapStateToProps(state) {
  return {
    otpdata: state.auth.otpdata,
    userdata: state.auth.userdata,
    loder: state.loder,
  };
}

export default connect(
  mapStateToProps,
  { login, sendOtp, verify_otp, registerTwillio, changeLang,checkGoogleLogin },
)(Login);
