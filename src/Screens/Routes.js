import React, { useEffect } from 'react';
import {
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
} from 'react-native';
import { CommonStyle, Colors } from '../Style';
import { scale } from '../components/Scale'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {
  createDrawerNavigator
} from '@react-navigation/drawer';
import { SafeAreaProvider, initialWindowMetrics } from 'react-native-safe-area-context'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const headerOptions = {
  headerShown: false
};
import SplashScreen from './SpashScreen'
import AuthLoadingScreen from './AuthLoading';
import Menu from './Drawer';
import Profile from './Drawer/Profile';
import UpdateWarehouse from './Drawer/Profile/UpdateWarehouse';
import AddWareHouse from './Drawer/Profile/AddWareHouse';

import AboutUs from './Drawer/AboutUs';
import CangeLang from './Drawer/ChangeLanguage';
import ContactUS from './Drawer/Contact';
import SwitchLocation from './Drawer/SwitchLocation';


import Login from './Login';
import Register from './Register';


import Shippment from './Shipment';
import CreateShipment from './Shipment/CreateShipment';
import ViewShipment from './Shipment/ViewShipment';
import FilterShipment from './Shipment/FilterShipment';
import ShipmentOption from './Shipment/ShipmentOption';
import UpdateShipment from './Shipment/UpdateShipment';
import ReceiveShipment from './Shipment/ReceiveShipment';
import ScanShipment from './Shipment/ScanShipment';
import ProductDetails from './Shipment/SendSignal/ProductDetails';
import PatientProductDetails from './Shipment/SendSignal/PatientProductDetails';
import Patient from './Shipment/SendSignal/Patient';


import Order from './Order'
import CreateOrder from './Order/CreateOrder'
import ReviewOrder from './Order/ReviewOrder'
import ViewOrder from './Order/ViewOrder'
import FilterOrder from './Order/FilterOrder'

import Inventory from './Inventory'
import AddInventory from './Inventory/AddInventory'
import RecentInventory from './Inventory/RecentInventory'
import ReviewInventory from './Inventory/ReviewInventory'
import ExpiredProduct from './Inventory/ExpiredProduct'
import ListOfProducts from './Inventory/ListOfProducts'
import NearExpiredProduct from './Inventory/NearExpiredProduct'
import OutOfStockProduct from './Inventory/OutOfStockProduct'
import ProductList from './Inventory/ProductList'
import ProductListByCategory from './Inventory/ProductListByCategory'
import ProductListByName from './Inventory/ProductListByName'
import TotalProductsCategory from './Inventory/TotalProductsCategory'
import FilterInventory from './Inventory/FilterInventory'
import ProductListCount from './Inventory/ProductListCount'



import Track from './Track'
import TrackDeatils from './Track/TrackDeatils'
import TrackTaggedShipment from './Track/TrackTaggedShipment'

import Notifications from './Notifications'
import UserPermission from './Notifications/UserPermission'

const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={headerOptions}
      initialRouteName="Login"
    >
      <Stack.Screen name="Login" component={Login} options={{
        headerShown: false
      }} />
      <Stack.Screen name="Signup" component={Register} options={{
        headerShown: false
      }} />
    </Stack.Navigator>
  );
}
const ShipmentStack = () => {
  return (
    <Stack.Navigator
      screenOptions={headerOptions}
    >
      <Stack.Screen name="Shipment" component={Shippment} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ViewShipment" component={ViewShipment} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ReceiveShipment" component={ReceiveShipment} options={{
        headerShown: false
      }} />
      <Stack.Screen name="CreateShipment" component={CreateShipment} options={{
        headerShown: false
      }} />
      <Stack.Screen name="FilterShipment" component={FilterShipment} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ShipmentOption" component={ShipmentOption} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ScanShipment" component={ScanShipment} options={{
        headerShown: false
      }} />
      <Stack.Screen name="UpdateShipment" component={UpdateShipment} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ProductDetails" component={ProductDetails} options={{
        headerShown: false
      }} />
      <Stack.Screen name="PatientProductDetails" component={PatientProductDetails} options={{
        headerShown: false
      }} />
      <Stack.Screen name="Patient" component={Patient} options={{
        headerShown: false
      }} />
    </Stack.Navigator>
  );
}
const OrderStack = () => {
  return (
    <Stack.Navigator
      screenOptions={headerOptions}
    >
      <Stack.Screen name="Order" component={Order} options={{
        headerShown: false
      }} />
      <Stack.Screen name="CreateOrder" component={CreateOrder} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ReviewOrder" component={ReviewOrder} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ViewOrder" component={ViewOrder} options={{
        headerShown: false
      }} />
      <Stack.Screen name="FilterOrder" component={FilterOrder} options={{
        headerShown: false
      }} />
    </Stack.Navigator>
  );
}
const InventoryStack = () => {
  return (
    <Stack.Navigator
      screenOptions={headerOptions}>
      <Stack.Screen name="Inventory" component={Inventory} options={{
        headerShown: false
      }} />
      <Stack.Screen name="AddInventory" component={AddInventory} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ReviewInventory" component={ReviewInventory} options={{
        headerShown: false
      }} />
      <Stack.Screen name="RecentInventory" component={RecentInventory} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ExpiredProduct" component={ExpiredProduct} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ListOfProducts" component={ListOfProducts} options={{
        headerShown: false
      }} />
      <Stack.Screen name="NearExpiredProduct" component={NearExpiredProduct} options={{
        headerShown: false
      }} />
      <Stack.Screen name="OutOfStockProduct" component={OutOfStockProduct} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ProductList" component={ProductList} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ProductListByName" component={ProductListByName} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ProductListByCategory" component={ProductListByCategory} options={{
        headerShown: false
      }} />
      <Stack.Screen name="TotalProductsCategory" component={TotalProductsCategory} options={{
        headerShown: false
      }} />
      <Stack.Screen name="FilterInventory" component={FilterInventory} options={{
        headerShown: false
      }} />
      <Stack.Screen name="ProductListCount" component={ProductListCount} options={{
        headerShown: false
      }} />
      
    </Stack.Navigator>
  );
}
const TrackStack = () => {
  return (
    <Stack.Navigator
      screenOptions={headerOptions}
    >
      <Stack.Screen name="Track" component={Track} options={{
        headerShown: false
      }} />
      <Stack.Screen name="TrackDeatils" component={TrackDeatils} options={{
        headerShown: false
      }} />
      <Stack.Screen name="TrackTaggedShipment" component={TrackTaggedShipment} options={{
        headerShown: false
      }} />
    </Stack.Navigator>
  );
}

const ProfileScreen = () => {
  return (
    <Stack.Navigator
      screenOptions={headerOptions}
    >
      <Stack.Screen name="Profile" component={Profile} options={{
        headerShown: false
      }} />
      <Stack.Screen name="UpdateWarehouse" component={UpdateWarehouse} options={{
        headerShown: false
      }} />
      <Stack.Screen name="AddWareHouse" component={AddWareHouse} options={{
        headerShown: false
      }} />
    </Stack.Navigator>
  );
}
const NotificationsScreen = () => {
  return (
    <Stack.Navigator
      screenOptions={headerOptions}
    >
      <Stack.Screen name="Notifications" component={Notifications} options={{
        headerShown: false
      }} />
      <Stack.Screen name="UserPermission" component={UserPermission} options={{
        headerShown: false
      }} />
    </Stack.Navigator>
  );
}
const VLTabs = () => {
  return (
    <Tab.Navigator screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        switch (route.name) {
          case 'Shipment':

            return (
              <Image
                style={{ width: scale(21.7), height: scale(16.06), borderWidth: 0 }}
                source={
                  focused
                    ? require('../assets/Shipment1.png')
                    : require('../assets/Shipment.png')
                }
                resizeMode="contain"
              />
            )
          case 'Order':
            return (
              <Image
                style={{ width: scale(16.59), height: scale(18), borderWidth: 0 }}
                source={
                  focused
                    ? require('../assets/order1.png')
                    : require('../assets/order.png')
                }
                resizeMode="contain"
              />
            )
          case 'Inventory':
            return (
              <Image
                style={{ width: scale(18), height: scale(18), borderWidth: 0 }}
                source={
                  focused
                    ? require('../assets/Inventory1.png')
                    : require('../assets/Inventory.png')
                }
                resizeMode="contain"
              />)
          case 'TrackTrace':
            return (
              <Image
                style={{ width: scale(18), height: scale(18), borderWidth: 0 }}
                source={
                  focused
                    ? require('../assets/TrackTrace.png')
                    : require('../assets/TrackTraceGray.png')
                }
                resizeMode="contain"
              />)
          default:
            return (
              <Image
                style={{ width: scale(18), height: scale(18), borderWidth: 0 }}
                source={
                  focused
                    ? require('../assets/TrackTrace.png')
                    : require('../assets/TrackTraceGray.png')
                }
                resizeMode="contain"
              />)
        }
      },
      tabBarLabel: ({ focused, color }) => {
        return <Text style={[CommonStyle.tablabel, { color: focused ? Colors.blueE9 : Colors.grayA8, fontSize: scale(8), fontWeight: 'bold' }]}>{route.name.toUpperCase()}</Text>
      },
    })}
    >
      <Tab.Screen name="Shipment" component={ShipmentStack} options={{
        headerShown: false
      }} />
      <Tab.Screen name="Order" component={OrderStack} options={{
        headerShown: false
      }} />

      <Tab.Screen name="Inventory" component={InventoryStack}
        options={{
          headerShown: false
        }}
      />
      <Tab.Screen name="Track" component={TrackStack} options={{
        headerShown: false
      }} />
    </Tab.Navigator>
  );
}
const VLDrwawer = () => {
  return (
    <Drawer.Navigator initialRouteName="Home" drawerContent={props => <Menu {...props} />}>
      <Drawer.Screen name="Home" component={VLTabs} options={{
        headerShown: false
      }} />
      <Drawer.Screen name='Notifications' component={NotificationsScreen} options={{
        headerShown: false
      }} />

      <Drawer.Screen name="Profile" component={ProfileScreen} options={{
        headerShown: false
      }} />
      <Drawer.Screen name="Contact" component={ContactUS} options={{
        headerShown: false
      }} />
      <Drawer.Screen name="AboutUs" component={AboutUs} options={{
        headerShown: false
      }} />
      <Drawer.Screen name="CangeLang" component={CangeLang} options={{
        headerShown: false
      }} />
      <Drawer.Screen name="SwitchLocation" component={SwitchLocation} options={{
        headerShown: false
      }} />
    </Drawer.Navigator>
  );
}


const AppContainer = () => (
  <NavigationContainer>
    <Stack.Navigator
      screenOptions={headerOptions}
      initialRouteName="SplashScreen"
    >
      <Stack.Screen name='SplashScreen' component={SplashScreen} />
      <Stack.Screen name='AuthLoading' component={AuthLoadingScreen} />
      <Stack.Screen name='App' component={VLDrwawer} />
      <Stack.Screen name='Auth' component={AuthStack} />

    </Stack.Navigator>
  </NavigationContainer>
)
const Route = () => {
  return (<SafeAreaProvider initialMetrics={initialWindowMetrics}>
    <AppContainer />
  </SafeAreaProvider>)
}

export default Route