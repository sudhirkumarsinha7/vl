import Realm from 'realm';
export const PRODUCTS_LIST = "products";

export const INVENTORY_LIST_SCHEMA = "Inventory";
export const SHIPMENT_SCHEMA = "ShipmentList";
export const ORDER_SCHEMA = "OrderList";
export const PRODUCTS_LIST_SHIPMENT = "Products_shipment";
export const FROM_LOC_SCHEMA = "Customer_location";

export const productSchema = {
    name: PRODUCTS_LIST,
    // primaryKey: 'id',
    properties: {
        // id: "int",
        productId: { type: 'string', optional: true },
        productName: { type: 'string', optional: true },
        id: { type: 'string', optional: true },
        name: { type: 'string', optional: true },
        productCategory: { type: 'string', optional: true },
        type: { type: 'string', optional: true },
        manufacturer: { type: 'string', optional: true},
        quantity: { type: 'int', optional: true },
        productQuantity: { type: 'string', optional: true },
        batchNumber: { type: 'string', optional: true },
        serialNumbersRange: { type: 'string', optional: true },
        mfgDate: { type: 'string',optional: true },
        expDate: { type: 'string', optional: true },
        unitofMeasure:"{}",
    }
};
export const productSchemaShipment = {
  name: PRODUCTS_LIST_SHIPMENT,
  // primaryKey: 'id',
  properties: {
      // id: "int",
      productID: { type: 'string', optional: true },
      productName: { type: 'string', optional: true },
      id: { type: 'string', optional: true },
      name: { type: 'string', optional: true },
      productCategory: { type: 'string', optional: true },
      type: { type: 'string', optional: true },
      manufacturer: { type: 'string', optional: true},
      quantity: { type: 'int', optional: true },
      productQuantity: { type: 'string', optional: true },
      batchNumber: { type: 'string', optional: true },
      serialNumbersRange: {
        type: 'list', 
        objectType: 'string',
     },
      mfgDate: { type: 'string',optional: true },
      expDate: { type: 'string', optional: true },
      unitofMeasure:"{}",
  }
};
export const InventorySchema = {
    name: INVENTORY_LIST_SCHEMA,
    // primaryKey: 'id',
    properties: {
        // id: "int",
        products: {
            type: 'list', 
            objectType: PRODUCTS_LIST,
         }
    }
};
// export const orderSchema = {
//   name: ORDER_SCHEMA,
//   properties: {
//       externalId: { type: 'string', optional: true },
//       creationDate: { type: 'string', optional: true },
//       lastUpdatedOn: { type: 'string', optional: true },
//       supplier: "{}",
//       customer:"{}",
//       poStatus: { type: 'string', optional: true },
//       products: {
//           type: 'list', 
//           objectType: PRODUCTS_LIST,
//        }
//   }
 
// };
export const fromSchema = {
  name: FROM_LOC_SCHEMA,
  properties: {
      customerOrganisation: { type: 'string', optional: true },
      region:{ type: 'string', optional: true },
      country:{ type: 'string', optional: true },
      customerIncharge: { type: 'string', optional: true },
      shippingAddress: "{}",
  }
};
export const orderSchema = {
  name: ORDER_SCHEMA,
  properties: {
      externalId: { type: 'string', optional: true },
      creationDate: { type: 'string', optional: true },
      lastUpdatedOn: { type: 'string', optional: true },
      poStatus: { type: 'string', optional: true },
      supplier:{ type: "{}", optional: true },
      customer:{ type: "object",  objectType: FROM_LOC_SCHEMA},
      products: {
          type: 'list', 
          objectType: PRODUCTS_LIST,
       }
  }
 
};
export const shipmetSchema = {
  name: SHIPMENT_SCHEMA,
  properties: {
       airWayBillNo:{ type: 'string', optional: true },
        label: "{}",
        externalShipmentId: { type: 'string', optional: true },
        supplier: "{}",
        receiver: "{}",
        shippingDate: { type: 'string', optional: true },
        expectedDeliveryDate:{ type: 'string', optional: true },
        actualDeliveryDate: { type: 'string', optional: true },
        status: { type: 'string', optional: true },
        products:  {
          type: 'list', 
          objectType: PRODUCTS_LIST_SHIPMENT,
       },
        poId: { type: 'string', optional: true },
        taggedShipments: { type: 'string', optional: true },
  }
 
};


const databaseOptions = {
    path: 'VL.realm',
    schema: [InventorySchema,productSchema,orderSchema,shipmetSchema,productSchemaShipment,fromSchema],
    schemaVersion: 0, //optional    
};

export const insertNewProductList = newTodoList => new Promise((resolve, reject) => {    
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.create(INVENTORY_LIST_SCHEMA, newTodoList);
            resolve(newTodoList);
        });
    }).catch((error) => reject(error));
});

export const queryAllInventryData = () => new Promise((resolve, reject) => {    
    Realm.open(databaseOptions).then(realm => {        
        let allTodoLists = realm.objects(INVENTORY_LIST_SCHEMA);
        resolve(allTodoLists);  
    }).catch((error) => {        
        reject(error);  
    });;
});
export const queryDeleteAllInventryData = () => new Promise((resolve, reject) => {    

    Realm.open(databaseOptions).then(realm => {   
        let allTodoLists = realm.objects(INVENTORY_LIST_SCHEMA);
        realm.write(() => {
          realm.delete(allTodoLists)
      })
        resolve();
    }).catch((error) => {    
      console.log('queryDeleteAllInventryData error '+ JSON.stringify(error));       
        reject(error);  
    });;
});

//Shipment
export const insertNewShipment = newTodoList => new Promise((resolve, reject) => {    
  Realm.open(databaseOptions).then(realm => {
      realm.write(() => {
          realm.create(SHIPMENT_SCHEMA, newTodoList);
          resolve(newTodoList);
      });
  }).catch((error) => reject(error));
});
export const queryAllShipment= () => new Promise((resolve, reject) => {    
  Realm.open(databaseOptions).then(realm => {        
      let allTodoLists = realm.objects(SHIPMENT_SCHEMA);
      resolve(allTodoLists);  
  }).catch((error) => {        
      reject(error);  
  });;
});
export const queryDeleteAllNewShipment = () => new Promise((resolve, reject) => {    

  Realm.open(databaseOptions).then(realm => {   
      let allTodoLists = realm.objects(SHIPMENT_SCHEMA);
      realm.write(() => {
        realm.delete(allTodoLists)
    })
      resolve();
  }).catch((error) => {    
    console.log('queryDeleteAllNewShipment error '+ JSON.stringify(error));       
      reject(error);  
  });;
});
//Order
export const insertNewOrder = newTodoList => new Promise((resolve, reject) => {    
  Realm.open(databaseOptions).then(realm => {
      realm.write(() => {
          realm.create(ORDER_SCHEMA, newTodoList);
          resolve(newTodoList);
      });
  }).catch((error) => reject(error));
});
export const queryAllorder = () => new Promise((resolve, reject) => {    
  Realm.open(databaseOptions).then(realm => {        
      let allTodoLists = realm.objects(ORDER_SCHEMA);
      resolve(allTodoLists);  
  }).catch((error) => {        
      reject(error);  
  });;
});
export const queryDeleteAllOrder = () => new Promise((resolve, reject) => {    

  Realm.open(databaseOptions).then(realm => {   
      let allTodoLists = realm.objects(ORDER_SCHEMA);
      realm.write(() => {
        realm.delete(allTodoLists)
    })
      resolve();
  }).catch((error) => {    
    console.log('queryDeleteAllOrder error '+ JSON.stringify(error));       
      reject(error);  
  });;
});
export default new Realm(databaseOptions);