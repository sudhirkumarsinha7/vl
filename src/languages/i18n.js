import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from './en.json';
import es from './es.json';
import {global} from '../Util/config'
i18n.use(initReactI18next).init({
    lng: global.defaultCountrycode === 'cr'?'es':'en',
    fallbackLng:  global.defaultCountrycode === 'cr'?'es':'en',
    compatibilityJSON: 'v3',
    resources: {
        en: en,
        es: es,
    },
    interpolation: {
        escapeValue: false // react already safes from xss
    }
});

export default i18n;
