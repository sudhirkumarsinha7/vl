import app from './Reducer';
import {createStore, applyMiddleware} from 'redux';
// import { configureStore } from '@reduxjs/toolkit'

import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';

export default createStore(app, composeWithDevTools(applyMiddleware(thunk)));