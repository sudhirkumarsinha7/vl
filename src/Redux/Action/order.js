import {
    USERINFO,
    
    PURCHASE_ORDER_IDS,
    TRACKSHIPMENT,
    TRACKTAGSHIPMENT,
    PURCHASE_ORDER_INBOUNDPOS,
    PURCHASE_ORDER_OUTBOUNDPOS,
    RESET_PURCHASE_ORDER_INBOUNDPOS,
    RESET_PURCHASE_ORDER_OUTBOUNDPOS,
    ORDER_SENT_FILTER,
    ORDER_RECEIVE_FILTER,
    GET_PRODUCT_CUSTOMER_lOCATION,
    SENT_ORDER_COUNT,
    RECEIVE_ORDER_COUNT,
    GET_ALL_ORDER_ID,
    GET_REGION_BY_ORGTYPE,
    GET_COUNTRIES_BYORGTYPE,
    GET_ORG_BYORGTYPE,
    GET_PRODUCT_BY_TYPE,
    GET_PRODUCT_BY_NAME,
    USERPERMISSION,
  } from '../constant';
  
  import axios from 'axios';
  import AsyncStorage from '@react-native-async-storage/async-storage';
  import { config } from '../../Util/config';
  import setAuthToken from '../../Util/setAuthToken';
import {loadingOn,loadingOff} from './loader'
import { ToastShow } from '../../components/Toast';


export const userinfo = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    const response = await axios.get(config().userInfoUrl);
    // const response = await axios.get(`${url}/usermanagement/api/auth/userInfo`);
    console.log('Action_userinfo_response' + JSON.stringify(response));
    dispatch({
      type: USERINFO,
      payload: response.data.data,
    });
    dispatch({
      type: USERPERMISSION,
      payload: response && response.data && response.data.data && response.data.data.permissions,
    });
    return response.data;
  } catch (e) {
    console.log('Action_userinfo_error' + JSON.stringify(e.response));
    //   
  } finally {
    loadingOff(dispatch);
  }
};

export const fetchOutboundPurchaseOrders = (skip, limit, params) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  console.log('fetchInboundPurchaseOrders' + JSON.stringify(params));
  try {
    console.log(
      'fetchInboundPurchaseOrders' +
      `${config().fetchInboundPurchaseOrders}?from=${params.toId}&orderId=${params.orderId
      }&productName=${params.productID}&dateFilter=${params.dateFilter
      }&deliveryLocation=${params.deliveryLocationId
      }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}&poStatus=${params.statusId}`,
    );
    const response = await axios.get(
      `${config().fetchInboundPurchaseOrders}?from=${params.toId}&orderId=${params.orderId
      }&productName=${params.productID}&dateFilter=${params.dateFilter
      }&deliveryLocation=${params.deliveryLocation
      }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}&poStatus=${params.statusId}`,
    );
    console.log(
      'Action_fetchInboundPurchaseOrders_response ' +
      JSON.stringify(response.data),
    );
    if (response.status === 200) {
      if (skip === 0) {
        dispatch({
          type: RESET_PURCHASE_ORDER_INBOUNDPOS,
          payload:
            (response.data &&
              response.data.data &&
              response.data.data.inboundPOs) ||
            [],
        });
        dispatch({
          type: SENT_ORDER_COUNT,
          payload:
            (response.data && response.data.data && response.data.data.count) ||
            0,
        });
      } else {
        dispatch({
          type: PURCHASE_ORDER_INBOUNDPOS,
          payload:
            (response.data &&
              response.data.data &&
              response.data.data.inboundPOs) ||
            [],
        });
        dispatch({
          type: SENT_ORDER_COUNT,
          payload:
            (response.data && response.data.data && response.data.data.count) ||
            0,
        });
      }
    }
    return response;
  } catch (e) {
    console.log('Action_fetchInboundPurchaseOrders_error', e.response);

  } finally {
    loadingOff(dispatch);
  }
};
export const fetchInboundPurchaseOrders = (skip, limit, params) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  console.log('fetchOutboundShipments' + JSON.stringify(params));
  try {
    console.log(
      'fetchOutboundShipments' +
      `${config().fetchOutboundPurchaseOrders}?to=${params.fromId}&orderId=${params.orderId
      }&productName=${params.productID}&dateFilter=${params.dateFilter
      }&deliveryLocation=${params.deliveryLocationId
      }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}&poStatus=${params.statusId}`,
    );
    const response = await axios.get(
      `${config().fetchOutboundPurchaseOrders}?to=${params.fromId}&orderId=${params.orderId
      }&productName=${params.productID}&dateFilter=${params.dateFilter
      }&deliveryLocation=${params.deliveryLocationId
      }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}&poStatus=${params.statusId}`,
    );
    console.log(
      'Action_ffetchOutboundShipments_response' + JSON.stringify(response.data),
    );

    if (response.status === 200) {
      if (skip === 0) {
        dispatch({
          type: RESET_PURCHASE_ORDER_OUTBOUNDPOS,
          payload:
            (response.data &&
              response.data.data &&
              response.data.data.outboundPOs) ||
            [],
        });
        dispatch({
          type: RECEIVE_ORDER_COUNT,
          payload:
            (response.data && response.data.data && response.data.data.count) ||
            0,
        });
      } else {
        dispatch({
          type: PURCHASE_ORDER_OUTBOUNDPOS,
          payload:
            (response.data &&
              response.data.data &&
              response.data.data.outboundPOs) ||
            [],
        });
        dispatch({
          type: RECEIVE_ORDER_COUNT,
          payload:
            (response.data && response.data.data && response.data.data.count) ||
            0,
        });
      }
    }
    return response;
  } catch (e) {
    console.log('Action_fetchInboundShipments_error', e.response);

  } finally {
    loadingOff(dispatch);
  }
};
export const filterOrderSent = data => async (dispatch, getState) => {
  console.log('filterOrderSent' + JSON.stringify(data));
  dispatch({
    type: ORDER_SENT_FILTER,
    payload: data,
  });
};
export const filterOrderReceive = data => async (dispatch, getState) => {
  dispatch({
    type: ORDER_RECEIVE_FILTER,
    payload: data,
  });
};
export const fetchProductIdsCustomerLocationsOrganisations = () => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  console.log('fetchProductIdsCustomerLocationsOrganisations');
  try {
    const response = await axios.get(
      config().fetchProductIdsCustomerLocationsOrganisations,
    );
    // console.log(
    //   'Action_fetchProductIdsCustomerLocationsOrganisations_response' +
    //     JSON.stringify(response.data),
    // );
    if (response.status === 200) {
      dispatch({
        type: GET_PRODUCT_CUSTOMER_lOCATION,
        payload: response.data.data,
      });
    }
    return response;
  } catch (e) {
    console.log(
      'Action_fetchProductIdsCustomerLocationsOrganisations_response' +
      JSON.stringify(e.response),
    );
    // console.log(
    //   'Action_fetchProductIdsCustomerLocationsOrganisations_response',
    //   e,
    // );

  } finally {
    loadingOff(dispatch);
  }
};
export const getOrderIds = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  console.log('getOrderIds');
  try {
    const response = await axios.get(config().getOrderIds);
    console.log('getOrderIds' + JSON.stringify(response));
    if (response.status === 200) {
      dispatch({
        type: GET_ALL_ORDER_ID,
        payload: response.data.data,
      });
    }
    return response;
  } catch (e) {
    console.log('Action_getOrderIds_response' + JSON.stringify(e.response));
    // console.log(
    //   'Action_fetchProductIdsCustomerLocationsOrganisations_response',
    //   e,
    // );

  } finally {
    loadingOff(dispatch);
  }
};
export const getRegions = orgType => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log('getRegions' + config().getRegions + orgType);
    const response = await axios.get(config().getRegions + orgType);
    console.log(
      'Action_getRegions_response' + JSON.stringify(response.data.data),
    );
    if (response.status === 200) {
      dispatch({
        type: GET_REGION_BY_ORGTYPE,
        payload: response.data.data,
      });
      return response.data.data;
    } else {
      return [];
    }
  } catch (e) {
    console.log('Action_getRegions_error', e);
    return [];
  } finally {
    loadingOff(dispatch);
  }
};
export const getCountryDetailsByRegion = (Region, orgType) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log(
      'getCountryDetailsByRegion' +
      `${config().getCountryDetailsByRegion}${Region}&orgType=${orgType}`,
    );
    const response = await axios.get(
      `${config().getCountryDetailsByRegion}${Region}&orgType=${orgType}`,
    );
    console.log(
      'Action_getCountryDetailsByRegion_response' + JSON.stringify(response),
    );
    if (response.status === 200) {
      dispatch({
        type: GET_COUNTRIES_BYORGTYPE,
        payload: response.data.data,
      });
      return response.data.data;
    } else {
      return [];
    }
  } catch (e) {
    console.log('Action_getCountryDetailsByRegion_error', e);
    return [];
  } finally {
    loadingOff(dispatch);
  }
};
export const getOrganizations = (country, orgType) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log(
      'getOrganizations' +
      `${config().getOrganizations}${orgType}&country=${country}`,
    );
    const response = await axios.get(
      `${config().getOrganizations}${orgType}&country=${country}`,
    );
    console.log('Action_getOrganizations_response' + JSON.stringify(response));
    if (response.status === 200) {
      dispatch({
        type: GET_ORG_BYORGTYPE,
        payload: response.data.data,
      });
      return response.data.data;
    } else {
      return [];
    }
  } catch (e) {
    console.log('Action_getOrganizationsn_error', e);
    return [];
  } finally {
    loadingOff(dispatch);
  }
};
export const Purchase_Order_Create = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Create_Purchase_Order start ' + JSON.stringify(data));
    const response = await axios.post(config().createPurchaseOrderUrl, data);
    console.log('Create_Purchase_Order response ' + JSON.stringify(response));
    if (response.status === 200) {
      // dispatch({
      //   type: ADD_PURCHASE_ORDER,
      //   payload: data,
      // });
      // dispatch({
      //   type: ADD_PURCHASE_ORDER_IDS,
      //   payload: response.data.orderID,
      // });
      callback();
    }

    return response;
  } catch (e) {
    console.log(
      'Action_Create_Purchase_Order_error' + JSON.stringify(e.response),
    );
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const fetch_Purchase_Orders_ids = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log(
      'Action_fetch_Purchase_Orders_ids_url' +
      JSON.stringify(config().fetchAllPurchaseOrdersUrl),
    );
    const response = await axios.get(config().fetchAllPurchaseOrdersUrl);
    console.log(
      'Action_fetch_Purchase_Orders_ids_response' + JSON.stringify(response),
    );
    if (response.status === 200) {
      dispatch({
        type: PURCHASE_ORDER_IDS,
        payload: response.data,
      });
    }
    return response;
  } catch (e) {
    console.log('Action_fetch_Purchase_Orders_ids_error', e.response);
    console.log('Action_fetch_Purchase_Orders_ids_error', e);
    return e;
  } finally {
    loadingOff(dispatch);
  }
};
 /**
   * Search bar functionality
   */
  export const getOrderDetails = (id, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_getOrderDetails Url' +
      //     JSON.stringify(`${config().getOrderDetails}`),
      // );
      const response = await axios.get(
        //config().fetchPurchaseOrderStatisticsUrl
        `${config().getOrderDetails}${id}&skip=0&limit=1`,
      );
      // console.log('Action_getOrderDetails_response' + JSON.stringify(response));
      return (
        (response.data && response.data.data && response.data.data.poDetails) ||
        []
      );
    } catch (e) {
      console.log('Action_fetch_Purchase_Orders_error', e.response);
      // console.log('Action_fetch_Purchase_Orders_error', e);
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  export const searchProductByType = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'searchProductByType url' +
        (config().searchProductByType + id + '&skip=0&limit=50'),
      );
      const response = await axios.get(
        config().searchProductByType + id + '&skip=0&limit=50',
      );
      console.log(
        'Action_searchProductByType_response' +
        JSON.stringify(response.data.data),
      );
      if (response.status === 200) {
        dispatch({
          type: GET_PRODUCT_BY_TYPE,
          payload: response.data && response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log(
        'Action_searchProductByType_error' + JSON.stringify(e.response),
      );
      // dispatch({
      //   type: GET_PRODUCT_BY_TYPE,
      //   payload: {},
      // });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const searchProductByName = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'searchProductByName url' +
        (config().searchProductByName + id + '&skip=0&limit=10'),
      );
      const response = await axios.get(
        config().searchProductByName + id + '&skip=0&limit=10',
      );
      console.log(
        'Action_searchProductByName_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      //   dispatch({
      //     type: GET_PRODUCT_BY_NAME,
      //     payload: response.data && response.data.data,
      //   });
      // } else {
      //   dispatch({
      //     type: GET_PRODUCT_BY_NAME,
      //     payload: [],
      //   });
      // }
      dispatch({
        type: GET_PRODUCT_BY_NAME,
        payload: response.data && response.data.data,
      });
      return response;
    } catch (e) {
      console.log(
        'Action_searchProductByName_error' + JSON.stringify(e.response),
      );
      dispatch({
        type: GET_PRODUCT_BY_NAME,
        payload: [],
      });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getTrackingDetails = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_getTrackingDetails_response' +
      //     (config().getProductDetailsByWarehouseId + id),
      // );
      const response = await axios.get(config().trackProduct + id);
      // console.log(
      //   'Action_getTrackingDetails_response' + JSON.stringify(response.data)
      // );
      if (response.status === 200) {
        dispatch({
          type: TRACKSHIPMENT,
          payload: response.data,
        });
      }
      return response.data;
    } catch (e) {
      console.log('Action_getWareHouseShipmentDetails_error', e);
      dispatch({
        type: TRACKSHIPMENT,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };

  export const changePOStatus = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log('changePOStatus start'+ JSON.stringify(data));
      console.log(
        'changePOStatus config().changePOStatus' + config().changePOStatus,
      );
  
      const response = await axios.post(config().changePOStatus, data);
      console.log('Action_changePOStatus_response' + JSON.stringify(response));
      // if (response.status === 200) {
      //   callback();
      // }
  
      return response;
    } catch (e) {
      console.log('Action_changePOStatus_error', e.response);
      console.log('Action_changePOStatus_error', e);
      // alert(e.response);
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };

    /**
   * Track && Trace Screen
   */
     export const trackJourney = (id, isTagged = false) => async (
      dispatch,
      getState,
    ) => {
      loadingOn(dispatch);
      try {
        console.log('trackJourney url' + (config().trackJourney + id));
        const response = await axios.get(config().trackJourney + id);
        console.log('Action_trackJourney_response' + JSON.stringify(response));
        if (response.status === 200) {
          if (isTagged) {
            dispatch({
              type: TRACKTAGSHIPMENT,
              payload: response.data && response.data.data,
            });
          } else {
            dispatch({
              type: TRACKSHIPMENT,
              payload: response.data && response.data.data,
            });
          }
        }
        return response;
      } catch (e) {
        console.log('Action_trackJourney_error', e);
        if (isTagged) {
          dispatch({
            type: TRACKTAGSHIPMENT,
            payload: {},
          });
        } else {
          dispatch({
            type: TRACKSHIPMENT,
            payload: {},
          });
        }
        return e.response;
        //   
      } finally {
        loadingOff(dispatch);
      }
    };
    

