import {
  REGISTER,
  OTP,
  LOGIN,
  USER_TOKEN,
  GET_REGION,
  GET_COUNTRIES,
  GET_STATES,
  GET_CITIES,
  ACTIVE_WAREHOUSE,
  USERPERMISSION,
  SELECTEDLANG,
} from '../constant';

import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { config } from '../../Util/config';
import setAuthToken from '../../Util/setAuthToken';
import { loadingOn, loadingOff } from './loader'
import { ToastShow } from '../../components/Toast';

export const changeLang = (lng, token = '') => async dispatch => {
  console.log('changeLang ' + lng)
  if (token) {
    setAuthToken(token, lng);
  }
  dispatch({
    type: SELECTEDLANG,
    payload: lng,
  });
};
export const sendOtp = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_sendOtp_data' + JSON.stringify(data));
    let response = await axios.post(config().sendOtpUrl, data);

    console.log('Action_sendOtp_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_sendOtp_error' + JSON.stringify(e));
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const verify_otp = (otpdata, lang = 'EN', callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  dispatch({
    type: OTP,
    payload: otpdata,
  });
  try {
    const response = await axios.post(config().verifyOtpUrl, otpdata);
    console.log('Action_verify_otp_response', response);

    await AsyncStorage.setItem('logged', '1');
    await AsyncStorage.setItem('token', response?.data?.data?.token);
    setAuthToken(response?.data?.data?.token, lang);
    dispatch({
      type: USER_TOKEN,
      payload: response?.data?.data?.token,
    });
    dispatch({
      type: USERPERMISSION,
      payload: response && response.data && response.data.data && response.data.data.permissions,
    });
    //const response = await axios.post(`${url}/usermanagement/api/auth/verify-otp`, otpdata);
    return response;
  } catch (e) {
    console.log('Action_verify_otp_error', e.response);

    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const login = (logindata, rem, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  dispatch({
    type: LOGIN,
  });

  try {
    console.log(config().loginUrl, logindata, getState().login.rem);

    const response = await axios.post(config().loginUrl, logindata);

    // console.log('Action_login_response'+JSON.stringify(response));
    if (response.data.message === 'Login Success.') {
      callback();
      setAuthToken(response.data.data.token, lang)
      dispatch({
        type: LOGIN,
        payload: response.data.data.token,
      });
    }
    return response;
  } catch (e) {
    // console.log('Action_login_error', e);
    // console.log('Action_login_error', e.response);
    const err = e.response.data.message;
    if (err === 'Email or Password wrong.') {
      return e.response;
    } else {
      return e;
    }
  } finally {
    loadingOff(dispatch);
  }
};




export const saveUser = (userdata, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log(
    //   'Action_user_saveUser_registerUrl123' +
    //     JSON.stringify(config().registerUrl),
    // );
    if (!userdata.mob) {
      delete userdata.phoneNumber;
    } else {
      // userdata.phoneNumber = userdata.phoneNumber.slice(1);
    }
    if (!userdata.emailId) {
      userdata.emailId = '';
    }
    console.log('Action_user_saveUser_userdata' + JSON.stringify(userdata));
    const response = await axios.post(config().registerUrl, userdata);
    console.log('Action_user_saveUser_response' + JSON.stringify(response));

    if (response.data.message === 'Registration Success.') {
      callback();
    }
    return response;
  } catch (e) {
    console.log("Action_saveUser_error", e.response);
    // console.log('Action_saveUser error  ' + JSON.stringify(e.response));

    const err = e.response && e.response.data && e.response.data.data && e.response.data.data[0];
    return e.response;

  } finally {
    loadingOff(dispatch);
  }
};
export const checkUser = (userdata, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  dispatch({
    type: REGISTER,
    payload: userdata,
  });
  try {
    // console.log('Action_checkUser' + JSON.stringify(config().checkUserUrl));
    // console.log('Action_user_checkUser_userdata' + JSON.stringify(userdata));
    const response = await axios.post(config().checkUserUrl, userdata);
    // console.log('Action_user_checkUser_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    //   console.log("Action_saveUser_error", e.response);
    console.log('Action_checkUser error  ' + JSON.stringify(e.response));

    // const err = e.response.data.data[0];
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const checkUseremailId = (emailID, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log(
    //   'Action_checkUseremailId' +
    //     JSON.stringify(config().checkUseremailId + emailID),
    // );
    // console.log('Action_checkUseremailIduserdata' + JSON.stringify(emailID));
    const response = await axios.get(config().checkUseremailId + emailID);
    // console.log('Action_checkUseremailId_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    // console.log("Action_checkUseremailId_error", e);
    console.log('Action_checkUser error  ' + JSON.stringify(e.response));

    // const err = e.response.data.data[0];
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const checkUserphoneNumber = (
  phoneNumber,
  callback = () => { },
) => async (dispatch, getState) => {
  loadingOn(dispatch);
  // dispatch({
  //   type: REGISTER,
  //   payload: userdata,
  // });
  try {
    // console.log(
    //   'checkUserphoneNumber' +
    //     JSON.stringify(config().checkUserphoneNumber + phoneNumber),
    // );
    phoneNumber = phoneNumber.slice(1);
    // console.log('Action_checkUserphoneNumber userdata' + JSON.stringify(userdata));
    const response = await axios.get(
      config().checkUserphoneNumber + phoneNumber,
    );
    // console.log('Action_checkUserphoneNumber_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    //   console.log("Action_checkUserphoneNumber_error", e.response);
    // console.log('Action_checkUser error  ' + JSON.stringify(e.response));

    // const err = e.response.data.data[0];
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const getAllRegion = () => async dispatch => {
  loadingOn(dispatch);
  console.log('getAllRegion');
  try {
    const response = await axios.get(config().getAllRegion);
    // console.log('Action_getAllRegion_response' + JSON.stringify(response.data));
    dispatch({
      type: GET_REGION,
      payload: (response.data && response.data.data) || [],
    });
    return response;
  } catch (e) {
    // console.log('Action_getAllRegion_response' + JSON.stringify(e.response));
    dispatch({
      type: GET_REGION,
      payload: [],
    });
    //   
  } finally {
    loadingOff(dispatch);
  }
};
export const getAllCountries = id => async dispatch => {
  loadingOn(dispatch);
  try {
    const response = await axios.get(config().getAllCountries + id);
    // console.log(
    //   'Action_getAllCountries_response' + JSON.stringify(response.data),
    // );
    dispatch({
      type: GET_COUNTRIES,
      payload: (response.data && response.data.data) || [],
    });
    return response;
  } catch (e) {
    // console.log('Action_getAllCountries_response' + JSON.stringify(e.response));
    dispatch({
      type: GET_COUNTRIES,
      payload: [],
    });
    //   
  } finally {
    loadingOff(dispatch);
  }
};

export const getAllState = id => async dispatch => {
  loadingOn(dispatch);
  console.log('getAllState');
  try {
    const response = await axios.get(config().getAllState + id);
    // console.log('Action_getAllState_response' + JSON.stringify(response.data));
    dispatch({
      type: GET_STATES,
      payload: (response.data && response.data.data) || [],
    });
    return response;
  } catch (e) {
    // console.log('Action_getAllState_response' + JSON.stringify(e.response));
    dispatch({
      type: GET_STATES,
      payload: [],
    });
    //   
  } finally {
    loadingOff(dispatch);
  }
};

export const getAllCities = id => async dispatch => {
  loadingOn(dispatch);
  console.log('getAllCities');
  try {
    // console.log('Action_getAllCities url' + (config().getAllCities + id));

    const response = await axios.get(config().getAllCities + id);
    // console.log('Action_getAllCities_response' + JSON.stringify(response.data));
    dispatch({
      type: GET_CITIES,
      payload: (response.data && response.data.data) || [],
    });
    return response;
  } catch (e) {
    console.log('Action_getAllCities_response' + JSON.stringify(e.response));
    dispatch({
      type: GET_CITIES,
      payload: [],
    });
    //  
  } finally {
    loadingOff(dispatch);
  }
};
export const switchLocation = (data, index, lang = 'EN', callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_user_switchLocation_' + config().switchLocation);
    // console.log('Action_user_switchLocation_data' + JSON.stringify(data));
    // let response = {};
    const response = await axios.post(config().switchLocation, data);
    // console.log(
    //   'Action_user_switchLocation_response' + JSON.stringify(response),
    // );
    // console.log(
    //   'Action_user_switchLocation_data.index' + JSON.stringify(index),
    // );
    if (response.status === 200) {
      await AsyncStorage.setItem('token', response?.data?.data?.token);
      setAuthToken(response?.data?.data?.token, lang);
      dispatch({
        type: USER_TOKEN,
        payload: response?.data?.data?.token,
      });
      dispatch({
        type: ACTIVE_WAREHOUSE,
        payload: index,
      });
    }
    return response;
  } catch (e) {
    console.log('Action_switchLocation error  ' + JSON.stringify(e));
    console.log(
      'Action_switchLocation e.response  ' + JSON.stringify(e.response),
    );

    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};

export const checkGoogleLogin = (data, lang = 'EN') => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    

    const response = await axios.post(config().googleLoginUrl, data);
    // console.log('Action_checkGoogleLogin response  ' + JSON.stringify(response));


    await AsyncStorage.setItem('logged', '1');
    await AsyncStorage.setItem('token', response?.data?.data?.token);
    setAuthToken(response?.data?.data?.token, lang);
    dispatch({
      type: USER_TOKEN,
      payload: response?.data?.data?.token,
    });
    dispatch({
      type: USERPERMISSION,
      payload: response && response.data && response.data.data && response.data.data.permissions,
    });

    return response;
  } catch (e) {
    console.log('Action_checkGoogleLogin error  ' + JSON.stringify(e));
    console.log('Action_checkGoogleLogin e.response  ' + JSON.stringify(e.response));

    // const err = e.response.data.data[0];
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
/**
 * Warehouse
 */
export const addWarehouse = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('addWarehouse data ' + JSON.stringify(data));
    // console.log(
    //   'config().addWarehouse ' + JSON.stringify(config().addWarehouse),
    // );
    const response = await axios.post(config().addWarehouse, data);
    // console.log('addWarehouse response ' + JSON.stringify(response));
    if (response.status === 200) {
      callback();
    }

    return response;
  } catch (e) {
    console.log('Action_addWarehouse_error' + JSON.stringify(e.response));
    // console.log('Action_addWarehouse_error', e);
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const updateWarehouse = (id, data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('updateWarehouse data ' + JSON.stringify(data));
    console.log(
      'config().updateWarehouse ' +
      JSON.stringify(config().updateWarehouse + id),
    );
    const response = await axios.post(config().updateWarehouse + id, data);
    console.log('updateWarehouse response ' + JSON.stringify(response));
    if (response.status === 200) {
      callback();
    }

    return response;
  } catch (e) {
    console.log('Action_updateWarehouse_error' + JSON.stringify(e.response));
    // console.log('Action_updateWarehouse_error', e);
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};