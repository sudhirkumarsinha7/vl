import {
    INVENTORY,
    INVENTORY_NEAREXPIRED,
    INVENTORY_EXPIRED,
    INVENTORY_COUNTS,
    INVENTORY_FILTER,
    INVENTORY_PRODUCTCOUNT,
    USER_TOKEN,
    ACTIVE_WAREHOUSE,
    GET_PRODUCT_BY_TYPE,
    GET_PRODUCT_BY_NAME,
    GET_PRODUCT_BY_INVEN,
    GET_OUTOF_STOCK,
} from '../constant';

import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { config } from '../../Util/config';
import setAuthToken from '../../Util/setAuthToken';
import { loadingOn, loadingOff } from './loader'
/**
  * Inventory Screen
  */
export const filterInverntoryData = data => async (dispatch, getState) => {
    console.log('filterInverntoryData' + JSON.stringify(data));
    dispatch({
        type: INVENTORY_FILTER,
        payload: data,
    });
};
export const getAllEventsWithFilter = (skip, limit, params) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        console.log(
            'Action_getAllEventsWithFilter_inventoriesUrl ' +
            JSON.stringify(
                `${config().getAllEventsWithFilter
                }?skip=${skip}&limit=${limit}&dateFilter=${params.dateFilter
                }&productName=${params.productID}&category=${params.type}&fromDate=${params.fromDate}&toDate=${params.toDate}&status=${params.statusId
                }`,
            ),
        );
        const response = await axios.get(
            `${config().getAllEventsWithFilter
            }?skip=${skip}&limit=${limit}&dateFilter=${params.dateFilter
            }&productName=${params.productID}&category=${params.type}&fromDate=${params.fromDate}&toDate=${params.toDate}&status=${params.statusId
            }`,
        );
        //test.vaccineledger.com/eventmanagement/api/event/getAllEventsWithFilter?skip=0&limit=10&dateFilter=&productName=&category=&status=
        // const response = await axios.get(`${config().inventoriesUrl}`);
        console.log(
            'Action_getAllEventsWithFilter_response ' + JSON.stringify(response),
        );
        if (response.status === 200) {
            dispatch({
                type: INVENTORY,
                payload: response.data.data.inventoryRecords,
            });
            dispatch({
                type: INVENTORY_COUNTS,
                payload: response.data.data.counts,
            });
        }

        return response;
    } catch (e) {
        console.log('Action_getAllEventsWithFilter_error', e.response);
        //   
    } finally {
        loadingOff(dispatch);
    }
};
export const getInventory = (skip, limit) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
        console.log(
            'Action_inventory_inventoriesUrl ' +
            JSON.stringify(`${config().inventoriesUrl}`),
        );
        const response = await axios.get(
            `${config().inventoriesUrl}?skip=${skip}&limit=${limit}`,
        );
        // const response = await axios.get(`${config().inventoriesUrl}`);
        console.log('response.status ' + JSON.stringify(response.status));

        console.log('Action_inventory_response ' + JSON.stringify(response));
        console.log('response.data.data ' + JSON.stringify(response.data.data));

        if (response.status === 200) {
            dispatch({
                type: GET_OUTOF_STOCK,
                payload: response.data.data,
            });
        }


        return response;
    } catch (e) {
        console.log('Action_inventory_error', e.response);
        //   
    } finally {
        loadingOff(dispatch);
    }
};

export const add_inventory = (data, callback = () => { }) => async (
    dispatch,
    getState,
) => {
    try {

        console.log(
            'start addProductsToInventory' +
            JSON.stringify({
                products: data,
            }),
        );
        console.log('start addProductsToInventory' + config().addProductsToInventory);


        const response = await axios.post(config().addProductsToInventory, {
            products: data,
        });

        return response;
    } catch (e) {
        console.log('Action_add_inventory_error', e);
        // alert(JSON.stringify(e.response))
        return e.response;
        //   
    }
};
export const getProductListCounts = () => async dispatch => {
    loadingOn(dispatch);
    console.log('getProductListCounts');
    try {
        const response = await axios.get(config().getProductListCounts);
        console.log(
            'Action_getProductListCounts_response' + JSON.stringify(response.data),
        );
        // if (response.status === 200) {
        //   dispatch({
        //     type: INVENTORY_PRODUCTCOUNT,
        //     payload: (response.data && response.data.message) || [],
        //   });
        // }
        dispatch({
            type: INVENTORY_PRODUCTCOUNT,
            payload: (response.data && response.data.message) || [],
        });
        return response;
    } catch (e) {
        console.log(
            'Action_getProductListCounts_response' + JSON.stringify(e.response),
        );
        dispatch({
            type: INVENTORY_PRODUCTCOUNT,
            payload: [],
        });

    } finally {
        loadingOff(dispatch);
    }
};
export const getBatchNearExpiration = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('getBatchNearExpiration');
    try {
        const response = await axios.get(config().getBatchNearExpiration);
        console.log(
            'Action_getBatchNearExpiration_response' + JSON.stringify(response.data),
        );
        if (response.status === 200) {
            dispatch({
                type: INVENTORY_NEAREXPIRED,
                payload: response.data.data,
            });
        }
        return response;
    } catch (e) {
        console.log(
            'Action_getBatchNearExpiration_response' + JSON.stringify(e.response),
        );
        console.log('Action_getBatchNearExpiration_response', e);

    } finally {
        loadingOff(dispatch);
    }
};
export const getBatchExpired = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('getBatchNearExpiration');
    try {
        const response = await axios.get(config().getBatchExpired);
        console.log(
            'Action_getBatchExpiredn_response' + JSON.stringify(response.data),
        );
        if (response.status === 200) {
            dispatch({
                type: INVENTORY_EXPIRED,
                payload: response.data.data,
            });
        }
        return response;
    } catch (e) {
        console.log('Action_getBatchExpired_response' + JSON.stringify(e.response));
        console.log('Action_getBatchExpired_response', e);

    } finally {
        loadingOff(dispatch);
    }
};

export const switchLocation = (data, index, lang = 'EN', callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log('Action_user_switchLocation_' + config().switchLocation);
      console.log('Action_user_switchLocation_data' + JSON.stringify(data));
      // let response = {};
      const response = await axios.post(config().switchLocation, data);
      console.log(
        'Action_user_switchLocation_response' + JSON.stringify(response),
      );
      console.log(
        'Action_user_switchLocation_data.index' + JSON.stringify(index),
      );
      if (response.status === 200) {
        await AsyncStorage.setItem('token', response?.data?.data?.token);
        setAuthToken(response?.data?.data?.token, lang);
        dispatch({
          type: USER_TOKEN,
          payload: response?.data?.data?.token,
        });
        dispatch({
          type: ACTIVE_WAREHOUSE,
          payload: index,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_switchLocation error  ' + JSON.stringify(e));
    //   console.log(
    //     'Action_switchLocation e.response  ' + JSON.stringify(e.response),
    //   );
  
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };



  export const searchProductByType = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'searchProductByType url' +
        (config().searchProductByType + id + '&skip=0&limit=50'),
      );
      const response = await axios.get(
        config().searchProductByType + id + '&skip=0&limit=50',
      );
      console.log(
        'Action_searchProductByType_response' +
        JSON.stringify(response.data.data),
      );
      if (response.status === 200) {
        dispatch({
          type: GET_PRODUCT_BY_TYPE,
          payload: response.data && response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log(
        'Action_searchProductByType_error' + JSON.stringify(e.response),
      );
      // dispatch({
      //   type: GET_PRODUCT_BY_TYPE,
      //   payload: {},
      // });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const searchProductByName = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'searchProductByName url' +
        (config().searchProductByName + id + '&skip=0&limit=10'),
      );
      const response = await axios.get(
        config().searchProductByName + id + '&skip=0&limit=10',
      );
      console.log(
        'Action_searchProductByName_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      //   dispatch({
      //     type: GET_PRODUCT_BY_NAME,
      //     payload: response.data && response.data.data,
      //   });
      // } else {
      //   dispatch({
      //     type: GET_PRODUCT_BY_NAME,
      //     payload: [],
      //   });
      // }
      dispatch({
        type: GET_PRODUCT_BY_NAME,
        payload: response.data && response.data.data,
      });
      return response;
    } catch (e) {
      console.log(
        'Action_searchProductByName_error' + JSON.stringify(e.response),
      );
      dispatch({
        type: GET_PRODUCT_BY_NAME,
        payload: [],
      });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getBatchWarehouse = (id, Pid) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'getBatchWarehouse url' +
        (config().getBatchWarehouse + id + '&product_id=' + Pid),
      );
      const response = await axios.get(
        config().getBatchWarehouse + id + '&product_id=' + Pid,
      );
      console.log(
        'Action_sgetBatchWarehouse_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      dispatch({
        type: GET_PRODUCT_BY_INVEN,
        payload: response.data && response.data.data,
      });
      // }
      return response;
    } catch (e) {
      console.log(
        'Action_getBatchWarehouse_error' + JSON.stringify(e.response),
      );
      // dispatch({
      //   type: GET_PRODUCT_BY_NAME,
      //   payload: {},
      // });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getBatchWarehouseLatest = (id, Wid = '') => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'fetchBatchesOfInventory url' +
        (config().fetchBatchesOfInventory + id + '&wareId=' + Wid),
      );
      const response = await axios.get(
        config().fetchBatchesOfInventory + id + '&wareId=' + Wid,
      );
      // const response = await axios.get(
      //   config().fetchBatchesOfInventory + 'COM3' + '&wareId=' + 'WAR100511',
      // );
      console.log(
        'Action_fetchBatchesOfInventory_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      dispatch({
        type: GET_PRODUCT_BY_INVEN,
        payload: response.data && response.data.data,
      });
      // }
      return response;
    } catch (e) {
      console.log(
        'Action_fetchBatchesOfInventory_error' + JSON.stringify(e.response),
      );
      // dispatch({
      //   type: GET_PRODUCT_BY_NAME,
      //   payload: {},
      // });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };