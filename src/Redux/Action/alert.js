import {
    GET_ALERTS,
    GET_MORE_ALERTS,
    GET_ALERTS_COUNT,
    GET_USERPERMISION,
    GET_TRANSACTIONS,
    GET_MORE_TRANSACTIONS,
    GET_TRANSACTIONS_COUNT,
    GET_ALERT_PAGE_COUNT,
    GET_TRANSACTION_PAGE_COUNT,
    GET_NEW_ALERT,
    GET_NEW_TRANSACTION,
  } from '../constant';
  
  import axios from 'axios';
  import { config,getIosDeviceTokeApi } from '../../Util/config';
  import {loadingOn,loadingOff} from './loader'

  export const registerTwillio = (userdata, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log('Action_user_registerTwillio123_' + config().registerTwillio);
      console.log(
        'Action_user_registerTwillio_response' + JSON.stringify(userdata),
      );
      // let response = {};
      const response = await axios.post(config().registerTwillio, userdata);
      console.log(
        'Action_user_registerTwillio_response' + JSON.stringify(response),
      );
  
      
  
      // if(userdata.device_type==='ios'){
      //   let body = {
      //     "application": "com.thevaccineledger",
      //     "sandbox": true,
      //     "apns_tokens": [
      //       userdata.token
      //     ]
      //   }  
      //   const configs = {
      //     headers: {
      //       'Authorization':'key=AAAA2jB22xM:APA91bH8nVgz9dl0W7Yd81lPGhZDH03TZwAWBRl3wqNfZErp0IcerRlW13OuWwOaxSiOoRFApAFZ-P2wWDl18hd-ow-VSNpuauGk30DwN1OqE5ds1iFqUL95jPH6qNa66BA75ox0n49P',
      //       'Content-Type':'application/json'
      //     },
      //   };
      //   const response = await axios.post(getIosDeviceTokeApi, body,configs);
      //   const newToken= response.data.results && response.data.results[0] && response.data.results[0].registration_token
      //   console.log(
      //     'Action_user_registerNotification_response' + JSON.stringify(response),
      //   );
      //   console.log(
      //     ' newToken' + JSON.stringify(newToken),
      //   );
      //   userdata.token =newToken
      //   console.log(
      //     ' userdata newToken' + JSON.stringify(userdata),
      //   );
      //   const response1 = await axios.post(config().registerTwillio, userdata);
      //   console.log(
      //     
      return response;
    } catch (e) {
      console.log('Action_registerTwillio error  ' + JSON.stringify(e));
      console.log(
        'Action_registerTwillio e.response  ' + JSON.stringify(e.response),
      );
  
      return e.response;
      // if (err.msg === 'E-mail already in use'||) {
      //   return e.response;
      // } else {
      //   return e;
      // }
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getAlertNotifications = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_getAlertNotifications_config().getAlertNotifications' +
      //   JSON.stringify(config().getAlertNotifications),
      // );
      const response = await axios.get(config().getAlertNotifications);
      // console.log(
      //   'Action_getAlertNotifications_response' + JSON.stringify(response),
      // );
      // if (response.status === 200) {
      dispatch({
        type: GET_ALERTS,
        payload: response.data && response.data.data && response.data.data.data,
      });
      let totalRecords = response.data && response.data.data && response.data.data.totalRecords;
      let totalLength = response.data && response.data.data && response.data.data.data && response.data.data.data.length;
      let totalNewRecords = response.data && response.data.data && response.data.data.new;
      dispatch({
        type: GET_NEW_ALERT,
        payload: totalNewRecords,
      });
      dispatch({
        type: GET_ALERTS_COUNT,
        payload: totalRecords,
      });
      if (totalRecords > totalLength) {
        dispatch({
          type: GET_ALERT_PAGE_COUNT,
          payload: 2,
        });
      }
      // }
      return response;
    } catch (e) {
      console.log('Action_getAlertNotifications_error', e.response);
      console.log('Action_getAlertNotifications_error', e);
      // alert(e);
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getTransactionNotifications = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_ggetTransactionNotifications_config().getAlertNotifications' +
      //   JSON.stringify(config().getTransactionNotifications + '&limit=20&page=1'),
      // );
      const response = await axios.get(config().getTransactionNotifications + '&limit=20&page=1');
      // console.log(
      //   'Action_getTransactionNotificationsresponse' + JSON.stringify(response),
      // );
      // if (response.status === 200) {
      dispatch({
        type: GET_TRANSACTIONS,
        payload: response.data && response.data.data && response.data.data.data,
      });
      let totalRecords = response.data && response.data.data && response.data.data.totalRecords;
      let totalNewRecords = response.data && response.data.data && response.data.data.new;
      dispatch({
        type: GET_NEW_TRANSACTION,
        payload: totalNewRecords,
      });
      dispatch({
        type: GET_TRANSACTIONS_COUNT,
        payload: totalRecords,
      });
  
      let totalLength = response.data && response.data.data && response.data.data.data && response.data.data.data.length;
      if (totalRecords > totalLength) {
        dispatch({
          type: GET_TRANSACTION_PAGE_COUNT,
          payload: 2,
        });
      }
      // }
      return response;
    } catch (e) {
      console.log('Action_getTransactionNotifications_error', e.response);
      console.log('Action_getTransactionNotifications_error', e);
      // alert(e);
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getMoreAlertNotifications = (page, length) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_getAlertNotifications_config().getAlertNotifications' +
      //   JSON.stringify(config().getAlertNotifications + '&limit=20&page=' + page),
      // );
      const response = await axios.get(config().getAlertNotifications + '&limit=20&page=' + page);
      // console.log(
      //   'Action_getAlertNotifications_response' + JSON.stringify(response),
      // );
      // if (response.status === 200) {
      dispatch({
        type: GET_MORE_ALERTS,
        payload: response.data && response.data.data && response.data.data.data,
      });
      let totalRecords = response.data && response.data.data && response.data.data.totalRecords;
      let totalLength = response.data && response.data.data && response.data.data.data && response.data.data.data.length + length;
      let totalNewRecords = response.data && response.data.data && response.data.data.new;
      dispatch({
        type: GET_NEW_ALERT,
        payload: totalNewRecords,
      });
      if (totalRecords > totalLength) {
        dispatch({
          type: GET_ALERT_PAGE_COUNT,
          payload: page + 1,
        });
      }
      // }
      return response;
    } catch (e) {
      console.log('Action_getAlertNotifications_error', e.response);
      console.log('Action_getAlertNotifications_error', e);
      // alert(e);
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getMoreTransactionNotifications = (page, length) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_ggetTransactionNotifications_config().getAlertNotifications' +
      //   JSON.stringify(config().getTransactionNotifications + '&limit=20&page=' + page),
      // );
      const response = await axios.get(config().getTransactionNotifications + '&limit=20&page=' + page);
      // console.log(
      //   'Action_getTransactionNotificationsresponse' + JSON.stringify(response),
      // );
      // if (response.status === 200) {
      dispatch({
        type: GET_MORE_TRANSACTIONS,
        payload: response.data && response.data.data && response.data.data.data,
      });
      let totalRecords = response.data && response.data.data && response.data.data.totalRecords;
      let totalLength = response.data && response.data.data && response.data.data.data && response.data.data.data.length + length;
      let totalNewRecords = response.data && response.data.data && response.data.data.new;
      dispatch({
        type: GET_NEW_TRANSACTION,
        payload: totalNewRecords,
      });
      if (totalRecords > totalLength) {
        dispatch({
          type: GET_TRANSACTION_PAGE_COUNT,
          payload: page + 1,
        });
      }
      // }
      return response;
    } catch (e) {
      console.log('Action_getTransactionNotifications_error', e.response);
      console.log('Action_getTransactionNotifications_error', e);
      // alert(e);
    } finally {
      loadingOff(dispatch);
    }
  };
  export const updateRequest = (id, status) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'updateRequest().updateRequest' +
        JSON.stringify(config().updateRequest + id + '&status=' + status),
      );
      const response = await axios.get(config().updateRequest + id + '&status=' + status);
      console.log(
        'Action_updateRequest_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      //   dispatch({
      //     type: GET_ALERTS,
      //     payload: response.data.data,
      //   });
      // }
      return response;
    } catch (e) {
      console.log('Action_updateRequest_error', e.response);
      console.log('Action_updateRequest_error', e);
      // alert(e);
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getRequestById = (id) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'updateRequest().getRequestById' +
        JSON.stringify(config().getRequestById + id),
      );
      const response = await axios.get(config().getRequestById + id);
      console.log(
        'Action_getRequestById_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        dispatch({
          type: GET_USERPERMISION,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_getRequestById_error', e.response);
      console.log('Action_getRequestById_error', e);
      // alert(e);
    } finally {
      loadingOff(dispatch);
    }
  };
  export const readNotification = (id) => async (dispatch, getState) => {
    // loadingOn(dispatch);
    try {
      console.log(
        'readNotification().readNotification' +
        JSON.stringify(config().readNotification + id),
      );
      const response = await axios.get(config().readNotification + id);
      console.log(
        'Action_readNotification_response' + JSON.stringify(response),
      );
  
      return response;
    } catch (e) {
      console.log('Action_greadNotification_error', e.response);
      console.log('Action_readNotification_error', e);
      // alert(e);
    } finally {
      loadingOff(dispatch);
    }
  };