import {
    INVENTORY_ANALYTICS,
    SHIPMENT_ANALYTICS,
    ORDER_ANALYTCS,
  } from '../constant';
  
  import axios from 'axios';
  import AsyncStorage from '@react-native-async-storage/async-storage';
  import { config } from '../../Util/config';
  import {loadingOn,loadingOff} from './loader'

    /**
   * Analytics
   */
   export const getShipmentAnalytics = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_inventory_getShipmentAnalyticssUrl ' +
      //     JSON.stringify(`${config().getShipmentAnalytics}`),
      // );
      const response = await axios.get(`${config().getShipmentAnalytics}`);
      // console.log(
      //   'Action_getShipmentAnalytics_response ' + JSON.stringify(response),
      // );
      // if (response.status === 200) {
      dispatch({
        type: SHIPMENT_ANALYTICS,
        payload: (response.data.data && response.data.data.shipment) || {},
      });
      // }
  
      return response;
    } catch (e) {
      console.log('Action_getInventoryAnalytics_error', e.response);
      //   
      dispatch({
        type: SHIPMENT_ANALYTICS,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getInventoryAnalytics = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_inventory_getInventoryAnalyticsUrl ' +
      //     JSON.stringify(`${config().getInventoryAnalytics}`),
      // );
      const response = await axios.get(`${config().getInventoryAnalytics}`);
      // console.log(
      //   'Action_getInventoryAnalytics_response ' + JSON.stringify(response),
      // );
      // if (response.status === 200) {
      dispatch({
        type: INVENTORY_ANALYTICS,
        payload: (response.data.data && response.data.data.inventory) || {},
      });
      // }
  
      return response;
    } catch (e) {
      console.log('Action_getInventoryAnalytics_error', e.response);
      //   
      dispatch({
        type: INVENTORY_ANALYTICS,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getOrderAnalytics = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'Action_getOrderAnalytics ' +
          JSON.stringify(`${config().getOrderAnalytics}`),
      );
      const response = await axios.get(`${config().getOrderAnalytics}`);
      console.log(
        'Action_getOrderAnalytics_response ' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      dispatch({
        type: ORDER_ANALYTCS,
        payload: (response.data.data && response.data.data.order) || {},
      });
      // }
  
      return response;
    } catch (e) {
      console.log('Action_getOrderAnalytics_error' + JSON.stringify(e.response));
      //   
      dispatch({
        type: ORDER_ANALYTCS,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };