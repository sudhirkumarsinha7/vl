import {
    REGISTER,
    OTP,
    LOGIN,
    ALL_USER,
    USERINFO,
    USERUPDATE,
    HIDELODER,
    SHOWLODER,
    SHIPMENT,
    SHIPPING_ORDER,
    INVENTORY,
    INVENTORY_NEAREXPIRED,
    INVENTORY_EXPIRED,
    INVENTORY_COUNTS,
    VACCINE_COUNTS,
    GET_WAREHOUSE_BYORGID,
    ADD_INVENTORY,
    UPDATE_SHIPMENT,
    ADD_SHIPMENT,
    PURCHASE_ORDER,
    PURCHASE_ORDER_IDS,
    ADD_PURCHASE_ORDER,
    ADD_PURCHASE_ORDER_IDS,
    SHIPMENT_COUNTS,
    PRODUCT_NAME,
    MANUFACTURERS,
    ORGANIZATIONS,
    SHIPING_ORDERIDS,
    SHIPING_ORDER_DETAILS,
    GET_WAREHOUSE,
    GET_WAREHOUSE_SHiPMENT,
    TRACKSHIPMENT,
    TRACKTAGSHIPMENT,
    INVENTORY_ANALYTICS,
    SHIPMENT_ANALYTICS,
    ORDER_ANALYTCS,
    CHAIN_SHIPMENT,
    FETCH_SHIPMENT,
    INBOUND_SHIPMENT,
    OUTBOUND_SHIPMENT,
    RESET_SHIPMENT,
    RESET_INBOUND_SHIPMENT,
    RESET_OUTBOUND_SHIPMENT,
    PURCHASE_ORDER_INBOUNDPOS,
    PURCHASE_ORDER_OUTBOUNDPOS,
    RESET_PURCHASE_ORDER_INBOUNDPOS,
    RESET_PURCHASE_ORDER_OUTBOUNDPOS,
    GET_ORGANIZATIONTYPE_LIST,
    GET_ALLSUPPLIER_RECEIVER,
    INBOUND_FILTER,
    OUTBOUND_FILTER,
    ORDER_SENT_FILTER,
    ORDER_RECEIVE_FILTER,
    GET_PRODUCT_CUSTOMER_lOCATION,
    INVENTORY_FILTER,
    INVENTORY_PRODUCTCOUNT,
    INBOUND_COUNT,
    OUTBOUND_COUNT,
    SENT_ORDER_COUNT,
    RECEIVE_ORDER_COUNT,
    GET_ALL_ORDER_ID,
    GET_ALL_SHIPMENT_ID,
    USER_TOKEN,
    GET_IOT_STATUS,
    GET_REGION,
    GET_COUNTRIES,
    GET_STATES,
    GET_CITIES,
    ACTIVE_WAREHOUSE,
    GET_REGION_BY_ORGTYPE,
    GET_COUNTRIES_BYORGTYPE,
    GET_ORG_BYORGTYPE,
    GET_PRODUCT_BY_TYPE,
    GET_PRODUCT_BY_NAME,
    GET_PRODUCT_BY_INVEN,
    USERPERMISSION,
    GET_OUTOF_STOCK,
    SELECTEDLANG,
    PROFILE_PIC,
  } from '../constant';
  
  import axios from 'axios';
  import AsyncStorage from '@react-native-async-storage/async-storage';
  import { config } from '../../Util/config';
  import setAuthToken from '../../Util/setAuthToken';
import { ToastShow } from '../../components/Toast';

  export const saveUser = (userdata, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_user_saveUser_registerUrl123' +
      //     JSON.stringify(config().registerUrl),
      // );
      if (!userdata.mob) {
        delete userdata.phoneNumber;
      } else {
        userdata.phoneNumber = userdata.phoneNumber.slice(1);
      }
      if (!userdata.emailId) {
        userdata.emailId = '';
      }
  
      // if (userdata.address && userdata.address.organisationName) {
      //   delete userdata.address.organisationName;
      // }
      console.log('Action_user_saveUser_userdata' + JSON.stringify(userdata));
      // let response = {};
      const response = await axios.post(config().registerUrl, userdata);
      console.log('Action_user_saveUser_response' + JSON.stringify(response));
  
      // const response = await axios.post(`${url}/usermanagement/api/auth/register`, userdata);
      if (response.data.message === 'Registration Success.') {
        callback();
      }
      return response;
    } catch (e) {
      console.log("Action_saveUser_error", e.response);
      // console.log('Action_saveUser error  ' + JSON.stringify(e.response));
  
      const err = e.response && e.response.data && e.response.data.data && e.response.data.data[0];
      return e.response;
      // if (err.msg === 'E-mail already in use'||) {
      //   return e.response;
      // } else {
      //   return e;
      // }
    } finally {
      loadingOff(dispatch);
    }
  };
  export const checkUser = (userdata, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    dispatch({
      type: REGISTER,
      payload: userdata,
    });
    try {
      // console.log('Action_checkUser' + JSON.stringify(config().checkUserUrl));
      // console.log('Action_user_checkUser_userdata' + JSON.stringify(userdata));
      const response = await axios.post(config().checkUserUrl, userdata);
      // console.log('Action_user_checkUser_response' + JSON.stringify(response));
      return response;
    } catch (e) {
      //   console.log("Action_saveUser_error", e.response);
      console.log('Action_checkUser error  ' + JSON.stringify(e.response));
  
      // const err = e.response.data.data[0];
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  export const sendOtp = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log('Action_sendOtp_data' + JSON.stringify(data));
      // alert(config().sendOtpUrl);
      // alert(JSON.stringify(config().sendOtpUrl));
      let response = await axios.post(config().sendOtpUrl, data);
      // let url = 'https://api.waybus.in/sendOtp/9491136764'
      // let response = await axios.get(url);
  
      console.log('Action_sendOtp_response' + JSON.stringify(response));
      // alert(JSON.stringify(response));
      return response;
    } catch (e) {
      console.log('Action_sendOtp_error' + JSON.stringify(e));
      // console.log('Action_sendOtp_error message' + JSON.stringify(e.response));
  
      // alert(JSON.stringify(e));
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  
  
    //   return fetch(config().sendOtpUrl, {
    //   method: 'POST',
    //   body: JSON.stringify(data),
    //   headers: {"Content-type": "application/json;charset=UTF-8"}
  
    // })
    // .then(response => response.json())
    // .then(result => {
    //   let a = {
    //     status:200
    //   }
    //   console.log('Success:', result);
    //   return a
    // })
    // .catch(error => {
    //   console.error('Error:', error);
    //   alert(JSON.stringify(error))
    //   return error
    // });
  };
  
  
  export const verify_otp = (otpdata, lang = 'EN', callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    dispatch({
      type: OTP,
      payload: otpdata,
    });
    try {
      const response = await axios.post(config().verifyOtpUrl, otpdata);
      await AsyncStorage.setItem('logged', '1');
      await AsyncStorage.setItem('token', response?.data?.data?.token);
      setAuthToken(response?.data?.data?.token, lang);
      dispatch({
        type: USER_TOKEN,
        payload: response?.data?.data?.token,
      });
      dispatch({
        type: USERPERMISSION,
        payload: response && response.data && response.data.data && response.data.data.permissions,
      });
      //const response = await axios.post(`${url}/usermanagement/api/auth/verify-otp`, otpdata);
      console.log('Action_verify_otp_response', response);
      return response;
    } catch (e) {
      console.log('Action_verify_otp_error', e.response);
  
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const resend_otp = (otpdata, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    // dispatch({
    //     type: OTP,
    //     payload: otpdata
    // })
    try {
      const response = await axios.post(
        `${url}/usermanagement/api/auth/resend-verify-otp`,
        otpdata,
      );
      console.log('Action_resend_otp_response', response);
      if (response.data.message === 'Confirm otp sent.') {
        alert('Resend OTP Sent Successfully');
      }
      return response;
    } catch (e) {
      console.log('Action_resend_otp_error', e.response);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const login = (logindata, rem, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    dispatch({
      type: LOGIN,
      payload: rem,
    });
  
    try {
      console.log(config().loginUrl, logindata, getState().login.rem);
  
      const response = await axios.post(config().loginUrl, logindata);
  
      // console.log('Action_login_response'+JSON.stringify(response));
      if (response.data.message === 'Login Success.') {
        callback();
        if (getState().login.rem) {
          console.log('true', getState().login.rem);
          await AsyncStorage.setItem('logged', '1');
          await AsyncStorage.setItem('token', response.data.data.token);
          setAuthToken(response.data.data.token, lang);
  
          dispatch({
            type: LOGIN,
            payload: response.data.data.token,
          });
        } else {
          console.log('false', getState().login.rem);
          setAuthToken(response.data.data.token, lang);
  
          dispatch({
            type: LOGIN,
            payload: response.data.data.token,
          });
        }
      }
      return response;
    } catch (e) {
      // console.log('Action_login_error', e);
      // console.log('Action_login_error', e.response);
      const err = e.response.data.message;
      if (err === 'Email or Password wrong.') {
        return e.response;
      } else {
        return e;
      }
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const fetchOrganizations = () => async (dispatch, getState) => {
    // loadingOn(dispatch);
    try {
      console.log(
        'Action_fetchOrganizations_config().getOrganisations' +
        JSON.stringify(config().getOrganisations),
      );
      const response = await axios.get(config().getOrganisations);
      console.log(
        'Action_fetchOrganizations_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        dispatch({
          type: ORGANIZATIONS,
          payload: response.data.data,
        });
      }
      return response.data.data;
    } catch (e) {
      console.log('Action_fetchOrganizations_error', e.response);
      // console.log('Action_fetchOrganizations_error', e);
  
    } finally {
      // loadingOff(dispatch);
    }
  };
  
  export const fetchShippingOrderIds = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('fetchShippingOrderIds');
    try {
      const response = await axios.get(`${config().getShippingOrderIds}`); //SHP9876
      // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
      console.log('Action_ fetchShippingOrderIds_response', response.data.data);
      if (response.status === 200) {
        dispatch({
          type: SHIPING_ORDERIDS,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_ fetchShippingOrderIds_error', e.response);
      console.log('Action_ fetchShippingOrderIds_error', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getViewShipmentOrderDetails = shipmentID => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    console.log('getViewShipmentOrderDetails');
    try {
      const response = await axios.get(
        `${config().viewShippingOrderById}` + shipmentID,
      ); //SHP9876
      console.log(
        'Action_getViewShipmentOrderDetails_response',
        response.data.data,
      );
      if (response.status === 200) {
        dispatch({
          type: SHIPING_ORDER_DETAILS,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_getViewShipmentOrderDetails_error', e.response);
      console.log('Action_getViewShipmentOrderDetails_error', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const upload_Image = (image, imageredux, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      let response;
      const configs = {
        headers: {
          'content-type': 'multipart/form-data',
        },
      };
      console.log(
        'Action_upload_ImagesUrl ' + JSON.stringify(`${config().upload}`),
      );
      console.log('Action_upload_Image ', image);
      response = await axios.post(config().upload, image, configs);
      // response = await axios.post(`${url}/usermanagement/api/auth/upload`, image, configs);
      if (response.data.message === 'Updated') {
        alert('Image successfully updated');
  
      }
      console.log('Action_upload_Image_response' + JSON.stringify(response));
      return response;
    } catch (e) {
      console.log('Action_upload_Image_error' + JSON.stringify(e.response));
  
    } finally {
      loadingOff(dispatch);
    }
  };
  
  // export const upload_Image = (data, callback = () => {}) => async (
  //   dispatch,
  //   getState,
  // ) => {
  //   loadingOn(dispatch);
  //   const token = await AsyncStorage.getItem('token');
  //   let uploadUrl = config().upload;
  //   const headers = {
  //     'Content-Type': 'multipart/form-data',
  //     Authorization: 'Bearer ' + token,
  //   };
  //   console.log('Action_upload_Image_url' + JSON.stringify(uploadUrl));
  //   console.log('Action_upload_Image_headers' + JSON.stringify(headers));
  //   console.log('Action_upload_Image_body' + JSON.stringify(data));
  //   return fetch(uploadUrl, {
  //     method: 'POST',
  //     headers: headers,
  //     body: data,
  //   })
  //     .then(response => response.json())
  //     .then(responseJson => {
  //       console.log(
  //         'Action_upload_Image_response ' + JSON.stringify(responseJson),
  //       );
  //     })
  //     .catch(error => {
  //       console.log('Action_upload_Image_error' + JSON.stringify(error));
  //     });
  // };
  export const user_update = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      let response;
      console.log('profileupdate' + config().updateProfileUrl);
      console.log('profileupdate data' + JSON.stringify(data));
  
      response = await axios.post(config().updateProfileUrl, data);
      //response = await axios.post(`${url}/usermanagement/api/auth/updateProfile`, data);
      if (response.status === 200) {
        dispatch({
          type: USERUPDATE,
          payload: data,
        });
      }
      console.log('Action_user_update_response', response);
      return response;
    } catch (e) {
      console.log('Action_user_update_error' + JSON.stringify(e.response));
      // console.log('Action_user_update_error', e);
      //   
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const userinfo = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      const response = await axios.get(config().userInfoUrl);
      // const response = await axios.get(`${url}/usermanagement/api/auth/userInfo`);
      console.log('Action_userinfo_response' + JSON.stringify(response));
      dispatch({
        type: USERINFO,
        payload: response.data.data,
      });
      dispatch({
        type: USERPERMISSION,
        payload: response && response.data && response.data.data && response.data.data.permissions,
      });
      return response.data;
    } catch (e) {
      console.log('Action_userinfo_error' + JSON.stringify(e.response));
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const getInventory = (skip, limit) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'Action_inventory_inventoriesUrl ' +
        JSON.stringify(`${config().inventoriesUrl}`),
      );
      const response = await axios.get(
        `${config().inventoriesUrl}?skip=${skip}&limit=${limit}`,
      );
      // const response = await axios.get(`${config().inventoriesUrl}`);
      console.log('response.status ' + JSON.stringify(response.status));
  
      console.log('Action_inventory_response ' + JSON.stringify(response));
      console.log('response.data.data ' + JSON.stringify(response.data.data));
  
      if (response.status === 200) {
        dispatch({
          type: GET_OUTOF_STOCK,
          payload: response.data.data,
        });
      }
  
  
      return response;
    } catch (e) {
      console.log('Action_inventory_error', e.response);
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const add_inventory = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    try {
  
      console.log(
        'start addProductsToInventory' +
        JSON.stringify({
          products: data,
        }),
      );
      console.log('start addProductsToInventory' + config().addProductsToInventory);
  
  
      const response = await axios.post(config().addProductsToInventory, {
        products: data,
      });
  
      return response;
    } catch (e) {
      console.log('Action_add_inventory_error', e);
      return e.response||e
      //   
    }
  };
  
  export const fetch_shipmemnt = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('Action_inventory');
    try {
      const response = await axios.get(config().shipmentsSearch + 'SHP2345'); //SHP9876
      // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
      console.log('Action_fetch_shipmemnt _response', response.data);
      if (response.data.data) {
        dispatch({
          type: SHIPMENT,
          payload: response.data,
        });
      }
      return response.data;
    } catch (e) {
      console.log('Action_fetch_shipmemnt_error', e);
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const create_shipmemnt = (data, address, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    console.log('Action_inventory', data);
    try {
      console.log(config().createShipmentUrladdress + address);
  
      const response = await axios.post(
        config().createShipmentUrladdress + address,
        { data: data },
      );
      console.log('Action_create_shipmemnt_response', response);
      if (response.status === 200) {
        dispatch({
          type: UPDATE_SHIPMENT,
          payload: data,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_create_shipmemnt_error', e.response);
      console.log('Action_create_shipmemnt_error', e);
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const create_new_shipment = (
    data,
    address,
    callback = () => { },
  ) => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('create_new_shipment' + JSON.stringify(data));
  
    try {
      const response = await axios.post(config().createShipmentUrl, data);
      console.log(
        'Action_create_new_shipment_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      //   dispatch({
      //     type: ADD_SHIPMENT,
      //     payload: response.data.data,
      //   });
      // }
      return response;
    } catch (e) {
      // console.log(
      //   'Action_create_new_shipment_error' + JSON.stringify(e.response),
      // );
      // console.log('Action_create_new_shipment_error', e);
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const getShippingOrders = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('getShippingOrders');
    try {
      const response = await axios.get(config().getShippingOrdersUrl);
      console.log('Action_getShippingOrders_response', response.data);
      if (response.status === 200) {
        dispatch({
          type: SHIPPING_ORDER,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_getShippingOrders_response', e.response);
      console.log('Action_getShippingOrders_response', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  
  // export const fetchPublisherShipments = (skip, limit) => async (
  //   dispatch,
  //   getState,
  // ) => {
  //   loadingOn(dispatch);
  //   console.log('fetchPublisherShipments');
  //   try {
  //     const response = await axios.get(
  //       `${config().shipmentsUrl}?skip=${skip}&limit=${limit}`,
  //     ); //SHP9876
  //     // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
  //     // console.log(
  //     //   'Action_fetchPublisherShipments_response' + JSON.stringify(response),
  //     // );
  //     if (response.status === 200) {
  //       if (skip === 0) {
  //         dispatch({
  //           type: RESET_SHIPMENT,
  //           payload: response.data.data,
  //         });
  //         dispatch({
  //           type: RESET_INBOUND_SHIPMENT,
  //           payload: (response.data && response.data.inboundShipments) || [],
  //         });
  //         dispatch({
  //           type: RESET_OUTBOUND_SHIPMENT,
  //           payload: (response.data && response.data.outboundShipments) || [],
  //         });
  //       } else {
  //         dispatch({
  //           type: SHIPMENT,
  //           payload: response.data.data,
  //         });
  //         dispatch({
  //           type: INBOUND_SHIPMENT,
  //           payload: (response.data && response.data.inboundShipments) || [],
  //         });
  //         dispatch({
  //           type: OUTBOUND_SHIPMENT,
  //           payload: (response.data && response.data.outboundShipments) || [],
  //         });
  //       }
  //     }
  //     // dispatch({
  //     //   type: SHIPMENT_COUNTS,
  //     //   payload: response.data.counts,
  //     // });
  //     return response;
  //   } catch (e) {
  //     console.log('Action_fetchPublisherShipments_error', e.response);
  //     console.log('Action_fetchPublisherShipments_error', e);
  //       
  //   } finally {
  //     loadingOff(dispatch);
  //   }
  // };
  
  export const fetchAllUsers = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      const response = await axios.get(config().getAllUsersUrl);
      console.log('Action_fetchAllUsers_response', response);
      if (response.status === 200) {
        dispatch({
          type: ALL_USER,
          payload: response.data.data,
        });
      }
      return response.data.data;
    } catch (e) {
      console.log('Action_fetchAllUsers_error', e.response);
      console.log('Action_fetchAllUsers_error', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const Purchase_Order_Create = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log('Create_Purchase_Order start ' + JSON.stringify(data));
      const response = await axios.post(config().createPurchaseOrderUrl, data);
      console.log('Create_Purchase_Order response ' + JSON.stringify(response));
      if (response.status === 200) {
        // dispatch({
        //   type: ADD_PURCHASE_ORDER,
        //   payload: data,
        // });
        // dispatch({
        //   type: ADD_PURCHASE_ORDER_IDS,
        //   payload: response.data.orderID,
        // });
        callback();
      }
  
      return response;
    } catch (e) {
      console.log(
        'Action_Create_Purchase_Order_error' + JSON.stringify(e.response),
      );
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const fetch_Purchase_Orders_ids = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'Action_fetch_Purchase_Orders_ids_url' +
        JSON.stringify(config().fetchAllPurchaseOrdersUrl),
      );
      const response = await axios.get(config().fetchAllPurchaseOrdersUrl);
      console.log(
        'Action_fetch_Purchase_Orders_ids_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        dispatch({
          type: PURCHASE_ORDER_IDS,
          payload: response.data,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_fetch_Purchase_Orders_ids_error', e.response);
      console.log('Action_fetch_Purchase_Orders_ids_error', e);
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const fetch_Purchase_Orders = (skip, limit) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_fetch_Purchase_Orders_fetchPurchaseOrderStatisticsUrl' +
      //     JSON.stringify(`${config().fetchPurchaseOrderStatisticsUrl}`),
      // );
      const response = await axios.get(
        //config().fetchPurchaseOrderStatisticsUrl
        `${config().fetchPurchaseOrderStatisticsUrl}?skip=${skip}&limit=${limit}`,
      );
      // console.log(
      //   'Action_fetch_Purchase_Orders_response skip ' +
      //     skip +
      //     ' ' +
      //     JSON.stringify(response),
      // );
      if (response.status === 200) {
        if (skip === 0) {
          dispatch({
            type: PURCHASE_ORDER,
            payload: response.data.data,
          });
          dispatch({
            type: RESET_PURCHASE_ORDER_INBOUNDPOS,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.inboundPOs) ||
              [],
          });
          dispatch({
            type: RESET_PURCHASE_ORDER_OUTBOUNDPOS,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.outboundPOs) ||
              [],
          });
        } else {
          dispatch({
            type: PURCHASE_ORDER,
            payload: response.data.data,
          });
          dispatch({
            type: PURCHASE_ORDER_INBOUNDPOS,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.inboundPOs) ||
              [],
          });
          dispatch({
            type: PURCHASE_ORDER_OUTBOUNDPOS,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.outboundPOs) ||
              [],
          });
        }
      }
      return response;
    } catch (e) {
      console.log('Action_fetch_Purchase_Orders_error 123', e);
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const selected_po = (poid, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      const response = await axios.get(config().fetchAllPurchaseOrderUrl + poid);
      console.log('Action_selected_po_response', response);
      return response.data.data;
    } catch (e) {
      console.log('Action_selected_po_error', e.response);
      console.log('Action_selected_po_error', e);
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const addInventory = (body, body2, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log(
        'body',
        body,
        body2,
        await axios.post(config().addInventory, { data: body }),
      );
      console.log(await axios.post(config().addInventory, { data: body }));
      const response = await axios.post(config().addInventory, { data: body });
      console.log('Action_addInventory_response', response);
      if (response.status === 200) {
        console.log('hello');
        try {
          const response = await axios.post(config().createShipmentUrl, {
            data: body2,
          });
          console.log('Action_create_new_shipment_response', response);
          if (response.status === 200) {
            dispatch({
              type: ADD_SHIPMENT,
              payload: body2,
            });
          }
          return response;
        } catch (e) {
          console.log('Action_create_new_shipment_error', e.response);
          console.log('Action_create_new_shipment_error', e);
          return e;
        }
      }
  
      callback();
      return response;
    } catch (e) {
      const response = await axios.post(config().updateInventory, { data: body });
      console.log('response', response);
      if (response.status === 200) {
        console.log('hello catch');
        try {
          const response = await axios.post(config().createShipmentUrl, {
            data: body2,
          });
          console.log('Action_create_new_shipment_response', response);
          if (response.status === 200) {
            dispatch({
              type: ADD_SHIPMENT,
              payload: body2,
            });
          }
          return response;
        } catch (e) {
          console.log('Action_create_new_shipment_error', e.response);
          console.log('Action_create_new_shipment_error', e);
          return e;
        }
      }
      // console.log("Action_addInventory_error", e.response)
      // console.log("Action_saddInventory_error", e)
      callback();
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const receiveShipment = (
    body,
    body2,
    address,
    callback = () => { },
  ) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
     
      // console.log('config().recieveShipment ' + JSON.stringify(config().recieveShipment));
  
      // console.log('config().recieveShipment body ' + JSON.stringify(body));
      const configs = {
        headers: {
          'content-type': 'multipart/form-data',
        },
      };
      const response = await axios.post(config().recieveShipment, body, configs);
      // console.log('Action_receiveShipment_response ' + JSON.stringify(response));
  
  
      return response;
    } catch (e) {
      // console.log("Action_addInventory_error", e)
      // console.log('Action_receiveShipment_error' + JSON.stringify(e.response));
      let err = e.response || e
      return err;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const getProductNames = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      const response = await axios.get(config().getProducts);
      // console.log(
      //   'Action_getProductNames_response' + JSON.stringify(response.data.data),
      // );
      if (response.status === 200) {
        dispatch({
          type: PRODUCT_NAME,
          payload: response.data.data,
        });
      }
      return response.data.data;
    } catch (e) {
      console.log('Action_getProductNames_error', e.response);
      // console.log('Action_getProductNames_error', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const getManufacturersName = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      const response = await axios.get(config().getManufacturers);
      console.log('Action_getManufacturersName_response', response);
      if (response.status === 200) {
        dispatch({
          type: MANUFACTURERS,
          payload: response.data.data,
        });
      }
      return response.data.data;
    } catch (e) {
      console.log('Action_getProductNames_error', e.response);
      console.log('Action_getProductNames_error', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const changePOStatus = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log('changePOStatus start'+ JSON.stringify(data));
      console.log(
        'changePOStatus config().changePOStatus' + config().changePOStatus,
      );
  
      const response = await axios.post(config().changePOStatus, data);
      console.log('Action_changePOStatus_response' + JSON.stringify(response));
      // if (response.status === 200) {
      //   callback();
      // }
  
      return response;
    } catch (e) {
      console.log('Action_changePOStatus_error', e.response);
      console.log('Action_changePOStatus_error', e);
      // alert(e.response);
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const loadingOn = dispatch => {
    dispatch({
      type: SHOWLODER,
    });
  };
  
  export const loadingOff = dispatch => {
    dispatch({
      type: HIDELODER,
    });
  };
  // export const getRegions = () => async (dispatch, getState) => {
  //   loadingOn(dispatch);
  //   try {
  //     const response = await axios.get(config().getRegionsUrl);
  //     console.log(
  //       'Action_getRegions_response' + JSON.stringify(response.data.data),
  //     );
  //     if (response.status === 200) {
  //       dispatch({
  //         type: GET_REGION,
  //         payload: response.data.data,
  //       });
  //     }
  //     return response.data.data;
  //   } catch (e) {
  //     console.log('Action_getRegions_error', e);
  //       
  //   } finally {
  //     loadingOff(dispatch);
  //   }
  // };
  // export const getCountry = id => async (dispatch, getState) => {
  //   loadingOn(dispatch);
  //   try {
  //     const response = await axios.get(config().getCountryByRegionUrl + id);
  //     console.log(
  //       'Action_getCountry_response' + JSON.stringify(response.data.data),
  //     );
  //     if (response.status === 200) {
  //       dispatch({
  //         type: GET_COUNTRY,
  //         payload: response.data.data,
  //       });
  //     }
  //     return response.data.data;
  //   } catch (e) {
  //     console.log('Action_getCountry_error', e);
  //       
  //   } finally {
  //     loadingOff(dispatch);
  //   }
  // };
  export const getWareHouses = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      const response = await axios.get(config().getWareHousesByCountryUrl + id);
      console.log(
        'Action_getWareHouse_response' + JSON.stringify(response.data.data),
      );
      if (response.status === 200) {
        dispatch({
          type: GET_WAREHOUSE,
          payload: response.data.data,
        });
      }
      return response.data.data;
    } catch (e) {
      console.log('Action_getWareHouse_error', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getWareHouseShipmentDetails = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_getWareHouseShipmentDetails_response' +
      //     (config().getProductDetailsByWarehouseId + id),
      // );
      const response = await axios.get(
        config().getProductDetailsByWarehouseId + id,
      );
      // console.log(
      //   'Action_getWareHouseShipmentDetails_response' + JSON.stringify(response)
      // );
      if (response.status === 200) {
        dispatch({
          type: GET_WAREHOUSE_SHiPMENT,
          payload: response.data.data,
        });
      } else {
        dispatch({
          type: GET_WAREHOUSE_SHiPMENT,
          payload: {},
        });
      }
      return response.data.data;
    } catch (e) {
      console.log('Action_getWareHouseShipmentDetails_error', e);
      dispatch({
        type: GET_WAREHOUSE_SHiPMENT,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const createShippingOrderUrl = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      const response = await axios.post(config().createShippingOrderUrl, data);
      callback();
      return response;
    } catch (e) {
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const getWarehouseByOrgId = id => async (dispatch, getState) => {
    //loadingOn(dispatch);
    try {
      console.log(
        'Action_getWareHouseShipmentDetails_response' +
        (config().getProductDetailsByWarehouseId + id),
      );
      const response = await axios.get(config().getWarehouseByOrgId + id);
      console.log(
        'Action_getWareHouseShipmentDetails_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        dispatch({
          type: GET_WAREHOUSE_BYORGID,
          payload: response.data.data,
        });
        return response.data.data;
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_getWareHouseShipmentDetails_error', e);
      return [];
    } finally {
      // loadingOff(dispatch);
    }
  };
  
  /**
   * After new Design API Integration
   */
  export const updateShipment = (data, id, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log('updateShipment start' + JSON.stringify(data));
      // console.log('updateShipment url ' + config().updateShipment);
      // let body = {
      //   id,
      //   shipmentUpdates: data,
      // };
      const configs = {
        headers: {
          'content-type': 'multipart/form-data',
        },
      };
      // console.log('updateShipment body ' + JSON.stringify(body));
      const response = await axios.post(config().updateShipment, data, configs);
      console.log('Action_updateShipment_response' + JSON.stringify(response));
      if (response.status === 200) {
        callback();
        // alert(response.data);
      }
  
      return response;
    } catch (e) {
      console.log(
        'Action_updateShipment_error' + JSON.stringify(e.response),
      );
      // alert(JSON.stringify(e.response));
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getProductsByCategory = id => async (dispatch, getState) => {
    //loadingOn(dispatch);
    try {
      console.log(
        'Action_getProductsByCategory_response' +
        (config().getProductsByCategory + id),
      );
      const response = await axios.get(config().getProductsByCategory + id);
      console.log(
        'Action_getProductsByCategory_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        return response.data.data;
      }
    } catch (e) {
      console.log('Action_getProductsByCategory_error', e);
      return [];
    } finally {
      // loadingOff(dispatch);
    }
  };
  export const getProductsByInventory = id => async (dispatch, getState) => {
    //loadingOn(dispatch);
    try {
      console.log(
        'Action_getProductsByInventory_response' +
        (config().getProductsByInventory + id),
      );
      const response = await axios.get(config().getProductsByInventory + id);
      console.log(
        'Action_getProductsByInventory_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        return response.data.data;
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_getProductsByInventory_error', e);
      return [];
    } finally {
      // loadingOff(dispatch);
    }
  };
  export const getTrackingDetails = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_getTrackingDetails_response' +
      //     (config().getProductDetailsByWarehouseId + id),
      // );
      const response = await axios.get(config().trackProduct + id);
      // console.log(
      //   'Action_getTrackingDetails_response' + JSON.stringify(response.data)
      // );
      if (response.status === 200) {
        dispatch({
          type: TRACKSHIPMENT,
          payload: response.data,
        });
      }
      return response.data;
    } catch (e) {
      console.log('Action_getWareHouseShipmentDetails_error', e);
      dispatch({
        type: TRACKSHIPMENT,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };
  // export const uploadImage = (data, id, callback = () => { }) => async (
  //   dispatch,
  //   getState,
  // ) => {
  //   loadingOn(dispatch);
  //   // try {
  //   //   console.log('uploadImage' + JSON.stringify(config().uploadImage + id));
  //   //   console.log('Action_uploadImage_data' + JSON.stringify(data));
  //   //   // const configs = {
  //   //   //   headers: {
  //   //   //     'content-type': 'multipart/form-data',
  //   //   //   },
  //   //   // };
  //   //   axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
  //   //   const response = await axios.post(config().uploadImage + id, data);
  
  //   //   console.log('Action_uploadImage_response' + JSON.stringify(response.data));
  //   //   // callback();
  //   //   // return response;
  //   // } catch (e) {
  //   //   console.log('Action_uploadImage_response' + JSON.stringify(e));
  //   //   return e;
  //   // } finally {
  //   //   loadingOff(dispatch);
  //   // }
  //   const token = await AsyncStorage.getItem('token');
  //   const uploadUrl1 = config().uploadImage + id;
  //   const headers = {
  //     'Content-Type': 'multipart/form-data',
  //     Authorization: 'Bearer ' + token,
  //   };
  //   // console.log('Action_uploadImage_url' + JSON.stringify(uploadUrl1));
  //   // console.log('Action_uploadImage_headers' + JSON.stringify(headers));
  //   // console.log('Action_uploadImage_body' + JSON.stringify(data));
  //   return fetch(uploadUrl1, {
  //     method: 'POST',
  //     headers: headers,
  //     body: data,
  //   })
  //     .then(response => response.json())
  //     .then(responseJson => {
  //       loadingOff(dispatch);
  //       console.log('Action_uploadImage_response' + JSON.stringify(responseJson));
  //     })
  //     .catch(error => {
  //       console.log('Action_uploadImage_response' + JSON.stringify(error));
  //       loadingOff(dispatch);
  //     });
  // };
  export const uploadImage = (data, id, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      let response;
      const configs = {
        headers: {
          'content-type': 'multipart/form-data',
        },
      };
      console.log(
        'Action_uuploadImagesUrl ' + JSON.stringify(`${config().uploadImage + id}`),
      );
      console.log('Action_upload_Image ', data);
      response = await axios.post(config().uploadImage + id, data, configs);
      // response = await axios.post(`${url}/usermanagement/api/auth/upload`, image, configs);
      // if (response.data.message === 'Updated') {
      //   alert('Image successfully updated');
  
      // }
      console.log('Action_upload_Image_response' + JSON.stringify(response));
      return response;
    } catch (e) {
      console.log('Action_upload_Image_error' + JSON.stringify(e.response));
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const fetchShipmentImage = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'Action_fetchShipmentImage url' + (config().fetchShipmentImage + id),
      );
      const response = await axios.get(config().fetchShipmentImage + id);
      console.log(
        'Action_fetchShipmentImage_response ' + JSON.stringify(response.data),
      );
      return response.data.data;
    } catch (e) {
      console.log('Action_fetchShipmentImage_error', e);
    } finally {
      loadingOff(dispatch);
    }
  };
  export const fetchProfileImage = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'Action_fetchProfileImage url' + (config().fetchProfileImage + id),
      );
      const response = await axios.get(config().fetchProfileImage + id);
      console.log(
        'Action_fetchProfileImage_response ' + JSON.stringify(response.data),
      );
      let returnValue = response.data && response.data.data || '';
      dispatch({
        type: PROFILE_PIC,
        payload: response.data && response.data.data || '',
      });
      return returnValue;
    } catch (e) {
      console.log('Action_fetchProfileImage_error', e);
    } finally {
      loadingOff(dispatch);
    }
  };
  export const AddOrganisations = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // const response = await axios.post(config().createShippingOrderUrl, data);
      // callback();
      // return response;
    } catch (e) {
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const getchainOfCustody = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_chainOfCustody_response' +
      //     (config().getProductDetailsByWarehouseId + id),
      // );
      const response = await axios.get(config().chainOfCustody + id);
      console.log(
        'Action_chainOfCustody_response' + JSON.stringify(response.data)
      );
      if (response.status === 200) {
        dispatch({
          type: CHAIN_SHIPMENT,
          payload: response.data.data,
        });
      }
      return response.data;
    } catch (e) {
      console.log('Action_chainOfCustody_error', e);
      dispatch({
        type: CHAIN_SHIPMENT,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getViewShipment = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log('Action_getViewShipment' + (config().getShipment + id));
      const response = await axios.get(config().getShipment + id);
      console.log(
        'Action_getViewShipment_response' + JSON.stringify(response.data),
      );
      if (response.status === 200) {
        dispatch({
          type: FETCH_SHIPMENT,
          payload: response.data.data,
        });
      }
      return response.data;
    } catch (e) {
      console.log('Action_getViewShipment_error', e);
      dispatch({
        type: FETCH_SHIPMENT,
        payload: {},
      });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const fetchShipmentsByQRCode = id => async (dispatch, getState) => {
    //loadingOn(dispatch);
    try {
      console.log(
        'Action_fetchShipmentsByQRCode_response' +
        (config().fetchShipmentsByQRCode + id),
      );
      const response = await axios.get(config().fetchShipmentsByQRCode + id);
      console.log(
        'Action_fetchShipmentsByQRCode_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        return (
          (response.data && response.data.data &&
            response.data.data.shipments &&
            response.data.data.shipments[0] &&
            response.data.data.shipments[0].products) ||
          (response.data && response.data.products) || (response.data && response.data.data && response.data.data.products) ||
          []
        );
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_fetchShipmentsByQRCode_error', e);
  
      return [];
    } finally {
      // loadingOff(dispatch);
    }
  };
  export const getOrganizationsByType = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'Action_getOrganizationsByType' + (config().getOrganizationsByType + id),
      );
      const response = await axios.get(config().getOrganizationsByType + id);
      console.log(
        'Action_getOrganizationsByType_response' + JSON.stringify(response.data),
      );
      console.log(
        'Action_getOrganizationsByType_response' + JSON.stringify(response.data.data &&
          response.data.data[0] &&
          response.data.data[0].organisationTypes),
      );
      dispatch({
        type: GET_ORGANIZATIONTYPE_LIST,
        payload:
          response.data.data &&
          response.data.data[0] &&
          response.data.data[0].organisationTypes,
      });
      return response.data;
    } catch (e) {
      console.log('Action_getViewShipment_error', e);
      // dispatch({
      //   type: GET_ORGANIZATIONTYPE_LIST,
      //   payload: [],
      // });
  
    } finally {
      loadingOff(dispatch);
    }
  };
  /**
   * Analytics
   */
  export const getShipmentAnalytics = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_inventory_getShipmentAnalyticssUrl ' +
      //     JSON.stringify(`${config().getShipmentAnalytics}`),
      // );
      const response = await axios.get(`${config().getShipmentAnalytics}`);
      // console.log(
      //   'Action_getShipmentAnalytics_response ' + JSON.stringify(response),
      // );
      // if (response.status === 200) {
      dispatch({
        type: SHIPMENT_ANALYTICS,
        payload: (response.data.data && response.data.data.shipment) || {},
      });
      // }
  
      return response;
    } catch (e) {
      console.log('Action_getInventoryAnalytics_error', e.response);
      //   
      dispatch({
        type: SHIPMENT_ANALYTICS,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getInventoryAnalytics = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_inventory_getInventoryAnalyticsUrl ' +
      //     JSON.stringify(`${config().getInventoryAnalytics}`),
      // );
      const response = await axios.get(`${config().getInventoryAnalytics}`);
      // console.log(
      //   'Action_getInventoryAnalytics_response ' + JSON.stringify(response),
      // );
      // if (response.status === 200) {
      dispatch({
        type: INVENTORY_ANALYTICS,
        payload: (response.data.data && response.data.data.inventory) || {},
      });
      // }
  
      return response;
    } catch (e) {
      console.log('Action_getInventoryAnalytics_error', e.response);
      //   
      dispatch({
        type: INVENTORY_ANALYTICS,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getOrderAnalytics = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_getOrderAnalytics ' +
      //     JSON.stringify(`${config().getOrderAnalytics}`),
      // );
      const response = await axios.get(`${config().getOrderAnalytics}`);
      // console.log(
      //   'Action_getOrderAnalytics_response ' + JSON.stringify(response),
      // );
      // if (response.status === 200) {
      dispatch({
        type: ORDER_ANALYTCS,
        payload: (response.data.data && response.data.data.order) || {},
      });
      // }
  
      return response;
    } catch (e) {
      console.log('Action_getOrderAnalytics_error' + JSON.stringify(e.response));
      //   
      dispatch({
        type: ORDER_ANALYTCS,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };
  /**
   * Warehouse
   */
  export const addWarehouse = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log('addWarehouse data ' + JSON.stringify(data));
      // console.log(
      //   'config().addWarehouse ' + JSON.stringify(config().addWarehouse),
      // );
      const response = await axios.post(config().addWarehouse, data);
      // console.log('addWarehouse response ' + JSON.stringify(response));
      if (response.status === 200) {
        callback();
      }
  
      return response;
    } catch (e) {
      console.log('Action_addWarehouse_error' + JSON.stringify(e.response));
      // console.log('Action_addWarehouse_error', e);
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  export const updateWarehouse = (id, data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log('updateWarehouse data ' + JSON.stringify(data));
      console.log(
        'config().updateWarehouse ' +
        JSON.stringify(config().updateWarehouse + id),
      );
      const response = await axios.post(config().updateWarehouse + id, data);
      console.log('updateWarehouse response ' + JSON.stringify(response));
      if (response.status === 200) {
        callback();
      }
  
      return response;
    } catch (e) {
      console.log('Action_updateWarehouse_error' + JSON.stringify(e.response));
      // console.log('Action_updateWarehouse_error', e);
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  /**
   * Last mile API
   */
  export const SentPatientOTP = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log('SentPatientOTP data ' + JSON.stringify(data));
      // const response = await axios.post(config().addWarehouse, data);
      // console.log('SentPatientOTP response ' + JSON.stringify(response));
      // if (response.status === 200) {
      //   callback();
      // }
  
      // return response;
      return null;
    } catch (e) {
      console.log('Action_SentPatientOTP_error', e.response);
      // console.log('Action_SentPatientOTP_error', e);
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  export const GetEOLInfoByProductId = id => async (dispatch, getState) => {
    //loadingOn(dispatch);
    try {
      console.log(
        'Action_GetEOLInfoByProductId_response' +
        (config().GetEOLInfoByProductId + id),
      );
      const response = await axios.get(config().GetEOLInfoByProductId + id);
      console.log(
        'Action_GetEOLInfoByProductId_response.data.data' +
        JSON.stringify(response),
      );
      if (response.status === 200) {
        return response.data;
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_GetEOLInfoByProductId_error', e);
      return [];
    } finally {
      // loadingOff(dispatch);
    }
  };
  export const AddNewEOL = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log('AddNewEOL data ' + JSON.stringify(data));
      const response = await axios.post(config().AddNewEOL, data);
      console.log('AddNewEOL response ' + JSON.stringify(response));
      if (response.status === 200) {
        callback();
      }
  
      return response;
    } catch (e) {
      console.log('Action_AddNewEOL_error', e.response);
      // console.log('Action_addWarehouse_error', e);
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  
  /**
   * Search bar functionality
   */
  export const getOrderDetails = (id, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_getOrderDetails Url' +
      //     JSON.stringify(`${config().getOrderDetails}`),
      // );
      const response = await axios.get(
        //config().fetchPurchaseOrderStatisticsUrl
        `${config().getOrderDetails}${id}&skip=0&limit=1`,
      );
      // console.log('Action_getOrderDetails_response' + JSON.stringify(response));
      return (
        (response.data && response.data.data && response.data.data.poDetails) ||
        []
      );
    } catch (e) {
      console.log('Action_fetch_Purchase_Orders_error', e.response);
      // console.log('Action_fetch_Purchase_Orders_error', e);
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
  /**
   * clickable Analytcs Inventory
   */
  export const getBatchNearExpiration = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('getBatchNearExpiration');
    try {
      const response = await axios.get(config().getBatchNearExpiration);
      console.log(
        'Action_getBatchNearExpiration_response' + JSON.stringify(response.data),
      );
      if (response.status === 200) {
        dispatch({
          type: INVENTORY_NEAREXPIRED,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log(
        'Action_getBatchNearExpiration_response' + JSON.stringify(e.response),
      );
      console.log('Action_getBatchNearExpiration_response', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getBatchExpired = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('getBatchNearExpiration');
    try {
      const response = await axios.get(config().getBatchExpired);
      console.log(
        'Action_getBatchExpiredn_response' + JSON.stringify(response.data),
      );
      if (response.status === 200) {
        dispatch({
          type: INVENTORY_EXPIRED,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_getBatchExpired_response' + JSON.stringify(e.response));
      console.log('Action_getBatchExpired_response', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  /**
   * Shipment Screen
   */
  
  export const fetchInboundShipments = (skip, limit, params) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    console.log('fetchInboundShipments');
    try {
      console.log(
        'Action_fetchInboundShipments_url' +
        `${config().fetchInboundShipments}?shipmentId=${params.shipmentId
        }&from=${params.fromId}&to=${params.toId}&dateFilter=${params.dateFilter
        }&status=${params.statusId}&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}`,
      );
      const response = await axios.get(
        `${config().fetchInboundShipments}?shipmentId=${params.shipmentId}&from=${params.fromId
        }&to=${params.toId}&dateFilter=${params.dateFilter}&status=${params.statusId
        }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}`,
      );
      console.log(
        'Action_fetchInboundShipments_response ' + JSON.stringify(response.data),
      );
      if (response.status === 200) {
        if (skip === 0) {
          dispatch({
            type: RESET_INBOUND_SHIPMENT,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.inboundShipments) ||
              [],
          });
          dispatch({
            type: INBOUND_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        } else {
          dispatch({
            type: INBOUND_SHIPMENT,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.inboundShipments) ||
              [],
          });
          dispatch({
            type: INBOUND_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        }
      }
      return response;
    } catch (e) {
      console.log('Action_fetchInboundShipments_error', e.response);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const fetchOutboundShipments = (skip, limit, params) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    // console.log('fetchOutboundShipments');
    try {
      console.log(
        'fetchOutboundShipments' +
        `${config().fetchOutboundShipments}?shipmentId=${params.shipmentId
        }&from=${params.fromId}&to=${params.toId}&dateFilter=${params.dateFilter
        }&status=${params.statusId}&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}`,
      );
      const response = await axios.get(
        `${config().fetchOutboundShipments}?shipmentId=${params.shipmentId
        }&from=${params.fromId}&to=${params.toId}&dateFilter=${params.dateFilter
        }&status=${params.statusId}&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}`,
      );
      console.log(
        'Action_ffetchOutboundShipments_response' + JSON.stringify(response.data),
      );
  
      if (response.status === 200) {
        if (skip === 0) {
          dispatch({
            type: RESET_OUTBOUND_SHIPMENT,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.outboundShipments) ||
              [],
          });
          dispatch({
            type: OUTBOUND_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        } else {
          dispatch({
            type: OUTBOUND_SHIPMENT,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.outboundShipments) ||
              [],
          });
          dispatch({
            type: OUTBOUND_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        }
      }
      return response;
    } catch (e) {
      console.log('Action_fetchInboundShipments_error', e.response);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const fetchSupplierAndReceiverList = () => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    console.log('fetchSupplierAndReceiverList');
    try {
      const response = await axios.get(config().fetchSupplierAndReceiverList);
      // console.log(
      //   'Action_fetchSupplierAndReceiverList_response' +
      //     JSON.stringify(response.data),
      // );
      if (response.status === 200) {
        dispatch({
          type: GET_ALLSUPPLIER_RECEIVER,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log(
        'Action_fetchSupplierAndReceiverList_response' +
        JSON.stringify(e.response),
      );
      // console.log('Action_fetchSupplierAndReceiverList_response', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const InboundFilter = data => async (dispatch, getState) => {
    dispatch({
      type: INBOUND_FILTER,
      payload: data,
    });
  };
  export const outboundFilter = data => async (dispatch, getState) => {
    dispatch({
      type: OUTBOUND_FILTER,
      payload: data,
    });
  };
  export const fetchShipmentIds = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('fetchShipmentIds');
    try {
      const response = await axios.get(config().fetchShipmentIds);
      // console.log(
      //   'fetchShipmentIds' +
      //     JSON.stringify(response.data),
      // );
      if (response.status === 200) {
        dispatch({
          type: GET_ALL_SHIPMENT_ID,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log(
        'Action_fetchShipmentIds_response' + JSON.stringify(e.response),
      );
      // console.log(
      //   'Action_fetchProductIdsCustomerLocationsOrganisations_response',
      //   e,
      // );
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getIotEnabledStatus = () => async dispatch => {
    loadingOn(dispatch);
    console.log('getIotEnabledStatus');
    try {
      const response = await axios.get(config().getIotEnabledStatus);
      console.log('Action_getIotEnabledStatus' + JSON.stringify(response.data));
      // if (response.status === 200) {
      //   dispatch({
      //     type: INVENTORY_PRODUCTCOUNT,
      //     payload: (response.data && response.data.message) || [],
      //   });
      // }
      dispatch({
        type: GET_IOT_STATUS,
        payload: (response.data && response.data.data) || [],
      });
      return response;
    } catch (e) {
      console.log(
        'Action_ggetIotEnabledStatus_response' + JSON.stringify(e.response),
      );
    } finally {
      loadingOff(dispatch);
    }
  };
  /**
   * Order Screen
   */
  
  export const fetchOutboundPurchaseOrders = (skip, limit, params) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    console.log('fetchInboundPurchaseOrders' + JSON.stringify(params));
    try {
      console.log(
        'fetchInboundPurchaseOrders' +
        `${config().fetchInboundPurchaseOrders}?from=${params.toId}&orderId=${params.orderId
        }&productName=${params.productID}&dateFilter=${params.dateFilter
        }&deliveryLocation=${params.deliveryLocationId
        }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}&poStatus=${params.statusId}`,
      );
      const response = await axios.get(
        `${config().fetchInboundPurchaseOrders}?from=${params.toId}&orderId=${params.orderId
        }&productName=${params.productID}&dateFilter=${params.dateFilter
        }&deliveryLocation=${params.deliveryLocation
        }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}&poStatus=${params.statusId}`,
      );
      console.log(
        'Action_fetchInboundPurchaseOrders_response ' +
        JSON.stringify(response.data),
      );
      if (response.status === 200) {
        if (skip === 0) {
          dispatch({
            type: RESET_PURCHASE_ORDER_INBOUNDPOS,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.inboundPOs) ||
              [],
          });
          dispatch({
            type: SENT_ORDER_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        } else {
          dispatch({
            type: PURCHASE_ORDER_INBOUNDPOS,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.inboundPOs) ||
              [],
          });
          dispatch({
            type: SENT_ORDER_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        }
      }
      return response;
    } catch (e) {
      console.log('Action_fetchInboundPurchaseOrders_error', e.response);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const fetchInboundPurchaseOrders = (skip, limit, params) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    console.log('fetchOutboundShipments' + JSON.stringify(params));
    try {
      console.log(
        'fetchOutboundShipments' +
        `${config().fetchOutboundPurchaseOrders}?to=${params.fromId}&orderId=${params.orderId
        }&productName=${params.productID}&dateFilter=${params.dateFilter
        }&deliveryLocation=${params.deliveryLocationId
        }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}&poStatus=${params.statusId}`,
      );
      const response = await axios.get(
        `${config().fetchOutboundPurchaseOrders}?to=${params.fromId}&orderId=${params.orderId
        }&productName=${params.productID}&dateFilter=${params.dateFilter
        }&deliveryLocation=${params.deliveryLocationId
        }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}&poStatus=${params.statusId}`,
      );
      console.log(
        'Action_ffetchOutboundShipments_response' + JSON.stringify(response.data),
      );
  
      if (response.status === 200) {
        if (skip === 0) {
          dispatch({
            type: RESET_PURCHASE_ORDER_OUTBOUNDPOS,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.outboundPOs) ||
              [],
          });
          dispatch({
            type: RECEIVE_ORDER_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        } else {
          dispatch({
            type: PURCHASE_ORDER_OUTBOUNDPOS,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.outboundPOs) ||
              [],
          });
          dispatch({
            type: RECEIVE_ORDER_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        }
      }
      return response;
    } catch (e) {
      console.log('Action_fetchInboundShipments_error', e.response);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const filterOrderSent = data => async (dispatch, getState) => {
    console.log('filterOrderSent' + JSON.stringify(data));
    dispatch({
      type: ORDER_SENT_FILTER,
      payload: data,
    });
  };
  export const filterOrderReceive = data => async (dispatch, getState) => {
    dispatch({
      type: ORDER_RECEIVE_FILTER,
      payload: data,
    });
  };
  export const fetchProductIdsCustomerLocationsOrganisations = () => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    console.log('fetchProductIdsCustomerLocationsOrganisations');
    try {
      const response = await axios.get(
        config().fetchProductIdsCustomerLocationsOrganisations,
      );
      // console.log(
      //   'Action_fetchProductIdsCustomerLocationsOrganisations_response' +
      //     JSON.stringify(response.data),
      // );
      if (response.status === 200) {
        dispatch({
          type: GET_PRODUCT_CUSTOMER_lOCATION,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log(
        'Action_fetchProductIdsCustomerLocationsOrganisations_response' +
        JSON.stringify(e.response),
      );
      // console.log(
      //   'Action_fetchProductIdsCustomerLocationsOrganisations_response',
      //   e,
      // );
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getOrderIds = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('getOrderIds');
    try {
      const response = await axios.get(config().getOrderIds);
      console.log('getOrderIds' + JSON.stringify(response));
      if (response.status === 200) {
        dispatch({
          type: GET_ALL_ORDER_ID,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_getOrderIds_response' + JSON.stringify(e.response));
      // console.log(
      //   'Action_fetchProductIdsCustomerLocationsOrganisations_response',
      //   e,
      // );
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getRegions = orgType => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log('getRegions' + config().getRegions + orgType);
      const response = await axios.get(config().getRegions + orgType);
      console.log(
        'Action_getRegions_response' + JSON.stringify(response.data.data),
      );
      if (response.status === 200) {
        dispatch({
          type: GET_REGION_BY_ORGTYPE,
          payload: response.data.data,
        });
        return response.data.data;
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_getRegions_error', e);
      return [];
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getCountryDetailsByRegion = (Region, orgType) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log(
        'getCountryDetailsByRegion' +
        `${config().getCountryDetailsByRegion}${Region}&orgType=${orgType}`,
      );
      const response = await axios.get(
        `${config().getCountryDetailsByRegion}${Region}&orgType=${orgType}`,
      );
      console.log(
        'Action_getCountryDetailsByRegion_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        dispatch({
          type: GET_COUNTRIES_BYORGTYPE,
          payload: response.data.data,
        });
        return response.data.data;
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_getCountryDetailsByRegion_error', e);
      return [];
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getOrganizations = (country, orgType) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log(
        'getOrganizations' +
        `${config().getOrganizations}${orgType}&country=${country}`,
      );
      const response = await axios.get(
        `${config().getOrganizations}${orgType}&country=${country}`,
      );
      console.log('Action_getOrganizations_response' + JSON.stringify(response));
      if (response.status === 200) {
        dispatch({
          type: GET_ORG_BYORGTYPE,
          payload: response.data.data,
        });
        return response.data.data;
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_getOrganizationsn_error', e);
      return [];
    } finally {
      loadingOff(dispatch);
    }
  };
  /**
   * Inventory Screen
   */
  export const filterInverntoryData = data => async (dispatch, getState) => {
    console.log('filterInverntoryData' + JSON.stringify(data));
    dispatch({
      type: INVENTORY_FILTER,
      payload: data,
    });
  };
  export const getAllEventsWithFilter = (skip, limit, params) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log(
        'Action_getAllEventsWithFilter_inventoriesUrl ' +
        JSON.stringify(
          `${config().getAllEventsWithFilter
          }?skip=${skip}&limit=${limit}&dateFilter=${params.dateFilter
          }&productName=${params.productID}&category=${params.type}&fromDate=${params.fromDate}&toDate=${params.toDate}&status=${params.statusId
          }`,
        ),
      );
      const response = await axios.get(
        `${config().getAllEventsWithFilter
        }?skip=${skip}&limit=${limit}&dateFilter=${params.dateFilter
        }&productName=${params.productID}&category=${params.type}&fromDate=${params.fromDate}&toDate=${params.toDate}&status=${params.statusId
        }`,
      );
      //test.vaccineledger.com/eventmanagement/api/event/getAllEventsWithFilter?skip=0&limit=10&dateFilter=&productName=&category=&status=
      // const response = await axios.get(`${config().inventoriesUrl}`);
      console.log(
        'Action_getAllEventsWithFilter_response ' + JSON.stringify(response),
      );
      if (response.status === 200) {
        dispatch({
          type: INVENTORY,
          payload: response.data.data.inventoryRecords,
        });
        dispatch({
          type: INVENTORY_COUNTS,
          payload: response.data.data.counts,
        });
      }
      // dispatch({
      //   type: INVENTORY_COUNTS,
      //   payload: [],
      //   // payload: response.data.counts,
      // });
      // dispatch({
      //   type: VACCINE_COUNTS,
      //   payload: [],
      //   // payload: response.data.dict,
      // });
  
      return response;
    } catch (e) {
      console.log('Action_getAllEventsWithFilter_error', e.response);
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getProductListCounts = () => async dispatch => {
    loadingOn(dispatch);
    console.log('getProductListCounts');
    try {
      const response = await axios.get(config().getProductListCounts);
      console.log(
        'Action_getProductListCounts_response' + JSON.stringify(response.data),
      );
      // if (response.status === 200) {
      //   dispatch({
      //     type: INVENTORY_PRODUCTCOUNT,
      //     payload: (response.data && response.data.message) || [],
      //   });
      // }
      dispatch({
        type: INVENTORY_PRODUCTCOUNT,
        payload: (response.data && response.data.message) || [],
      });
      return response;
    } catch (e) {
      console.log(
        'Action_getProductListCounts_response' + JSON.stringify(e.response),
      );
      dispatch({
        type: INVENTORY_PRODUCTCOUNT,
        payload: [],
      });
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const searchProductByType = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'searchProductByType url' +
        (config().searchProductByType + id + '&skip=0&limit=50'),
      );
      const response = await axios.get(
        config().searchProductByType + id + '&skip=0&limit=50',
      );
      console.log(
        'Action_searchProductByType_response' +
        JSON.stringify(response.data.data),
      );
      if (response.status === 200) {
        dispatch({
          type: GET_PRODUCT_BY_TYPE,
          payload: response.data && response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log(
        'Action_searchProductByType_error' + JSON.stringify(e.response),
      );
      // dispatch({
      //   type: GET_PRODUCT_BY_TYPE,
      //   payload: {},
      // });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const searchProductByName = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'searchProductByName url' +
        (config().searchProductByName + id + '&skip=0&limit=10'),
      );
      const response = await axios.get(
        config().searchProductByName + id + '&skip=0&limit=10',
      );
      console.log(
        'Action_searchProductByName_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      //   dispatch({
      //     type: GET_PRODUCT_BY_NAME,
      //     payload: response.data && response.data.data,
      //   });
      // } else {
      //   dispatch({
      //     type: GET_PRODUCT_BY_NAME,
      //     payload: [],
      //   });
      // }
      dispatch({
        type: GET_PRODUCT_BY_NAME,
        payload: response.data && response.data.data,
      });
      return response;
    } catch (e) {
      console.log(
        'Action_searchProductByName_error' + JSON.stringify(e.response),
      );
      dispatch({
        type: GET_PRODUCT_BY_NAME,
        payload: [],
      });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getBatchWarehouse = (id, Pid) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'getBatchWarehouse url' +
        (config().getBatchWarehouse + id + '&product_id=' + Pid),
      );
      const response = await axios.get(
        config().getBatchWarehouse + id + '&product_id=' + Pid,
      );
      console.log(
        'Action_sgetBatchWarehouse_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      dispatch({
        type: GET_PRODUCT_BY_INVEN,
        payload: response.data && response.data.data,
      });
      // }
      return response;
    } catch (e) {
      console.log(
        'Action_getBatchWarehouse_error' + JSON.stringify(e.response),
      );
      // dispatch({
      //   type: GET_PRODUCT_BY_NAME,
      //   payload: {},
      // });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getBatchWarehouseLatest = (id, Wid = '') => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'fetchBatchesOfInventory url' +
        (config().fetchBatchesOfInventory + id + '&wareId=' + Wid),
      );
      const response = await axios.get(
        config().fetchBatchesOfInventory + id + '&wareId=' + Wid,
      );
      // const response = await axios.get(
      //   config().fetchBatchesOfInventory + 'COM3' + '&wareId=' + 'WAR100511',
      // );
      console.log(
        'Action_fetchBatchesOfInventory_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      dispatch({
        type: GET_PRODUCT_BY_INVEN,
        payload: response.data && response.data.data,
      });
      // }
      return response;
    } catch (e) {
      console.log(
        'Action_fetchBatchesOfInventory_error' + JSON.stringify(e.response),
      );
      // dispatch({
      //   type: GET_PRODUCT_BY_NAME,
      //   payload: {},
      // });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  /**
   * Track && Trace Screen
   */
  export const trackJourney = (id, isTagged = false) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log('trackJourney url' + (config().trackJourney + id));
      const response = await axios.get(config().trackJourney + id);
      console.log('Action_trackJourney_response' + JSON.stringify(response));
      if (response.status === 200) {
        if (isTagged) {
          dispatch({
            type: TRACKTAGSHIPMENT,
            payload: response.data && response.data.data,
          });
        } else {
          dispatch({
            type: TRACKSHIPMENT,
            payload: response.data && response.data.data,
          });
        }
      }
      return response;
    } catch (e) {
      console.log('Action_trackJourney_error', e);
      // if (isTagged) {
      //   dispatch({
      //     type: TRACKTAGSHIPMENT,
      //     payload: {},
      //   });
      // } else {
      //   dispatch({
      //     type: TRACKSHIPMENT,
      //     payload: {},
      //   });
      // }
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  
  /**
   * User Management
   */
  export const checkUseremailId = (emailID, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_checkUseremailId' +
      //     JSON.stringify(config().checkUseremailId + emailID),
      // );
      // console.log('Action_checkUseremailIduserdata' + JSON.stringify(emailID));
      const response = await axios.get(config().checkUseremailId + emailID);
      // console.log('Action_checkUseremailId_response' + JSON.stringify(response));
      return response;
    } catch (e) {
      // console.log("Action_checkUseremailId_error", e);
      console.log('Action_checkUser error  ' + JSON.stringify(e.response));
  
      // const err = e.response.data.data[0];
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  export const checkUserphoneNumber = (
    phoneNumber,
    callback = () => { },
  ) => async (dispatch, getState) => {
    loadingOn(dispatch);
    // dispatch({
    //   type: REGISTER,
    //   payload: userdata,
    // });
    try {
      // console.log(
      //   'checkUserphoneNumber' +
      //     JSON.stringify(config().checkUserphoneNumber + phoneNumber),
      // );
      phoneNumber = phoneNumber.slice(1);
      // console.log('Action_checkUserphoneNumber userdata' + JSON.stringify(userdata));
      const response = await axios.get(
        config().checkUserphoneNumber + phoneNumber,
      );
      // console.log('Action_checkUserphoneNumber_response' + JSON.stringify(response));
      return response;
    } catch (e) {
      //   console.log("Action_checkUserphoneNumber_error", e.response);
      console.log('Action_checkUser error  ' + JSON.stringify(e.response));
  
      // const err = e.response.data.data[0];
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getAllRegion = () => async dispatch => {
    loadingOn(dispatch);
    console.log('getAllRegion');
    try {
      const response = await axios.get(config().getAllRegion);
      console.log('Action_getAllRegion_response' + JSON.stringify(response.data));
      dispatch({
        type: GET_REGION,
        payload: (response.data && response.data.data) || [],
      });
      return response;
    } catch (e) {
      console.log('Action_getAllRegion_response' + JSON.stringify(e.response));
      dispatch({
        type: GET_REGION,
        payload: [],
      });
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getAllCountries = id => async dispatch => {
    loadingOn(dispatch);
    console.log('getAllCountries');
    try {
      const response = await axios.get(config().getAllCountries + id);
      console.log(
        'Action_getAllCountries_response' + JSON.stringify(response.data),
      );
      dispatch({
        type: GET_COUNTRIES,
        payload: (response.data && response.data.data) || [],
      });
      return response;
    } catch (e) {
      console.log('Action_getAllCountries_response' + JSON.stringify(e.response));
      dispatch({
        type: GET_COUNTRIES,
        payload: [],
      });
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const getAllState = id => async dispatch => {
    loadingOn(dispatch);
    console.log('getAllState');
    try {
      const response = await axios.get(config().getAllState + id);
      console.log('Action_getAllState_response' + JSON.stringify(response.data));
      dispatch({
        type: GET_STATES,
        payload: (response.data && response.data.data) || [],
      });
      return response;
    } catch (e) {
      console.log('Action_getAllState_response' + JSON.stringify(e.response));
      dispatch({
        type: GET_STATES,
        payload: [],
      });
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const getAllCities = id => async dispatch => {
    loadingOn(dispatch);
    console.log('getAllCities');
    try {
      console.log('Action_getAllCities url' + (config().getAllCities + id));
  
      const response = await axios.get(config().getAllCities + id);
      console.log('Action_getAllCities_response' + JSON.stringify(response.data));
      dispatch({
        type: GET_CITIES,
        payload: (response.data && response.data.data) || [],
      });
      return response;
    } catch (e) {
      console.log('Action_getAllCities_response' + JSON.stringify(e.response));
      dispatch({
        type: GET_CITIES,
        payload: [],
      });
      //  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const switchLocation = (data, index, lang = 'EN', callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log('Action_user_switchLocation_' + config().switchLocation);
      console.log('Action_user_switchLocation_data' + JSON.stringify(data));
      // let response = {};
      const response = await axios.post(config().switchLocation, data);
      console.log(
        'Action_user_switchLocation_response' + JSON.stringify(response),
      );
      console.log(
        'Action_user_switchLocation_data.index' + JSON.stringify(index),
      );
      if (response.status === 200) {
        await AsyncStorage.setItem('token', response?.data?.data?.token);
        setAuthToken(response?.data?.data?.token, lang);
        dispatch({
          type: USER_TOKEN,
          payload: response?.data?.data?.token,
        });
        dispatch({
          type: ACTIVE_WAREHOUSE,
          payload: index,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_switchLocation error  ' + JSON.stringify(e));
      console.log(
        'Action_switchLocation e.response  ' + JSON.stringify(e.response),
      );
  
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  export const changeLang = (lng, token = '') => async dispatch => {
    console.log('changeLang ' + lng)
    if (token) {
      setAuthToken(token, lng);
    }
    dispatch({
      type: SELECTEDLANG,
      payload: lng,
    });
  };
  export const getShipmentFromList = data => async (dispatch, getState) => {
    dispatch({
      type: FETCH_SHIPMENT,
      payload: data,
    });
  };