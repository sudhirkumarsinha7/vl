import {
    
    GET_WAREHOUSE_BYORGID,
    
    PRODUCT_NAME,
    ORGANIZATIONS,
    SHIPING_ORDERIDS,
    
    CHAIN_SHIPMENT,
    FETCH_SHIPMENT,
    INBOUND_SHIPMENT,
    OUTBOUND_SHIPMENT,
    RESET_INBOUND_SHIPMENT,
    RESET_OUTBOUND_SHIPMENT,
   
    GET_ORGANIZATIONTYPE_LIST,
    GET_ALLSUPPLIER_RECEIVER,
    INBOUND_FILTER,
    OUTBOUND_FILTER,
    INBOUND_COUNT,
    OUTBOUND_COUNT,
    GET_ALL_SHIPMENT_ID,
    GET_IOT_STATUS,
  } from '../constant';
  
  import axios from 'axios';
  import AsyncStorage from '@react-native-async-storage/async-storage';
  import { config } from '../../Util/config';
  import setAuthToken from '../../Util/setAuthToken';
import {loadingOn,loadingOff} from './loader'

/**
   * Shipment Screen
   */
  
 export const fetchInboundShipments = (skip, limit, params) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    console.log('fetchInboundShipments');
    try {
      console.log(
        'Action_fetchInboundShipments_url' +
        `${config().fetchInboundShipments}?shipmentId=${params.shipmentId
        }&from=${params.fromId}&to=${params.toId}&dateFilter=${params.dateFilter
        }&status=${params.statusId}&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}`,
      );
      const response = await axios.get(
        `${config().fetchInboundShipments}?shipmentId=${params.shipmentId}&from=${params.fromId
        }&to=${params.toId}&dateFilter=${params.dateFilter}&status=${params.statusId
        }&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}`,
      );
      console.log(
        'Action_fetchInboundShipments_response ' + JSON.stringify(response.data),
      );
      if (response.status === 200) {
        if (skip === 0) {
          dispatch({
            type: RESET_INBOUND_SHIPMENT,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.inboundShipments) ||
              [],
          });
          dispatch({
            type: INBOUND_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        } else {
          dispatch({
            type: INBOUND_SHIPMENT,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.inboundShipments) ||
              [],
          });
          dispatch({
            type: INBOUND_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        }
      }
      return response;
    } catch (e) {
      console.log('Action_fetchInboundShipments_error', e.response);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const fetchOutboundShipments = (skip, limit, params) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    // console.log('fetchOutboundShipments');
    try {
      console.log(
        'fetchOutboundShipments' +
        `${config().fetchOutboundShipments}?shipmentId=${params.shipmentId
        }&from=${params.fromId}&to=${params.toId}&dateFilter=${params.dateFilter
        }&status=${params.statusId}&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}`,
      );
      const response = await axios.get(
        `${config().fetchOutboundShipments}?shipmentId=${params.shipmentId
        }&from=${params.fromId}&to=${params.toId}&dateFilter=${params.dateFilter
        }&status=${params.statusId}&fromDate=${params.fromDate}&toDate=${params.toDate}&skip=${skip}&limit=${limit}`,
      );
      console.log(
        'Action_ffetchOutboundShipments_response' + JSON.stringify(response.data),
      );
  
      if (response.status === 200) {
        if (skip === 0) {
          dispatch({
            type: RESET_OUTBOUND_SHIPMENT,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.outboundShipments) ||
              [],
          });
          dispatch({
            type: OUTBOUND_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        } else {
          dispatch({
            type: OUTBOUND_SHIPMENT,
            payload:
              (response.data &&
                response.data.data &&
                response.data.data.outboundShipments) ||
              [],
          });
          dispatch({
            type: OUTBOUND_COUNT,
            payload:
              (response.data && response.data.data && response.data.data.count) ||
              0,
          });
        }
      }
      return response;
    } catch (e) {
      console.log('Action_fetchInboundShipments_error', e.response);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  
  export const fetchSupplierAndReceiverList = () => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    console.log('fetchSupplierAndReceiverList');
    try {
      const response = await axios.get(config().fetchSupplierAndReceiverList);
      // console.log(
      //   'Action_fetchSupplierAndReceiverList_response' +
      //     JSON.stringify(response.data),
      // );
      if (response.status === 200) {
        dispatch({
          type: GET_ALLSUPPLIER_RECEIVER,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log(
        'Action_fetchSupplierAndReceiverList_response' +
        JSON.stringify(e.response),
      );
      // console.log('Action_fetchSupplierAndReceiverList_response', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const InboundFilter = data => async (dispatch, getState) => {
    dispatch({
      type: INBOUND_FILTER,
      payload: data,
    });
  };
  export const outboundFilter = data => async (dispatch, getState) => {
    dispatch({
      type: OUTBOUND_FILTER,
      payload: data,
    });
  };
  export const fetchShipmentIds = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('fetchShipmentIds');
    try {
      const response = await axios.get(config().fetchShipmentIds);
      // console.log(
      //   'fetchShipmentIds' +
      //     JSON.stringify(response.data),
      // );
      if (response.status === 200) {
        dispatch({
          type: GET_ALL_SHIPMENT_ID,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log(
        'Action_fetchShipmentIds_response' + JSON.stringify(e.response),
      );
      // console.log(
      //   'Action_fetchProductIdsCustomerLocationsOrganisations_response',
      //   e,
      // );
  
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getIotEnabledStatus = () => async dispatch => {
    loadingOn(dispatch);
    console.log('getIotEnabledStatus');
    try {
      const response = await axios.get(config().getIotEnabledStatus);
      console.log('Action_getIotEnabledStatus' + JSON.stringify(response.data));
      // if (response.status === 200) {
      //   dispatch({
      //     type: INVENTORY_PRODUCTCOUNT,
      //     payload: (response.data && response.data.message) || [],
      //   });
      // }
      dispatch({
        type: GET_IOT_STATUS,
        payload: (response.data && response.data.data) || [],
      });
      return response;
    } catch (e) {
      console.log(
        'Action_ggetIotEnabledStatus_response' + JSON.stringify(e.response),
      );
    } finally {
      loadingOff(dispatch);
    }
  };


  export const getProductNames = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      const response = await axios.get(config().getProducts);
      // console.log(
      //   'Action_getProductNames_response' + JSON.stringify(response.data.data),
      // );
      if (response.status === 200) {
        dispatch({
          type: PRODUCT_NAME,
          payload: response.data.data,
        });
      }
      return response.data.data;
    } catch (e) {
      console.log('Action_getProductNames_error', e.response);
      // console.log('Action_getProductNames_error', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };

  export const getchainOfCustody = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log(
      //   'Action_chainOfCustody_response' +
      //     (config().getProductDetailsByWarehouseId + id),
      // );
      const response = await axios.get(config().chainOfCustody + id);
      console.log(
        'Action_chainOfCustody_response' + JSON.stringify(response.data)
      );
      if (response.status === 200) {
        dispatch({
          type: CHAIN_SHIPMENT,
          payload: response.data.data,
        });
      }
      return response.data;
    } catch (e) {
      console.log('Action_chainOfCustody_error', e);
      dispatch({
        type: CHAIN_SHIPMENT,
        payload: {},
      });
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getViewShipment = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log('Action_getViewShipment' + (config().getShipment + id));
      const response = await axios.get(config().getShipment + id);
      console.log(
        'Action_getViewShipment_response' + JSON.stringify(response.data),
      );
      if (response.status === 200) {
        dispatch({
          type: FETCH_SHIPMENT,
          payload: response.data.data,
        });
      }
      return response.data;
    } catch (e) {
      console.log('Action_getViewShipment_error', e);
      dispatch({
        type: FETCH_SHIPMENT,
        payload: {},
      });
      return e.response;
      //   
    } finally {
      loadingOff(dispatch);
    }
  };
  export const fetchShipmentsByQRCode = id => async (dispatch, getState) => {
    //loadingOn(dispatch);
    try {
      console.log(
        'Action_fetchShipmentsByQRCode_response' +
        (config().fetchShipmentsByQRCode + id),
      );
      const response = await axios.get(config().fetchShipmentsByQRCode + id);
      console.log(
        'Action_fetchShipmentsByQRCode_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        return (
          (response.data && response.data.data &&
            response.data.data.shipments &&
            response.data.data.shipments[0] &&
            response.data.data.shipments[0].products) ||
          (response.data && response.data.products) || (response.data && response.data.data && response.data.data.products) ||
          []
        );
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_fetchShipmentsByQRCode_error', e);
  
      return [];
    } finally {
      // loadingOff(dispatch);
    }
  };
  export const getOrganizationsByType = id => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log(
        'Action_getOrganizationsByType' + (config().getOrganizationsByType + id),
      );
      const response = await axios.get(config().getOrganizationsByType + id);
      console.log(
        'Action_getOrganizationsByType_response' + JSON.stringify(response.data),
      );
      console.log(
        'Action_getOrganizationsByType_response' + JSON.stringify(response.data.data &&
          response.data.data[0] &&
          response.data.data[0].organisationTypes),
      );
      dispatch({
        type: GET_ORGANIZATIONTYPE_LIST,
        payload:
          response.data.data &&
          response.data.data[0] &&
          response.data.data[0].organisationTypes,
      });
      return response.data;
    } catch (e) {
      console.log('Action_getViewShipment_error', e);
      // dispatch({
      //   type: GET_ORGANIZATIONTYPE_LIST,
      //   payload: [],
      // });
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };
    
  export const fetchOrganizations = () => async (dispatch, getState) => {
    // loadingOn(dispatch);
    try {
      console.log(
        'Action_fetchOrganizations_config().getOrganisations' +
        JSON.stringify(config().getOrganisations),
      );
      const response = await axios.get(config().getOrganisations);
      console.log(
        'Action_fetchOrganizations_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        dispatch({
          type: ORGANIZATIONS,
          payload: response.data.data,
        });
      }
      return response.data.data;
    } catch (e) {
      console.log('Action_fetchOrganizations_error', e.response);
      // console.log('Action_fetchOrganizations_error', e);
  
    } finally {
      // loadingOff(dispatch);
    }
  };
  
  export const fetchShippingOrderIds = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('fetchShippingOrderIds');
    try {
      const response = await axios.get(`${config().getShippingOrderIds}`); //SHP9876
      // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
      console.log('Action_ fetchShippingOrderIds_response', response.data.data);
      if (response.status === 200) {
        dispatch({
          type: SHIPING_ORDERIDS,
          payload: response.data.data,
        });
      }
      return response;
    } catch (e) {
      console.log('Action_ fetchShippingOrderIds_error', e.response);
      console.log('Action_ fetchShippingOrderIds_error', e);
  
    } finally {
      loadingOff(dispatch);
    }
  };

  export const getWarehouseByOrgId = id => async (dispatch, getState) => {
    //loadingOn(dispatch);
    try {
      console.log(
        'Action_getWareHouseShipmentDetails_response' +
        (config().getProductDetailsByWarehouseId + id),
      );
      const response = await axios.get(config().getWarehouseByOrgId + id);
      console.log(
        'Action_getWareHouseShipmentDetails_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        dispatch({
          type: GET_WAREHOUSE_BYORGID,
          payload: response.data.data,
        });
        return response.data.data;
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_getWareHouseShipmentDetails_error', e);
      return [];
    } finally {
      // loadingOff(dispatch);
    }
  };

  export const getProductsByInventory = id => async (dispatch, getState) => {
    //loadingOn(dispatch);
    try {
      console.log(
        'Action_getProductsByInventory_response' +
        (config().getProductsByInventory + id),
      );
      const response = await axios.get(config().getProductsByInventory + id);
      console.log(
        'Action_getProductsByInventory_response' + JSON.stringify(response),
      );
      if (response.status === 200) {
        return response.data.data;
      } else {
        return [];
      }
    } catch (e) {
      console.log('Action_getProductsByInventory_error', e);
      return [];
    } finally {
      // loadingOff(dispatch);
    }
  };

  export const create_new_shipment = (
    data,
    callback = () => { },
  ) => async (dispatch, getState) => {
    loadingOn(dispatch);
    console.log('create_new_shipment' + JSON.stringify(data));
  
    try {
      const response = await axios.post(config().createShipmentUrl, data);
      console.log(
        'Action_create_new_shipment_response' + JSON.stringify(response),
      );
      // if (response.status === 200) {
      //   dispatch({
      //     type: ADD_SHIPMENT,
      //     payload: response.data.data,
      //   });
      // }
      return response;
    } catch (e) {
      // console.log(
      //   'Action_create_new_shipment_error' + JSON.stringify(e.response),
      // );
      // console.log('Action_create_new_shipment_error', e);
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };

  export const receiveShipment = (
    body,
    body2,
    address,
    callback = () => { },
  ) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
     
      // console.log('config().recieveShipment ' + JSON.stringify(config().recieveShipment));
  
      // console.log('config().recieveShipment body ' + JSON.stringify(body));
      const configs = {
        headers: {
          'content-type': 'multipart/form-data',
        },
      };
      const response = await axios.post(config().recieveShipment, body, configs);
      // console.log('Action_receiveShipment_response ' + JSON.stringify(response));
  
  
      return response;
    } catch (e) {
      // console.log("Action_addInventory_error", e)
      // console.log('Action_receiveShipment_error' + JSON.stringify(e.response));
      let err = e.response || e
      return err;
    } finally {
      loadingOff(dispatch);
    }
  };

  export const updateShipment = (data, id, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      // console.log('updateShipment start' + JSON.stringify(data));
      // console.log('updateShipment url ' + config().updateShipment);
      // let body = {
      //   id,
      //   shipmentUpdates: data,
      // };
      const configs = {
        headers: {
          'content-type': 'multipart/form-data',
        },
      };
      // console.log('updateShipment body ' + JSON.stringify(body));
      const response = await axios.post(config().updateShipment, data, configs);
      console.log('Action_updateShipment_response' + JSON.stringify(response));
      if (response.status === 200) {
        callback();
        // alert(response.data);
      }
  
      return response;
    } catch (e) {
      console.log(
        'Action_updateShipment_error' + JSON.stringify(e.response),
      );
      // alert(JSON.stringify(e.response));
      return e;
    } finally {
      loadingOff(dispatch);
    }
  };


  export const AddNewEOL = (data, callback = () => { }) => async (
    dispatch,
    getState,
  ) => {
    loadingOn(dispatch);
    try {
      console.log('AddNewEOL data ' + JSON.stringify(data));
      const response = await axios.post(config().AddNewEOL, data);
      console.log('AddNewEOL response ' + JSON.stringify(response));
      if (response.status === 200) {
        callback();
      }
  
      return response;
    } catch (e) {
      console.log('Action_AddNewEOL_error', e.response);
      // console.log('Action_addWarehouse_error', e);
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
  