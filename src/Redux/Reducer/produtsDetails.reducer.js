import {
  PRODUCT_NAME,
  MANUFACTURERS,
  ORGANIZATIONS,
  GET_REGION,
  GET_COUNTRIES,
  GET_STATES,
  GET_CITIES,
  GET_REGION_BY_ORGTYPE,
  GET_COUNTRIES_BYORGTYPE,
  GET_ORG_BYORGTYPE,
  GET_PRODUCT_BY_TYPE,
  GET_PRODUCT_BY_NAME,
  GET_PRODUCT_BY_INVEN,
} from '../constant';
export default function(
  state = {
    product_name: [],
    manufacturers: [],
    Organizations: [],
    regions: [],
    countries: [],
    states: [],
    cities: [],
    regionsByOrgType: [],
    countriesByOrgType: [],
    orgListByOrgType: [],
    productsByType: [],
    productsByName: [],
    productsByInv: [],
  },
  action,
) {
  switch (action.type) {
    case PRODUCT_NAME:
      return {
        ...state,
        product_name: action.payload,
        // product_name: action.payload.map(product => product.name),
      };

    case GET_PRODUCT_BY_TYPE:
      return {
        ...state,
        productsByType: action.payload,
      };
    case GET_PRODUCT_BY_NAME:
      return {
        ...state,
        productsByName: action.payload,
      };
    case GET_PRODUCT_BY_INVEN:
      return {
        ...state,
        productsByInv: action.payload,
      };
    case MANUFACTURERS:
      return {
        ...state,
        manufacturers: action.payload,
      };
    case GET_REGION:
      return {
        ...state,
        regions: action.payload,
      };
    case GET_COUNTRIES:
      return {
        ...state,
        countries: action.payload,
      };
    case GET_STATES:
      return {
        ...state,
        states: action.payload,
      };
    case GET_CITIES:
      return {
        ...state,
        cities: action.payload,
      };
    case ORGANIZATIONS:
      return {
        ...state,
        Organizations: action.payload,
      };
    case GET_REGION_BY_ORGTYPE:
      return {
        ...state,
        regionsByOrgType: action.payload,
      };
    case GET_COUNTRIES_BYORGTYPE:
      return {
        ...state,
        countriesByOrgType: action.payload,
      };
    case GET_ORG_BYORGTYPE:
      return {
        ...state,
        orgListByOrgType: action.payload,
      };
    default:
      return state;
  }
}
