import {
  SHIPMENT,
  UPDATE_SHIPMENT,
  ADD_SHIPMENT,
  SHIPMENT_COUNTS,
  SHIPING_ORDERIDS,
  SHIPING_ORDER_DETAILS,
  TRACKSHIPMENT,
  SHIPMENT_ANALYTICS,
  CHAIN_SHIPMENT,
  FETCH_SHIPMENT,
  INBOUND_SHIPMENT,
  OUTBOUND_SHIPMENT,
  RESET_SHIPMENT,
  RESET_INBOUND_SHIPMENT,
  RESET_OUTBOUND_SHIPMENT,
  GET_ALLSUPPLIER_RECEIVER,
  INBOUND_FILTER,
  OUTBOUND_FILTER,
  RESET_INBOUND_FILTER,
  RESET_OUTBOUND_FILTER,
  INBOUND_COUNT,
  OUTBOUND_COUNT,
  GET_ALL_SHIPMENT_ID,
  GET_IOT_STATUS,
  TRACKTAGSHIPMENT,
} from '../constant';
export default function (
  state = {
    shipmentdata: [],
    shipmentOrderids: [],
    inboundShipments: [],
    outboundShipments: [],
    shipmentcounts: {},
    shipmentOrderDetails: {},
    TrackingData: {},
    TrackTagShipmentData: {},
    shipmentAnalytics: {},
    shipmentChain: {},
    shipment: {},
    supplierAndReceivers: [],
    ShipmentIDList: [],
    IotEnabled: {},
    filterInbound: {
      shipmentId: '',
      from: '',
      to: '',
      dateFilter: '',
      status: '',
      statusId: '',
      fromId: '',
      toId: '',
      fromDate: '',
      toDate: '',
    },
    inboundCount: '',
    outboundCount: '',
    filterOutBound: {
      shipmentId: '',
      from: '',
      to: '',
      dateFilter: '',
      status: '',
      statusId: '',
      fromId: '',
      toId: '',
      fromDate: '',
      toDate: '',
    },
  },
  action,
) {
  switch (action.type) {
    case SHIPMENT:
      // console.log('action.payload', action.payload, [
      //   ...state.shipmentdata,
      //   ...action.payload,
      // ]);
      return {
        ...state,
        shipmentdata: [...state.shipmentdata, ...action.payload],
      };
    case GET_ALL_SHIPMENT_ID:
      return {
        ...state,
        ShipmentIDList: action.payload,
      };
    case GET_IOT_STATUS:
      return {
        ...state,
        IotEnabled: action.payload,
      };
    case INBOUND_SHIPMENT:
      return {
        ...state,
        inboundShipments: [...state.inboundShipments, ...action.payload],
      };
    case OUTBOUND_SHIPMENT:
      return {
        ...state,
        outboundShipments: [...state.outboundShipments, ...action.payload],
      };
    case RESET_SHIPMENT:
      return {
        ...state,
        shipmentdata: [...action.payload],
      };
    case RESET_INBOUND_SHIPMENT:
      return {
        ...state,
        inboundShipments: [...action.payload],
      };
    case RESET_OUTBOUND_SHIPMENT:
      return {
        ...state,
        outboundShipments: [...action.payload],
      };
    case GET_ALLSUPPLIER_RECEIVER:
      return {
        ...state,
        supplierAndReceivers: [...action.payload],
      };
    case INBOUND_FILTER:
      console.log('action.payload', action.payload);
      return {
        ...state,
        filterInbound: action.payload,
      };
    case OUTBOUND_FILTER:
      return {
        ...state,
        filterOutBound: action.payload,
      };
    case INBOUND_COUNT:
      return {
        ...state,
        inboundCount: action.payload,
      };
    case OUTBOUND_COUNT:
      return {
        ...state,
        outboundCount: action.payload,
      };
    case SHIPING_ORDERIDS:
      return {
        ...state,
        shipmentOrderids: action.payload,
      };
    case SHIPING_ORDER_DETAILS:
      return {
        ...state,
        shipmentOrderDetails: action.payload,
      };
    case UPDATE_SHIPMENT:
      // console.log('update', action.payload);
      // console.log('state', state.shipmentdata);
      // console.log('index',this.state.indexOf(shipmentdata => shipmentdata.shipmentId === action.payload.shipmentId));
      // const shipmentid = this.state.findIndex(shipmentdata => shipmentdata.shipmentId === action.payload.shipmentId)
      const p = state.shipmentdata.map(item => {
        if (item.shipmentId === action.payload.shipmentId) {
          return { ...item, ...action.payload };
        } else {
          return item;
        }
      });
      return {
        ...state,
        shipmentdata: p,
      };

    case ADD_SHIPMENT:
      let shipmentdata = state.shipmentdata;
      shipmentdata.push(action.payload);
      console.log('data', shipmentdata);
      return {
        ...state,
        shipmentdata: shipmentdata,
      };
    case SHIPMENT_ANALYTICS:
      return {
        ...state,
        shipmentAnalytics: action.payload,
      };
    case SHIPMENT_COUNTS:
      console.log('action.payloadshipmentcounts', action.payload);
      const totals = [];
      totals.push(action.payload.totalShipments.total);
      totals.push(action.payload.totalShipmentsSent.total);
      totals.push(action.payload.totalShipmentsReceived.total);
      totals.push(action.payload.currentShipments.total);
      console.log('su', totals);

      return {
        ...state,
        shipmentcounts: totals,
      };
    case TRACKSHIPMENT:
      return {
        ...state,
        TrackingData: action.payload,
      };
    case TRACKTAGSHIPMENT:
      return {
        ...state,
        TrackTagShipmentData: action.payload,
      };
    case CHAIN_SHIPMENT:
      return {
        ...state,
        shipmentChain: action.payload,
      };
    case FETCH_SHIPMENT:
      return {
        ...state,
        shipment: action.payload,
      };
    default:
      return state;
  }
}
