import {combineReducers} from 'redux';
// import register from './register.reducer';
import auth from './auth';
// import login from './login.reducer';
import userinfo from './userinfo.reducer';
// import loder from './loder.reducer';
import inventory from './inventory.reducer';
import shipment from './shipment.reducer';
// import alluser from './alluser.reducer';
import purchaseorder from './purchaseorder.reducer';
import productdetail from './produtsDetails.reducer';
// import shipping from './shippingOrder.reducer';
import alert from './alert';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {
  persistReducer,
} from 'redux-persist'
const persistConfigUser = {
  key: 'userinfo',
  storage: AsyncStorage,
  whitelist: ['userpermission','user','userLang','userProfPic','OrgTypeList'],
}
const persistConfigProduct = {
  key: 'productdetail',
  storage: AsyncStorage,
  whitelist: ['product_name','Organizations','orgListByOrgType'],
}

   
const persistConfigInventory = {
  key: 'inventory',
  storage: AsyncStorage,
  whitelist: ['filterInventory','inventoryExpired','inventoryNearExpired','wareHouseDetails','wareHouseDetailsbyorgid','wareHouseList','countryList','inventorydata','inventoryout','inventorycounts','inventorycounts','inventoryAnalytics','vaccinecounts','productCountList','regions'],
}
    
const persistConfigShipment = {
  key: 'shipment',
  storage: AsyncStorage,
  whitelist: ['shipmentdata','shipmentOrderids','inboundShipments','outboundShipments','shipmentcounts','shipmentOrderDetails','TrackingData','TrackTagShipmentData','shipmentAnalytics','shipmentChain','shipment','supplierAndReceivers','ShipmentIDList','IotEnabled','filterInbound','inboundCount','outboundCount','filterOutBound'],
}

const persistConfigPurchaseorder = {
  key: 'purchaseorder',
  storage: AsyncStorage,
  whitelist: ['purchaseorderids','OrderIDList','purchaseorder','outboundPOs','inboundPOs','orderSentFilter','orderSentFilter','sentOrderCount','receiveOrderCount','prodCustomerLoc']  ,
}
const persistConfigAlert = {
  key: 'alert',
  storage: AsyncStorage,
  whitelist: ['alerts','alertCount','transaction','transactionCount','userPermision'],
}

// const persistedReducer = persistReducer(persistConfig, app)
export default combineReducers({
  auth,
  // login,
  userinfo:persistReducer(persistConfigUser, userinfo),
  inventory:persistReducer(persistConfigInventory,inventory),
  // loder,
  shipment:persistReducer(persistConfigShipment,shipment),
  // alluser,
  purchaseorder: persistReducer(persistConfigPurchaseorder,purchaseorder),
  productdetail:persistReducer(persistConfigProduct,productdetail),
  // shipping,
  alert:persistReducer(persistConfigAlert,alert),
});
