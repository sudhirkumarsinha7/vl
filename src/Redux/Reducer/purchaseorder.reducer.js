import {
  PURCHASE_ORDER,
  PURCHASE_ORDER_IDS,
  ADD_PURCHASE_ORDER,
  ADD_PURCHASE_ORDER_IDS,
  ORDER_ANALYTCS,
  PURCHASE_ORDER_INBOUNDPOS,
  PURCHASE_ORDER_OUTBOUNDPOS,
  RESET_PURCHASE_ORDER_INBOUNDPOS,
  RESET_PURCHASE_ORDER_OUTBOUNDPOS,
  ORDER_SENT_FILTER,
  ORDER_RECEIVE_FILTER,
  GET_PRODUCT_CUSTOMER_lOCATION,
  SENT_ORDER_COUNT,
  RECEIVE_ORDER_COUNT,
  GET_ALL_ORDER_ID,
} from '../constant';
export default function (
  state = {
    purchaseorderids: [],
    OrderIDList: [],
    purchaseorder: {},
    outboundPOs: [],
    inboundPOs: [],
    orderSentFilter: {
      from: '',
      orderId: '',
      productName: '',
      dateFilter: '',
      deliveryLocation: '',
      fromId: '',
      toId: '',
      productID: '',
      deliveryLocationId: '',
      statusId: '',
      status: '',
      fromDate: '',
      toDate: '',
    },
    orderReceiveFilter: {
      fromId: '',
      toId: '',
      from: '',
      to: '',
      orderId: '',
      productName: '',
      dateFilter: '',
      deliveryLocation: '',
      deliveryLocationId: '',
      productID: '',
      statusId: '',
      status: '',
      fromDate: '',
      toDate: '',
    },
    sentOrderCount: '',
    receiveOrderCount: '',
    prodCustomerLoc: {},
  },
  action,
) {
  switch (action.type) {
    case PURCHASE_ORDER_IDS:
      return {
        ...state,
        purchaseorderids: action.payload.data,
      };
    case GET_ALL_ORDER_ID:
      return {
        ...state,
        OrderIDList: action.payload,
      };
    case PURCHASE_ORDER_OUTBOUNDPOS:
      return {
        ...state,
        outboundPOs: [...state.outboundPOs, ...action.payload],
      };
    case PURCHASE_ORDER_INBOUNDPOS:
      return {
        ...state,
        inboundPOs: [...state.inboundPOs, ...action.payload],
      };
    case RESET_PURCHASE_ORDER_OUTBOUNDPOS:
      return {
        ...state,
        outboundPOs: action.payload,
      };
    case RESET_PURCHASE_ORDER_INBOUNDPOS:
      return {
        ...state,
        inboundPOs: action.payload,
      };
    case ORDER_SENT_FILTER:
      return {
        ...state,
        orderSentFilter: action.payload,
      };
    case ORDER_RECEIVE_FILTER:
      return {
        ...state,
        orderReceiveFilter: action.payload,
      };
    case SENT_ORDER_COUNT:
      return {
        ...state,
        sentOrderCount: action.payload,
      };
    case RECEIVE_ORDER_COUNT:
      return {
        ...state,
        receiveOrderCount: action.payload,
      };
    case GET_PRODUCT_CUSTOMER_lOCATION:
      return {
        ...state,
        prodCustomerLoc: action.payload,
      };
    case PURCHASE_ORDER:
      return {
        ...state,
        //  purchaseorder: final,
        purchaseorder: action.payload,
        //action.payload.data.map(purchaseorderdata => JSON.parse(purchaseorderdata.data)),
      };

    case ADD_PURCHASE_ORDER_IDS:
      let purchaseorderidsdata = state.purchaseorderids;
      purchaseorderidsdata.push(action.payload);
      console.log('data', purchaseorderidsdata);
      return {
        ...state,
        purchaseorderids: purchaseorderidsdata,
      };

    // case ADD_PURCHASE_ORDER:
    //   let purchaseorderdata = state.purchaseorder;
    //   purchaseorderdata.push({...action.payload});
    //   console.log('data', purchaseorderdata);
    //   return {
    //     ...state,
    //     purchaseorder: purchaseorderdata,
    //   };
    case ORDER_ANALYTCS:
      return {
        ...state,
        ordersAnalytics: action.payload,
      };
    default:
      return state;
  }
}
