import {SHIPPING_ORDER} from '../constant';
export default function(state = {shipping: []}, action) {
  switch (action.type) {
    case SHIPPING_ORDER:
      return {
        ...state,
        shipping: action.payload,
      };
    default:
      return state;
  }
}
