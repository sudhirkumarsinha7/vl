import {
  GET_ALERTS,
  RESET_GET_ALERTS,
  GET_MORE_ALERTS,
  GET_ALERTS_COUNT,
  GET_TRANSACTIONS,
  GET_MORE_TRANSACTIONS,
  GET_TRANSACTIONS_COUNT,
  RESET_GET_TRANSACTIONS,
  GET_USERPERMISION,
  GET_ALERT_PAGE_COUNT,
  GET_TRANSACTION_PAGE_COUNT,
  GET_NEW_ALERT,
  GET_NEW_TRANSACTION,
} from '../constant';
export default function (
  state = {
    alerts: [],
    alertCount: 0,
    transaction: [],
    transactionCount: 0,
    userPermision: {},
    alertPage: 1,
    transactionPage: 1,
    newAlert: 0,
    newTransaction: 0,
  },
  action,
) {
  switch (action.type) {
    case GET_ALERTS:
      return {
        ...state,
        alerts: action.payload,
      };
    case GET_USERPERMISION:
      return {
        ...state,
        userPermision: action.payload,
      };
    case GET_MORE_ALERTS:
      return {
        ...state,
        alerts: [...state.alerts, ...action.payload],
      };
    case GET_ALERT_PAGE_COUNT:
      return {
        ...state,
        alertPage: action.payload,
      };
    case GET_TRANSACTIONS:
      return {
        ...state,
        transaction: action.payload,
      };
    case GET_MORE_TRANSACTIONS:
      return {
        ...state,
        transaction: [...state.transaction, ...action.payload],
      };
    case GET_TRANSACTION_PAGE_COUNT:
      return {
        ...state,
        transactionPage: action.payload,
      };
    case GET_ALERTS_COUNT:
      return {
        ...state,
        alertCount: action.payload,
      };
    case GET_TRANSACTIONS_COUNT:
      return {
        ...state,
        transactionCount: action.payload,
      };
    case GET_NEW_ALERT:
      return {
        ...state,
        newAlert: action.payload,
      };
    case GET_NEW_TRANSACTION:
      return {
        ...state,
        newTransaction: action.payload,
      };
    default:
      return state;
  }
}
