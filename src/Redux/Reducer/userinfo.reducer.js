import {
  USERINFO,
  USERUPDATE,
  GET_ORGANIZATIONTYPE_LIST,
  USER_TOKEN,
  ACTIVE_WAREHOUSE,
  USERPERMISSION,
  SELECTEDLANG,
  PROFILE_PIC
} from '../constant';
export const userInitalState ={
  user: {}, OrgTypeList: [], token: '', activeWareHouse: 0, userpermission: {}, userLang: 'en', userProfPic: ''
}
export default function (
  state = { userInitalState },
  action,
) {
  switch (action.type) {
    case SELECTEDLANG:
      return {
        ...state,
        userLang: action.payload,
      };
    case PROFILE_PIC:
      return {
        ...state,
        userProfPic: action.payload,
      };
    case USERINFO:
      return {
        ...state,
        user: action.payload,
      };
    case USERPERMISSION:
      return {
        ...state,
        userpermission: action.payload,
      };
    case USER_TOKEN:
      return {
        ...state,
        token: action.payload,
      };
    case GET_ORGANIZATIONTYPE_LIST:
      return {
        ...state,
        OrgTypeList: action.payload,
      };
    case USERUPDATE:
      return {
        ...state,
        user: {
          ...state.user,
          ...action.payload,
        },
      };
    case ACTIVE_WAREHOUSE:
      console.log('ACTIVE_WAREHOUSE action ' + JSON.stringify(action));
      return {
        ...state,
        activeWareHouse: action.payload,
      };
    default:
      return state;
  }
}
