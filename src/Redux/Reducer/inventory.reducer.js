import {
  INVENTORY,
  GET_OUTOF_STOCK,
  INVENTORY_COUNTS,
  VACCINE_COUNTS,
  GET_REGION,
  GET_COUNTRY,
  GET_WAREHOUSE,
  GET_WAREHOUSE_SHiPMENT,
  GET_WAREHOUSE_BYORGID,
  INVENTORY_ANALYTICS,
  INVENTORY_NEAREXPIRED,
  INVENTORY_EXPIRED,
  INVENTORY_FILTER,
  INVENTORY_PRODUCTCOUNT,
} from '../constant';
export default function (
  state = {
    inventorydata: [],
    inventoryout: [],
    inventorycounts: {},
    inventoryAnalytics: {},
    vaccinecounts: {},
    productCountList: {},
    regions: [],
    countryList: [],
    wareHouseList: [],
    wareHouseDetails: {},
    wareHouseDetailsbyorgid: [],
    inventoryNearExpired: [],
    inventoryExpired: [],
    filterInventory: {
      date: '',
      manufacturer: '',
      dateFilter: '',
      status: '',
      statusId: '',
      productName: '',
      type: '',
      productID: '',
      fromDate: '',
      toDate: '',
    },
  },
  action,
) {
  switch (action.type) {
    case INVENTORY:
      return {
        ...state,
        inventorydata: action.payload,
      };
    case GET_OUTOF_STOCK:
      return {
        ...state,
        inventoryout: action.payload,
      };
    case INVENTORY_NEAREXPIRED:
      return {
        ...state,
        inventoryNearExpired: action.payload,
      };
    case INVENTORY_EXPIRED:
      return {
        ...state,
        inventoryExpired: action.payload,
      };
    case INVENTORY_COUNTS:
      return {
        ...state,
        inventorycounts: action.payload,
      };
    case INVENTORY_FILTER:
      return {
        ...state,
        filterInventory: action.payload,
      };
    case INVENTORY_ANALYTICS:
      return {
        ...state,
        inventoryAnalytics: action.payload,
      };
    case INVENTORY_PRODUCTCOUNT:
      return {
        ...state,
        productCountList: action.payload,
      };
    case VACCINE_COUNTS:
      return {
        ...state,
        vaccinecounts: action.payload,
      };
    case GET_REGION:
      return {
        ...state,
        regions: action.payload,
      };
    case GET_COUNTRY:
      return {
        ...state,
        countryList: action.payload,
      };
    case GET_WAREHOUSE:
      return {
        ...state,
        wareHouseList: action.payload,
      };
    case GET_WAREHOUSE_SHiPMENT:
      return {
        ...state,
        wareHouseDetails: action.payload,
      };
    case GET_WAREHOUSE_BYORGID:
      return {
        ...state,
        wareHouseDetailsbyorgid: action.payload,
      };
    default:
      return state;
  }
}
