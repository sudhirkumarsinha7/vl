import {REGISTER,LOGIN,OTP,} from '../constant';
export default function(state = {userdata: '',otpdata: '',logindata: '',userdata:''}, action) {
  switch (action.type) {
    case REGISTER:
      return {
        ...state,
        userdata: action.payload,
      };
      case LOGIN:
        return {
          ...state,
          logindata: action.payload,
        };
    case OTP:
      return {
        ...state,
        otpdata: action.payload,
      };
    default:
      return state;
  }
}
