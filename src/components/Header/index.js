import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  BackHandler,
  View,
  Image,
  Text,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import LinearGradient from 'react-native-linear-gradient';
import HamburgerIcon from '../HamburgerIcon';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import { CommonStyle, Colors } from '../../Style';
import { Badge } from '@rneui/base'
import { connect } from 'react-redux';
import {
  getTransactionNotifications,
  getAlertNotifications,
} from '../../Redux/Action/alert';
const IPHONE12_H = 844;
const IPHONE12_Max = 926;
const IPHONE12_Mini = 780;
const { height: D_HEIGHT, width: D_WIDTH } = Dimensions.get('window');
import {
  fetchProfileImage
} from '../../Redux/Action';
const Header =(props)=> {
 
  const getImageDetails = async (item, id) => {
    const { user = {} } = props;
    await props.fetchProfileImage(user.photoId)
  };
 
    const {

      newAlert = 0,
      newTransaction = 0,
    } = props;
    const notificationCount = newAlert + newTransaction
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={['#0093E9', '#36C2CF']}
        style={{
          height: scale(150),
          borderBottomLeftRadius: 24,
          borderBottomRightRadius: 24,
        }}>
        <View
          style={{
            marginTop: Platform.OS==='ios'? verticalScale(40): verticalScale(20),
            marginLeft: scale(16),
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <HamburgerIcon getDetails={() => getImageDetails()} navigation={props.navigation}/>
          <Text
            style={{
              marginLeft: scale(23),
              fontSize: scale(24),
              
              color: '#FFFFFF',
              width: props.scan ? '60%' : '80%',
              fontWeight: 'bold',
            }}>
            {props.name}
          </Text>
          {props.scan ? (
            <TouchableOpacity
              // style={{backgroundColor:'white'}}
              onPress={props.onPress}>
              <Image
                style={{
                  width: scale(20),
                  height: scale(20),
                  alignSelf: 'center',
                }}
                source={require('../../assets/qr-code.png')}
                resizeMode="contain"
              />
              {/* <Icon2 name="scan1" color="white" size={25} /> */}
            </TouchableOpacity>
          ) : null}
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Notifications')}
            style={{
              justifyContent: 'flex-end',
              right: 20,
              position: 'absolute',
              flexDirection: 'row'
            }}>
            <Image
              style={{ width: scale(20), height: scale(20) }}
              source={require('../../assets/Bell.png')}
              resizeMode="contain"
            />
            {notificationCount ? <Badge value={notificationCount + ''} status="error" containerStyle={{ marginTop: verticalScale(-8), marginLeft: verticalScale(-10), }} /> : null}
          </TouchableOpacity>
        </View>
      </LinearGradient>
    );
}
function mapStateToProps(state) {
  return {
    newAlert: state.alert.newAlert,
    newTransaction: state.alert.newTransaction,
    user: state.userinfo.user,
  };
}

export default connect(
  mapStateToProps,
  {
    getTransactionNotifications,
    getAlertNotifications,
    fetchProfileImage
  },
)(Header);

export const HeaderWithBack =(props)=> {

    const { isfilter = false } = props;
    return (
      <View
        style={{
          height: scale(150),
          borderBottomLeftRadius: 24,
          borderBottomRightRadius: 24,
        }}>
        <View
          style={{
            marginTop: verticalScale(20),
            marginLeft: scale(16),
            flexDirection: 'row',
            alignItems: 'center',

          }}>
          {/* <HamburgerIcon /> */}
          <TouchableOpacity
            onPress={() => props.navigation.goBack()}
            style={{ width: 50 }}>
            <Icon name="angle-left" size={50} color={Colors.black70} />
          </TouchableOpacity>
          <Text
            style={{
              // marginLeft: scale(23),
              fontSize: scale(24),
              
              width: props.scan ? '60%' : isfilter ? '53%' : '80%',
              fontWeight: 'bold',
              color: Colors.black0,
            }}>
            {props.name}
          </Text>
          {props.scan ? (
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={props.onPress}>
              <Image
                style={{
                  width: scale(20),
                  height: scale(20),
                  alignSelf: 'center',
                }}
                source={require('../../assets/qr-code.png')}
                resizeMode="contain"
              />
              {/* <Icon2 name="scan1" color="white" size={25} /> */}
            </TouchableOpacity>
          ) : null}
          {isfilter ? (
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignContent: 'center',
              }}
              onPress={props.onPress}>
              <Text
                style={{
                  fontSize: scale(14),
                  
                  color: Colors.black0,
                  alignSelf: 'center'
                }}>
                {props.clearFilter}
              </Text>
              {/* <Icon1
                  name="delete-circle-outline"
                  size={35}
                /> */}
              <Image
                style={{ width: scale(13.5), height: scale(35) }}
                source={require('../../assets/deletegrey.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          ) : (
            <View
              style={{
                justifyContent: 'flex-end',
                right: 16,
                position: 'absolute',
              }}>
              <Image
                style={{ width: scale(20), height: scale(20) }}
                source={require('../../assets/Bell.png')}
                resizeMode="contain"
              />

            </View>
          )}
        </View>
      </View>
    );
}
export const HeaderWithBackShipmentScreen =(props)=> {


    let { isfilter = false, t } = props;
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={['#0093E9', '#36C2CF']}
        style={{
          height: scale(150),
          borderBottomLeftRadius: 24,
          borderBottomRightRadius: 24,
        }}>
        <View
          style={{
            marginTop: verticalScale(20),
            marginLeft: scale(16),
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Shipment')}
            style={{ width: 50 }}>
            <Icon name="angle-left" color="white" size={50} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: scale(24),
              
              color: '#FFFFFF',
              width: props.noOfAlert ? '50%' : isfilter ? '45%' : '80%',
              fontWeight: 'bold',
            }}>
            {props.name}
          </Text>
          {props.noOfAlert ? (
            <View
              style={{ flexDirection: 'row', backgroundColor: Colors.red23, borderRadius: 5, padding: 3, elevation: 4 }}
            >
              <Text style={{ paddingLeft: 5, paddingRight: 5, color: Colors.whiteFF }}>{props.noOfAlert + ' ' + t('new')}</Text>
            </View>
          ) : null}
        </View>
      </LinearGradient>
    );
}

export const HeaderWithBackBlue =(props)=> {

    let { isfilter = false } = props;
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={['#0093E9', '#36C2CF']}
        style={{
          // width: Dimensions.get('window').width,
          height: scale(150),
          borderBottomLeftRadius: 24,
          borderBottomRightRadius: 24,
        }}>
        <View
          style={{
            marginTop: verticalScale(20),
            marginLeft: scale(16),
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          {/* <HamburgerIcon /> */}
          <TouchableOpacity
            onPress={() => props.navigation.goBack()}
            style={{ width: 30 }}>
            <Icon name="angle-left" color="white" size={50} />
          </TouchableOpacity>
          <Text
            style={{
              // marginLeft: scale(23),
              fontSize: scale(22),
              
              color: '#FFFFFF',
              width: props.scan ? '60%' : isfilter ? '45%' : '80%',
              fontWeight: 'bold',
            }}>
            {props.name}
          </Text>
          {props.scan ? (
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={props.onPress}>
              <Image
                style={{
                  width: scale(20),
                  height: scale(20),
                  alignSelf: 'center',
                }}
                source={require('../../assets/qr-code.png')}
                resizeMode="contain"
              />
              {/* <Icon2 name="scan1" color="white" size={25} /> */}
            </TouchableOpacity>
          ) : null}
          {isfilter ? (
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={props.onPress}>
              <Text
                style={{
                  fontSize: scale(16),
                  
                  color: '#FFFFFF',
                }}>
                {props.clearFilter}
              </Text>
              <Icon1 name="delete-circle-outline" color="#FFFFFF" size={35} />
            </TouchableOpacity>
          ) : (
            <View
              style={{
                justifyContent: 'flex-end',
                right: 16,
                position: 'absolute',
              }}>
              {/* <Image
                style={{width: scale(13.5), height: scale(15)}}
                source={require('../../assets/Bell.png')}
                resizeMode="contain"
              /> */}
            </View>
          )}
        </View>
      </LinearGradient>
    );
}
