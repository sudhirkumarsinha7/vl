import React from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  Animated,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import { Colors } from '../../Style';
import { useTranslation } from 'react-i18next';

const Section_Select =(props)=> {

  const { t, i18n } = useTranslation();
    return (
      <TouchableOpacity
        onPress={props.onClick}
        activeOpacity={0.8}
        style={{
          flexDirection: 'row',
          marginTop: verticalScale(15),
          marginLeft: scale(10),
        }}>
        {props.button.selected ? (
          <View
            style={{
              borderBottomWidth: 4,
              borderColor: props.selectedTextColor || Colors.blueE9,
              borderRadius: 5,
            }}
          >
            <Text
            numberOfLines={1} 
            ellipsizeMode='head'
              style={{
                color: props.selectedTextColor || Colors.blueE9,
                fontSize: props.fontSize || scale(13),
                marginBottom:5
              }}>
              {t(props.button.label)}
            </Text>
          </View>
        ) : (
          <View
            style={{
            }}>
            <Text
              style={{
                color: '#707070',
                fontSize: props.fontSize || scale(13),
                
              }}>
              {t(props.button.label)}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
}
export default Section_Select;
