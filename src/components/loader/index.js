import React, { useEffect,useState } from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import {global} from '../../Util/config'

const Loader = (props) => {
  const { t, i18n } = useTranslation();
const {Processing=false,loder} = props
  return (
      <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}>
             {loder||Processing?<ActivityIndicator color="#0000ff" size="large" />:null}
            </View>
  );

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

function mapStateToProps(state) {
  return {
    loder: state.loder,
  };
}

export default connect(mapStateToProps, {  })(Loader);
