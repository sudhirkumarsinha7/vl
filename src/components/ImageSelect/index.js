import React, { useState, useEffect } from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  ActivityIndicator,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  StatusBar,
  Switch,
  Alert,
  Linking,
} from 'react-native';import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import Permissions from 'react-native-permissions';
import { useTranslation } from 'react-i18next';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import Modal from 'react-native-modal';
import { Colors } from '../../Style';

export const CustomImagePicker =(props)=>{
    const [isModalVisible,setModalVisible] = useState(false);
    const { t, i18n } = useTranslation();
    const {filePath={}}= props
    useEffect(() => {
      async function fetchData() {
        requestCameraPermission();
      }
      fetchData();
    }, []);
   
    const requestCameraPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "App Camera Permission",
            message:"App needs access to your camera ",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("Camera permission given");
        } else {
          console.log("Camera permission denied");
        }
      } catch (err) {
        console.warn(err);
      }
    };
    const chooseFileFromCamera = () => {
        var options = {
          title: 'Select Image',
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        launchCamera(options, response => {
          console.log('User response' + JSON.stringify(response));
          if (response.didCancel) {
            console.log('User canceled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            // alert(response.customButton);
          } else if (response.assets) {
            props.setImage(response.assets[0])
          }
          setModalVisible(false)

        });
      };
    const chooseFileFromGallary = () => {
        var options = {
          title: 'Select Image',
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        launchImageLibrary(options, response => {
          console.log('User response' + JSON.stringify(response));
          if (response.didCancel) {
            console.log('User canceled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            // alert(response.customButton);
          } else if (response.assets) {
            props.setImage(response.assets[0])
          }
          setModalVisible(false)

        });
      };
    return <View
    style={{
      margin: verticalScale(10),
      borderRadius: 10,
      padding: 5,
    }}>
   
    <SelectOptionPopUp
         TakePic={chooseFileFromCamera}
         selectPic={chooseFileFromGallary}
          isModalVisible={isModalVisible}
          cancel={()=>setModalVisible(false)}
        />
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
      }}>
      <Text
        style={{
          color: '#707070',
          fontWeight: 'bold',
        }}>
        {t('Upload Image')}
      </Text>
      <View
        style={{
          flex: 3,
          marginRight: 1,
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}>
        <TouchableOpacity
          style={{
            width: scale(105),
            height: scale(35),
            borderRadius: 10,
            backgroundColor: '#58b1da',
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#58b1da',
            borderWidth: 1,
            flexDirection: 'row',
          }}
          //disabled={true}
          onPress={() => setModalVisible(true)}>
          <Icon name="cloud-upload" color="white" size={20} />
          <Text
            style={{
              fontSize: scale(16),
              color: '#FFFFFF',
              fontWeight: 'bold',
            }}>
            {'   ' + t('Upload')}
          </Text>
        </TouchableOpacity>
      </View>
    </View>

    <View
      style={{
        borderStyle: 'dashed',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'gray',
        padding: 5,
        backgroundColor: '#FFFFFF',
        marginTop: 10,
      }}>
      {filePath && filePath.uri ? (
        <Image
          style={{ height: 240, borderStyle: 'dotted' }}
          resizeMode="cover"
          source={{
            uri: filePath.uri,
          }}
        />
      ) : (
        <TouchableOpacity
          onPress={() => setModalVisible(true)}
          style={{
            marginTop: 10,
            alignItems: 'center',
            justifyContent: 'center',
            width: 328,
            height: 240,
          }}>

          <Icon name="cloud-upload" color="#58b1da" size={50} />
          <View
            style={{
              borderRadius: 10,
              backgroundColor: '#58b1da',
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#58b1da',
              borderWidth: 1,
              padding: scale(10)
            }}>
            <Text
              style={{
                fontSize: scale(16),
                color: '#FFFFFF',
                fontWeight: 'bold',
              }}>
              {t('Browse Files')}
            </Text>
          </View>
        </TouchableOpacity>
      )}
    </View>
  </View>
}


export const SelectOptionPopUp = props => {
    return (
      <Modal
        isVisible={props.isModalVisible}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 20,
        }}
        animationInTiming={1000}
        animationOutTiming={1000}
        backdropTransitionInTiming={1000}
        backdropTransitionOutTiming={1000}>
        <View
          style={{
            width: scale(250),
            height: scale(200),
            backgroundColor: '#ffffff',
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
             <View style={{ width: '20%',marginLeft:'80%', marginTop: verticalScale(-50), }}>
        <TouchableOpacity
          style={{
            borderRadius: 10,
            height: scale(30),
          }}
          onPress={props.cancel}>
          <Text
            style={{
              textAlign: 'right',
              marginRight: 20,
              fontSize:30,
              color:Colors.red23
            }}>
            {'X'}
          </Text>
        </TouchableOpacity>
      </View>
         <TouchableOpacity onPress={props.TakePic} style={{margin:10,backgroundColor:Colors.blue1EA,padding:10,borderRadius:10}}>
            <Text style={{color:Colors.whiteFF,fontSize:16,fontWeight:'bold'}}>Take Photo</Text>
         </TouchableOpacity>
         <TouchableOpacity  onPress={props.selectPic}style={{margin:10,backgroundColor:Colors.blue1EA,padding:10,borderRadius:10}}>
            <Text  style={{color:Colors.whiteFF,fontSize:16,fontWeight:'bold'}}>Upload Photo</Text>
         </TouchableOpacity>
        </View>
      </Modal>
    );
  };
  