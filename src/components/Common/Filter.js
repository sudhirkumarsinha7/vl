import React, { Component, useState } from 'react';
import {
  Image,
  View,
  Text,
  TouchableOpacity,

  FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import { CommonStyle, Colors } from '../../Style';
import DatePicker from 'react-native-neat-date-picker'
import { numberWithCommas, formatDateDDMMYYYY } from '../../Util/utils';

import _ from 'lodash';
import Modal from 'react-native-modal';
import { useTranslation } from 'react-i18next';

let FilterData = [
  { name: 'Today', id: 'today' },
  { name: 'This Week', id: 'week' },
  { name: 'This Month', id: 'month' },
  { name: 'Last 3 Months', id: 'threeMonth' },
  { name: 'Last 6 Months', id: 'sixMonth' },
  { name: 'This Year', id: 'year' },
];
export const Filter =(props)=> {
  const { t, i18n } = useTranslation();

  const APIcall=(item)=> {
    const { isFilter, updateState, filterAPI } = props;
    updateState({ dateFilter: item.id });
    updateState({ isFilter: !isFilter });
    filterAPI();
  }
  const eachData=(item) =>{

    return (
      <View
        style={{
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 1,
          margin: 15,
          padding: 10,
          borderColor: '#0093E9',
        }}>
        <TouchableOpacity onPress={() => APIcall(item)}>
          <Text
            style={{
              color: '#0093E9',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            {t(item.name)}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }


    return (
      <Modal
        isVisible={isFilter}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 20,
        }}
        animationInTiming={1000}
        animationOutTiming={1000}
        backdropTransitionInTiming={1000}
        backdropTransitionOutTiming={1000}>
        <View
          style={{
            width: scale(250),
            backgroundColor: '#ffffff',
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <FlatList
            onEndReachedThreshold={0.7}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={FilterData}
            renderItem={({ item }) => eachData(item)}
          />
        </View>
      </Modal>
    );
}

export const FilterByDate =(props)=> {
  const { t, i18n } = useTranslation();

  const eachData=(item) =>{
    const { dateFilter, onChangeValue,} = props;

    return (
      <View
        style={{
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 1,
          margin: 7,
          padding: 10,
          borderColor: Colors.blueF0,
          // width: '30%',
          backgroundColor: item.id === dateFilter ? Colors.blueF0 : Colors.whiteFF,
        }}>
        <TouchableOpacity onPress={() =>  onChangeValue(item.id)}>
          <Text
            style={{
              color:
                item.id === dateFilter ? Colors.whiteFF : Colors.blueE9,
              fontSize: 14,
              fontWeight: 'bold',
            }}>
            {t(item.name)}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

    return (
      <View
        style={{
          // backgroundColor:  Colors.whiteFF,
          borderRadius: 10,
          margin: 11,
          padding: 5,
        }}>
        <Text
          style={{
            fontSize: scale(16),
            color: Colors.black0,
          }}>

          {t('Filter By Date')}
        </Text>
        <View
          style={{
            marginTop: 15,
          }}>
          <FlatList
            onEndReachedThreshold={0.7}
            columnWrapperStyle={{ justifyContent: 'space-evenly' }}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={FilterData}
            numColumns={2}
            renderItem={({ item }) => eachData(item)}
          />
        </View>
      </View>
    );
}
export const CustomDateRange =(props)=> {
  const { t, i18n } = useTranslation();
  const [showDateRange,setShowDateRange]=useState(false)



    const {
      displayName,
      image = '',
      fromDate,
      toDate,
      onChangeValue
    } = props;
    return (
      <TouchableOpacity
        style={{
          marginBottom: 8,
          padding: scale(8),
          borderRadius:scale(4),
          marginBottom:scale(10),
          backgroundColor: '#FFFFFF',
        }}
        onPress={() => setShowDateRange(true)}
      >
        <View
          style={{
            flexDirection: 'row',

          }}>
          {image ? (
            <Image
              style={{ width: scale(15), height: scale(15), margin: 5 }}
              source={image}
              resizeMode="contain"
            />
          ) : null}
          <View
            style={{
              ...CommonStyle.dropdownleftView,
            }}>
            <Text
              style={{                
              }}>
              {t(displayName)}
            </Text>
          </View>
        </View>
        <View style={{ marginLeft: 10, flexDirection: 'row', marginTop: scale(10) }}>

          <Text style={{ color: fromDate ? Colors.blueE9 : Colors.gray9, width: '50%', fontWeight: '300' }}>{formatDateDDMMYYYY(fromDate) || 'DD/MM/YYYY'}</Text>
          <Text style={{ color: toDate ? Colors.blueE9 : Colors.gray9, width: '50%', fontWeight: '300' }}>{formatDateDDMMYYYY(toDate) || 'DD/MM/YYYY'}</Text>

        </View>
        <DatePicker
          isVisible={showDateRange}
          mode={'range'}
          onCancel={() => { setShowDateRange(false) }}
          onConfirm={(val) => {
            onChangeValue(val)
            setShowDateRange(false)
          }}
        />
      </TouchableOpacity>
    );
  
}
