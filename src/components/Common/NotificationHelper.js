import React, { Component } from 'react';
import {
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import { CustomTextView, } from './common';
import { useTranslation } from 'react-i18next';

import{timeAgo} from '../../Util/utils'
import { CommonStyle, Colors } from '../../Style';

export function getAlertIcon(type) {
  if (type === 'SHIPMENT' || type === 'SHIPMENT_TRACKING') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15) }}
        source={require('../../assets/truck.png')}
        resizeMode="contain"
      />
    );
  } else if (type === 'ORDER') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15) }}
        source={require('../../assets/order2.png')}
        resizeMode="contain"
      />
    );
  } else if (
    type === 'INVENTORY' ||
    type === 'PRODUCTTYPE' ||
    type === 'PRODUCT'
  ) {
    return (
      <Image
        style={{ width: scale(13), height: scale(15) }}
        source={require('../../assets/Inventory1.png')}
        resizeMode="contain"
      />
    );
  } else {
    return (
      <Image
        style={{ width: scale(13), height: scale(15) }}
        source={require('../../assets/user.png')}
        resizeMode="contain"
      />
    );
  }
}

export const NotificationHelper =(props)=> {

    const { t, i18n } = useTranslation();

    let { item } = props;
    return (
      <TouchableOpacity
        onPress={() => props.navigateNextScreen(item)}
        style={{
          marginLeft: scale(10),
          marginRight: scale(10),
          // backgroundColor: item.isRead ? Colors.whiteFF : '#D3D3D3',
          backgroundColor: Colors.whiteFF,
          borderBottomWidth: 1,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 10,
          borderBottomColor: Colors.grayA8,
          padding: 5,
          paddingBottom: scale(15),
          paddingTop: scale(15)
        }}>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
          }}>
          <View
            style={{
              flex: 0.1,
            }}>
            <View
              style={{
                width: scale(30),
                height: scale(30),
                borderRadius: 400,
                backgroundColor: Colors.whiteFF,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              opacity={1}>
              {getAlertIcon(item.eventType)}
            </View>
          </View>
          <View
            style={{
              flex: 0.7,
              marginLeft: 5,
            }}>
            <Text
              style={{
                fontSize: scale(14),
                // color: Colors.blue87,
                color: item.isRead ? Colors.black70 : Colors.blue87,

              }}>
              {item.message}
            </Text>
            <Text
              style={{
                fontSize: scale(12),
                color: Colors.black70,
              }}>
              {item.title}
            </Text>
          </View>
          <View
            style={{
              flex: 0.3,
              justifyContent: 'center',
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              onPress={() => props.navigateNextScreen(item)}
              style={{ marginBottom: scale(20) }}>
              <Image
                style={{
                  width: scale(20),
                  height: scale(15),
                  justifyContent: 'center',
                  alignItems: 'center',
                  // ,
                  tintColor: item.isRead ? Colors.black70 : Colors.blueE9,

                }}
                source={require('../../assets/rightArrow.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <Text>{timeAgo(item.createdAt)}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  
}

export const NotificationPermissionHelper =(props)=> {


const { t, i18n } = useTranslation();

    let { item} = props;
    return (
      <View
        style={{
          marginLeft: scale(10),
          marginRight: scale(10),
          backgroundColor: Colors.whiteFF,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 10,
          padding: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
          }}>
          <View
            style={{
              flex: 0.1,
            }}>
            <View
              style={{
                width: scale(30),
                height: scale(30),
                borderRadius: 400,
                backgroundColor: Colors.whiteFF,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              opacity={1}>
              {getAlertIcon(item.eventType)}
            </View>
          </View>
          <View
            style={{
              flex: 0.7,
              marginLeft: 5,
            }}>
            <Text
              style={{
                fontSize: scale(14),
                color: Colors.blue87,
              }}>
              {item.message}
            </Text>
            <Text
              style={{
                fontSize: scale(12),
                color: Colors.black70,
              }}>
              {item.title}
            </Text>
          </View>
          <View
            style={{
              flex: 0.3,
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
            }}>
            <Text>{timeAgo(item.createdAt)}</Text>
          </View>
        </View>
      </View>
    );
  
}
export const ShipmentDetails =(props)=> {

const { t, i18n } = useTranslation();

    let { item, userPermision = {} } = props;
    let { shipment = {}, shipmentId } = userPermision;

    return (
      <View
        style={{
          margin: verticalScale(10),
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
          margin: 15,
        }}>
        <Text
          style={{
            fontSize: scale(12),
            fontWeight: 'bold',
            margin: 10,
          }}>
          {t('Shipment Details')}
        </Text>
        <CustomTextView
          leftText={t('Shipment ID')}
          rightText={shipmentId || ''}
        />
        <CustomTextView
          leftText={t('Delivery Location')}
          rightText={shipment.deliveryLocation || ''}
        />
        <CustomTextView
          leftText={t('Organisation Name')}
          rightText={shipment.organisationName || ''}
        />
      </View>
    );
  
}
export const ScanningLocationDetails =(props)=> {

const { t, i18n } = useTranslation();

    let { item, userPermision = {}} = props;
    let { from = {} } = userPermision;
    // console.log('props.userPermision from' + JSON.stringify(from))
    return (
      <View
        style={{
          margin: verticalScale(10),
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
          margin: 15,
        }}>
        <Text
          style={{
            fontSize: scale(12),
            fontWeight: 'bold',
            margin: 10,
          }}>
          {t('Scanning Location Details')}
        </Text>
        <CustomTextView leftText={t('User Name')} rightText={from.name + ' (' + from.role + ')' || ''} />
        <CustomTextView
          leftText={t('Delivery Location')}
          rightText={from.deliveryLocation || ''}
        />
        <CustomTextView
          leftText={t('Organisation Name')}
          rightText={from.organisationName || ''}
        />
      </View>
    );
  
}