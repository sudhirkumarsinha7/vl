import React, { useCallback, useState } from 'react';
import {
  Image,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StatusBar,
  ScrollView,
  Platform,
  PermissionsAndroid,
  Alert,
  Dimensions,
  StyleSheet,
  FlatList,
  Button,
  SafeAreaView,
  Linking,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import { CommonStyle, Colors, DeviceHeight, DeviceWidth } from '../../Style';
import { formatDateMMYYYY, formatDateDDMMYYYY } from '../../Util/utils'
import { Dropdown as ElDropdown } from 'react-native-element-dropdown';
import { TextInput as PaperInput } from 'react-native-paper';

import { useTranslation } from 'react-i18next';
import { fonts, Input } from '@rneui/base';
import MonthPicker from 'react-native-month-year-picker';
import moment from 'moment';
import DatePicker from 'react-native-date-picker'

import { ToastShow } from '../Toast'
export const DropdownIcon = () => {
  return (
    <Image
      style={{ width: scale(10.2), height: scale(14.09) }}
      source={require('../../assets/updown.png')}
      resizeMode="contain"
    />
  );
};
export const DropdownIcon1 = () => {
  return (
    <Icon name="chevron-down" size={20} color={Colors.grayA8} />

  );
};
export function getProductIcon(type, color = null) {
  if (type === 'Product ID') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15), marginRight: scale(10) }}
        source={require('../../assets/product/id.png')}
        resizeMode="contain"
      />
    );
  } else if (type === 'Product Name') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15), marginRight: scale(10) }}
        source={require('../../assets/product/product.png')}
        resizeMode="contain"
      />
    );
  } else if (type === 'Batch Number') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15), marginRight: scale(10) }}
        source={require('../../assets/product/batch.png')}
        resizeMode="contain"
      />
    );
  } else if (type === 'Label Code') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15), marginRight: scale(10) }}
        source={require('../../assets/product/label.png')}
        resizeMode="contain"
      />
    );
  } else if (type === 'Manufacturer') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15), marginRight: scale(10) }}
        source={require('../../assets/product/manufacturer.png')}
        resizeMode="contain"
      />
    );
  } else if (type === 'Shipment') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15), marginRight: scale(10) }}
        source={require('../../assets/product/shipment.png')}
        resizeMode="contain"

      />
    );
  } else if (type === 'Date') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15), marginRight: scale(10), tintColor: color }}
        source={require('../../assets/product/date.png')}
        resizeMode="contain"
      />
    );
  } else if (type === 'Truck') {
    return (
      <Image
        style={{ width: scale(13), height: scale(15), marginRight: scale(10), tintColor: color }}
        source={require('../../assets/truck.png')}
        resizeMode="contain"
      />
    );
  } else {
    return (
      <Image
        style={{ width: scale(13), height: scale(15), marginRight: scale(10) }}
        source={require('../../assets/product/shipment.png')}
        resizeMode="contain"
      />
    );
  }
}
export const SearchableDropdown = (props) => {
  const { t, i18n } = useTranslation();

  const {
    eachRow,
    state,
    dropDowndata = [],
    label = '',
    image = '',
    mapKey = '',
    search = false,
    mandatory = false,
  } = props;
  // console.log('SearchableDropdown eachRow' + JSON.stringify(eachRow));
  // console.log('SearchableDropdown dropDowndata' + JSON.stringify(dropDowndata));
  // console.log('state[eachRow.stateName]' + state[eachRow.stateName]);

  return (
    <View style={{ marginTop: 10 }}>
      <Text style={{ fontWeight: '400', marginLeft: scale(7), }}>{t(eachRow.displayName)}</Text>
      <View
        style={{
          borderWidth: 1,
          // borderColor: '#f2f2f2',
          borderRadius: 15,
          marginTop: 10
        }}>
        <ElDropdown
          data={dropDowndata.map(eachElement => {
            return {
              value: eachElement[mapKey] || eachElement,
              label: eachElement[label] || eachElement[mapKey] || eachElement,
              eachItem: eachElement
            };
          })}
          search
          maxHeight={300}
          labelField="label"
          valueField="value"
          searchPlaceholder="Search..."
          style={styles.dropdown}
          placeholderStyle={{
            color: '#8c8c8c',
            fontWeight: 'normal',
            fontSize: 14
          }}
          selectedTextStyle={styles.selectedTextStyle}
          inputSearchStyle={styles.inputSearchStyle}
          boderStyle={styles.borderStyle1}
          value={state[eachRow.stateName] || null}
          placeholder={t(eachRow.displayName)}
          activeColor={Colors.blueE9}
          renderLeftIcon={() => (
            <Image
              style={{ width: scale(15), height: scale(15), margin: 5 }}
              source={image}
              resizeMode="contain"
            />
          )}
          onChange={val => props.onChangeValue(val)}
          searchable={search}
        />
      </View>
      {mandatory && !state[eachRow.stateName] ? <Text
        style={{
          color: Colors.red,
          marginLeft: scale(5),
          marginBottom: scale(5),
        }}>
        {t('Required')}
      </Text> : null}
    </View>
  );
}
export const CustomDropdown = (props) => {
  const { t, i18n } = useTranslation();
  const {
    dropDowndata = [],
    label = '',
    image = '',
    mapKey = '',
    search = true,
    mandatory = false,
    displayName,
    stateName,
    index
  } = props;

  return (
    <View style={{
      marginLeft: 1,
      marginRight: 3,
      marginTop: 5,
    }}>
      <View
        style={{
          borderBottomWidth: 1,
          // borderColor: '#f2f2f2',
          borderRadius: 10,
          marginLeft: 5,
          marginRight: 5
        }}>
        {stateName ? <Text style={{ fontWeight: '400', marginLeft: scale(5),color: Colors.blueE9 }}>{displayName}</Text> : null}
        <ElDropdown
          data={dropDowndata.map(eachElement => {
            return {
              value: eachElement[mapKey] || eachElement,
              label: eachElement[label] || eachElement[mapKey] || eachElement,
              eachItem: eachElement,

            };
          })}
          search
          maxHeight={300}
          labelField="label"
          valueField="value"
          searchPlaceholder="Search..."
          style={styles.dropdown}
          placeholderStyle={{
            color: '#8c8c8c',
            fontWeight: 'normal',
            fontSize: 14
          }}
          selectedTextStyle={styles.selectedTextStyle}
          inputSearchStyle={styles.inputSearchStyle}
          boderStyle={styles.borderStyle1}
          value={stateName || null}
          // placeholder={t("Select") + ' ' + t(displayName)}
          placeholder={t(displayName)}

          activeColor={Colors.blueE9}
          onChange={val => props.onChangeValue(val)}

          searchable={search}
        />

      </View>
      {mandatory && !stateName ? <Text
        style={{
          color: Colors.red,
          margin: 5,
          marginLeft: 12,
          fontSize: 12,
        }}>
        {t('Required')}
      </Text> : null}
    </View>
  );
}
export const CustomDropDownLocation = (props) => {

  const { t, i18n } = useTranslation();
  const {
    dropDowndata = [],
    label = '',
    image = '',
    mapKey = '',
    search = false,
    mandatory = false,
    displayName,
    stateName,
    index
  } = props;

  return (
    <View style={{
      marginLeft: 1,
      marginRight: 3,
      marginTop: 5,
    }}>
      <View
        style={{
          borderBottomWidth: 1,
          // borderColor: '#f2f2f2',
          borderRadius: 10,
          marginLeft: 5,
          marginRight: 5
        }}>
        {stateName ? <Text style={{ fontWeight: '400', marginLeft: scale(8) }}>{displayName}</Text> : null}
        <ElDropdown
          data={dropDowndata.map(eachElement => {
            return {
              value: eachElement[mapKey] || eachElement,
              label: eachElement.warehouseAddress
                ? eachElement?.title +
                ' / ' +
                eachElement?.warehouseAddress?.firstLine +
                ',' +
                eachElement?.warehouseAddress?.city
                : eachElement?.title + ' /  ' + eachElement?.postalAddress,
              eachItem: eachElement
            };
          })}
          search
          maxHeight={300}
          labelField="label"
          valueField="value"
          searchPlaceholder="Search..."
          style={styles.dropdown}
          placeholderStyle={{
            color: '#8c8c8c',
            fontWeight: 'normal',
            fontSize: 14
          }}
          selectedTextStyle={styles.selectedTextStyle}
          inputSearchStyle={styles.inputSearchStyle}
          boderStyle={styles.borderStyle1}
          value={stateName || null}
          // placeholder={t("Select") + ' ' + t(displayName)}
          placeholder={t(displayName)}

          activeColor={Colors.blueE9}
          onChange={val => props.onChangeValue(val)}

          searchable={search}
        />

      </View>
      {mandatory && !stateName ? <Text
        style={{
          color: Colors.red,
          margin: 5,
          marginLeft: 12,
          fontSize: 12,
        }}>
        {t('Required')}
      </Text> : null}
    </View>
  );
}
export const CustomDropDownFilter = (props) => {
  const { t, i18n } = useTranslation();
  const {
    dropDowndata = [],
    label = '',
    image = '',
    mapKey = '',
    search = false,
    mandatory = false,
    displayName,
    stateName,
    index
  } = props;

  return (
    <View
      style={{
        padding: scale(8),
        borderRadius: scale(4),
        marginBottom: scale(10),
        backgroundColor: '#FFFFFF',
      }}>
      {stateName ? <View
        style={{
          flexDirection: 'row',
          alignItems: 'center'
        }}>
        {image ? (
          <Image
            style={{ width: scale(15), height: scale(15), margin: 5 }}
            source={image}
            resizeMode="contain"
          />
        ) : null}
        {stateName ? <Text style={{ fontWeight: '400', marginLeft: scale(8) }}>{displayName}</Text> : null}

      </View> : null}
      <View style={{ width: '100%' }}>

        <ElDropdown
          data={dropDowndata.map(eachElement => {
            return {
              value: eachElement[mapKey] || eachElement,
              label: eachElement[label] || eachElement[mapKey] || eachElement,
              eachItem: eachElement
            };
          })}
          search
          maxHeight={300}
          labelField="label"
          valueField="value"
          searchPlaceholder="Search..."
          style={styles.dropdown}
          placeholderStyle={{
            color: '#8c8c8c',
            fontWeight: 'normal',
            fontSize: 14
          }}
          // boderStyle={styles.borderStyle}
          selectedTextStyle={styles.selectedTextStyle1}
          inputSearchStyle={styles.inputSearchStyle}
          // renderLeftIcon={() => state[eachRow.stateName] ? null : (
          //   <Image
          //     style={{ width: scale(15), height: scale(15), margin: 5 }}
          //     source={image}
          //     resizeMode="contain"
          //   />
          // )}
          value={stateName || null}
          placeholder={t(displayName)}

          activeColor={Colors.blueE9}
          onChange={val => props.onChangeValue(val)}

          searchable={search}
        />
      </View>
    </View>
  );
}
export const CustomInputText = (props) => {
  const { t, i18n } = useTranslation();
  const {
    label = '',
    mandatory = false,
    onChange,
    val,
    errorMsg = '',
    disabled = false,
    keyboardType = ''
  } = props;
  // console.log('CustomInputText errorMsg '+errorMsg)
  return (
    <View style={{
      marginLeft: 1,
      marginRight: 3,
      marginTop: 5,
    }}>
      <View
        style={{
          borderBottomWidth: 1,
          // borderColor: '#f2f2f2',
          borderRadius: 10,
          marginLeft: 5,
          marginRight: 5,
          marginTop: 5,
        }}>
        {val ? <Text style={{ fontWeight: '400', color: Colors.blueE9, marginLeft: scale(5) }}>{t(label)}</Text> : null}
        <TextInput
          style={{
            padding: Platform.OS==='ios'?10: 7,
            color: '#333333',
          }}
          placeholder={t(label)}
          placeholderTextColor={'#8c8c8c'}
          keyboardType={keyboardType}
          value={val}
          onChangeText={onChange}
          multiline={true}
          editable={!disabled}
        />

      </View>
      {mandatory && !val ? <Text
        style={{
          color: Colors.red,
          margin: 5,
          marginLeft: 12,
          fontSize: 12,
        }}>
        {errorMsg}
      </Text> : null}
    </View>
    // <Input
    //   label={val ? t(label) : null}
    //   labelStyle={{ fontWeight: '400', color: Colors.blueE9 }}
    //   // containerStyle={{width: '100%',paddingHorizontal: 5,}}
    //   // inputStyle={{ minHeight: 10 }}
    //   placeholder={t(label)}
    //   onChangeText={onChange}
    //   value={val}
    //   errorMessage={mandatory ? errorMsg : null}
    //   disabled={disabled}
    //   keyboardType={KeyboardType}
    // />

  );
}
export const CustomInputTextImage = (props) => {
  const { t, i18n } = useTranslation();
  const {
    label = '',
    mandatory = false,
    onChange,
    val,
    errorMsg = '',
    disabled = false,
    keyboardType = '',
    imgStyle,
    img,
  } = props;
  // console.log('CustomInputText errorMsg '+errorMsg)
  return (
    <View style={{
      marginLeft: 1,
      marginRight: 3,
      marginTop: 5,

    }}>
      <View
        style={{
          marginLeft: 5,
          marginRight: 5,
        }}>
        <Text style={{ fontWeight: '400', marginLeft: scale(5) }}>{t(label)}</Text>
        {/* {val ? <Text style={{ fontWeight: '400', marginLeft: scale(5) }}>{t(label)}</Text> : null} */}
        <View
          style={{
            flexDirection: 'row',
            marginTop: 5,
            borderWidth: 1,
            padding: 2,
            borderRadius: 10,
          }}>
          <Image
            source={img}
            style={{
              ...imgStyle,
              marginLeft: 10,
              marginRight: 5,
              alignItems: 'center',
              marginTop: Platform.OS === 'ios' ? 8 : 15
            }}
          />
          <TextInput
            style={{
              padding: Platform.OS==='ios'?10: 7,
              color: '#333333',
            }}
            placeholder={t(label)}
            placeholderTextColor={'#8c8c8c'}
            keyboardType={keyboardType}
            value={val}
            onChangeText={onChange}
            multiline={true}
            editable={!disabled}
          />
        </View>
      </View>

      {mandatory && errorMsg ? <Text
        style={{
          color: Colors.red,
          margin: 5,
          marginLeft: 12,
          fontSize: 12,
        }}>
        {errorMsg}
      </Text> : null}
    </View>


  );
}
export const CustomButton = (props) => {

  return (
    <TouchableOpacity
      style={{
        height: scale(70),
        borderRadius: 10,
        width: '30%',
        // paddingLeft:scale(10),
        // paddingRight:scale(10),
        backgroundColor: props.backgroundColor || '#0093E9',
        margin: verticalScale(5),
        justifyContent: 'center',
        alignItems: 'center',
      }}
      onPress={props.Press}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 10,
        }}>
        <Image
          style={{
            width: props.imgWidth || scale(18.73),
            height: props.imgHight || scale(18.73),
          }}
          source={props.img}
        />
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginLeft: scale(10),
          marginTop: scale(5)
        }}>
        <Text
          style={{
            fontSize: scale(13),
            color: '#FFFFFF',
            fontWeight: 'bold',
          }}>
          {props.label1}
        </Text>
        <Text
          style={{
            fontSize: scale(13),
            color: '#FFFFFF',
            fontWeight: 'bold',
          }}>
          {props.label2}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
export const CustomButton1 = (props) => {

  return (
    <TouchableOpacity
      style={{
        width: scale(328),
        height: scale(45),
        borderRadius: 10,
        backgroundColor: props.backgroundColor || '#0093E9',
        flexDirection: 'row',
        margin: verticalScale(5),
        alignItems: 'center',
        justifyContent: 'center',
      }}
      onPress={props.Press}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 10,
        }}>
        <Image
          style={{
            width: props.imgWidth || scale(18.73),
            height: props.imgHight || scale(18.73),
          }}
          source={props.img}
        />
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginLeft: scale(10),
          marginTop: scale(5)
        }}>
        <Text
          style={{
            fontSize: scale(13),
            color: '#FFFFFF',
            fontWeight: 'bold',
          }}>
          {props.label1}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

export const MonthYearPickerView = (props) => {
  const { t, i18n } = useTranslation();
  const {
    label = '',
    onChange,
    val,
    minDate = '',
    maxDate = '',
    isMfgDate = false,
  } = props;
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const CurrentYear = new Date().getFullYear();
  const CurrentMonth = new Date().getMonth()
  const NextMonth = Number(CurrentMonth) + 1
  const [date1, setDate1] = useState(new Date(CurrentYear, NextMonth));

  const showPicker = useCallback((value) => setShow(value), []);

  const onValueChange = useCallback(
    (event, newDate) => {
      console.log('event ', event)
      setShow(false)
      if (event === 'dateSetAction') {
        if (minDate) {
          if (new Date(minDate) < new Date(newDate)) {
            onChange(newDate)
          } else {
            ToastShow(
              t('Exp Date should be grater than Mfg Date'),
              'error',
              'long',
              'top',
            )
          }
        } else {
          onChange(newDate)
        }
      }
      showPicker(false);
    },
    [],
  );

  console.log('show ' + label + ' ' + show)
  return (
    <View>
      <Text style={{ fontWeight: '400', color: Colors.blueE9 }}>{label}</Text>

      <TouchableOpacity onPress={() => showPicker(true)} style={{ flexDirection: 'row', marginTop: 10 }}>
        <Text
          style={{
            color:
              val === ''
                ? '#A8A8A8'
                : '#000',
            marginRight: scale(10),
            fontSize: scale(14.04)
          }}>
          {val ? formatDateMMYYYY(val) : 'MM/YYYY'}
        </Text>
        <Image
          style={{ width: scale(14.04), height: scale(14.04) }}
          source={require('../../assets/calendar.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      {show && (
        <MonthPicker
          onChange={onValueChange}
          value={isMfgDate ? date : date1}
          minimumDate={isMfgDate ? new Date(2020, 1) : new Date(CurrentYear, NextMonth)}
          maximumDate={isMfgDate ? new Date() : new Date(2100, 1)}
          mode={'short'}
        />
      )}
    </View>
  );
};

export const CustomTextView = (props) => {
  const { t, i18n } = useTranslation();

  const { leftText, rightText, rightTextStyle } = props;
  return (
    <View style={{ ...CommonStyle.dropdownView }}>
      <View style={{ ...CommonStyle.dropdownleftView }}>
        <Text
          style={{
            color: '#333333',
            fontSize: scale(12),
          }}>
          {t(leftText)}
        </Text>
      </View>
      <View
        style={{
          width: '50%',
        }}>
        <Text
          style={{
            fontSize: scale(12),
            color: '#333333',
            ...rightTextStyle
          }}
          numberOfLines={3}
          ellipsizeMode={'tail'}>
          {rightText ? rightText + '' : ''}
        </Text>
      </View>
    </View>
  );
}
export const CustomTextViewNew = (props) => {
  const { t, i18n } = useTranslation();

  const { leftText, rightText, rightTextStyle } = props;
  return (
    <View style={{ ...CommonStyle.dropdownView }}>
      <View style={{ width: '40%', justifyContent: 'center', }}>
        <Text
          style={{
            color: '#333333',
            fontSize: scale(12),
          }}>
          {t(leftText)}
        </Text>
      </View>
      <View
        style={{
          width: '60%',
          borderBottomColor: '#333333',
        }}>
        <Text
          style={{
            fontSize: scale(12),
            color: '#333333',
            ...rightTextStyle
          }}
          numberOfLines={3}
          ellipsizeMode={'tail'}>
          {rightText ? rightText + '' : ''}
        </Text>
      </View>
    </View>
  );
}
export const CustomTextViewWithImage = (props) => {
  const { t, i18n } = useTranslation();

  const { leftText, rightText } = props;

  return (
    <View style={{ ...CommonStyle.dropdownView }}>
      {getProductIcon(leftText)}
      <View style={{ ...CommonStyle.dropdownleftView }}>
        <Text
          style={{
            color: Colors.black70,
            fontSize: scale(12),
          }}>
          {t(leftText)}
        </Text>
      </View>
      <View
        style={{
          width: '50%',
          borderBottomColor: '#E8E8E8',
        }}>
        <Text
          style={{
            fontSize: scale(12),
            // fontWeight: 'bold',
            color: '#2585F3',
          }}
          numberOfLines={3}
          ellipsizeMode={'tail'}>
          {rightText ? rightText + '' : ''}
        </Text>
      </View>
    </View>
  );
}
export const LocationIcon = () => {
  return (
    <Image
      style={{ width: scale(10.2), height: scale(14.09) }}
      source={require('../../assets/product/location.png')}
      resizeMode="contain"
    />
  );
};
export const CustomeDatePicker = (props) => {
  const [date, setDate] = useState(new Date())
  const [open, setOpen] = useState(false)


  const { t, i18n } = useTranslation();
  const {
    label = '',
    mandatory = false,
    onChange,
    val,
    minDate = '',
    labelStyle,
  } = props;
  const CurrentYear = new Date(minDate).getFullYear();
  const Month = new Date(minDate).getMonth()
  return (
    <View style={{
      margin: 5,
    }}>
      <View
        style={{
          borderBottomWidth: 1,
          borderColor: 'gray',
          borderRadius: 10,
          marginLeft: 5,
          marginRight: 5,
          justifyContent: 'center',
          // alignItems:'center'
        }}>
        <Text
          style={labelStyle}
        >
          {label}
        </Text>
        <TouchableOpacity style={{ flexDirection: 'row', marginBottom: 10, marginTop: 10 }} onPress={() => setOpen(true)} >

          <Text
            style={{
              fontSize: scale(12),
              color: Colors.black0,
              marginLeft: 5,
              marginRight: 20,

            }}
          >
            {val ? val : "DD/MM/YYYY"}
          </Text>
          <Image
            style={{ width: scale(14.04), height: scale(14.04) }}
            source={require('../../assets/calendar.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>

        <DatePicker
          modal
          open={open}
          date={minDate ? new Date(CurrentYear, Month) : date}
          mode="date"
          minimumDate={minDate ? new Date(CurrentYear, Month) : date}
          maximumDate={new Date("2051-12-31")}
          onConfirm={(date) => {
            let newD = formatDateDDMMYYYY(date)
            setOpen(false)
            onChange(newD)
          }}
          onCancel={() => {
            setOpen(false)
          }}
        />
      </View>
      {mandatory && !val ? <Text
        style={{
          color: Colors.red,
          margin: 5,
          marginLeft: 12,
          fontSize: 12,
        }}>
        {t('Required')}
      </Text> : null}
    </View>
  );
}
export const CustomInputText1 = (props) => {
  const { t, i18n } = useTranslation();

  const { leftText, rightText, keyboardType, onChangeValue, } = props;
  return (
    <View style={{ ...CommonStyle.dropdownView }}>
      <View style={{ ...CommonStyle.dropdownleftView }}>
        <Text
          style={{
            color: Colors.black70,
            fontSize: scale(12),
          }}>
          {leftText}
        </Text>
      </View>
      <View
        style={{
          width: '50%',
          borderWidth: 1,
          borderColor: Colors.blueE9
        }}>
        <TextInput
          style={{
            padding: Platform.OS==='ios'?10: 7,
          }}
          placeholder={t(leftText)}
          keyboardType={keyboardType}
          value={rightText + ''}
          onChangeText={text => onChangeValue(text)}
          multiline={true}
        />

      </View>
    </View>
  );
}
export const CustomDate = (props) => {
  const { t, i18n } = useTranslation();

  const { date, label } = props
  return (<View style={{ backgroundColor: '#2585F3', padding: 5, borderRadius: scale(5), paddingLeft: scale(15), paddingRight: scale(15) }}>
    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

      {getProductIcon('Date', 'white')}
      <Text
        style={{
          fontSize: scale(12),
          fontWeight: '600',
          color: Colors.whiteFF,
          marginLeft: scale(-6)
        }}>
        {t(label)}
      </Text>
    </View>
    <Text
      style={{
        fontSize: scale(14),
        fontWeight: '600',
        color: Colors.whiteFF,
        alignSelf: 'center'
      }}>
      {date}
    </Text>
  </View>)
}
const styles = StyleSheet.create({
  content: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    flexShrink: 1,
  },
  inputSearchStyle: {
    fontSize: scale(14),
  },
  dropdown: {
    paddingHorizontal: 8,
    // paddingTop: 10,
    padding: 5,
  },
  placeholderStyle: {
    color: Colors.grayA8,
    fontSize: scale(14)
  },
  selectedTextStyle: {
    fontSize: scale(14),
    color: '#333333'

  },
  selectedTextStyle1: {
    color: Colors.blueE9,
    fontSize: scale(14),
  },
  borderStyle: {},
  borderStyle1: {},
});