import React, { Component, useState } from 'react';
import {
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import Entypo from 'react-native-vector-icons/Entypo';
import moment from 'moment';
import { CommonStyle, Colors, DeviceHeight, DeviceWidth } from '../../Style';
import { CustomTextView, CustomTextViewWithImage, getProductIcon } from './common'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Svg, {
  Circle,
  Image as SvgImage,
  G,
  Path,
  Line,
  Text as SVGText
} from 'react-native-svg';
import { numberWithCommas, formatDateDDMMYYYYTime, formatDateMMYYYY } from '../../Util/utils';
import { useTranslation } from 'react-i18next';

import MapView from 'react-native-maps';
import { constant } from 'lodash';


export const CustomMapView = (props) => {
  return (<View style={{ borderRadius: 10, backgroundColor: 'white', margin: 10 }}>
    <View style={{ height: DeviceHeight / 1.5, width: DeviceWidth / 1.12, margin: 8 }}>
      <MapView
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          borderRadius: 10,
        }}
        showsUserLocation={false}
        zoomEnabled={true}
        zoomControlEnabled={true}
        initialRegion={{
          latitude: 20.5937,
          longitude: 78.9629,
          latitudeDelta: 100,
          longitudeDelta: 100,
        }}>

      </MapView>
    </View>
  </View>)
}

export const TrackHeader = (props) => {
  const { t, i18n } = useTranslation();

  const { Id, Title, Address, image, isIcon = true } = props;
  return (
    <View>
      {isIcon ? (
        <View
          style={{ flexDirection: 'row', margin: verticalScale(5), flex: 1 }}>
          <View
            style={{
              backgroundColor: Colors.blueFA,
              width: scale(25.01),
              height: scale(22.38),
              borderRadius: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            opacity={1}>
            <Image
              style={{ width: scale(23.27), height: scale(23.34) }}
              source={image}
              resizeMode="contain"
            />
          </View>
          <View
            style={{
              flex: 1,
              marginLeft: 20,
            }}>
            <Text
              style={{
                fontSize: scale(12),

                color: Colors.grayA8,
              }}>
              {Title}
            </Text>
            <Text
              style={{
                fontSize: scale(12),

                color: Colors.black0,
              }}>
              {Id}
            </Text>
          </View>
        </View>
      ) : null}
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <FontAwesome name="circle" color={Colors.blueC1} size={15} />

        <Text
          style={{
            fontSize: scale(12),

            color: Colors.blueC1,
            fontWeight: 'bold',
            marginLeft: 10,
          }}>
          {Address}
        </Text>
      </View>
    </View>
  );

}
export const TrackPoDetails = (props) => {
  const { t, i18n } = useTranslation();

  const [isExpend, setExpend] = useState(false)

  const { item, orderPermission = {} } = props;
  let firstLine =
    (item.supplier &&
      item.supplier.warehouse &&
      item.supplier.warehouse.warehouseAddress &&
      item.supplier.warehouse.warehouseAddress.firstLine) ||
    '';
  let title =
    (item.receiver &&
      item.receiver.warehouse &&
      item.receiver.warehouse.title) ||
    '';
  let orgName =
    item.receiver && item.receiver.name
      ? (item.receiver && item.receiver.name) + ' / '
      : '';
  let Location = orgName + title;

  return (
    <View
      style={{
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
      }}>
      <TrackHeader
        image={require('../../assets/Inventory1.png')}
        Title={'Order ID'}
        Id={item.id}
        Address={Location}
      />
      <View
        style={{
          backgroundColor: '#FFFFFF',
          borderRadius: 20,
          padding: 10,
        }}>
        <Text
          style={{
            fontSize: scale(12),
            margin: verticalScale(5),
            color: Colors.black0,
            fontWeight: 'bold',
          }}>
          {'ORDER ' + item.poStatus}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            margin: verticalScale(5),
            flex: 1,
            marginBottom: isExpend ? 0 : 20,
          }}>
          <View
            style={{
              flex: 0.6,
            }}>
            <Text
              style={{
                fontSize: scale(12),

                color: Colors.blueC1,
                fontWeight: 'bold',
              }}>
              {'Order ID:  ' + item.id}
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
              flex: 0.4,
            }}>
            <Text
              style={{
                fontSize: scale(10),

                color: '#707070',
                alignSelf: 'flex-end',
              }}>
              {formatDateDDMMYYYYTime(item.createdAt)}
            </Text>
          </View>
        </View>
        {isExpend ? (
          <View>
            <ViewProdutCountOrder
              Products={item.products}
            />
            {orderPermission.viewPO ? (<TouchableOpacity
              style={{
                backgroundColor: Colors.orange1D,
                padding: 8,
                borderRadius: 12,
                justifyContent: 'center',
                alignItems: 'center',
                width: '40%',
                marginBottom: 10,
                marginLeft: 20,
              }}
              onPress={() => props.onClickFunction(item.id)}>
              <Text
                style={{
                  fontSize: scale(13),

                  color: '#FFFFFF',
                }}>
                {t('View Order')}
              </Text>
            </TouchableOpacity>) : null}
          </View>
        ) : null}
      </View>
      <TouchableOpacity
        style={{
          alignItems: 'flex-end',
          justifyContent: 'flex-end',
          marginTop: -20,
        }}
        onPress={() => setExpend(!isExpend)}>
        <View
          style={{
            width: 30,
            height: 30,
            backgroundColor: isExpend ? '#0093E9' : 'white',
            borderRadius: 20,
          }}>
          <Entypo
            name={!isExpend ? 'chevron-down' : 'chevron-up'}
            size={30}
            color={!isExpend ? '#0093E9' : 'white'}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
}

export const ViewProdutCount = (props) => {
  const { t, i18n } = useTranslation();


  const eachProduct = (item, index) => {
    let unit =
      item.unitofMeasure && item.unitofMeasure.name
        ? ' ' + item.unitofMeasure.name
        : '';
    return (
      <View>
        <CustomTextView
          leftText={item.id || item.productId || item.productID || ''}
          rightText={
            item.productQuantityDelivered
              ? item.productQuantityDelivered + unit
              : item.productQuantity
                ? item.productQuantity + unit
                : ''
          }
        />
      </View>
    );
  }

  const { Products = [] } = props;
  // console.log('Products ' + JSON.stringify(Products));
  return (
    <View>
      {Products.length
        ? Products.map((item, index) => eachProduct(item, index))
        : null}
    </View>
  );
}
export const ViewProdutCountOrder = (props) => {
  const { t, i18n } = useTranslation();


  const eachProduct = (item, index) => {
    let unit =
      item.unitofMeasure && item.unitofMeasure.name
        ? ' ' + item.unitofMeasure.name
        : '';
    return (
      <View>
        <CustomTextView
          leftText={item.id || item.productId || item.productID || ''}
          rightText={item.productQuantity + unit}
        />
      </View>
    );
  }

  const { Products = [] } = props;
  return (
    <View>
      {Products.length
        ? Products.map((item, index) => eachProduct(item, index))
        : null}
    </View>
  );
}

export const TrackedShipmentDetails = (props) => {
  const { t, i18n } = useTranslation();

  const [isExpend, setExpend] = useState(false)


  const { item, navigation, isTagged = true, shipmentPermission = {} } = props;
  const { shipmentUpdates = [] } = item;
  // console.log(JSON.stringify(item));
  let firstLine =
    (item.supplier &&
      item.supplier.warehouse &&
      item.supplier.warehouse.warehouseAddress &&
      item.supplier.warehouse.warehouseAddress.firstLine) ||
    '';
  let title =
    (item.receiver &&
      item.receiver.warehouse &&
      item.receiver.warehouse.title) ||
    '';
  let orgName =
    (item.receiver && item.receiver.org && item.receiver.org.name) || '';
  let Location = orgName + ' / ' + title;
  let productData =
    shipmentUpdates.length &&
    shipmentUpdates.filter(each => {
      if (each.status === 'RECEIVED') {
        return each;
      }
    });
  // console.log('productData ' + JSON.stringify(productData));
  return (
    <View
      style={{
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
      }}>
      <TrackHeader
        image={require('../../assets/Inventory1.png')}
        Title={'Shipment ID'}
        Id={item.id}
        Address={Location}
        isIcon={false}
      />
      <View
        style={{
          backgroundColor: '#FFFFFF',
          borderRadius: 20,
          padding: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
          }}>
          <Text
            style={{
              fontSize: scale(12),
              color: Colors.black0,
              margin: verticalScale(5),
              fontWeight: 'bold',
              flex: 1,
            }}>
            {'SHIPMENT ' + item.status}
          </Text>
          {isTagged ? (
            <TouchableOpacity
              onPress={() => props.onClickNextScreen(item.id)}
              style={{ margin: 5 }}>
              <Image
                style={{
                  width: scale(20),
                  height: scale(20),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                source={require('../../assets/rightArrow.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          ) : null}
        </View>
        <View
          style={{
            flexDirection: 'row',
            margin: verticalScale(5),
            flex: 1,
            marginBottom: isExpend ? 0 : 20,
          }}>
          <View
            style={{
              flex: 0.6,
            }}>
            <Text
              style={{
                fontSize: scale(12),
                color: Colors.blueC1,
                fontWeight: 'bold',
              }}>
              {'Shipment ID:  ' + item.id}
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
              flex: 0.4,
            }}>
            <Text
              style={{
                fontSize: scale(10),

                color: '#707070',
                alignSelf: 'flex-end',
              }}>
              {formatDateDDMMYYYYTime(item.createdAt)}
            </Text>
          </View>
        </View>

        {isExpend ? (
          <View>
            <ViewProdutCount Products={item.products} />
            {shipmentPermission.viewShipment ? (<TouchableOpacity
              style={{
                backgroundColor: Colors.orange1D,
                padding: 8,
                borderRadius: 12,
                justifyContent: 'center',
                alignItems: 'center',
                width: '40%',
                marginBottom: 10,
                marginLeft: 20,
              }}
              onPress={() => props.onClickFunction(item.id)}>
              <Text
                style={{
                  fontSize: scale(13),

                  color: '#FFFFFF',
                }}>
                {t('view_shipment')}
              </Text>
            </TouchableOpacity>) : null}
          </View>
        ) : null}
      </View>
      <TouchableOpacity
        style={{
          alignItems: 'flex-end',
          justifyContent: 'flex-end',
          marginTop: -20,
        }}
        onPress={() => setExpend(!isExpend)}>
        <View
          style={{
            width: 30,
            height: 30,
            backgroundColor: isExpend ? '#0093E9' : 'white',
            borderRadius: 20,
          }}>
          <Entypo
            name={!isExpend ? 'chevron-down' : 'chevron-up'}
            size={30}
            color={!isExpend ? '#0093E9' : 'white'}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
}
export const TrackRecord = (props) => {
  return (
    <View
      style={{
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: Colors.whiteFF,
        padding: 10,
        borderRadius: scale(10)
      }}>

      <View
        style={{ flexDirection: 'row', borderBottomWidth: 0.5, borderColor: Colors.grayA8, alignItems: 'center' }}>
        <View
          style={{
            backgroundColor: '#9992A747',
            width: scale(30.),
            height: scale(30),
            borderRadius: scale(5),
            justifyContent: 'center',
            alignItems: 'center',
          }}
          opacity={1}>
          <Image
            style={{ width: scale(23.27), height: scale(25.34) }}
            source={require('../../assets/truck.png')}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            marginLeft: 10,
            marginBottom: scale(10)
          }}>
          <Text
            style={{
              fontSize: scale(16),
              fontWeight: '600',
              color: Colors.grey4A,
            }}>
            {'Shipment Number : SH100904'}
          </Text>
        </View>
      </View>


      <Svg style={{ height: scale(120) }}>
        <Line
          x1="15"
          y1="40"
          x2="15"
          y2={Platform.OS === 'ios' ? "70" : "70"}
          stroke={Colors.breenAO}
          strokeWidth="2"
        />
        <View>
          <View
            style={{ flexDirection: 'row', marginTop: scale(10), alignItems: 'center' }}>
            <Entypo
              name={'location-pin'}
              size={30}
              color={Colors.gray9}
            />
            <View
              style={{
                // marginLeft: 10,
              }}>
              <Text
                style={{
                  fontSize: scale(10),
                  color: Colors.grey4A,
                }}>
                {'Starting Location'}
              </Text>
              <Text
                style={{
                  fontSize: scale(14),
                  fontWeight: '600',
                  color: Colors.blackOA,
                }}>
                {'Amazon Adrar Adrar 89009 Algeria'}
              </Text>
            </View>
          </View>
          <View
            style={{ flexDirection: 'row', marginTop: scale(10), alignItems: 'center' }}>
            <FontAwesome
              name={'location-arrow'}
              size={30}
              color={Colors.gray9}
            />
            <View
              style={{
                marginLeft: 10,
              }}>
              <Text
                style={{
                  fontSize: scale(10),
                  color: Colors.grey4A,
                }}>
                {'Destination'}
              </Text>
              <Text
                style={{
                  fontSize: scale(14),
                  fontWeight: '600',
                  color: Colors.blackOA,
                }}>
                {'IT Hub, VP road Pune Maharashtra 400096 India'}
              </Text>
            </View>
          </View>
        </View>
      </Svg>

    </View>
  );
}

export const TrackLocation = (props) => {
  const { t, i18n } = useTranslation();

  const { currentLocationList } = props
  // console.log('currentLocationList  '+JSON.stringify(currentLocationList))


  const eachLocation = (each) => {
    return <TrackLocationList t={t} item={each} />
  }

  return (
    <View>
      {/* {currentLocationList.map(each=>{
       <TrackLocationList t={t}  item ={each}/>
      })} */}
      <FlatList
        onEndReachedThreshold={0.7}
        style={{ width: '100%' }}
        keyExtractor={(item, index) => index.toString()}
        data={currentLocationList}
        renderItem={({ item, index }) => eachLocation(item, index)}
      />
      {/* <TrackLocationList t={t} /> */}

    </View>
  );
}
export const TrackLocationList = (props) => {
  const { t, i18n } = useTranslation();

  const { item = [] } = props;
  const WhLocation = item[0]
  const { organisation = {}, warehouse = {} } = WhLocation

  const { warehouseAddress = {} } = warehouse

  const eachLocation = (each) => {
    // console.log('eachLocation1 each '+JSON.stringify(each))

    return <TrackProduct
      t={t}
      item={each} />
  }

  const title = warehouse.title + ' ' + warehouseAddress.firstLine + ' ' + warehouseAddress.city + ' ' + warehouseAddress.state + ' ' + warehouseAddress.zipCode + ' ' + warehouseAddress.country;
  return (
    <View
      style={{
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: Colors.whiteFF,
        padding: 10,
        borderRadius: scale(10)
      }}>
      <View
        style={{
          alignItems: 'flex-end'
        }}>
        <Text
          style={{
            fontSize: scale(10),
            fontWeight: '600',
          }}>
          {formatDateDDMMYYYYTime(WhLocation.updatdAt)}
        </Text>
      </View>
      <View
        style={{ flexDirection: 'row', borderBottomWidth: 0.5, borderColor: Colors.grayA8, alignItems: 'center' }}>
        <View
          style={{
            backgroundColor: '#9992A747',
            width: scale(30.),
            height: scale(30),
            borderRadius: scale(5),
            justifyContent: 'center',
            alignItems: 'center',
          }}
          opacity={1}>
          <Entypo
            name={'location-pin'}
            size={30}
            color={'#F94F4F'}
          />
        </View>
        <View
          style={{
            marginLeft: 10,
            marginBottom: scale(10),
            marginRight: scale(15)
          }}>
          <Text
            style={{
              fontSize: scale(12),
              fontWeight: '600',
              color: '#2585F3',
            }}>
            {organisation.name}
          </Text>
          <Text
            style={{
              fontSize: scale(10),
              fontWeight: '600',
              color: Colors.gray9,

            }}>
            {title}
          </Text>
        </View>
      </View>
      <Text style={{ marginTop: scale(10), color: '#333333', fontSize: scale(12) }}>{'Product List'}</Text>
      <FlatList
        onEndReachedThreshold={0.7}
        style={{ width: '100%' }}
        keyExtractor={(item, index) => index.toString()}
        data={item}
        renderItem={({ item, index }) => eachLocation(item, index)}
      />


    </View>
  );
}
export const TrackProduct = (props) => {
  const { t, item } = props
  const { received = 0, sent = 0, stock = 0,productQuantity=0, productInfo = {}, label = {}, productAttributes = {} } = item
  const { unitofMeasure = {} } = productInfo
  const { mfgDate = '', expDate = '' } = productAttributes

  let receivedQty = received ? received : 0
  let sentQty = sent ? sent : 0
  let stockQty = stock ? stock : 0
  const unit = unitofMeasure.name ? '(' + unitofMeasure.name + ')' : ''
  // let receivedQtyPer =  stockQty? (receivedQty/stockQty)*100:0
  // let sentQtyPer =  stockQty? (sentQty/stockQty)*100:0
  let receivedQtyPer = sentQty ? (receivedQty / sentQty) : 0
  receivedQtyPer = receivedQtyPer > 100 ? 100 : receivedQtyPer;
  // let Qty=item.productQuantity||item.stock
  let Qty=stockQty

  // Sent will be always 100% but received will be received quantity / Sent quantity as per mahesh suggetion
  return (
    <View
      style={{
        backgroundColor: '#F6FBFF',
        padding: 10,
        borderRadius: scale(10),
        marginTop: scale(10)
      }}>
      <CustomTextViewWithImage
        leftText={'Product Name'}
        rightText={item.productName}
        t={t}
      />
      <CustomTextViewWithImage
        leftText={'Manufacturer'}
        rightText={item.manufacturer}
        t={t}
      />
      <CustomTextViewWithImage
        leftText={'Batch Number'}
        rightText={item.batchNumber}
        t={t}
      />
      <CustomTextViewWithImage
        leftText={'Label Code'}
        rightText={label.labelId}
        t={t}
      />
      <CustomTextViewWithImage
        leftText={'Quantity'}
        rightText={Qty + unit}
        t={t}
      />
      {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

        <View style={{ alignItems: 'center' }}>
          <CircularProgress
            size={100}
            strokeWidth={7}
            text={sentQty+' (PAC)'}
            progressPercent={100} />
          <View  style={{ flexDirection: 'row' }}>
          {getProductIcon('Truck')}

          <Text
            style={{
              fontSize: scale(10),
              fontFamily: 'Roboto-Regular',
              fontWeight: '600',
              color: Colors.gray9,
            }}>
            {t('Quantity Sent')}
          </Text>
          </View>

        </View>
        <View style={{ alignItems: 'center' }}>
          <CircularProgress
            size={100}
            strokeWidth={7}
            text={receivedQty+' (PAC)'}
            progressPercent={receivedQtyPer} />
             <View  style={{ flexDirection: 'row' }}>
          {getProductIcon('Truck')}
          <Text
            style={{
              fontSize: scale(10),
              fontFamily: 'Roboto-Regular',
              fontWeight: '600',
              color: Colors.gray9,
            }}>
            {t('Quantity Received')}
          </Text>
          </View>
        </View>
      </View> */}
      <View style={{ flexDirection: 'row', marginTop: 10 }}>
        <View style={{ flex: 0.3 }}>
          {mfgDate ? <CustomDate
            label={'Mfg Date'}
            date={formatDateMMYYYY(mfgDate)}
            t={t} /> : null}
        </View>
        <View style={{ flex: 0.2 }} />
        <View style={{ flex: 0.3 }}>
          {expDate ? <CustomDate
            label={'Exp Date'}
            date={formatDateMMYYYY(expDate)}
            t={t} /> : null}
        </View>
      </View>
    </View>
  );
}
export const CustomDate = (props) => {
  const { t, i18n } = useTranslation();

  const { date, label } = props
  return (<View style={{ backgroundColor: '#2585F3', padding: 5, borderRadius: scale(5), paddingLeft: scale(15), paddingRight: scale(15) }}>
    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

      {getProductIcon('Date', 'white')}
      <Text
        style={{
          fontSize: scale(12),
          fontWeight: '600',
          color: Colors.whiteFF,
          marginLeft: scale(-6)
        }}>
        {t(label)}
      </Text>
    </View>
    <Text
      style={{
        fontSize: scale(14),
        fontWeight: '600',
        color: Colors.whiteFF,
        alignSelf: 'center'
      }}>
      {date}
    </Text>
  </View>)
}
export const CircularProgress = (props) => {
  const { t, i18n } = useTranslation();

  const { size, strokeWidth, text } = props;
  const radius = (size - strokeWidth) / 2;
  const circum = radius * 2 * Math.PI;
  const svgProgress = 100 - props.progressPercent;

  return (
    <View style={{ margin: 10 }}>
      <Svg width={size} height={size}>
        {/* Background Circle */}
        <Circle
          stroke={props.bgColor ? props.bgColor : "#f2f2f2"}
          fill="none"
          cx={size / 2}
          cy={size / 2}
          r={radius}
          {...{ strokeWidth }}
        />

        {/* Progress Circle */}
        <Circle
          stroke={props.pgColor ? props.pgColor : "#FF872B"}
          fill="none"
          cx={size / 2}
          cy={size / 2}
          r={radius}
          strokeDasharray={`${circum} ${circum}`}
          strokeDashoffset={radius * Math.PI * 2 * (svgProgress / 100)}
          strokeLinecap="round"
          transform={`rotate(-90, ${size / 2}, ${size / 2})`}
          {...{ strokeWidth }}
        />

        {/* Text */}
        <SVGText
          fontSize={props.textSize ? props.textSize : "10"}
          x={size / 2}
          y={size / 2 + (props.textSize ? (props.textSize / 2) - 1 : 5)}
          textAnchor="middle"
          fill={props.textColor ? props.textColor : "#333333"}
        >
          {t(text)}
        </SVGText>
      </Svg>
    </View>
  )
}

