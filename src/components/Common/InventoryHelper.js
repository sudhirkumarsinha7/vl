import React, { Component, useState } from 'react';
import {
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  LayoutAnimation,
  Platform,
  ActivityIndicator,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import { CustomTextView,CustomTextViewNew,CustomDate } from '../../components/Common/common';
import Icon from 'react-native-vector-icons/Foundation';
import moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
import Icon1 from 'react-native-vector-icons/SimpleLineIcons';
import { backgroundColor, Colors } from '../../Style';
import { config } from '../../Util/config';
import { numberWithCommas, formatDateDDMMYYYYTime, formatDateDDMMYYYY, formatDateMMYYYY } from '../../Util/utils';
import axios from 'axios';
import { useTranslation } from 'react-i18next';
import { ToastShow } from '../Toast';
export function getStatus(status, t) {
  if (status === 'CREATE') {
    return t('Sent');
  } else if (status === 'RECEIVED') {
    return t('Received');
  } else if (status === 'ADD' || status === 'ADDED') {
    return t('added');
  } else if (status === 'BUY') {
    return t('SOLD');
  } else {
    return t(status);
  }
}
export function getColor(status) {

  if (status === 'CREATE') {
    return '#ffc107';
  } else if (status === 'RECEIVED') {
    return Colors.green;
  } else if (status === 'ADD' || status === 'ADDED') {
    return Colors.Blue;
  } else if (status === 'BUY') {
    return '#FFAB1D';
  } else {
    return Colors.Blue;
  }

}


export const InventoryList = (props) => {
  const { t, i18n } = useTranslation();

  const [isMore, setIsmore] = useState(false)


  const update = (index) => {
    setIsmore(!isMore)
  }


  let { index, item, shipmentPersmission, } = props;
  let { productDetails = {}, payloadData = {} } = item;
  let payloadData1 =
    payloadData && payloadData.data && payloadData.data.products;
  let payloadData2 =
    payloadData && payloadData.data && payloadData.data.quantityPurchased;
  let unit =
    productDetails.unitofMeasure && productDetails.unitofMeasure.name
      ? ' ' + productDetails.unitofMeasure.name
      : '';
  return (
    <View
      style={{
        justifyContent: 'center',
        alignContent: 'center',
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
      }}>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginLeft: scale(10),
          marginTop: verticalScale(10),
          justifyContent: 'space-between',
        }}>
        <Text
          style={{
            fontSize: scale(10),
            color: '#000000',
            fontWeight: 'bold',
          }}>
          {formatDateDDMMYYYY(item.createdAt)}
        </Text>
        <View style={{
          backgroundColor: getColor(item.eventTypePrimary),
          marginRight: 5,
          padding: 7,
          borderRadius: 5,
        }}>
          <Text
            style={{
              fontSize: scale(10),
              color: 'white',
              alignSelf: 'flex-end',

            }}>
            {getStatus(item.eventTypePrimary, t)}

          </Text>
        </View>

      </View>
      <CustomTextViewNew
          leftText={'Product Name'}
          rightText={productDetails.name || ''}
        />
      {isMore &&
        (item.eventTypePrimary === 'CREATE' ||
          item.eventTypePrimary === 'RECEIVE') ? (
        <View>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: scale(10),
            }}>
            <Text
              style={{
                fontSize: scale(12),

                color: '#717171',
                width: '40%',
              }}>
              {t('Shipment ID')}
            </Text>
            <Text
              style={{
                marginLeft: scale(11),
                fontSize: scale(12),

                color: '#000000',
              }}>
              {item.payloadData &&
                item.payloadData.data &&
                item.payloadData.data.id}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: scale(10),
            }}>
            <Text
              style={{
                fontSize: scale(12),

                color: '#717171',
                width: '40%',
              }}>
              {t('From Organisation')}
            </Text>
            <Text
              style={{
                marginLeft: scale(11),
                fontSize: scale(12),

                color: '#000000',
              }}>
              {item.actorOrgName
                ? item.actorOrgName !== 'null'
                  ? item.actorOrgName
                  : item.actorOrgId
                : item.actorOrgId}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: scale(10),
            }}>
            <Text
              style={{
                fontSize: scale(12),

                color: '#717171',
                width: '40%',
              }}>
              {t('From Location')}
            </Text>
            <Text
              style={{
                marginLeft: scale(11),
                fontSize: scale(12),

                color: '#000000',
                width: '55%',
              }}
              numberOfLines={4}>
              {item.actorOrgAddress}
            </Text>
          </View>
        </View>
      ) : null}
       <CustomTextViewNew
          leftText={'Category'}
          rightText={productDetails.type || ''}
        />
     <CustomTextViewNew
          leftText={'Manufacturer'}
          rightText={productDetails.manufacturer || ''}
        />
     <CustomTextViewNew
          leftText={'Quantity'}
          rightTextStyle={{fontWeight:'bold'}}
          rightText={item.inventoryQuantity > 0
            ? '+ ' + item.inventoryQuantity + unit
            : item.inventoryQuantity || item.inventoryQuantity === 0
              ? item.inventoryQuantity + unit
              : payloadData2 ? payloadData2 + unit : ''}
        />
      
      {isMore ? (
        <View>
           <CustomTextViewNew
          leftText={'Batch number'}
          rightText={payloadData1.batchNumber || item.transactionId || 'NA'}
        />
          <View style={{ flexDirection: 'row', marginTop: 10,marginLeft:10 }}>
        <View style={{ flex: 0.3 }}>
          {payloadData1.mfgDate ? <CustomDate
            label={'Mfg Date'}
            date={formatDateMMYYYY(payloadData1.mfgDate)}
            t={t} /> : null}
        </View>
        <View style={{ flex: 0.1 }} />
        <View style={{ flex: 0.3 }}>
          {payloadData1.expDate ? <CustomDate
            label={'Exp Date'}
            date={formatDateMMYYYY(payloadData1.expDate)}
            t={t} /> : null}
        </View>
      </View>
          
        </View>
      ) : null}
      {isMore &&
        (item.eventTypePrimary === 'CREATE' ||
          item.eventTypePrimary === 'RECEIVE') && shipmentPersmission.viewShipment ? (
        <View
          style={{
            marginTop: verticalScale(10),
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{
              marginTop: verticalScale(10),
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: '#0093E9',
              borderRadius: 10,
              padding: 5,
            }}
            onPress={props.ViewShipment}>
            <Text
              style={{
                fontSize: scale(12),

                color: '#0093E9',
                fontWeight: 'bold',
              }}>
              {t('view_shipment')}
            </Text>
          </TouchableOpacity>
        </View>
      ) : null}
      <TouchableOpacity
        style={{
          marginTop: verticalScale(10),
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => update(index)}>
        <Text
          style={{
            fontSize: scale(10),

            color: '#0093E9',
            borderWidth: 1,
            padding: 5,
            borderColor: '#0093E9',
            borderRadius: 5
          }}>
          {isMore ? t('SHOW LESS') : t('SHOW MORE')}
        </Text>
      </TouchableOpacity>
      <View style={{ height: scale(15) }} />
    </View>
  );
}
export const InventoryProductList = (props) => {
  const { t, i18n } = useTranslation();

  let { item } = props;
  let unit =
    item && item.unitofMeasure && item.unitofMeasure.name
      ? ' ' + item.unitofMeasure.name
      : '';
  if (item && item.quantity) {
    return (
      <TouchableOpacity
        onPress={() => props.navigation.navigate('ListOfProducts', { product: item })}
        style={{
          width: scale(99),
          height: scale(110),
          justifyContent: 'center',
          margin: scale(5),
          borderRadius: 8,
          // backgroundColor: '#fff',
          backgroundColor:
            backgroundColor[
            Math.floor(Math.random() * backgroundColor.length)
            ],
        }}>
        <View
          style={{
            margin: 10,
          }}>
          <Text
            style={{
              fontSize: scale(11),
              color: '#333333',
              fontWeight: 'bold',
            }}
            numberOfLines={2}
            ellipsizeMode="tail">
            {(item && item.productName) || ''}
          </Text>
        </View>

        <Text
          style={{
            fontSize: scale(11),

            color: '#333333',
            fontWeight: 'bold',
            margin: 10,
          }}>
          Qty: {item && item.quantity ? numberWithCommas(item.quantity) + unit : ''}
        </Text>
      </TouchableOpacity>
    );
  } else {
    return null
  }
}
export const InventoryProductList1 = (props) => {
  const { t, i18n } = useTranslation();

  let { item, selectedProduct, } = props;
  let unit =
    item && item.unitofMeasure && item.unitofMeasure.name
      ? ' ' + item.unitofMeasure.name
      : '';
  if (item && item.quantity) {
    return (
      <TouchableOpacity
        onPress={() => props.nextScreen1(item)}
        style={{
          width: scale(99),
          height: scale(110),
          justifyContent: 'center',
          margin: scale(5),
          borderRadius: 8,
          backgroundColor:
            selectedProduct === item.productName
              ? backgroundColor[
              Math.floor(Math.random() * backgroundColor.length)
              ]
              : Colors.greyE2,
        }}>
        <View
          style={{
            margin: 10,
          }}>
          <Text
            style={{
              fontSize: scale(11),
              color: '#333333',
              fontWeight: 'bold',
            }}
            numberOfLines={2}
            ellipsizeMode="tail">
            {(item && item.productName) || ''}
          </Text>
        </View>

        <Text
          style={{
            fontSize: scale(11),

            color: '#333333',
            fontWeight: 'bold',
            // alignSelf: 'center',
            margin: 10,
          }}>
          {t('Qty:') + '' + item && item.quantity ? numberWithCommas(item.quantity) + unit : ''}
        </Text>
      </TouchableOpacity>
    );
  } else {
    return null
  }
}
export const InventoryProductType = (props) => {
  const { t, i18n } = useTranslation();

  let { item, selectedProductType } = props;
  return (
    <TouchableOpacity
      onPress={() => props.nextScreen1(item)}
      style={{
        width: scale(99),
        height: scale(110),
        justifyContent: 'center',
        alignItems: 'center',
        margin: scale(5),
        borderRadius: 8,
        backgroundColor:
          selectedProductType === item.type
            ? backgroundColor[
            Math.floor(Math.random() * backgroundColor.length)
            ]
            : Colors.greyE2,
      }}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          style={{ width: scale(40), height: scale(40) }}
          source={require('../../assets/product.png')}
          resizeMode="contain"
        />
        <Text
          style={{
            margin: scale(8),
            fontSize: scale(11),
            color: '#333333',
            fontWeight: 'bold',
          }}>
          {item && item.type}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

export const InventoryProducList = (props) => {
  const { t, i18n } = useTranslation();

  let { item = {} } = props;
  let unit =
    item.unitofMeasure && item.unitofMeasure.name
      ? ' ' + item.unitofMeasure.name
      : '';
  if (item.quantity) {
    return (
      <View
        style={{
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
          justifyContent: 'center',
          alignContent: 'center',
          margin: 15,
        }}>
        <CustomTextView
          leftText={t('Product ID')}
          rightText={item.productId || ''}
        />
        <CustomTextView
          leftText={t('Product Name')}
          rightText={item.productName || ''}
        />
        <CustomTextView
          leftText={t('Manufacturer')}
          rightText={item.manufacturer || ''}
        />
        <CustomTextView
          leftText={t('Quantity')}
          rightText={numberWithCommas(item.quantity) + unit || ''}
        />
      </View>
    );
  } else {
    return null
  }
}
export const InventoryProductListNew = (props) => {
  const { t, i18n } = useTranslation();

  let { item = {}, ShowMore = false, } = props;
  let {
    inventoryDetails = {},
    products = {},
    quantity,
    batchNumbers = [],
  } = item;
  let unit =
    products.unitofMeasure && products.unitofMeasure.name
      ? ' ' + products.unitofMeasure.name
      : '';
  // console.log('InventoryProductListNew  ' + JSON.stringify(item));
  let Qty = quantity ? numberWithCommas(quantity) + unit : numberWithCommas(inventoryDetails.quantity) + unit;
  let checkQty = quantity
    ? quantity
    : inventoryDetails.quantity
      ? inventoryDetails.quantity
      : '';

  if (checkQty) {
    return (
      <View
        style={{
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
          padding: 10,
          justifyContent: 'center',
          alignContent: 'center',
          margin: 15,
        }}>
        <CustomTextView
          leftText={t('Product ID')}
          rightText={products.id || ''}
        />
        <CustomTextView
          leftText={t('Product Name')}
          rightText={products.name || ''}
        />
        <CustomTextView
          leftText={t('Manufacturer')}
          rightText={products.manufacturer || ''}
        />
        <CustomTextView leftText={t('Quantity')} rightText={Qty} />
        {batchNumbers.length ? (
          <CustomTextView
            leftText={t('Batch Number')}
            rightText={batchNumbers[0] || ''}
          />
        ) : null}
        {ShowMore ? (
          <View
            style={{
              justifyContent: 'center',
              alignContent: 'center',
              borderRadius: 8,
            }}>
            <TouchableOpacity onPress={() => props.onChangeScreen(item)}>
              <Text
                style={{
                  alignSelf: 'center',
                  fontSize: scale(14),
                  color: Colors.blueE9,
                  fontWeight: 'bold',
                }}>
                {t('show_more')}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  } else {
    return null
  }

}
export const InventoryProductListExpired = (props) => {
  const { t, i18n } = useTranslation();

  let { item = {} } = props;
  let { products = {}, attributeSet = {} } = item;

  console.log('InventoryProducList  ' + JSON.stringify(item));

  let unit =
    products.unitofMeasure && products.unitofMeasure.name
      ? ' ' + products.unitofMeasure.name
      : '';
  return (
    <View
      style={{
        justifyContent: 'center',
        alignContent: 'center',
        margin: 15,
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
      }}>
      <CustomTextView leftText={t('Product ID')} rightText={products.id || ''} />
      <CustomTextView
        leftText={t('Product Name')}
        rightText={products.name || ''}
      />
      <CustomTextView
        leftText={t('Product Category')}
        rightText={products.type || ''}
      />
      <CustomTextView
        leftText={t('Manufacturer')}
        rightText={products.manufacturer || ''}
      />
      <CustomTextView
        leftText={t('Quantity')}
        rightText={numberWithCommas(item.quantity) + unit || ''}
      />
      <CustomTextView
        leftText={t('Batch Number')}
        rightText={
          (item.batchNumbers &&
            item.batchNumbers.length &&
            item.batchNumbers[0]) ||
          ''
        }
      />
      <CustomTextView
        leftText={t('Mfg Date')}
        rightText={formatDateMMYYYY(attributeSet.mfgDate)}
      />
      <CustomTextView
        leftText={t('Exp Date')}
        rightText={formatDateMMYYYY(attributeSet.expDate)}
      />
    </View>
  );
}
export const ProductListNew = (props) => {
  const { t, i18n } = useTranslation();
  const [isloading, setLoading] = useState(false)
  const [sentQty, setSentQty] = useState('')

  const setProductQty = async () => {
    const { item } = props
    let {
      quantity,
    } = item;
    let {
      batchNumbers = [],
    } = item;
    setLoading(true)
    if (Number(quantity) >= Number(sentQty) && Number(sentQty) > 0) {
      try {
        console.log('setProductQty getDetails' + config().reduceBatchQty + batchNumbers[0] + '&quantity=' + sentQty);
        // const response = await axios.get(config().getShipment + id);
        const response = await axios.get(config().reduceBatchQty + batchNumbers[0] + '&quantity=' + Number(sentQty));
        setLoading(false)

        // fetchShipmentsByQRCode
        console.log('setProductQty getDetails' + JSON.stringify(response));
        if (response.status === 200) {
          props.getlatestRecord();

          ToastShow(
            t("Success!"),
            'success',
            'long',
            'top',
          )
          props.navigation.navigate('Inventory')
        } else {

          ToastShow(
            t('somethingWrong',),
            'error',
            'long',
            'top',
          )
        }
      } catch (e) {
        let result = e
        console.log('setProductQty result' + JSON.stringify(e));
        ToastShow(
          t('somethingWrong',),
          'error',
          'long',
          'top',
        )


      }
    } else {

      ToastShow(
        t('Sold Qty is greater than In a Stock',),
        'error',
        'long',
        'top',
      )
    }
  }


  let { item = {}, selectedProduct } = props;
  let {
    inventoryDetails = {},
    products = {},
    quantity,
    batchNumbers = [],
  } = item;
  let unit =
    selectedProduct.unitofMeasure && selectedProduct.unitofMeasure.name
      ? ' ' + selectedProduct.unitofMeasure.name
      : '';
  let Qty = quantity ? numberWithCommas(quantity) + unit : numberWithCommas(inventoryDetails.quantity) + unit;
  let checkQty = quantity
    ? quantity
    : inventoryDetails.quantity
      ? inventoryDetails.quantity
      : '';

  if (checkQty) {
    return (
      <View
        style={{
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
          padding: 10,
          justifyContent: 'center',
          alignContent: 'center',
          margin: 15,
        }}>

        <CustomTextView
          leftText={t('Product ID')}
          rightText={item.productId || ''}
        />
        <CustomTextView
          leftText={t('Product Name')}
          rightText={selectedProduct.productName || ''}
        />
        {batchNumbers.length ? (
          <CustomTextView
            leftText={t('Batch Number')}
            rightText={batchNumbers[0] || ''}
          />
        ) : null}
        <CustomTextView leftText={t('In Stock')} rightText={Qty} />
        {isloading ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
            }}>
            <ActivityIndicator color="#0000ff" />
          </View>
        ) : null}
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View
            style={{

              marginLeft: 10,
              marginRight: 10,
              flexDirection: 'row',
              flex: 0.7,

            }}>
            <Text
              style={{

                width: '40%',
                color: Colors.black70,
                marginTop: scale(7)
              }}>
              {t('Quantity')}
            </Text>
            <View style={{ width: "40%", height: Platform.OS === 'ios' ? null : scale(35), alignSelf: 'center' }}>

              <TextInput style={{
                borderWidth: 1,
                borderColor: '#084787',
                borderRadius: scale(5),
                padding: scale(5)
              }}
                placeholder={t('')}
                keyboardType={'number-pad'}
                value={sentQty}
                returnKeyType={'done'}
                onChangeText={text => setSentQty(text)}
              />
            </View>

            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              colors={sentQty ? ['#FA7923', '#FFAB1D'] : ['gray', 'gray']}

              style={{
                borderRadius: scale(5),
                padding: scale(5),
                marginLeft: scale(15)

              }}>
              <TouchableOpacity disabled={sentQty ? false : true} onPress={() => setProductQty()}>
                <Text style={{ color: Colors.whiteFF, fontWeight: 'bold', marginTop: scale(3) }}>{t('SOLD')}</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

        </View>
      </View >
    );
  } else {
    return null
  }
}
