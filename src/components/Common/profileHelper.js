import React, { Component } from 'react';
import {
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  LayoutAnimation,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import {
  profileData,
  NewLocation,
  CountriesData,
} from '../../components/Common/defaultValue';
import {
  CustomDropDownHelper1,
} from '../../components/Common/common';
import { CommonStyle, Colors } from '../../Style';
import { useTranslation } from 'react-i18next';


export const WareHouseDetails = (props)=> {
    const { t, i18n } = useTranslation();

  const navigateToUpdateWh =async()=> {
    let { item = {} } = props;
    props.navigation.navigate('UpdateWarehouse', {
      item: item,
    });
  }

    let { item = {}, index } = props;
    let { warehouseAddress = {} } = item;
    let count = index + 1;
    if (item.status === 'REJECTED') {
      return null
    }
    return (
      <View
        style={{
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
          marginLeft: verticalScale(15),
          marginRight: verticalScale(15),
          marginTop: verticalScale(10),
          padding: verticalScale(5),
        }}>
        <View style={{ flexDirection: 'row', flex: 1 }}>
          <Text
            style={{
              fontSize: scale(13),
              color: '#0B65C1',
              fontWeight: 'bold',
              flex: 0.8,
              margin: verticalScale(5),
            }}>
            {item.title}
          </Text>
          {item.status === 'ACTIVE' ? (
            <View style={{ flex: 0.25 }}>
              <TouchableOpacity
                style={{
                  borderRadius: 8,
                  borderColor: '#0093E9',
                  borderWidth: 1,
                  backgroundColor: 'transparent',
                  flexDirection: 'row',
                  alignItems: 'center',
                  padding: 5,
                  justifyContent: 'space-evenly',
                }}
                onPress={navigateToUpdateWh}>
                <Image
                  style={{ width: scale(7.89), height: scale(7.89) }}
                  source={require('../../assets/edit.png')}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    color: '#0093E9',
                    fontSize: scale(10),
                  }}>
                  {t('edit')}
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
        {item.status === 'ACTIVE' ? null : null}
        <View style={{ marginLeft: scale(10), marginRight: scale(10) }}>
          {item.status === 'ACTIVE' ? null : (
            <Text
              style={{
                fontSize: scale(11),
                color: Colors.red,
              }}>
              {t('Approval Pending')}
            </Text>
          )}
          <Text
            style={{
              fontSize: scale(13),
            }}>
            {warehouseAddress.firstLine +
              ',' +
              warehouseAddress.city +
              ',' +
              warehouseAddress.state +
              ',' +
              warehouseAddress.country +
              ',' +
              warehouseAddress.region}
          </Text>
          <Text
            style={{
              fontSize: scale(13),
            }}>
            {t('pincode') + ': ' + warehouseAddress.zipCode}
          </Text>
        </View>
      </View>
    );
}
export const WareHouseList = (props)=> {
    const { t, i18n } = useTranslation();

    let { item = {}, index, activeWareHouse } = props;
    let { warehouseAddress = {} } = item;
    return (
      <TouchableOpacity
        style={{
          borderRadius: 8,
          flexDirection: 'row',
          alignItems: 'center',
          flex: 1,
          backgroundColor:
            index === activeWareHouse ? Colors.blueF0 : '#FFFFFF',
          marginTop: 10,
          padding: 10,
        }}
        onPress={() => props.onChangeValue(item.id, index)}>
        <View style={{ flex: 0.1 }}>
          <Image
            style={{ width: scale(25), height: scale(25) }}
            source={require('../../assets/locationblue.png')}
            resizeMode="contain"
          />
        </View>
        <View style={{ flex: 0.8, marginLeft: 10 }}>
          <Text
            style={{
              fontSize: scale(16),
              color: Colors.black0,
            }}>
            {item.title}
          </Text>
          <Text
            style={{
              fontSize: scale(14),
              color: Colors.black0,
            }}>
            {warehouseAddress.city + ',' + warehouseAddress.country}
          </Text>
        </View>
      </TouchableOpacity>
    );
}
