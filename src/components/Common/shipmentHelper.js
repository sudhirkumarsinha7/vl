import React, { useEffect, useState } from 'react';
import {
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  Platform
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import { CustomTextView } from '../../components/Common/common';
import Icon from 'react-native-vector-icons/Foundation';
import moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
import Icon1 from 'react-native-vector-icons/SimpleLineIcons';
import EntypoIcon from 'react-native-vector-icons/FontAwesome';
import IconEn from 'react-native-vector-icons/Entypo';
import { CommonStyle, Colors,DeviceWidth } from '../../Style';
import { ShipmentData } from './defaultValue';
import Svg, {
  Circle as SvgCircle,
  Image as SvgImage,
  G,
  Path,
  Line,
} from 'react-native-svg';
let dot = [1, 2, 3, 4, 5, 6, 7, 8];
import { config, global } from '../../Util/config';
import { useSSR, useTranslation } from 'react-i18next';
import { ToastShow } from '../Toast';

import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';
import { numberWithCommas, formatDateDDMMYYYYTime,formatDateMMYYYY, formatDateDDMMYYYY } from '../../Util/utils';

export function getStatus(status, t) {
  if (status === 'CREATED') {
    return t('Shipped');
  } else if (status === 'RECEIVED') {
    return t('Delivered');
  } else if (status === 'UPDATED') {
    return t('Updated');
  } else {
    return t(status);
  }
}
export function getColor(status) {
  if (status === 'CREATED') {
    return Colors.Blue;
  } else if (status === 'REJECTED') {
    return Colors.red;
  } else if (
    status === 'ACCEPTED' ||
    status === 'RECEIVED' ||
    status === 'UPDATED'
  ) {
    return Colors.green;
  } else if (status === 'CANCELLED') {
    return Colors.grayA8;
  } else {
    return Colors.orange;
  }
}
export function getPOStatus(status, userOrgID, suplierOrgID, selectedTab, t) {
  console.log('getPOStatus status', status)
  if (
    status === 'CREATED' &&
    userOrgID === suplierOrgID
  ) {
    return t('Received');
  } else if (status === 'CREATED') {
    return t('Sent');
  } else {
    return t(status);
  }
}

export const InboundList = (props) => {
  const { t, i18n } = useTranslation();
  let { item, viewShipment, trackPermission, } = props;
  let { shipmentDetails = [] } = item;
  return (
    <View
      style={{
        marginTop: verticalScale(10),
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
      }}>
      <View style={{ flexDirection: 'row', marginTop: verticalScale(10) }}>
        <View
          style={{
            marginLeft: 10,
            width: '45%',

          }}>
          <View
            style={{
              // alignSelf: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontSize: scale(14),

                color: Colors.blueE9,
                marginRight: 10,
                fontWeight: 'bold',
              }}>
              {item.id}
            </Text>
          </View>
          <Text
            style={{
              color: '#000000',
            }}>
            {t('Shipment ID')}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            width: '50%',
          }}>
          <Text
            style={{
              fontSize: scale(10),

              alignSelf: 'flex-end',
              fontWeight: 'bold',
            }}>
            {formatDateDDMMYYYY(item.shippingDate)}
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                borderRadius: 12,
                backgroundColor: getColor(item.status),
                alignSelf: 'flex-end',
                marginTop: verticalScale(5),
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10,
              }}>
              <Text
                style={{
                  fontSize: scale(10),

                  color: '#FFFFFF',
                }}>
                {getStatus(item.status, t)}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <Svg>
          <Line
            x1="17"
            y1="17"
            x2="17"
            y2={Platform.OS === 'ios' ? "40" : "60"}
            stroke={Colors.blueE9}
            strokeWidth="0.5"
          />
          <View>
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 10,
                alignItems: 'center',
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <EntypoIcon name="circle" color={Colors.blueE9} size={15} />

                  <Text
                    style={{
                      fontSize: scale(14),

                      marginLeft: 10,
                      color: Colors.black0,
                    }}>
                    {item.supplier &&
                      item.supplier.org &&
                      item.supplier.org.name}
                  </Text>
                </View>
                <Text
                  style={{
                    fontSize: scale(11),

                    color: Colors.black0,
                    marginLeft: 25,
                  }}>
                  {item.supplier &&
                    item.supplier.warehouse &&
                    item.supplier.warehouse.warehouseAddress
                    ? item.supplier.warehouse.title +
                    ' / ' +
                    item.supplier.warehouse.warehouseAddress.firstLine +
                    ' ' +
                    item.supplier.warehouse.warehouseAddress.city
                    : ''}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 10,
                alignItems: 'center',
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <EntypoIcon name="circle" color={Colors.blueE9} size={15} />

                  <Text
                    style={{
                      fontSize: scale(14),

                      color: Colors.black0,
                      marginLeft: 10,
                    }}>
                    {item.receiver &&
                      item.receiver.org &&
                      item.receiver.org.name}
                  </Text>
                </View>
                <Text
                  style={{
                    fontSize: scale(11),

                    color: Colors.black0,
                    marginLeft: 25,
                  }}>
                  {item.receiver &&
                    item.receiver.warehouse &&
                    item.receiver.warehouse.warehouseAddress
                    ? item.receiver.warehouse.title +
                    ' / ' +
                    item.receiver.warehouse.warehouseAddress.firstLine +
                    ' ' +
                    item.receiver.warehouse.warehouseAddress.city
                    : ''}
                </Text>
              </View>
            </View>
          </View>
        </Svg>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
          marginTop: 10,
        }}>
        {/* {viewShipment ? (<TouchableOpacity
            style={{
              flexDirection: 'row',
              padding: 8,
              borderRadius: 10,
            }}
            onPress={() => props.onView()}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 1 }}
              colors={['#0093E9', '#36C2CF']}
              style={{
                width: scale(80),
                height: scale(30),
                backgroundColor: '#FFFFFF',
                borderRadius: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon1 name="eye" color="white" size={16} />
              <Text
                style={{
                  color: 'white',
                  fontSize: 16,
                  fontWeight: 'bold',
                  alignItems: 'center',
                }}>
                {t('View')}
              </Text>
            </LinearGradient>
          </TouchableOpacity>) : null} */}
        <View
          style={{
            padding: 8,
            borderRadius: 10,
            alignItems: 'center',
          }}>
          {viewShipment ? (<TouchableOpacity
            style={{
              width: scale(80),
              // height: scale(30),
              borderRadius: 10,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: Colors.blueE9,
              borderWidth: 1,
              padding: scale(8)
            }}
            onPress={() => props.onView()}>
            <Text
              style={{
                color: Colors.blueE9,
                fontWeight: 'bold',
                textAlign: 'center',
                fontSize: 16,
              }}>
              {t('View')}
            </Text>
          </TouchableOpacity>) : null}
        </View>
        {trackPermission.trackAndTrace ? (<TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 8,
            borderRadius: 10,
          }}
          onPress={() => props.onClickFunction(item.id)}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}
            colors={['#0093E9', '#36C2CF']}
            style={{
              // width: scale(80),
              // height: scale(30),
              backgroundColor: '#FFFFFF',
              borderRadius: 10,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              padding: scale(8)
            }}>
            <Icon1 name="location-pin" color="white" size={16} />
            <Text
              style={{
                color: 'white',
                fontSize: 16,
                fontWeight: 'bold',
                alignItems: 'center',
                marginLeft: scale(5)
              }}>
              {t('Track')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>) : null}
      </View>
    </View>
  );
}

export const OrderList = (props) => {
  const { t, i18n } = useTranslation();

  let { item, userOrgID, selectedTab, oprderPermission, trackPermission, } = props;
  let { products = [] } = item;
  let newList = products.map(each => {
    return each.name || each.productName;
  });
  // console.log('shipment Details ' + JSON.stringify(item));
  return (
    <View
      style={{
        marginTop: verticalScale(10),
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
      }}>
      <View
        style={{
          flexDirection: 'row',
          marginTop: verticalScale(10),
          flex: 1,
          marginRight: 10,
        }}>
        <View
          style={{
            marginLeft: 10,
            flex: 0.4,
          }}>
          <Text
            style={{
              fontSize: scale(14),

              color: Colors.blueE9,
              fontWeight: 'bold',
            }}>
            {item.id}
          </Text>
          <Text
            style={{
              fontSize: scale(11),

            }}>
            {t('Order ID')}
          </Text>
        </View>
        <View
          style={{
            flex: 0.6,
          }}>
          <Text
            style={{
              fontSize: scale(10),

              alignSelf: 'flex-end',
              fontWeight: 'bold',
            }}>
            {formatDateMMYYYY(item.creationDate)}
          </Text>
          <View>
            <View
              style={{
                padding: 10,
                borderRadius: 12,
                backgroundColor: getColor(item.poStatus),
                alignSelf: 'flex-end',
                marginTop: verticalScale(5),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: scale(10),

                  color: '#FFFFFF',
                  alignSelf: 'flex-end',
                }}>
                {getPOStatus(
                  item.poStatus,
                  userOrgID,
                  item.supplier.supplierOrganisation,
                  selectedTab,
                  t
                )}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <Svg>
          <Line
            x1="17"
            y1="17"
            x2="17"
            y2={Platform.OS === 'ios' ? "40" : "60"}
            stroke={Colors.blueE9}
            strokeWidth="0.5"
          />

          <View>
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 10,
                alignItems: 'center',
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <EntypoIcon name="circle" color={Colors.blueE9} size={15} />

                  <Text
                    style={{
                      fontSize: scale(14),

                      marginLeft: 10,
                      color: Colors.black0,
                    }}>
                    {item.supplier &&
                      item.supplier.organisation &&
                      item.supplier.organisation.name}
                  </Text>
                </View>
                <Text
                  style={{
                    fontSize: scale(11),

                    color: Colors.black0,
                    marginLeft: 25,
                  }}>
                  {item.supplier &&
                    item.supplier.organisation &&
                    item.supplier.organisation.id}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 10,
                alignItems: 'center',
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <EntypoIcon name="circle" color={Colors.blueE9} size={15} />

                  <Text
                    style={{
                      fontSize: scale(14),

                      color: Colors.black0,
                      marginLeft: 10,
                    }}>
                    {item.customer &&
                      item.customer.organisation &&
                      item.customer.organisation.name}
                  </Text>
                </View>
                <Text
                  style={{
                    fontSize: scale(11),

                    color: Colors.black0,
                    marginLeft: 25,
                  }}>
                  {item.customer &&
                    item.customer.organisation &&
                    item.customer.organisation.id}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 30,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Text
                  style={{
                    fontSize: scale(12),

                  }}
                  numberOfLines={2}
                  ellipsizeMode="tail">
                  {newList.length > 1
                    ? t('Products') + ': ' +
                    newList[0] +
                    ' + ' +
                    (newList.length - 1) +
                    ' ' + t('More')
                    : t('Products') + ': ' + newList.toString()}
                </Text>
              </View>
            </View>
          </View>
        </Svg>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
          marginTop: 10,
          // marginLeft: '40%',
        }}>
        <View
          style={{
            padding: 8,
            borderRadius: 10,
            alignItems: 'center',
          }}>
          {oprderPermission.viewPO ? (<TouchableOpacity
            style={{
              width: scale(80),
              borderRadius: 10,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: Colors.blueE9,
              borderWidth: 1,
              padding: scale(8)
            }}
            onPress={() => props.onView()}>
            <Text
              style={{
                color: Colors.blueE9,
                fontWeight: 'bold',
                textAlign: 'center',
                fontSize: 16,
              }}>
              {t('View')}
            </Text>
          </TouchableOpacity>) : null}
        </View>
        {trackPermission.trackAndTrace ? (<TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 8,
            borderRadius: 10,

          }}
          onPress={() => props.onClickFunction(item.id)}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}
            colors={['#0093E9', '#36C2CF']}
            style={{
              // width: scale(80),
              // height: scale(30),
              backgroundColor: '#FFFFFF',
              borderRadius: 10,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              padding: scale(8)
            }}>
            <Icon1 name="location-pin" color="white" size={16} />
            <Text
              style={{
                color: 'white',
                fontSize: 16,
                fontWeight: 'bold',
                alignItems: 'center',
                marginLeft: scale(5)
              }}>
              {t('Track')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>) : null}
      </View>
    </View>
  );
}
export const OrderViewCard = (props) => {
  const { t, i18n } = useTranslation();
  let { item, userOrgID, selectedTab = '' } = props;
  let { shipmentDetails = [] } = item;
  return (
    <View
      style={{
        marginTop: verticalScale(10),
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
      }}>
      <View
        style={{ flexDirection: 'row', marginTop: verticalScale(10), flex: 1 }}>
        <View
          style={{
            marginLeft: 10,
            flex: 0.5,
          }}>
          <Text
            style={{
              fontSize: scale(14),

              // color: Colors.blue1EA,
              fontWeight: 'bold',
            }}>
            {item.id}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            // width: '75%',
            flex: 0.4,
          }}>
          <Text
            style={{
              fontSize: scale(10),

              // color: '#707070',
              alignSelf: 'flex-end',
              fontWeight: 'bold'
            }}>
            {formatDateMMYYYY(item.creationDate)}
          </Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <View
          style={{
            flex: 0.5,
            borderRadius: 12,
            backgroundColor: getColor(item.poStatus),
            marginTop: verticalScale(5),
            marginLeft: 10,
            justifyContent: 'center',
            alignItems: 'center',
            padding: 5,
          }}>
          <Text
            style={{
              fontSize: scale(13),

              color: '#FFFFFF',
            }}>
            {getPOStatus(
              item.poStatus,
              userOrgID,
              item.supplier.supplierOrganisation,
              selectedTab,
              t
            )}
          </Text>
        </View>
      </View>
      <Text
        style={{
          fontSize: scale(14),

          fontWeight: 'bold',
          color: Colors.blue1EA,
          marginLeft: 10,
        }}>
        {t('From')}
      </Text>
      <CustomTextView
        leftText={t('Organization Name')}
        rightText={
          (item.supplier &&
            item.supplier.organisation &&
            item.supplier.organisation.name) ||
          ''
        }
      />
      <CustomTextView
        leftText={t('Organization ID')}
        rightText={
          (item.supplier &&
            item.supplier.organisation &&
            item.supplier.organisation.id) ||
          ''
        }
      />
    
      <Text
        style={{
          fontSize: scale(14),

          fontWeight: 'bold',
          color: Colors.blue1EA,
          marginLeft: 10,
        }}>
        {t('To')}
      </Text>
      {global.defaultCountrycode === 'cr' ? null : <View><CustomTextView
        leftText={t('Region')}
        rightText={(item.customer && item.customer.region) || ''}
      />
        <CustomTextView
          leftText={t('Country')}
          rightText={(item.customer && item.customer.country) || ''}
        /></View>}
      <CustomTextView
        leftText={t('Organization Name')}
        rightText={
          (item.customer &&
            item.customer.organisation &&
            item.customer.organisation.name) ||
          ''
        }
      />
      <CustomTextView
        leftText={t('Organization Location')}
        rightText={
          item &&
            item.customer &&
            item.customer.warehouse &&
            item.customer.warehouse.warehouseAddress
            ? item.customer.warehouse.title +
            ' / ' +
            item.customer.warehouse.warehouseAddress.firstLine +
            ' ' +
            item.customer.warehouse.warehouseAddress.city
            : ''
        }
      />
      <CustomTextView
        leftText={t('Organization ID')}
        rightText={
          (item.customer &&
            item.customer.organisation &&
            item.customer.organisation.id) ||
          ''
        }
      />
    </View>
  );
}

export const ShimpmetViewCard = (props) => {
  const { t, i18n } = useTranslation();

  let { item } = props;
  console.log('ShimpmetViewCard item' + JSON.stringify(item));
  return (
    <View
      style={{
        marginTop: verticalScale(10),
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
      }}>
      <View
        style={{ flexDirection: 'row', marginTop: verticalScale(10), flex: 1 }}>
        <View
          style={{
            marginLeft: 10,
            flex: 0.5,
          }}>
          <Text
            style={{
              fontSize: scale(16),

              color: Colors.black0,
              fontWeight: 'bold',
            }}>
            {item.id}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            // width: '75%',
            flex: 0.4,
          }}>
          <Text
            style={{
              fontSize: scale(10),

              color: '#707070',
              alignSelf: 'flex-end',
            }}>
            {formatDateDDMMYYYY(item.shippingDate)}
          </Text>
        </View>
      </View>
      <View
        style={{
          padding: 10,
          height: scale(30),
          borderRadius: 12,
          backgroundColor: getColor(item.status),
          marginTop: verticalScale(5),
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 3,
          width: '30%',
        }}>
        <Text
          style={{
            fontSize: scale(10),

            color: '#FFFFFF',
          }}>
          {getStatus(item.status, t)}
        </Text>
      </View>
      {item.taggedShipments &&
        item.taggedShipments.length &&
        item.taggedShipments[0] ? (
        <CustomTextView
          leftText={t('Ref Shipment ID')}
          rightText={item.taggedShipments[0] || ''}
        />
      ) : null}
      <Text
        style={{
          fontSize: scale(14),

          fontWeight: 'bold',
          color: Colors.blueC1,
          marginLeft: 10,
        }}>
        {t('From')}
      </Text>
      <CustomTextView
        leftText={t('Organization Name')}
        rightText={
          (item.supplier && item.supplier.org && item.supplier.org.name) || ''
        }
      />
      <CustomTextView
        leftText={t('Organization Location')}
        rightText={
          item.supplier &&
            item.supplier.warehouse &&
            item.supplier.warehouse.warehouseAddress
            ? item.supplier.warehouse.title +
            ' / ' +
            item.supplier.warehouse.warehouseAddress.firstLine +
            ' ' +
            item.supplier.warehouse.warehouseAddress.city
            : ''
        }
      />
      <Text
        style={{
          fontSize: scale(14),

          fontWeight: 'bold',
          color: Colors.blueC1,
          marginLeft: 10,
        }}>
        {t('To')}
      </Text>
      <CustomTextView
        leftText={t('Organization Name')}
        rightText={
          (item.receiver && item.receiver.org && item.receiver.org.name) || ''
        }
      />
      <CustomTextView
        leftText={t('Organization Location')}
        rightText={
          item.receiver &&
            item.receiver.warehouse &&
            item.receiver.warehouse.warehouseAddress
            ? item.receiver.warehouse.title +
            ' / ' +
            item.receiver.warehouse.warehouseAddress.firstLine +
            ' ' +
            item.receiver.warehouse.warehouseAddress.city
            : ''
        }
      />
    </View>
  );
}
export const ReceiveShipmetHepler1 = (props) => {
  const { t, i18n } = useTranslation();

  let { item, } = props;
  return (
    <View
      style={{
        marginTop: verticalScale(10),
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
      }}>
      <View style={{ flexDirection: 'row', marginTop: verticalScale(10) }}>
        <View>
          <Image
            style={{ width: scale(30), height: scale(30) }}
            source={require('../../assets/user.png')}
          />
        </View>
        <View
          style={{
            marginLeft: 10,
          }}>
          <Text
            style={{
              fontSize: scale(12),

              color: '#707070',
            }}>
            {item.id}
          </Text>
          <Text
            style={{
              color: '#000000',
            }}>
            {t('Shipment ID')}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            width: '57%',
          }}>
          <Text
            style={{
              fontSize: scale(10),

              color: '#707070',
              alignSelf: 'flex-end',
            }}>
            {formatDateMMYYYY(item.expectedDeliveryDate)}
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                alignSelf: 'center',
                marginTop: verticalScale(5),
                marginRight: 10,
              }}>
              <Icon name="alert" color="red" size={25} />
            </View>
            <View
              style={{
                padding: 10,
                borderRadius: 12,
                backgroundColor: getColor(item.status),
                alignSelf: 'flex-end',
                marginTop: verticalScale(5),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: scale(10),

                  color: '#FFFFFF',
                }}>
                {getStatus(item.status, t)}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginTop: verticalScale(10),
          marginLeft: 10,
        }}>
        <View style={{ width: '20%' }}>
          <Text
            style={{
              fontSize: scale(12),

              color: '#707070',
            }}>
            {t('From')}
          </Text>
        </View>
        <View style={{ width: '80%' }}>
          <Text
            style={{
              fontSize: scale(12),

              fontWeight: 'bold',
            }}>
            {item.supplier && item.supplier.locationId}
          </Text>
          <Text
            style={{
              fontSize: scale(12),

              color: '#707070',
            }}>
            {item.supplier && item.supplier.org && item.supplier.org.name}
          </Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginTop: verticalScale(10),
          marginLeft: 10,
        }}>
        <View style={{ width: '20%' }}>
          <Text
            style={{
              fontSize: scale(12),

              color: '#707070',
            }}>
            {t('To')}
          </Text>
        </View>
        <View style={{ width: '80%' }}>
          <Text
            style={{
              fontSize: scale(12),

              fontWeight: 'bold',
            }}>
            {item.receiver && item.receiver.locationId}
          </Text>
          <Text
            style={{
              fontSize: scale(12),

              color: '#707070',
            }}>
            {item.receiver && item.receiver.org && item.receiver.org.name}
          </Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginTop: verticalScale(10),
          marginLeft: 10,
        }}>
        <View style={{ width: '20%' }}>
          <Text
            style={{
              fontSize: scale(12),

              color: '#707070',
            }}>
            {'Date'}
          </Text>
        </View>
        <View style={{ width: '80%', marginBottom: verticalScale(10) }}>
          <Text
            style={{
              fontSize: scale(12),

              fontWeight: 'bold',
            }}>
            {formatDateMMYYYY(item.shippingDate)}
            
          </Text>
        </View>
      </View>
    </View>
  );

}
export const ViewProdutList = (props) => {
  const { t, i18n } = useTranslation();


  const eachProduct = (item, index) => {
    let unit =
      item.unitofMeasure && item.unitofMeasure.name
        ? ' ' + item.unitofMeasure.name
        : '';
    return (
      <ViewProdutListComponet
      unit={unit}
      index={index}
      item={item}
      products={Products}
      isReviewOrder={isReviewOrder}
      receiveProduct={receiveProduct}
      scanObj={scanObj}
      updateProductQty={(val)=>updateProductQty(val)}
      />
    
    );
  }

  const { Products = [], isReviewOrder = false,receiveProduct=false,scanObj={},updateProductQty } = props;
  return (
    <View
      style={{
        marginTop: verticalScale(15),
        flex: 1,
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
      }}>
      {isReviewOrder ? (
        <Text
          style={{
            fontSize: scale(14),

            fontWeight: 'bold',
            marginLeft: 10,
            marginTop: verticalScale(10),
          }}>
          {t('Products Details')}
        </Text>
      ) : null}
      {Products.length
        ? Products.map((item, index) => eachProduct(item, index))
        : null}
    </View>
  );
}

export const ViewProdutListComponet = (props) => {
  const { t, i18n } = useTranslation();
  const [isEditQty, setEditQty] = useState(true);
  const [Qty, setQty] = useState(false);
  useEffect(() => {
    async function fetchData() {
    setQty(item.productQuantitySent)
    }
    fetchData();
  }, []);
  const setQuantyEdit = () => {
    setEditQty(!isEditQty)
  };
  const saveQty = () => {
    let {products} =scanObj
    // delete products[index].productQuantityDelivered;
    if(Qty){
      if (Number(products[index].productQuantitySent) >= Number(Qty)) {
        setEditQty(!isEditQty)
  
        products[index].productQuantityDelivered = Qty || 0;
        products[index].productQuantity = Number(Qty) || 0;
        updateProductQty(scanObj)
        console.log('products '+JSON.stringify(products))
      }
    }else{
      let emsg = 'Qty requied'
      ToastShow(
        emsg,
        'error',
        'long',
        'top',
      )
    }
    
}
  setQuanty = (text ) => {
    let {products} =scanObj
    if (Number(products[index].productQuantitySent) >= Number(text)) {
      setQty(Number(text))
    } else if(!text) {
      setQty(0)

    }else {
    }
  };


  let { Products = [], isReviewOrder = false,unit='',index,item,receiveProduct,updateProductQty,scanObj={} } = props;

    return (
      <View>
        <View>
        <View style={{ flexDirection: 'row', flex: 1 }}>
          <Text
            style={{
              fontSize: scale(13),

              color: Colors.blue1EA,
              fontWeight: 'bold',
              flex: 0.8,
              marginLeft: verticalScale(8),
              marginTop: verticalScale(10),
            }}>
            {t('Product') + ' ' + (index + 1)}
          </Text>
         
        { receiveProduct? <View style={{ flex: 0.15, marginTop: scale(10) }}>
              {isEditQty ? (
                <TouchableOpacity
                  style={{
                    borderRadius: 8,
                    borderColor: '#0093E9',
                    borderWidth: 1,
                    backgroundColor: 'transparent',
                    flexDirection: 'row',
                    alignItems: 'center',
                    padding: 5,
                    justifyContent: 'space-evenly',
                  }}
                  onPress={() => setQuantyEdit(index)}>
                  <Image
                    style={{ width: scale(7.89), height: scale(7.89) }}
                    source={require('../../assets/edit.png')}
                    resizeMode="contain"
                  />
                  <Text
                    style={{
                      color: Colors.blue1EA,
                      fontSize: scale(10),
                      
                    }}>
                    {t('edit')}
                  </Text>
                </TouchableOpacity>
              ) :
                (<TouchableOpacity
                  style={{
                    borderRadius: 8,
                    backgroundColor: '#0093E9',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                    padding: 5,
                  }}
                  onPress={() => saveQty()}>
                  <Text
                    style={{
                      color: '#fff',
                      fontSize: scale(10),
                      
                    }}>
                    {t('save')}
                  </Text>
                </TouchableOpacity>)
              }
            </View>:null}
        </View>
        </View>
        <CustomTextView
          leftText={t('Product Category')}
          rightText={item.productCategory || item.type || ''}
        />
        <CustomTextView
          leftText={t('Product Name')}
          rightText={item.productName || item.name}
        />
        <CustomTextView
          leftText={t('Product ID')}
          rightText={item.productID || item.id || item.productId || ''}
        />
        <CustomTextView
          leftText={t('Manufacturer')}
          rightText={item.manufacturer}
        />
         { receiveProduct?<CustomTextView
              leftText={t('Quantity Sent')}
              rightText={
                item.productQuantitySent ? numberWithCommas(item.productQuantitySent) + unit : ''
              }
            />:null}
        { receiveProduct?isEditQty ?(

<CustomTextView
leftText={t('Quantity Received')}
rightText={
  item.productQuantityDelivered
    ? numberWithCommas(item.productQuantityDelivered) +unit
    : ''
}
/>
         
            
        ):(
          <View style={{ ...CommonStyle.dropdownView }}>
              <View style={{ ...CommonStyle.dropdownleftView }}>
                <Text
                  style={{
                    color: '#707070',
                    
                  }}>
                  {t('Quantity Received') + '(' + unit + ')'}
                </Text>
              </View>
              <View
                style={{
                  width: '50%',
                  height: scale(35),
                  borderBottomColor: '#E8E8E8',
                }}>
                <TextInput
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#E8E8E8',
                  }}
                  placeholder={t('Enter Quantity')}
                  keyboardType={'numeric'}
                  value={
                    scanObj.products[index].productQuantity
                      ? Qty + ''
                      : ''
                  }
                  onChangeText={text => setQuanty(text, index)}
                />
              </View>
            </View>
        ): <CustomTextView
        leftText={t('Quantity')}
        rightText={
          item.productQuantity
            ? numberWithCommas(item.productQuantity) + unit
            : item.quantity
              ? numberWithCommas(item.quantity) + unit
              : ''
        }
      />}

      
        {item.mfgDate ? (
          <CustomTextView leftText={t('Mfg Date')} rightText={formatDateMMYYYY(item.mfgDate)} />
        ) : null}
        {item.expDate ? (
          <CustomTextView leftText={t('Exp Date')} rightText={formatDateMMYYYY(item.expDate)} />
        ) : null}
        {item.batchNumber ? (
          <CustomTextView
            leftText={t('Batch Number')}
            rightText={item.batchNumber}
          />
        ) : null}
        {item.serialNumbersRange ? (
          <CustomTextView
            leftText={t('Serial Number')}
            rightText={item.serialNumbersRange.length ? item.serialNumbersRange.toString() : item.serialNumbersRange}
          />
        ) : null}
      </View>
    );
}
export const DeliveryDetails = (props) => {
  const { t, i18n } = useTranslation();

  let { item } = props;
  let { shipmentDetails = [] } = item;
  return (
    <View
      style={{
        marginTop: verticalScale(10),
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
      }}>
      <Text
        style={{
          fontSize: scale(16),

          fontWeight: 'bold',
          color: Colors.blueC1,
          marginLeft: 10,
          marginTop: verticalScale(10),
        }}>
        {t('Delivery Details')}
      </Text>
      <CustomTextView
        leftText={t(ShipmentData.cardView1.airWayBillNo.displayName)}
        rightText={item.airWayBillNo || ''}
      />
      <CustomTextView
        leftText={t('Label Code')}
        rightText={(item.label && item.label.labelId) || ''}
      />

      <CustomTextView
        leftText={t('Shipment Date')}
        rightText={formatDateDDMMYYYY(item.shippingDate)}
      />
      <CustomTextView
        leftText={t('Estimate delivery date')}
        rightText={formatDateDDMMYYYY(item.expectedDeliveryDate)}
      />
    </View>
  );

}
export const ReceiveShipmetHepler = (props) => {
  const { t, i18n } = useTranslation();

  let { item } = props;
  return (
    <View
      style={{
        marginTop: verticalScale(10),
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
      }}>
      <CustomTextView leftText={t('Shipment ID')} rightText={item.id || ''} />
      <CustomTextView
        leftText={t('Transit Number')}
        rightText={item.airWayBillNo || ''}
      />
    </View>
  );
}
export const ReceiveShipmentDetails = (props) => {
  const { t, i18n } = useTranslation();

  let { item } = props;
  let { shipmentDetails = [] } = item;
  return (
    <View
      style={{
        marginTop: verticalScale(10),
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
      }}>
      <Text
        style={{
          fontSize: scale(12),

          fontWeight: 'bold',
          color: Colors.blue1EA,
          marginLeft: 10,
          marginTop: verticalScale(10),
        }}>
        {t('Shipment Details')}
      </Text>

      <CustomTextView
        leftText={'Shipment Date'}
        rightText={ formatDateMMYYYY(item.shippingDate)}
      />
      <Text
        style={{
          fontSize: scale(14),

          fontWeight: 'bold',
          marginLeft: 10,
          marginTop: verticalScale(10),
          color: Colors.black0,
        }}>
        {t('From')}
      </Text>
      <CustomTextView
        leftText={t('Organization Name')}
        rightText={
          (item.supplier && item.supplier.org && item.supplier.org.name) || ''
        }
      />
      <CustomTextView
        leftText={t('Organization Location')}
        rightText={
          (item.supplier &&
            item.supplier.org &&
            item.supplier.org.postalAddress) ||
          ''
        }
      />
      <Text
        style={{
          fontSize: scale(14),

          fontWeight: 'bold',
          marginLeft: 10,
          marginTop: verticalScale(10),
          color: Colors.black0,
        }}>
        {t('To')}
      </Text>
      <CustomTextView
        leftText={t('Organization Name')}
        rightText={
          (item.receiver && item.receiver.org && item.receiver.org.name) || ''
        }
      />
      <CustomTextView
        leftText={t('Organization Location')}
        rightText={
          (item.receiver &&
            item.receiver.org &&
            item.receiver.org.postalAddress) ||
          ''
        }
      />
    </View>
  );
}

export const ChainOfStudy = (props) => {
  const { t, i18n } = useTranslation();



  let { shipmentDetails, shipmentChain, token, } = props;
  let { poChainOfCustody = [], shipmentChainOfCustody = [] } = shipmentChain;
  let shipmentUpdates =
    shipmentChainOfCustody &&
    shipmentChainOfCustody.length &&
    shipmentChainOfCustody[0].shipmentUpdates;
  // console.log('ChainOfStudy shipmentChain' + JSON.stringify(shipmentChain));

  // console.log('token render' + token);
  return (
    <View
      style={{
        marginTop: verticalScale(10),
        borderRadius: 8,
        margin: 15,
      }}>
      <ChainOfStudyHeader
        image={require('../../assets/Inventory1.png')}
        Title={t('Shipment Number')}
        Id={shipmentDetails.id}
        shipment={shipmentDetails}
      />
      {poChainOfCustody &&
        poChainOfCustody.length &&
        poChainOfCustody[0].id ? (
        <ChainOfStudyOrder poChainOfCustody={poChainOfCustody[0]} t={t} />
      ) : null}
      {shipmentUpdates.length
        ? shipmentUpdates.map((each, index) => {
          return (
            <ChainOfShipment
              each={each}
              shipmentDetails={shipmentDetails}
              token={token}
              t={t}
            />
          );
        })
        : null}
    </View>
  );
}
export const ShipmentGraph = (props) => {
  const { t, i18n } = useTranslation();

  return (
    <View
      style={{
        flex: 1,
        margin: 10,
        borderRadius: 10,
        // flexDirection: 'row',
      }}>
      <View style={{ flex: 1, marginLeft: 10 }}>
        <Text style={{ fontSize: scale(12), color: Colors.blueE9 }} numberOfLines={2} ellipsizeMode="tail">
          {t('Last Updated on') + ' : ' + '6:23 PM'}
        </Text>
        {/* <Text>6:23 PM</Text> */}
        <Text style={{ fontSize: scale(12), color: Colors.blueE9 }}>{t('Current Temperature') + ' : ' + '23°C'}</Text>
        {/* <Text>23°C</Text> */}
      </View>
      <View style={{ flex: 1, margin: 5 }}>
        <LineChart
          data={{
            labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
            datasets: [
              {
                data: [
                  Math.random(),
                  Math.random(),
                  Math.random(),
                  Math.random(),
                  Math.random(),
                  Math.random(),
                  Math.random(),
                  Math.random(),
                  Math.random(),
                  Math.random(),
                ],
              },
            ],
          }}
          height={scale(180)}
          width={DeviceWidth / 1.1}
          chartConfig={{
            backgroundGradientFrom: 'white',
            backgroundGradientTo: 'white',
            color: (opacity = 1) => `rgba(255, 140, 0, ${opacity})`,
          }}
          verticalLabelRotation={30}
          withInnerLines={false}


          bezier
        />
      </View>
    </View>
  );
}
export const ChainOfStudyHeader = (props) => {
  const { t, i18n } = useTranslation();

  let { Id, Title, shipment, image, isIcon = true } = props;
  return (
    <View>
      {isIcon ? (
        <View
          style={{ flexDirection: 'row', margin: verticalScale(5), flex: 1 }}>
          <View
            style={{
              width: scale(41),
              height: scale(41),
              borderRadius: 400,
              backgroundColor: Colors.blue1EA,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            opacity={0.3}>
            <Image
              style={{ width: scale(23.27), height: scale(23.34) }}
              source={image}
              resizeMode="contain"
            />
          </View>
          <View
            style={{
              flex: 1,
              marginLeft: 20,
            }}>
            <Text
              style={{
                fontSize: scale(12),

                color: Colors.grayA8,
              }}>
              {Title}
            </Text>
            <Text
              style={{
                fontSize: scale(12),

                color: Colors.black0,
                fontWeight: 'bold',
              }}>
              {Id}
            </Text>
          </View>
        </View>
      ) : null}
      <View style={{ flex: 1, marginLeft: 15, alignSelf: 'center' }}>
        <Svg>
          <Line
            x1="8"
            y1="8"
            x2="8"
            y2={Platform.OS === 'ios' ? "40" : "60"}
            strokeDasharray="1, 5"
            stroke-linecap="round"
            stroke={Colors.blueE9}
            strokeWidth="1"
          />
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <EntypoIcon name="circle" color={Colors.black70} size={15} />
            <Text
              style={{
                color: '#000000',

                fontWeight: 'bold',
                fontSize: scale(12),
                marginLeft: 5,
              }}>
              {shipment.supplier &&
                shipment.supplier.org &&
                shipment.supplier.org.name}
            </Text>
          </View>
          <View>
            <Text
              style={{
                color: Colors.black70,
                fontSize: scale(12),
                marginLeft: 15,
              }}>
              {shipment.supplier &&
                shipment.supplier.warehouse &&
                shipment.supplier.warehouse.warehouseAddress
                ? shipment.supplier.warehouse.title +
                ' / ' +
                shipment.supplier.warehouse.warehouseAddress.firstLine +
                ' ' +
                shipment.supplier.warehouse.warehouseAddress.city
                : ''}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <EntypoIcon name="circle" color={Colors.blueFB} size={15} />
            <Text
              style={{
                color: '#000000',

                fontWeight: 'bold',
                fontSize: scale(12),
                marginLeft: 5,
              }}>
              {shipment.receiver &&
                shipment.receiver.org &&
                shipment.receiver.org.name}
            </Text>
          </View>
          <View>
            <Text
              style={{
                color: Colors.black70,
                fontSize: scale(12),
                marginLeft: 15,
              }}>
              {shipment.receiver &&
                shipment.receiver.warehouse &&
                shipment.receiver.warehouse.warehouseAddress
                ? shipment.receiver.warehouse.title +
                ' / ' +
                shipment.receiver.warehouse.warehouseAddress.firstLine +
                ' ' +
                shipment.receiver.warehouse.warehouseAddress.city
                : ''}
            </Text>
          </View>
        </Svg>
      </View>
    </View>
  );
}
export const ChainOfStudyOrder = (props) => {
  const { t, i18n } = useTranslation();

  let { poChainOfCustody } = props;
  return (
    <View>
      <View style={{ flexDirection: 'row', margin: 10, alignItems: 'center' }}>
        <EntypoIcon name="circle" color={Colors.blue1EA} size={15} />
        <Text
          style={{
            fontSize: scale(12),

            color: Colors.blue1EA,
            fontWeight: 'bold',
            marginLeft: 10,
          }}>
          {t('Order Placed')}
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
        }}>
        <View style={{ flexDirection: 'row' }}>
          <Text
            style={{

              marginLeft: 8,
              width: '55%',
              color: Colors.black0,
              fontWeight: 'bold',
            }}>
            {t('Order Placed')}
          </Text>
          <Text style={{ alignSelf: 'flex-end' }}>
            {formatDateDDMMYYYY(poChainOfCustody.createdAt)}
          </Text>
        </View>
        <CustomTextView
          leftText={t('Order ID')}
          rightText={poChainOfCustody.id || ''}
        />
        <CustomTextView
          leftText={t('From')}
          rightText={
            (poChainOfCustody.supplier &&
              poChainOfCustody.supplier.organisation &&
              poChainOfCustody.supplier.organisation.name) ||
            ''
          }
        />
        <CustomTextView
          leftText={t('To')}
          rightText={
            (poChainOfCustody.customer &&
              poChainOfCustody.customer.organisation &&
              poChainOfCustody.customer.organisation.name) ||
            ''
          }
        />
      </View>
    </View>
  );
}
export const ChainOfShipment = (props) => {
  const { t, i18n } = useTranslation();
  const [isExpend, setExpend] = useState(false);

  let { each, shipmentDetails, image, } = props;
  return (
    <View>
      <View style={{ flexDirection: 'row', margin: 10, alignItems: 'center' }}>
        <EntypoIcon name="circle" color={Colors.blue1EA} size={15} />

        <Text
          style={{
            fontSize: scale(12),

            color: Colors.blue1EA,
            fontWeight: 'bold',
            marginLeft: 10,
          }}>
          {each.status === 'CREATED'
            ? t('SHIPPED')
            : each.status === 'RECEIVED'
              ? t('DELIVERED')
              : t(each.status)}
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          borderRadius: 20,
          padding: 10,
        }}>
        <View style={{ flexDirection: 'row' }}>
          <Text
            style={{

              marginLeft: 8,
              width: '55%',
              color: Colors.black0,
              fontWeight: 'bold',
            }}>
            {t('Shipment') + ' ' + getStatus(each.status, t)}
          </Text>
         
          <Text style={{ alignSelf: 'flex-end' }}>{formatDateDDMMYYYYTime(each.updatedOn)}</Text>
        </View>
        <CustomTextView
          leftText={t('Shipment ID')}
          rightText={shipmentDetails.id || ''}
        />
        {each.status === 'CREATED' ? (
          <View>
            <Text
              style={{
                fontSize: scale(12),

                fontWeight: 'bold',
                color: Colors.black0,
                marginLeft: 10,
              }}>
              {t('From')}
            </Text>
            <CustomTextView
              leftText={t('Organization Name')}
              rightText={
                (shipmentDetails.supplier &&
                  shipmentDetails.supplier.org &&
                  shipmentDetails.supplier.org.name) ||
                ''
              }
            />
            <CustomTextView
              leftText={t('Organization Location')}
              rightText={
                shipmentDetails.supplier &&
                  shipmentDetails.supplier.warehouse &&
                  shipmentDetails.supplier.warehouse.warehouseAddress
                  ? shipmentDetails.supplier.warehouse.title +
                  ' / ' +
                  shipmentDetails.supplier.warehouse.warehouseAddress
                    .firstLine +
                  ' ' +
                  shipmentDetails.supplier.warehouse.warehouseAddress.city
                  : ''
              }
            />
          </View>
        ) : null}
        {each.status === 'UPDATED' ? (
          <View>
            <CustomTextView
              leftText={t('By')}
              rightText={each.updatedBy || ''}
            />
            <CustomTextView
              leftText={t('Organization Name')}
              rightText={each.orgid || each.orgId || ''}
            />
            <CustomTextView
              leftText={t('Organization Location')}
              rightText={each.updatedAt || each.orgLocation || ''}
            />
          </View>
        ) : (
          <View>
            <Text
              style={{
                fontSize: scale(12),

                fontWeight: 'bold',
                color: Colors.black0,
                marginLeft: 10,
              }}>
              {t('To')}
            </Text>
            <CustomTextView
              leftText={t('Organization Name')}
              rightText={
                (shipmentDetails.receiver &&
                  shipmentDetails.receiver.org &&
                  shipmentDetails.receiver.org.name) ||
                ''
              }
            />
            <CustomTextView
              leftText={t('Organization Location')}
              rightText={
                shipmentDetails.receiver &&
                  shipmentDetails.receiver.warehouse &&
                  shipmentDetails.receiver.warehouse.warehouseAddress
                  ? shipmentDetails.receiver.warehouse.title +
                  ' / ' +
                  shipmentDetails.receiver.warehouse.warehouseAddress
                    .firstLine +
                  ' ' +
                  shipmentDetails.receiver.warehouse.warehouseAddress.city
                  : ''
              }
            />
          </View>
        )}

        {isExpend ? (
          <View>
            <Text
              style={{

                color: Colors.black0,
                fontWeight: 'bold',
                marginLeft: 10,
                marginRight: 10,
              }}>
              {t('Comment')}
            </Text>
            <View style={{ borderStyle: 'dotted' }}>
              <Text
                style={{

                  color: '#707070',
                  marginLeft: 10,
                  marginRight: 10,
                }}>
                {typeof each.updateComment === 'string'
                  ? each.updateComment
                  : 'N/A'}
              </Text>
            </View>

            {/* {console.log('url  index ' + each.image)} */}
            {each.image ? (
              <Image
                style={{
                  height: 240,
                  borderStyle: 'dotted',
                }}
                resizeMode="cover"
                source={{
                  uri: each.image,
                  // headers: {
                  //   'Conten t-Type': 'application/json',
                  //   Authorization: 'Bearer ' + props.token,
                  // },
                  // method: 'GET',
                }}
              />
            ) : null}
          </View>
        ) : null}
      </View>
      {each.status !== 'CREATED' ? (
        <TouchableOpacity
          style={{
            alignItems: 'flex-end',
            justifyContent: 'flex-end',
            marginTop: -20,
          }}
          onPress={() => setExpend(!isExpend)} >
          <View
            style={{
              width: 30,
              height: 30,
              backgroundColor: isExpend
                ? Colors.blueE9
                : Colors.whiteFF,
              borderRadius: 20,
            }}>
            <IconEn
              name={!isExpend ? 'chevron-down' : 'chevron-up'}
              size={30}
              color={!isExpend ? Colors.blueE9 : Colors.whiteFF}
            />
          </View>
        </TouchableOpacity>
      ) : null}
    </View>
  );
}
