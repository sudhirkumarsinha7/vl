export const CustomDropdown = (props) => {
    const {
      dropDowndata,
      label = '',
      image = '',
      mapKey = '',
      search = false,
      mandatory = false,
      displayName,
      stateName,
      t
    } = props;
   
    return (
      <View
        style={{
          marginTop: 10,
          borderBottomWidth:1,
          borderColor:'gray',
          borderRadius:15
        }}>
  
        <ElDropdown
          data={dropDowndata.map(eachElement => {
            return {
              value: eachElement[mapKey] || eachElement,
              label: eachElement[label] || eachElement[mapKey] || eachElement,
              eachItem: eachElement
            };
          })}
          search
          maxHeight={300}
          labelField="label"
          valueField="value"
          searchPlaceholder="Search..."
          style={styles.dropdown}
          placeholderStyle={{
            color: Colors.grayA8,
          }}
          selectedTextStyle={styles.selectedTextStyle}
          inputSearchStyle={styles.inputSearchStyle}
          boderStyle={styles.borderStyle1}
          value={stateName || null}
          placeholder={t("Select") + ' ' + t(displayName)}
          activeColor={Colors.blueE9}
          renderLeftIcon={() => (
            <Image
              style={{ width: scale(15), height: scale(15), margin: 5 }}
              source={image}
              resizeMode="contain"
            />
          )}
          onChange={val => props.onChangeValue(val)}
          searchable={search}
        />
        {mandatory && !stateName ? <Text
          style={{
            color: Colors.red23,
            marginLeft: scale(5),
            marginBottom: scale(5),
          }}>
          {t('Select') + ' ' + t(displayName)}
        </Text> : null}
      </View>
    );
  }