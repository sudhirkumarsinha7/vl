import React, { useState, useEffect } from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  ActivityIndicator,
  Vibration,
  Dimensions,
  KeyboardAvoidingView,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  StatusBar,
  Switch,
  Alert,
  Linking,
} from 'react-native';import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import Header, { HeaderWithBackBlue } from '../../components/Header';
import Permissions from 'react-native-permissions';
import { useTranslation } from 'react-i18next';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import PopUp, { PopUp1 } from '../../components/PopUp';
import { Colors } from '../../Style';
import {CameraScreen} from 'react-native-camera-kit';
import LinearGradient from 'react-native-linear-gradient';
import _ from 'lodash';
import { config } from '../../Util/config';
import {ToastShow} from '../../components/Toast'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
export const CustomScanner =(props)=>{
    const [opneScanner,setOpneScanner] = useState(false);
    const { t, i18n } = useTranslation();
    const {index=0,products}= props
    useEffect(() => {
      async function fetchData() {
  
      }
      fetchData();
    }, []);
   
    const onOpneScanner=()=> {
      setOpneScanner(true)

    }

   const onBarcodeScan=(qrvalue)=>{
    console.log('onBarcodeScan result baseUrl' + qrvalue);
    console.log('qrvalue' + qrvalue);
    let baseUrl = 'http://';
    qrvalue = qrvalue.replace(baseUrl, '');
    //called after te successful scanning of QRCode/Barcode

    console.log('onBarcodeScan result baseUrl' + qrvalue);
    // qrvalue = qrvalue.replaceAll('\u001d', '')
    qrvalue = qrvalue.replace('\u001d', '')

    // qrvalue = replaceAll(qrvalue, '\u001d', '');
    console.log('onBarcodeScan result qrvalue ' + qrvalue);
    Vibration.vibrate(30);
    let newIndex = index;

    let comp = qrvalue.startsWith('01');
    if (comp) {
      let compId = qrvalue.slice(2, 16);
      let rem1 = qrvalue.slice(16);
      let check = rem1.substr(0, 2);
      let expdateId = '';
      let serialId = '';
      let rem3;
      let rem2;
      let batch = '';
      let check2;
      let check3;
      let MM;
      let YYYY;
      let expD;
      if (check === '17') {
        expdateId = rem1.slice(2, 8);
        rem2 = rem1.slice(8);
        check2 = rem2.substr(0, 2);
        if (check2 === '21') {
          serialId = rem2.slice(2, 22);
          rem3 = rem2.slice(22);
          check3 = rem3.substr(0, 2);
          console.log('onBarcodeScan result check3 ' + rem3);
          batch = rem3.slice(2);
          if (check3 === '10') {
            batch = rem3.slice(2);
            // console.log('onBarcodeScan result check3 10 ' + batch);
          }
        } else if (check2 === '10') {
          batch = rem3.slice(2, 7);
          rem3 = rem2.slice(7);
          check3 = rem3.substr(0, 2);
          if (check3 === '21') {
            serialId = rem3.slice(2);
          }
        }
      } else if (check === '21') {
        serialId = rem1.slice(2, 22);
        rem2 = rem1.slice(22);
        check2 = rem2.substr(0, 2);
        if (check2 === '17') {
          expdateId = rem2.slice(2, 8);
          rem3 = rem2.slice(8);
          check3 = rem3.substr(0, 2);
          batch = rem3.slice(2);
          if (check3 === '10') {
            batch = rem3.slice(2);
          }
        } else if (check2 === '10') {
          batch = rem3.slice(2, 7);
          rem3 = rem2.slice(7);
          check3 = rem3.substr(0, 2);
          expdateId = rem3.slice(2);
          if (check3 === '17') {
            expdateId = rem3.slice(2);
          }
        }
      }
      if (expdateId) {
        YYYY = expdateId.slice(0, 2);
        YYYY = '20' + YYYY;
        MM = expdateId.slice(2, 4);
        expD = MM + '/' + YYYY;
      }
      console.log('company id ' + compId);
      console.log('expire date ' + expdateId);
      console.log('serialId  ' + serialId);
      console.log('batch ' + batch);
      let checkBatch = false;
      let index = newIndex;
      if (batch) {
        for (let j = 0; j < products.length; j++) {
          console.log('products[j].batchNumber ' + products[j].batchNumber);
          if (products[j].batchNumber === batch) {
            console.log('find batch number ' + products[j].batchNumber);
            checkBatch = true;
            index = j;
          } else {
          }
        }
      }
      console.log('index ' + index);
      if (checkBatch) {
        if (products[index].batchNumber === batch) {
          let existSerialNo = products[index].serialNumbersRange;
          let newSerial = [serialId];
          let newSerialNumlist = [...existSerialNo, ...newSerial];
          newSerialNumlist = _.uniqBy(newSerialNumlist, function(x) {
            return x;
          });
          products[index].serialNumbersRange = newSerialNumlist;
          products[index].productQuantity = newSerialNumlist.length;
          console.log('products 1st ' + JSON.stringify(products));
          // this.setState({products});
          props.updatePRoduct(products)

        } else if (products[index].batchNumber !== batch) {
          let newList = [];
          let addData = {
            productCategory: products[index].productCategory,
            productID: products[index].productID,
            productName: products[index].productName,
            manufacturer: products[index].manufacturer,
            batchNumber: products[index].batchNumber,
            quantity: products[index].batchNumber,
            serialNumbersRange: [serialId],
            expDate: expD,
          };
          newList.push(addData);
          let newDataList = [...products, ...newList];
          let nindx = index + 1;
          console.log('products 2nd ' + JSON.stringify(products));
          props.updatePRoduct(newDataList)
          // this.setState({products: newDataList, index: nindx});
        } else {
          products[index].productQuantity = '1';
          products[index].batchNumber = batch;
          products[index].serialNumbersRange = [serialId];
          products[index].expDate = expD;
          console.log('products 3rd ' + JSON.stringify(products));
          props.updatePRoduct(products)

          // this.setState({products});
        }
      } else {
        if (products[index].batchNumber != null) {
          index = newIndex + 1;
        }
        console.log('new index ' + index);
        // console.log('products 5th+1 ' + JSON.stringify(products[index].productID))
        if (index === 0) {
          products[index].productQuantity = '1';
          products[index].batchNumber = batch;
          products[index].expDate = expD;
          products[index].serialNumbersRange = [serialId];
          console.log('products 4th ' + JSON.stringify(products));
          props.updatePRoduct(products)

          // this.setState({products});
        } else {
          // console.log('products 5th-1 ' + JSON.stringify(products[index - 1].productID))

          if (products[index] && products[index].productID) {
            products[index].productQuantity = '1';
            products[index].batchNumber = batch;
            products[index].expDate = expD;
            products[index].serialNumbersRange = [serialId];
            console.log('products 5th ' + JSON.stringify(products));
          } else {
            let newList = [];
            let addData = {
              productCategory: products[index - 1].productCategory,
              productID: products[index - 1].productID,
              productName: products[index - 1].productName,
              manufacturer: products[index - 1].manufacturer,
              batchNumber: batch,
              quantity: '1',
              expDate: expD,
              productQuantity: '1',
              serialNumbersRange: [serialId],
            };
            newList.push(addData);
            let newDataList = [...products, ...newList];
            let nindx = index;
            console.log('products 6th ' + JSON.stringify(products));
            props.updatePRoduct(newDataList)

            // this.setState({products: newDataList, index: nindx});
          }
        }
      }
    } else {
      alert(t('Scan a valid QR'));
    }
    setOpneScanner(false)
   }
    if(!opneScanner){return <View>
   {products[index].productID ?<View  style={{
      flexDirection: 'row',
      alignSelf: 'flex-end',
      marginBottom: 10,
    }}>
    <TouchableOpacity
      style={{
        marginTop: verticalScale(5),
        width: scale(90),
        height: scale(35),
        marginRight: scale(20),
        borderRadius: 8,
        borderWidth: 1,
        borderColor:props.enable ? '#D6D6D6' : '#0093E9',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        alignSelf: 'flex-end',
      }}
      onPress={() => cancel()}
      disabled={props.enable? false : true}>
      <Text
        style={{
          fontSize: scale(14),
          color: props.enable ? '#D6D6D6' : '#0093E9',
          fontWeight: 'bold',
        }}>
        {t('Cancel')}
      </Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={{
        marginTop: verticalScale(5),
        width: scale(90),
        height: scale(35),
        borderRadius: 8,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        alignSelf: 'flex-end',
      }}
      onPress={() => onOpneScanner()}>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        colors={['#0093E9', '#36C2CF']}
        style={{
          marginTop: verticalScale(45),
          width: scale(90),
          height: scale(35),
          borderRadius: 8,
          borderWidth: 1,
          borderColor: '#0093E9',
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          alignSelf: 'flex-end',
        }}>
        <Image
          style={{ width: scale(15), height: scale(15), tintColor: 'white' }}
          source={require('../../assets/Scan.png')}
        />
        <Text
          style={{
            fontSize: scale(14),
            color: '#FFFFFF',
            fontWeight: 'bold',
            marginLeft: 5
          }}>
          {t('SCAN')}
        </Text>
      </LinearGradient>
    </TouchableOpacity>
    </View>:null}
   
  </View>}
  return (
    <View style={{
      // flexDirection: 'row',
      // alignSelf: 'flex-end',
      // marginBottom: 10,
      // backgroundColor:'black',
      // height: Platform.OS === 'ios' ? 400: 10,

      alignItems: 'center',
      justifyContent: 'center',
    }}>
      
      <CameraScreen
        actions={{
          leftButtonText: (
            <Icon name="close" style={{color: 'red', fontSize: 20}} />
          ),
          rightButtonText: (
            <Icon name="images" style={{color: 'red', fontSize: 20}} />
          ),
        }}
        // style={{height: Platform.OS === 'ios' ? DeviceHeight / 1.2 : '1%'}}
        style={{height: Platform.OS === 'ios' ? 300: 300,width:300,marginTop:Platform.OS==='ios'?-200: -200,}}

        onBottomButtonPressed={event =>
         setOpneScanner(false)
        }
        showFrame={true}
        //Show/hide scan frame
        scanBarcode={true}
        //Can restrict for the QR Code only
        laserColor={'green'}
        //Color can be of your choice
        frameColor={'green'}
        //If frame is visible then frame color
        colorForScannerFrame={'green'}
        //Scanner Frame color
        onReadCode={event =>
          onBarcodeScan(event.nativeEvent.codeStringValue)
        }
      />
    </View>
  );
}

export const CustomQrcodeScanner =(props)=>{
  const [opneScanner,setOpneScanner] = useState(false);
  const [reqData,setReqData] =useState({})
  const [isModalVisible2,setModalVisible2] =useState(false)
  const [errMsg,setErrorMsg] =useState('')
  const [userToken,setToken] =useState('')
  const { t, i18n } = useTranslation();
  useEffect(() => {
    async function fetchData() {
      const token = await AsyncStorage.getItem('token');
      setToken(token)
      // onOpneScanner()
    }
    fetchData();
  }, []);
 
  const onOpneScanner=()=> {
    setOpneScanner(true)
   

  }

  const onBarcodeScan =(qrvalue) =>{
    //called after te successful scanning of QRCode/Barcode
    console.log('qr', qrvalue);
    let baseUrl = 'http://'
    let newval = qrvalue.replace(baseUrl, "");
    console.log('qr', newval);
    Vibration.vibrate(30);
    setOpneScanner(false)

    getDetails(newval);
    // }
  }
  const getDetails =async (id)=>{
    let url = config().fetchShipmentsByQRCode + id
    fetch(url, {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + userToken,
  },
}).then(response => response.json())
.then(result => {
  console.log('Success:'+JSON.stringify(result));
  if(result.success){
    const ShipmentDetails = (result.data && result.data.shipments &&result.data.shipments.length &&
      result.data.shipments[0] || {});
      if (ShipmentDetails.createdAt) {
        if (ShipmentDetails.status !== 'RECEIVED') {

          delete ShipmentDetails.__v;
          delete ShipmentDetails.updatedAt;
          delete ShipmentDetails.createdAt;
          delete ShipmentDetails._id;
          // shimentId = ShipmentDetails.id;
         
          let modifiedProduct = ShipmentDetails.products.map(each => {
            each.isEditQty = true;
            each.isUserQty = false;
            each.productQuantityDelivered = each.productQuantity
              ? each.productQuantity + ''
              : '';
            each.productQuantitySent = each.productQuantity
              ? each.productQuantity + ''
              : '';
            return each;
          });
          ShipmentDetails.products = modifiedProduct;
          props.updateProductQty(ShipmentDetails)
          props.closeScanner()
        } else {
          ToastShow(
            t('Shipment  is already received') + ' Id ' + ShipmentDetails.id,
            'error',
            'long',
            'top',
          )
        }
      }
  }else{
    if(result.message && result.message.requestType){
              let reqData1 = {
            "labelId": id,
            "type": result.message.requestType
          }
          setModalVisible2(true)
          setReqData(reqData1)
          setErrorMsg(result.message.message)
    }
  }
})
  }

  const fail = () => {
    setModalVisible2(false)
  }
  const sendRequest = async () => {
    try {
      console.log(
        'Action_sendRequest_reqData' + JSON.stringify(reqData),
      );
      setModalVisible2(false)

      const res = await axios.post(config().createRequest, reqData);
      console.log(
        'Action_sendRequest_response' + JSON.stringify(res),
      );

      const smsg1 = "Request sended succcuessfully"
      ToastShow(
        smsg1,
        'success',
        'long',
        'top',
      )

    } catch (e) {
      console.log(
        'Action_sendRequest_error' + JSON.stringify(e),
      );
      ToastShow(
        t(JSON.stringify(e.response)),
        'error',
        'long',
        'top',
      )
    }
  }
 
  // console.log('props.token, '+userToken)
  if(!opneScanner){return <View
    style={{
      backgroundColor: '#FFFFFF',
      margin: verticalScale(10),
      borderRadius: 10,
      padding: 10,
      alignItems: 'center',
      justifyContent: 'center',

    }}>
      <PopUp1
                isModalVisible={isModalVisible2}
                image={require('../../assets/fail.png')}
                text={t("Unauthorized Scan!")}
                text2={
                  errMsg ||
                  t('You don’t have permission to scan this shipment.Would you like to send an access request?')
                }
                label={t("Cancel")}
                label1={t("SEND REQUEST")}
                Ok={() => sendRequest()}
                cancel={() => fail()}
              />
    <Image
      style={{
        width: scale(150),
        height: scale(150),
        alignSelf: 'center',
      }}
      source={require('../../assets/blackberry.png')}
      resizeMode="contain"
    />
    <View
      style={{
        flexDirection: 'row',
        marginTop: 10,
      }}>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          backgroundColor: '#0093E9',
          padding: 8,
          borderRadius: 10,
        }}
        onPress={() => onOpneScanner()}>
        <MaterialCommunityIcons name="qrcode-scan" color="white" size={20} />
        <Text
          style={{
            color: 'white',
            fontSize: 16,
            fontWeight: 'bold',
            alignItems: 'center',
          }}>
          {t('Scan')}
        </Text>
      </TouchableOpacity>
    </View>
  </View>}
return (
  <View style={{
    // margin: verticalScale(10),
    alignItems: 'center',
    justifyContent: 'center',
    // marginRight:10
    height: Platform.OS === 'ios' ? 300: 100,
  }}>
    
    <CameraScreen
      actions={{
        leftButtonText: (
          <Icon name="close" style={{color: 'red', fontSize: 20}} />
        ),
        rightButtonText: (
          <Icon name="images" style={{color: 'red', fontSize: 20}} />
        ),
      }}
      // style={{height: Platform.OS === 'ios' ? 300: '1%',width:300,}}
      style={{height: Platform.OS === 'ios' ? 300: 300,width:300,marginTop:Platform.OS==='ios'?0: -200,}}
      onBottomButtonPressed={event =>
       setOpneScanner(false)
      }
      showFrame={true}
      //Show/hide scan frame
      scanBarcode={true}
      //Can restrict for the QR Code only
      laserColor={'green'}
      //Color can be of your choice
      frameColor={'green'}
      //If frame is visible then frame color
      colorForScannerFrame={'green'}
      //Scanner Frame color
      onReadCode={event =>
        onBarcodeScan(event.nativeEvent.codeStringValue)
      }
    />
  </View>
);
}



