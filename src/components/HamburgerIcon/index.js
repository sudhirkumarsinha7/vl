import React, { Component } from 'react';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { scale, moderateScale, verticalScale } from '../Scale';
import {
  TouchableOpacity,
  View,
  Text,
  Image,
  Dimensions,
  TouchableHighlight,
} from 'react-native';

const HamburgerIcon=(props) =>{
    return (
      <TouchableOpacity
        style={{
          width: scale(16),
          height: scale(14),

          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => {
          props.getDetails();
          props.navigation.toggleDrawer();
        }}>
        <Image
          style={{ width: scale(22), height: scale(22), borderWidth: 0 }}
          source={require('../../assets/Menu.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
    );
}
export default HamburgerIcon;
