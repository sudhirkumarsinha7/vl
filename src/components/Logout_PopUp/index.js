import React, { Component } from 'react';
import { FlatList, Button, View, Text, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import { useTranslation } from 'react-i18next';
const PopUp = (props) => {
    const { t, i18n } = useTranslation();
    return (
        <Modal
            isVisible={props.isModalVisible}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
            }}
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}>
            <View style={{ width: scale(250), height: scale(220), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontSize: scale(20), textAlign: 'center', }}>{t('Log Out')}</Text>
                <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(50), color: "#707070", marginRight: scale(20), marginLeft: scale(20) }}>{t('Are you sure you want to logout?')}</Text>
                <View style={{ width: scale(230), flexDirection: "row", marginTop: verticalScale(50), alignItems: "center", justifyContent: "space-between" }}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 10,
                            width: scale(105),
                            height: scale(35),
                        }}
                        onPress={props.Cancel}>
                        <LinearGradient
                            start={{ x: 0.1, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                            locations={[0, 0.5, 0.6]}
                            colors={['#0093E9', '#36C2CF']}
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 10,
                                width: scale(105),
                                height: scale(35),
                            }}>
                            <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{t('Cancel')}</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 10,
                            width: scale(105),
                            borderColor: "#0091E6",
                            backgroundColor: "#FFFFFF",
                            borderWidth: 1,
                            height: scale(35),
                        }}
                        onPress={props.Logout}>
                        <Text style={{ color: '#0093E9', fontSize: scale(15) }}>{t('Log out')}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </Modal>
    );
}

export default (PopUp);