import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  BackHandler,
  View,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import '../../languages/i18n'
import { useTranslation } from 'react-i18next';
import { compose } from 'redux';
const Empty_Card = (props)=> {
  const { t, i18n } = useTranslation();


    return (
      <View
        style={{
          marginTop: verticalScale(20),
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
          // width: scale(328),
          margin: 15,
          alignItems: 'center',
          justifyContent: 'center',
          height: scale(421),
        }}>
        <Text
          style={{ fontSize: scale(20), width: scale(190), textAlign: 'center' }}
          numberOfLines={2}>
          {props.isYou ? t('Looks like') : t('')}
        </Text>
        <Text
          style={{ fontSize: scale(20), width: scale(190), textAlign: 'center' }}
          numberOfLines={2}>
          {t(props.Text)}
        </Text>
        <Image
          style={{
            width: scale(260),
            height: scale(211),
            marginTop: verticalScale(11),
          }}
          resizeMode="cover"
          source={require('../../assets/Empty.png')}
        />
      </View>
    );
}
export default Empty_Card;
