/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Button,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {scale, moderateScale, verticalScale} from '../Scale';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import axios from 'axios';
import {config} from '../../config';
import {CustomTextView,CustomInputText1} from '../Common/common';
import {formatDateDDMMYYYY,formatDateMMYYYY} from '../../Util/utils';
import { useTranslation } from 'react-i18next';
import { Colors, DeviceWidth,DeviceHeight } from '../../Style';
import { ToastShow } from '../Toast';
const PopUp = props => {
  return (
    <Modal
      isVisible={props.isModalVisible}
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
      }}
      animationInTiming={1000}
      animationOutTiming={1000}
      backdropTransitionInTiming={1000}
      backdropTransitionOutTiming={1000}>
      <View
        style={{
          width: scale(250),
          height: scale(300),
          backgroundColor: '#ffffff',
          borderRadius: 20,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          style={{width: scale(46.16), height: scale(46.16)}}
          source={props.image}
          resizeMode="contain"
        />

        <Text
          style={{
            fontSize: scale(20),
            textAlign: 'center',
            marginTop: verticalScale(48),
            
          }}>
          {props.text}
        </Text>

        <Text
          style={{
            fontSize: scale(15),
            textAlign: 'center',
            marginTop: verticalScale(20),
            
            color: '#707070',
            marginRight: scale(27),
            marginLeft: scale(27),
          }}>
          {props.text2}
        </Text>

        <TouchableOpacity
          style={{
            justifyContent: 'center',

            alignItems: 'center',

            borderRadius: 10,
            padding: scale(10),
            marginTop: verticalScale(44),
          }}
          onPress={props.Ok}>
          <LinearGradient
            start={{x: 0.1, y: 1.0}}
            end={{x: 1.0, y: 0.0}}
            locations={[0, 0.5, 0.6]}
            colors={['#0093E9', '#36C2CF']}
            style={{
              justifyContent: 'center',

              alignItems: 'center',

              borderRadius: 10,
              padding: scale(10),
            }}>
            <Text
              style={{
                color: '#FFFFFF',
                
                fontSize: scale(15),
              }}>
              {props.label}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};
export default PopUp;
export const PopUp1 = props => {
  return (
    <Modal
      isVisible={props.isModalVisible}
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
      }}
      animationInTiming={1000}
      animationOutTiming={1000}
      backdropTransitionInTiming={1000}
      backdropTransitionOutTiming={1000}>
      <View
        style={{
          width: scale(250),
          height: scale(300),
          backgroundColor: '#ffffff',
          borderRadius: 20,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          style={{width: scale(46.16), height: scale(46.16)}}
          source={props.image}
          resizeMode="contain"
        />

        <Text
          style={{
            fontSize: scale(20),
            textAlign: 'center',
            marginTop: verticalScale(20),
            
          }}>
          {props.text}
        </Text>

        <Text
          style={{
            fontSize: scale(15),
            textAlign: 'center',
            marginTop: verticalScale(20),
            
            color: '#707070',
            marginRight: scale(27),
            marginLeft: scale(27),
          }}>
          {props.text2}
        </Text>

        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            margin: 10,
            marginTop: verticalScale(44),
            justifyContent: 'space-between',
          }}>
          {/* <View style={{ flex: 0.4, }}> */}
          <TouchableOpacity
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              // width: scale(105),
              // padding: 15,
              // height: scale(35),
            }}
            onPress={props.cancel}>
            <LinearGradient
              start={{x: 0.1, y: 1.0}}
              end={{x: 1.0, y: 0.0}}
              locations={[0, 0.5, 0.6]}
              colors={['white', 'white']}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#0093E9',
                width: scale(105),
                padding: 10,
                // height: scale(35),
              }}>
              <Text
                style={{
                  color: '#0093E9',
                  
                  fontSize: scale(12),
                }}>
                {props.label}
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          {/* </View> */}
          {/* <View style={{ flex: 0.6, marginLeft: 10 }}> */}
          <TouchableOpacity
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 10,
              marginLeft: 10,
              // width: scale(125),
              // height: scale(40),
            }}
            onPress={props.Ok}>
            <LinearGradient
              start={{x: 0.1, y: 1.0}}
              end={{x: 1.0, y: 0.0}}
              locations={[0, 0.5, 0.6]}
              colors={['#0093E9', '#36C2CF']}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
                padding: 10,

                width: scale(125),
                // height: scale(35),
              }}>
              <Text
                style={{
                  color: '#FFFFFF',
                  
                  fontSize: scale(12),
                }}>
                {props.label1}
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          {/* </View> */}
        </View>
      </View>
    </Modal>
  );
};

export const BatchPoup = props => {
  const { t, i18n } = useTranslation();

  // const [batchDetails, setBatchDetails] = useState([]);
  const {
    selectedProductData = {},
    selectedIndx,
    list,
    updateState,
    batchDetails,
    label,
    
  } = props;

 
  const eacBatch = rowData => {
    return (
      <BatchPopData
        item={rowData}
        selectedProduct={selectedProductData}
        productList={list}
        selectedIndx={selectedIndx}
        updateState={updateState}
       Ok={ props.Ok}
      />
    );
  };
  const showEmptyListView = () => {
    return (
      <View style={{justifyContent:'center',alignItems:'center'}}>
        <Text style={{fontSize:scale(20)}}>{t('No Records Found')}</Text>
      </View>
    );
  };
  return (
    <Modal
      isVisible={props.isModalVisible}
      style={{
        borderRadius: 5,
        marginTop: 50

      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          borderRadius: 5,
          height: DeviceHeight / 1.3
        }}>
          <Text style={{color:Colors.blueE9,fontSize:scale(20),margin:scale(20)}}>{t('Fetch Batch Details')}</Text>
        <FlatList
          onEndReachedThreshold={0.7}
          style={{ width: '100%' }}
          keyExtractor={(item, index) => index.toString()}
          data={batchDetails}
          renderItem={({ item }) => eacBatch(item)}
          ListEmptyComponent={showEmptyListView()}
        />
        <TouchableOpacity
          style={{ justifyContent: 'center', alignItems: 'center', padding: 10, borderColor: Colors.blueE9, margin: 20, borderRadius: 5,borderWidth:1 }}
          onPress={props.Ok}>
          <Text style={{ fontSize: scale(16),fontWeight:'bold',color:Colors.blueE9 }}>{label}</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

export const BatchPopData = props => {
  const { t, i18n } = useTranslation();

  const [SelectedBatch, SetSelectedBatch] = useState(false);
  const [Qty, SetQty] = useState('');
  const [editQty, SetEditQty] = useState(false);

  const { item = {}, selectedIndx, updateState, selectedProduct, } = props;
  const { attributeSet = {} } = item;
  const {unitofMeasure={}}=selectedProduct;
  const {name=''}=unitofMeasure;

  useEffect(() => {
    async function fetchData() {
      const Pqty = item.quantity;
      SetQty(Pqty)
    }
    fetchData();
  }, []);
  const selectCard = () => {
    SetSelectedBatch(!SelectedBatch);
  };
  const UpdateBatch = () => {
    let { productList } = props;
    if (Qty) {
    productList[selectedIndx].batchNumber =
      item.batchNumbers && item.batchNumbers.length && item.batchNumbers[0];
    productList[selectedIndx].expDate = formatDateMMYYYY(
      attributeSet.mfgDate,
    );
    productList[selectedIndx].mfgDate = formatDateMMYYYY(
      attributeSet.expDate,
    );
   
      if(Number(productList[selectedIndx].MaxQty)>=Number(Qty)){
        productList[selectedIndx].productQuantity = Qty;
        updateState(productList);
        props.Ok()
      }else{
        ToastShow(
          t('Quantity cannot be more than ordered quantity '+productList[selectedIndx].productQuantity + ' qt '+Number(Qty)),
            'error',
            'long',
            'top',
          )
      }    
    }else{
      ToastShow(
        t('Quantity Required'),
          'error',
          'long',
          'top',
        )
    }
  };
  const updateQty=(val)=>{
    if(item.quantity>=Number(val)){
      SetQty(val)
    }
  }
  // console.log('selectedProduct Product'+ JSON.stringify(selectedProduct))
  return (
    <TouchableOpacity
      style={{
        borderWidth: 1,
        margin: 5,
        backgroundColor: SelectedBatch ? Colors.blueFA : Colors.whiteFF,
        borderColor: Colors.blueE9
      }}
      onPress={() => selectCard()}>
      <CustomTextView
        leftText={'Product Name'}
        rightText={selectedProduct.productName}
        t={t}
      />
      <CustomTextView
        leftText={'Manufature'}
        rightText={selectedProduct.manufacturer}
        t={t}
      />

      <CustomTextView
        leftText={'Batch No'}
        rightText={
          item.batchNumbers && item.batchNumbers.length && item.batchNumbers[0]
        }
        t={t}
      />

      {editQty ? <CustomInputText1 keyboardType={'numeric'} leftText={'Quantity '+item.quantity+' ('+name+')'} rightText={Qty} onChangeValue={(val) =>updateQty(val)} t={t} />
        : <CustomTextView leftText={'Quantity'} rightText={Qty+' ('+name+')'} t={t} />}
      {attributeSet.mfgDate?<CustomTextView 
        leftText={'Mfg Date'}
        rightText={formatDateMMYYYY(attributeSet.mfgDate)}
      />:null}
     {attributeSet.expDate? <CustomTextView
        leftText={'Exp Date'}
        rightText={formatDateMMYYYY(attributeSet.expDate)}
      />:null}
      {SelectedBatch ? <View style={{ flexDirection: 'row', marginBottom: scale(10) }}>
        <View style={{ flex: 0.5 }} />
        <View style={{ flexDirection: 'row', alignItems: 'flex-end', flex: 0.5 }}>
          <TouchableOpacity
            style={{ justifyContent: 'center', alignItems: 'center', paddingLeft: 10, paddingRight: 10, padding: 5, backgroundColor: Colors.orange1D, borderRadius: 5 }}
            onPress={() => SetEditQty(true)}>
            <Text style={{ fontSize: scale(16) }}>{t('edit')}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ justifyContent: 'center', alignItems: 'center', paddingLeft: 10, paddingRight: 10, padding: 5, marginLeft: scale(20), backgroundColor: Colors.blueE9, borderRadius: 5 }}
            onPress={() => UpdateBatch()}>
            <Text style={{ fontSize: scale(16) }}>{t('Next')}</Text>
          </TouchableOpacity>
        </View>
      </View> : null}
    </TouchableOpacity>
  );
};
