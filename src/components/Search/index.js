import React, { memo, useCallback, useRef, useState } from 'react'
import { View, Text, Image } from 'react-native';
import { FilterData } from '../Common/defaultValue';
import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown';
import { Colors, DeviceHeight, DeviceWidth } from '../../Style';
import { useTranslation } from 'react-i18next';
import axios from 'axios';
import { scale, verticalScale } from '../Scale';
import { config } from '../../Util/config'
import { connect } from 'react-redux';
import {ToastShow} from '../Toast'
import {
  getViewShipment,
  searchProductByType,
  searchProductByName,
  getOrderDetails,
} from '../../Redux/Action';

import {
  getchainOfCustody,

}from '../../Redux/Action/shipment';
import { delay } from 'lodash';
export const GloblaSearchBox = (props) => {
  const { t, i18n } = useTranslation();

  const [loading, setLoading] = useState(false)
  const [suggestionsList, setSuggestionsList] = useState([])
  const dropdownController = useRef(null)

  const searchRef = useRef(null)

  const getData = async (text) => {
    setLoading(true);
    setSuggestionsList([])
    try {
      let response = await axios.get(config().getsearchsuggestions + text);
      console.log(
        'GloblaSearchBox getData response  ' + JSON.stringify(response),
      );
      if (response.status === 200) {
        setLoading(false);
        setSuggestionsList(response.data.data)
      } else {
        setLoading(true);
        setSuggestionsList([])
      }
    } catch (e) {
      console.log(
        'GloblaSearchBox getData error  ' + JSON.stringify(e.response),
      );
      setLoading(true);
      setSuggestionsList([])
    }
  }

  const {
    eachRow,
    search = true,
    isRecentTrasaction = false,
  } = props;
  // console.log('GloblaSearchBox state' + JSON.stringify(state));
  return (
    <View
      style={{
        marginBottom: 8,
        width: isRecentTrasaction ? DeviceWidth / 1.6 : DeviceWidth / 1.07,
      }}>
    <AutocompleteDropdown
        ref={searchRef}
        controller={controller => {
          dropdownController.current = controller
        }}
        direction={Platform.select({ ios: 'down' })}
        closeOnBlur={false}

        dataSet={suggestionsList.map(eachElement => {
          return {
            id: eachElement.type,
            title: eachElement._id,
          };
        })}
        initialValue={props.searchText}
        onSelectItem={props.onChangeValue}
        useFilter={false}
        onChangeText={getData}
        loading={loading}
        textInputProps={{
          placeholder: t('Search'),
          autoCorrect: false,
          autoCapitalize: 'none',
          style: {
            borderRadius: 5,
            color: Colors.blueE9,
          },
          backgroundColor: Colors.whiteFF
        }}
        rightButtonsContainerStyle={{
          borderRadius: 5,
          right: 8,
          alignSelfs: 'center',
          backgroundColor: Colors.whiteFF
        }}
        placeholderTextColor={Colors.blueE9}
        // ClearIconComponent={
        //   // <Icon1 name="search" size={25} color={Colors.blueE9} />
        //   <Image
        //     style={{
        //       width: scale(16.85),
        //       height: scale(16.85),
        //       margin: scale(5),
        //     }}

        //     source={require('../../assets/Search.png')}
        //   />
        // }
        ChevronIconComponent={
          // <Icon1 name="search" size={25} color={Colors.blueE9} />
          <Image
            style={{
              width: scale(16.85),
              height: scale(16.85),
              margin: scale(5),
            }}

            source={require('../../assets/Search.png')}
          />
        }
      />
    </View>
  );
}
const SearchScreen = (props) => {
  const { t, i18n } = useTranslation();
  const [searchText, setSearchText] = useState('')
  const {fromScreen=''}=props
  
  const onSearchByText = async(item) =>{
    setSearchText(item.id)
    if (item.id) {
      if (item.id === 'order') {
        SearchOrder(item.title);
      } else if (item.id === 'shipment' || item.id === 'transitNumber') {
       await props.getchainOfCustody(item.title);
        await props.getViewShipment(item.title);
        if(fromScreen==='Shipment'){
          props.navigation.navigate('ViewShipment');

        }else{
          props.navigation.navigate('Shipment', { screen: 'ViewShipment' });

        }
      } else if (item.id === 'productType') {
       await props.searchProductByType(item.title);
       if(fromScreen==='Inventory'){
        props.navigation.navigate('ProductListByCategory',{selectedProductType:  item.title});

      }else{
       props.navigation.navigate('Inventory', { screen: 'ProductListByCategory',params: { selectedProductType:  item.title } });;
      }
      } else if (item.id === 'productName') {
       await props.searchProductByName(item.title);
       if(fromScreen==='Inventory'){
        props.navigation.navigate('ProductListByName',{selectedProductType:  item.title});

      }else{
       props.navigation.navigate('Inventory', { screen: 'ProductListByName' });
      }
      }
    }
    await 100
    setSearchText('')

  }
 
  const SearchOrder = async text => {
    const result = await props.getOrderDetails(text);
    console.log(
      'SearchOrder getOrderDetails result' + JSON.stringify(result),
    );
    if (result.length) {

      const orderData = result[0];
      if(fromScreen==='Order'){
        props.navigation.navigate('ViewOrder',{item:  orderData});
      }else{
      props.navigation.navigate('Order', { screen: 'ViewOrder',params: { item: orderData } });;
      }
    } else {
      ToastShow(
        t('Please Check your order Id :') + text,
         'error',
         'long',
         'top',
       )
    }

  };
 
  return (
    <View style={{ marginTop: verticalScale(-DeviceWidth / 7), alignSelf: 'center' }}>
   { !searchText?<GloblaSearchBox
        eachRow={FilterData.searchString}
        onChangeValue={onSearchByText}
        search={true}
        isRecentTrasaction={props.isRecentTrasaction}
        searchText={''}
      />:null}
    </View>
  );
};

function mapStateToProps(state) {
  return {
    loder: state.loder,
    alerts: state.alert.alerts,
    alertPage: state.alert.alertPage,
    alertCount: state.alert.alertCount,
  };
}

export default connect(
  mapStateToProps,
  {
    getViewShipment,
    getchainOfCustody,
    searchProductByType,
    searchProductByName,
    getOrderDetails,
  },
)(SearchScreen);
