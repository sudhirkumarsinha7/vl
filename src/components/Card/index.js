import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  BackHandler,
  View,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import Icon from 'react-native-vector-icons/Foundation';
import { numberWithCommas } from '../../Util/utils';

 const Card =(props) =>{
  
    const { isDisable = true } = props;
    return (
      <TouchableOpacity
        style={{
          height: scale(92),
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
          paddingLeft: 5,
          paddingRight: 15,
        }}
        onPress={props.nextScreen}
        disabled={isDisable}>
        <View
          style={{
            flexDirection: 'row',
            marginTop: verticalScale(15),
            marginLeft: scale(15),
            marginRight: scale(15),
          }}>
          <View
            style={{
              width: scale(41),
              height: scale(41),
              marginRight: scale(30),
              borderRadius: 400,
              backgroundColor: props.bgcolor,
              justifyContent: 'center',
              alignItems: 'center',
              shadowColor: '#00000029',
              shadowOffset: {width: -2, height: 4},
              shadowOpacity: 0.7,
              shadowRadius: 3,
            }}
            opacity={1}>
            {props.icon ? (
              <Icon
                name={props.icon}
                color={props.textcolor}
                size={25}
              />
            ) : (
              <Image
                style={{ width: scale(23.27), height: scale(23.34), tintColor: props.textcolor }}
                source={props.image}
                resizeMode="contain"
              />
            )}
          </View>
          <Text
            style={{
              color: props.textcolor,
              fontSize: scale(25),
            }}>
            {numberWithCommas(props.quantity)}
          </Text>
        </View>

        <Text
          style={{
            color: props.textcolor,
            fontSize: scale(13),
            marginLeft: scale(15),
            marginTop: verticalScale(10),
          }}>
          {props.card_name}
        </Text>
      </TouchableOpacity>
    );
}
export default Card